-- MySQL dump 10.13  Distrib 5.5.24, for Win64 (x86)
--
-- Host: localhost    Database: ip360
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alarmpanels`
--

DROP TABLE IF EXISTS `alarmpanels`;
CREATE TABLE `alarmpanels` (
  `alarmPanelId` bigint(20) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `alarmPanelTypeId` varchar(16) NOT NULL,
  `ipAddress` varchar(32) DEFAULT NULL,
  `tcpPort` int(11) DEFAULT NULL,
  `linuxHardwarePortId` varchar(256) DEFAULT NULL,
  `comAddress` varchar(256) DEFAULT NULL,
  `baudRate` int(11) DEFAULT NULL,
  `dataBits` int(11) DEFAULT NULL,
  `parity` varchar(10) DEFAULT NULL,
  `stopBits` varchar(10) DEFAULT NULL,
  `FlowControl` varchar(10) DEFAULT NULL,
  `armCode` varchar(32) DEFAULT NULL,
  `disarmCode` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`alarmPanelId`),
  UNIQUE KEY `alarmPanelId_UNIQUE` (`alarmPanelId`),
  KEY `APalarmPanelTypeIdIdx` (`alarmPanelTypeId`),
  CONSTRAINT `FK_AP_APT_APTID` FOREIGN KEY (`alarmPanelTypeId`) REFERENCES `alarmpaneltypes` (`alarmPanelTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*LOCK TABLES `alarmpanels` WRITE;
INSERT INTO `alarmpanels` VALUES (1,'Alarm Panel','IT100','',0,NULL,'/dev/ttyS0',9600,8,'None','1','None','','',NULL);
UNLOCK TABLES;
*/


DROP TABLE IF EXISTS `alarmpaneltypes`;
CREATE TABLE `alarmpaneltypes` (
  `alarmPanelTypeId` varchar(16) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ipConnection` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`alarmPanelTypeId`),
  UNIQUE KEY `APTalarmPanelTypeIdIdx` (`alarmPanelTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `alarmpaneltypes` WRITE;
INSERT INTO `alarmpaneltypes` VALUES ('IT100','IT-100 (COM)',0),('TL100','TL-100 (COM)',0),('TL150','TL-150 (IP)',1);
UNLOCK TABLES;

DROP TABLE IF EXISTS `alertactions`;
CREATE TABLE `alertactions` (
  `alert_id` bigint(20) unsigned NOT NULL,
  `seq_id` int(11) NOT NULL,
  `action_type_enum_id` varchar(32) NOT NULL,
  `device_id` bigint(20) unsigned DEFAULT NULL,
  `value1` varchar(255) DEFAULT NULL,
  `value2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alert_id`,`seq_id`),
  KEY `fk_aa_a_aid` (`alert_id`) USING HASH,
  KEY `fk_aa_e_atid` (`action_type_enum_id`) USING HASH,
  KEY `idx_aa_sid` (`seq_id`) USING BTREE,
  KEY `idx_aa_did` (`device_id`) USING HASH,
  CONSTRAINT `fk_aa_a_aid` FOREIGN KEY (`alert_id`) REFERENCES `alerts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_aa_e_atid` FOREIGN KEY (`action_type_enum_id`) REFERENCES `enumerations` (`enum_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `alertconditions`;
CREATE TABLE `alertconditions` (
  `alert_id` bigint(20) unsigned NOT NULL,
  `seq_id` int(11) NOT NULL,
  `cond_type_enum_id` varchar(32) NOT NULL,
  `device_id` bigint(20) unsigned DEFAULT NULL,
  `value_comp_enum_id` varchar(32) DEFAULT NULL,
  `value1` varchar(64) DEFAULT NULL,
  `value2` varchar(64) DEFAULT NULL,
  `value_enum_id` varchar(32) DEFAULT NULL,
  `duration` decimal(18,2) DEFAULT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`alert_id`,`seq_id`),
  KEY `fk_ac_a_alert_id` (`alert_id`) USING HASH,
  KEY `fk_ac_e_acid` (`cond_type_enum_id`) USING HASH,
  KEY `fk_ac_e_vcid` (`value_comp_enum_id`) USING HASH,
  KEY `fk_ac_e_vid` (`value_enum_id`) USING HASH,
  KEY `idx_ac_seqid` (`seq_id`) USING BTREE,
  KEY `idx_ac_did` (`device_id`) USING HASH,
  KEY `idx_ac_uid` (`user_id`) USING HASH,
  CONSTRAINT `fk_ac_a_alert_id` FOREIGN KEY (`alert_id`) REFERENCES `alerts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ac_e_acid` FOREIGN KEY (`cond_type_enum_id`) REFERENCES `enumerations` (`enum_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ac_e_vcid` FOREIGN KEY (`value_comp_enum_id`) REFERENCES `enumerations` (`enum_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ac_e_vid` FOREIGN KEY (`value_enum_id`) REFERENCES `enumerations` (`enum_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `alerts`;
CREATE TABLE `alerts` (
  `id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `message` text NOT NULL,
  `schedule_id` bigint(20) unsigned DEFAULT NULL,
  `ap_status_enum_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_UNIQUE` (`id`) USING HASH,
  KEY `AnameIdx` (`name`) USING BTREE,
  KEY `fk_a_s_schedule_id` (`schedule_id`) USING HASH,
  KEY `fk_a_e_apstat` (`ap_status_enum_id`) USING HASH,
  CONSTRAINT `fk_a_e_apstat` FOREIGN KEY (`ap_status_enum_id`) REFERENCES `enumerations` (`enum_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_a_s_schedule_id` FOREIGN KEY (`schedule_id`) REFERENCES `schedules` (`scheduleId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `cameras`;
CREATE TABLE `cameras` (
  `camera_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `camera_type_id` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `ip_address` varchar(32) NOT NULL,
  `access_code` varchar(32) DEFAULT NULL,
  `mac_address` varchar(64) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`camera_id`),
  KEY `fk_c_ct_ctid` (`camera_type_id`) USING HASH,
  KEY `idx_c_name` (`name`) USING BTREE,
  KEY `idx_c_ip` (`ip_address`) USING HASH,
  KEY `idx_c_mac` (`mac_address`) USING HASH,
  CONSTRAINT `fk_c_ct_ctid` FOREIGN KEY (`camera_type_id`) REFERENCES `cameratypes` (`camera_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `cameratypes`;
CREATE TABLE `cameratypes` (
  `camera_type_id` varchar(32) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`camera_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cameratypes` WRITE;
INSERT INTO `cameratypes` VALUES ('AXIS','Axis IP Camera');
UNLOCK TABLES;

--
-- Table structure for table `deviceclass`
--

DROP TABLE IF EXISTS `deviceclass`;
CREATE TABLE `deviceclass` (
  `device_class_id` varchar(32) NOT NULL,
  `device_class_name` varchar(64) NOT NULL,
  PRIMARY KEY (`device_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `deviceclass` WRITE;
INSERT INTO `deviceclass` VALUES ('AP_SENSOR','Alarm Panel Sensor'),('CAMERA','IP Camera'),('ISONAS_READER','Isonas Powernet Reader'),('XBEE_SENSOR','XBee Sensor'),('ZWAVE_SENSOR','Z-Wave Sensor');
UNLOCK TABLES;

DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices` (
  `device_address` varchar(64) NOT NULL,
  `device_class_id` varchar(32) NOT NULL,
  `device_type_id` varchar(32) DEFAULT NULL,
  `device_ip_address` varchar(32) DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `device_status_id` varchar(32) DEFAULT NULL,
  `last_registration_error` varchar(255) DEFAULT NULL,
  `device_target_ip_address` varchar(32) DEFAULT NULL,
  `next_config_schedule` datetime DEFAULT NULL,
  `last_config_error` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`device_address`),
  KEY `FK_D_DT_dtid` (`device_type_id`),
  KEY `FK_D_ST_stid` (`device_status_id`),
  KEY `IDX_D_dcid` (`device_class_id`) USING HASH,
  KEY `IDX_D_dtid` (`device_type_id`) USING HASH,
  KEY `IDX_D_stid` (`device_status_id`) USING HASH,
  KEY `FK_D_DC_dcid` (`device_class_id`) USING HASH,
  KEY `idx_ncfg` (`next_config_schedule`),
  CONSTRAINT `FK_D_DC_dcid` FOREIGN KEY (`device_class_id`) REFERENCES `deviceclass` (`device_class_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_D_DT_dtid` FOREIGN KEY (`device_type_id`) REFERENCES `devicetypes` (`device_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_D_ST_stid` FOREIGN KEY (`device_status_id`) REFERENCES `devicestatus` (`device_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `devicestatus`;
CREATE TABLE `devicestatus` (
  `device_status_id` varchar(32) NOT NULL,
  `device_status_name` varchar(64) NOT NULL,
  PRIMARY KEY (`device_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `devicestatus` WRITE;
INSERT INTO `devicestatus` VALUES ('DEVICE_STATUS_ERROR','Error/Failed'),('DEVICE_STATUS_MAPPED','Configuring Device'),('DEVICE_STATUS_REGISTERED','Registered'),('DEVICE_STATUS_UNREGISTERED','Unregistered');
UNLOCK TABLES;

DROP TABLE IF EXISTS `devicetypes`;
CREATE TABLE `devicetypes` (
  `device_type_id` varchar(32) NOT NULL,
  `device_class_id` varchar(32) NOT NULL,
  `device_type_name` varchar(64) NOT NULL,
  `sequence_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`device_type_id`),
  KEY `IDX_DT_dcid` (`device_class_id`),
  KEY `FK_DT_DC_dcid` (`device_class_id`),
  CONSTRAINT `FK_DT_DC_dcid` FOREIGN KEY (`device_class_id`) REFERENCES `deviceclass` (`device_class_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `devicetypes` WRITE;
INSERT INTO `devicetypes` VALUES ('APS_CO','AP_SENSOR','CO Detector',1),('APS_CO2','AP_SENSOR','CO2 Detector',2),('APS_DOOR_CONTACT','AP_SENSOR','Door Contact Sensor',3),('APS_FLOW','AP_SENSOR','Water Flow Sensor',4),('APS_GLASS_BREAK','AP_SENSOR','Glass Break Detector',5),('APS_HEAT','AP_SENSOR','Heat',6),('APS_MOTION','AP_SENSOR','Motion Detector',7),('APS_TAMPER','AP_SENSOR','Tamper Sensor',8),('APS_TEMP','AP_SENSOR','Temperature',9),('APS_WATER','AP_SENSOR','Water detector',10),('CAM_AXIS_IP','CAMERA','Axis Ip Camera',1),('IR_R2','ISONAS_READER','RC-02',1),('IR_R3','ISONAS_READER','RC-03',2),('XB_DIGI_TEMP','XBEE_SENSOR','Xbee Digi Temperature (LTH)',1);
UNLOCK TABLES;

DROP TABLE IF EXISTS `doorgroupassociations`;
CREATE TABLE `doorgroupassociations` (
  `doorId` bigint(20) unsigned NOT NULL,
  `doorGroupId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`doorId`,`doorGroupId`),
  KEY `FK_doorgroupassociations_DGID` (`doorGroupId`),
  KEY `DGAdoorGroupIdIdx` (`doorGroupId`) USING HASH,
  KEY `DGAdoorIdIdx` (`doorId`) USING HASH,
  CONSTRAINT `FK_doorgroupassociations_DGID` FOREIGN KEY (`doorGroupId`) REFERENCES `doorgroups` (`doorGroupId`),
  CONSTRAINT `FK_doorgroupassociations_DID` FOREIGN KEY (`doorId`) REFERENCES `doors` (`doorId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `doorgroups`;
CREATE TABLE `doorgroups` (
  `doorGroupId` bigint(20) unsigned NOT NULL,
  `doorGroupName` varchar(32) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`doorGroupId`),
  UNIQUE KEY `doorGroupId_UNIQUE` (`doorGroupId`) USING HASH,
  KEY `DGdoorGroupNameIdx` (`doorGroupName`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `doors`;
CREATE TABLE `doors` (
  `doorId` bigint(20) unsigned NOT NULL,
  `macAddress` varchar(20) DEFAULT NULL,
  `ipAddress` varchar(20) DEFAULT NULL,
  `doorName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `tamperEnabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rexEnabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rexUnlockUserId` bigint(20) unsigned DEFAULT NULL,
  `rexUnlockLocalMode` tinyint(1) NOT NULL DEFAULT '0',
  `auxUnlockUserId` bigint(20) unsigned DEFAULT NULL,
  `armAlarmPanel` tinyint(3) NOT NULL DEFAULT '0',
  `disarmAlarmPanel` tinyint(3) NOT NULL DEFAULT '1',
  `unlockScheduleId` bigint(20) unsigned DEFAULT NULL,
  `unlockRequested` tinyint(3) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deletedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`doorId`),
  UNIQUE KEY `doorId_UNIQUE` (`doorId`),
  KEY `DmacAddressIdx` (`macAddress`) USING HASH,
  KEY `DipAddressIdx` (`ipAddress`) USING HASH,
  KEY `DdoorNameIdx` (`doorName`) USING BTREE,
  KEY `FK_DOOR_USER_REX` (`rexUnlockUserId`) USING HASH,
  KEY `FK_DOOR_USER_AUX` (`auxUnlockUserId`) USING HASH,
  KEY `FK_DOOR_SCHED_UNSID` (`unlockScheduleId`) USING HASH,
  KEY `DOOR_UNLOCK_REQ` (`unlockRequested`),
  KEY `DOOR_STAT` (`status`) USING HASH,
  KEY `DOOR_ACTIVE` (`active`),
  KEY `DOOR_DON` (`deletedOn`),
  CONSTRAINT `FK_DOOR_SCHED_UNSID` FOREIGN KEY (`unlockScheduleId`) REFERENCES `schedules` (`scheduleId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_DOOR_USER_AUX` FOREIGN KEY (`auxUnlockUserId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_DOOR_USER_REX` FOREIGN KEY (`rexUnlockUserId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `enumerations`;
CREATE TABLE `enumerations` (
  `enum_id` varchar(32) NOT NULL,
  `enum_type_id` varchar(32) NOT NULL,
  `abbrev` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `seq_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`enum_id`),
  KEY `idx_e_name` (`description`),
  KEY `fk_e_et_enum_type_id` (`enum_type_id`) USING HASH,
  KEY `idx_e_abbrev` (`abbrev`) USING HASH,
  CONSTRAINT `fk_e_et_enum_type_id` FOREIGN KEY (`enum_type_id`) REFERENCES `enumerationtypes` (`enum_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `enumerations` WRITE;
INSERT INTO `enumerations` VALUES ('ALERT_ACTION_CALL','ALERT_ACTION_TYPE','Call','Call one person',-1),('ALERT_ACTION_CALL_ALL','ALERT_ACTION_TYPE','Call All','Call everyone',-1),('ALERT_ACTION_EMAIL','ALERT_ACTION_TYPE','Send email','Send email to',1),('ALERT_ACTION_FIRE','ALERT_ACTION_TYPE','Alert Fire Dept','Alert Fire Department',-1),('ALERT_ACTION_GUARD','ALERT_ACTION_TYPE','Alert guard','Alert the guard on duty',-1),('ALERT_ACTION_POLICE','ALERT_ACTION_TYPE','Alert Police','Alert Police',-1),('ALERT_ACTION_RECORD','ALERT_ACTION_TYPE','Record Camera','Record Camera Footage',-1),('ALERT_ACTION_TEXT','ALERT_ACTION_TYPE','Send SMS','Send SMS message to',2),('ALERT_APSTAT_ALARM','ALERT_AP_STATUS','Alarmed','Alarmed',4),('ALERT_APSTAT_ANY','ALERT_AP_STATUS','*','*ANY*',0),('ALERT_APSTAT_ARMED','ALERT_AP_STATUS','Armed','Armed',3),('ALERT_APSTAT_BUSY','ALERT_AP_STATUS','Busy','Busy',2),('ALERT_APSTAT_ENDEL','ALERT_AP_STATUS','Entry Delay','Entry Delay',5),('ALERT_APSTAT_EXDEL','ALERT_AP_STATUS','Exit Delay','Exit Delay',6),('ALERT_APSTAT_OFF','ALERT_AP_STATUS','Offline','Offline',7),('ALERT_APSTAT_READY','ALERT_AP_STATUS','Ready','Ready',1),('ALERT_COND_DOOR','ALERT_CONDITION_TYPE','Door','Door',1),('ALERT_COND_HUM','ALERT_CONDITION_TYPE','Humidity Sensor','Humidity Sensor',-1),('ALERT_COND_LIGHT','ALERT_CONDITION_TYPE','Light Sensor','Light Sensor',-1),('ALERT_COND_TEMP','ALERT_CONDITION_TYPE','Temperature Sensor','Temperature Sensor',-1),('ALERT_COND_ZONE','ALERT_CONDITION_TYPE','Zone/Sensor','Zone/Sensor',2),('ALERT_DSTAT_FOP','ALERT_DOOR_STATUS','Forced Open','Forced Open',5),('ALERT_DSTAT_HOP','ALERT_DOOR_STATUS','Held Open','Held Open',4),('ALERT_DSTAT_LOC','ALERT_DOOR_STATUS','Locked','Locked',1),('ALERT_DSTAT_OFF','ALERT_DOOR_STATUS','Offline','Offline',7),('ALERT_DSTAT_OPN','ALERT_DOOR_STATUS','Opened','Opened',3),('ALERT_DSTAT_TMP','ALERT_DOOR_STATUS','Tampered','Tampered',6),('ALERT_DSTAT_UNL','ALERT_DOOR_STATUS','Unlocked','Unlocked',2),('ALERT_XBEE_COMP_BET','ALERT_XBEE_COMP','Between','Between',3),('ALERT_XBEE_COMP_DEC','ALERT_XBEE_COMP','Decreased','Decreased By',6),('ALERT_XBEE_COMP_GT','ALERT_XBEE_COMP','>','Greater than',2),('ALERT_XBEE_COMP_INC','ALERT_XBEE_COMP','Increased','Increased By',5),('ALERT_XBEE_COMP_LT','ALERT_XBEE_COMP','<','Less than',1),('ALERT_XBEE_COMP_NBET','ALERT_XBEE_COMP','Not Between','Not Between',4),('ALERT_ZSTAT_ALARM','ALERT_ZONE_STATUS','Alarm Triggered','Alarm Triggered',3),('ALERT_ZSTAT_OFF','ALERT_ZONE_STATUS','Not Triggered','Not Triggered',1),('ALERT_ZSTAT_ON','ALERT_ZONE_STATUS','Triggered','Triggered',2);
UNLOCK TABLES;

DROP TABLE IF EXISTS `enumerationtypes`;
CREATE TABLE `enumerationtypes` (
  `enum_type_id` varchar(32) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`enum_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `enumerationtypes` WRITE;
INSERT INTO `enumerationtypes` VALUES ('ALERT_ACTION_TYPE','Alert Action Type'),('ALERT_AP_STATUS','Alert Condition Alarm Panel Status'),('ALERT_CONDITION_TYPE','Alert Condition Type'),('ALERT_DOOR_STATUS','Alert Condition Door Status'),('ALERT_XBEE_COMP','Alert Condition XBee Comparator'),('ALERT_ZONE_STATUS','Alert Condition Zone Status');
UNLOCK TABLES;

DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `eventType` varchar(16) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(255) DEFAULT NULL,
  `userId` bigint(20) unsigned DEFAULT NULL,
  `doorId` bigint(20) unsigned DEFAULT NULL,
  `zoneId` int(16) unsigned DEFAULT NULL,
  `sensorId` bigint(20) unsigned DEFAULT NULL,
  `sequenceNum` int(16) unsigned DEFAULT NULL,
  `inputNum` varchar(16) DEFAULT NULL,
  `inputType` varchar(16) DEFAULT NULL,
  `detailedDescription` text,
  `alarmPanelId` bigint(20) unsigned DEFAULT NULL,
  `sensorValue` decimal(18,6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID_UNIQUE` (`id`) USING BTREE,
  KEY `FK_E_U_userId` (`userId`),
  KEY `FK_E_S_sensorId` (`sensorId`),
  KEY `FK_E_A_alarmPanelId` (`alarmPanelId`),
  KEY `EeventTypeIdx` (`eventType`) USING HASH,
  KEY `EcreatedDateIdx` (`createdDate`) USING BTREE,
  KEY `EuserIdIdx` (`userId`) USING HASH,
  KEY `EdoorIdIdx` (`doorId`) USING HASH,
  KEY `EZoneIdIdx` (`zoneId`) USING HASH,
  KEY `ESensorIdIdx` (`sensorId`) USING HASH,
  KEY `ESequenceNumIdx` (`sequenceNum`) USING HASH,
  KEY `EInputNumIdx` (`inputNum`) USING HASH,
  KEY `EInputTypeIdx` (`inputType`) USING HASH,
  KEY `EalarmPanelIdIdx` (`alarmPanelId`) USING HASH,
  KEY `EsensorValueIdx` (`sensorValue`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `events` WRITE;
UNLOCK TABLES;

DROP TABLE IF EXISTS `eventtypes`;
CREATE TABLE `eventtypes` (
  `id` varchar(16) NOT NULL,
  `Description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `eventtypes` WRITE;
INSERT INTO `eventtypes` VALUES ('ACCESS_DENIED','Access Denied'),('ACCESS_ERROR','Access Error'),('ACCESS_GRANTED','Access Granted'),('ACCESS_TIMEOUT','Access Timeout'),('ALERT','Alert Sent'),('APSTAT_ALARM','Alarm Panel Alarm Triggered'),('APSTAT_ARMED','Alarm Panel Armed'),('APSTAT_BUSY','Alarm Panel Busy'),('APSTAT_ENDELAY','Alarm Panel Entry Delay'),('APSTAT_EXDELAY','Alarm Panel Exit Delay'),('APSTAT_READY','Alarm Panel Disarmed & Ready'),('APZONE_ALARM','Alarm Panel Zone Alarm'),('APZONE_OFF','Alarm Panel Zone Restore'),('APZONE_TRIGGER','Alarm Panel Zone Trigger'),('AP_DISARMED','Alarm Panel Disarmed'),('AP_ERROR','Alarm Panel Error'),('AP_INVCODE','Alarm Panel Invalid Code Entry'),('AP_LOCKOUT','Alarm Panel Lockout'),('AP_PARTITION','Alarm Panel Partition'),('CREDENTIAL_ERROR','Credential Error'),('CREDENTIAL_OK','Credential Presented'),('DEVICE_OFFLINE','Device Offline'),('DOOR_ACTIVE','Door Active (Listening)'),('DOOR_ERROR','Door Error'),('DOOR_FOPEN','Door Forced Open'),('DOOR_HOPEN','Door Held Open'),('DOOR_LOCKDOWN','Door Locked Down'),('DOOR_OPEN','Door Opened'),('DOOR_TAMPER','Door Lock Tampered'),('DOOR_UNLOCK','Door Unlocked'),('LOGIN_ERROR','Login Error'),('LOGIN_SUCCESS','Login Success'),('SYSTEM_ERROR','System Error'),('SYSTEM_LOG','System Log'),('SYSTEM_WARNING','System Warning'),('XBEE_DATA','XBee Sensor Data'),('XBEE_HUM','XBee Humidity Sensor Data'),('XBEE_LIGHT','XBee Light Sensor Data'),('XBEE_TEMP','XBee Temperature Sensor Data'),('ZWAVE_DATA','Z-Wave Device Data');
UNLOCK TABLES;

DROP TABLE IF EXISTS `globalconfig`;
CREATE TABLE `globalconfig` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(16) DEFAULT NULL,
  `requestReset` tinyint(3) unsigned NOT NULL,
  `requestReload` tinyint(3) unsigned NOT NULL,
  `relayInterval` int(10) unsigned NOT NULL,
  `heldOpenInterval` int(10) unsigned NOT NULL,
  `lockDownOnCount` int(10) unsigned NOT NULL,
  `lockDownOffCount` int(10) unsigned NOT NULL,
  `lockDownInterval` int(10) unsigned NOT NULL,
  `tamperSustainInterval` int(10) unsigned NOT NULL,
  `rexEnabled` tinyint(3) unsigned NOT NULL,
  `lockDownEnabled` tinyint(3) unsigned NOT NULL,
  `tamperEnabled` tinyint(3) unsigned NOT NULL,
  `forcedOpenSustainInterval` int(10) unsigned NOT NULL,
  `credentialSustainInterval` int(10) unsigned NOT NULL,
  `readerPollInterval` int(10) unsigned NOT NULL DEFAULT '30',
  `alarmPanelPollInterval` int(10) NOT NULL DEFAULT '30',
  `smtpServer` varchar(255) DEFAULT NULL,
  `emailUserId` varchar(64) DEFAULT NULL,
  `emailUserPassword` varchar(255) DEFAULT NULL,
  `emailSender` varchar(255) DEFAULT NULL,
  `nextDoorProgrammingSchedule` timestamp NULL DEFAULT NULL,
  `isRexReversed` tinyint(3) NOT NULL DEFAULT '1',
  `isTamperReversed` tinyint(3) NOT NULL DEFAULT '0',
  `startIpRange` varchar(32) DEFAULT NULL,
  `endIpRange` varchar(32) DEFAULT NULL,
  `nextIpAddress` varchar(32) DEFAULT NULL,
  `scanDevices` tinyint(1) NOT NULL DEFAULT '0',
  `requestError` varchar(255) DEFAULT NULL,
  `systemSubnet` varchar(32) DEFAULT NULL,
  `systemGateway` varchar(32) DEFAULT NULL,
  `isonasBroadcast1` varchar(32) DEFAULT NULL,
  `isonasBroadcast2` varchar(32) DEFAULT NULL,
  `pingTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
LOCK TABLES `globalconfig` WRITE;
INSERT INTO `globalconfig` VALUES (1,NULL,0,0,10,60,5,3,60,60,0,0,0,5,5,40,30,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,0,'register',NULL,NULL,'255.255.255.255',NULL,NULL);
UNLOCK TABLES;
*/

LOCK TABLES `globalconfig` WRITE;
INSERT INTO `globalconfig` VALUES (1,NULL,0,0,10,60,5,3,60,60,0,0,0,5,5,40,30,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,0,NULL,NULL,NULL,'255.255.255.255',NULL,NULL);
UNLOCK TABLES;


DROP TABLE IF EXISTS `holidays`;
CREATE TABLE `holidays` (
  `id` int(11) NOT NULL,
  `holidayDate` date NOT NULL,
  `nameOfHoliday` varchar(64) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID_UNIQUE` (`id`),
  KEY `Hdate` (`holidayDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `locationassociations`;
CREATE TABLE `locationassociations` (
  `locationId` bigint(20) unsigned NOT NULL,
  `doorId` bigint(20) unsigned NOT NULL,
  `alarmPanelId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`locationId`,`alarmPanelId`,`doorId`),
  KEY `FK_locationassociations_DID` (`doorId`) USING HASH,
  KEY `FK_locationassociations_APID` (`alarmPanelId`) USING HASH,
  CONSTRAINT `FK_locationassociations_APID` FOREIGN KEY (`alarmPanelId`) REFERENCES `alarmpanels` (`alarmPanelId`),
  CONSTRAINT `FK_locationassociations_DID` FOREIGN KEY (`doorId`) REFERENCES `doors` (`doorId`),
  CONSTRAINT `FK_locationassociations_LID` FOREIGN KEY (`locationId`) REFERENCES `locations` (`locationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `locationId` bigint(20) unsigned NOT NULL,
  `locationName` varchar(64) DEFAULT NULL,
  `locationDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`locationId`),
  UNIQUE KEY `locationId_UNIQUE` (`locationId`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `scheduleId` bigint(20) unsigned NOT NULL,
  `userId` bigint(20) unsigned NOT NULL,
  `doorId` bigint(20) unsigned NOT NULL,
  `pin` tinyint(4) NOT NULL DEFAULT '0',
  `card` tinyint(4) NOT NULL DEFAULT '0',
  `bio` tinyint(4) NOT NULL DEFAULT '0',
  `credentialType` enum('single','dual','triple') NOT NULL DEFAULT 'single',
  PRIMARY KEY (`userId`,`scheduleId`,`doorId`),
  KEY `PscheduleIdIdx` (`scheduleId`) USING HASH,
  KEY `PuserIdIdx` (`userId`) USING HASH,
  KEY `PdoorIdIdx` (`doorId`) USING HASH,
  KEY `PpinIdx` (`pin`) USING HASH,
  KEY `PcardIdx` (`card`) USING HASH,
  KEY `PbioIdx` (`bio`) USING HASH,
  KEY `pcredentialTypeIdx` (`credentialType`) USING HASH,
  CONSTRAINT `P_SCH_SCHID` FOREIGN KEY (`scheduleId`) REFERENCES `schedules` (`scheduleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedules`;
CREATE TABLE `schedules` (
  `scheduleId` bigint(20) unsigned NOT NULL,
  `scheduleName` varchar(32) NOT NULL,
  `saturdayEnd` time DEFAULT NULL,
  `saturdayStart` time DEFAULT NULL,
  `fridayEnd` time DEFAULT NULL,
  `fridayStart` time DEFAULT NULL,
  `thursdayEnd` time DEFAULT NULL,
  `thursdayStart` time DEFAULT NULL,
  `wednesdayEnd` time DEFAULT NULL,
  `wednesdayStart` time DEFAULT NULL,
  `tuesdayEnd` time DEFAULT NULL,
  `tuesdayStart` time DEFAULT NULL,
  `mondayEnd` time DEFAULT NULL,
  `mondayStart` time DEFAULT NULL,
  `sundayEnd` time DEFAULT NULL,
  `sundayStart` time DEFAULT NULL,
  `exceptHolidays` enum('Yes','No') NOT NULL,
  `lastUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`scheduleId`),
  UNIQUE KEY `scheduleId_UNIQUE` (`scheduleId`),
  KEY `SscheduleNameIdx` (`scheduleName`),
  KEY `SsaturdayStartIdx` (`saturdayStart`),
  KEY `SsaturdayEndIdx` (`saturdayEnd`),
  KEY `SsundayStartIdx` (`sundayStart`),
  KEY `SsundayEndIdx` (`sundayEnd`),
  KEY `SmondayStartIdx` (`mondayStart`),
  KEY `SmondayEndIdx` (`mondayEnd`),
  KEY `StuesdayStartIdx` (`tuesdayStart`),
  KEY `StuesdayEndIdx` (`tuesdayEnd`),
  KEY `SwednessdayStartIdx` (`wednesdayStart`),
  KEY `SwednessdayEndIdx` (`wednesdayEnd`),
  KEY `SthursdayStartIdx` (`thursdayStart`),
  KEY `SthursdayEndIdx` (`thursdayEnd`),
  KEY `SfridayStartIdx` (`fridayStart`),
  KEY `SfridayEndIdx` (`fridayEnd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sensors`;
CREATE TABLE `sensors` (
  `id` bigint(20) unsigned NOT NULL,
  `sensorName` varchar(255) DEFAULT NULL,
  `sensorType` varchar(16) DEFAULT NULL,
  `zoneNumber` int(20) DEFAULT NULL,
  `alarmPanelId` bigint(20) unsigned DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deletedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`) USING HASH,
  KEY `SNSsensorNameIdx` (`sensorName`),
  KEY `FK_S_A_alarmPanelId` (`alarmPanelId`),
  KEY `SNSsensorTypeIdx` (`sensorType`) USING HASH,
  KEY `SNSzoneIdx` (`zoneNumber`) USING HASH,
  KEY `SalarmPanelIdIdx` (`alarmPanelId`) USING HASH,
  KEY `idx_act` (`active`),
  KEY `idx_del` (`deletedOn`),
  CONSTRAINT `FK_S_A_alarmPanelId` FOREIGN KEY (`alarmPanelId`) REFERENCES `alarmpanels` (`alarmPanelId`),
  CONSTRAINT `SNS_SNSTYPE_FK` FOREIGN KEY (`sensorType`) REFERENCES `devicetypes` (`device_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sensortypes`;
CREATE TABLE `sensortypes` (
  `id` varchar(16) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `sensortypes` WRITE;
INSERT INTO `sensortypes` VALUES ('DOOR','Door Contact'),('MOTION','Motion Sensor'),('SMOKE','Smoke Dectector'),('WATER','Water Sensor'),('WATER_FLOW','Water Flow Dectector');
UNLOCK TABLES;

DROP TABLE IF EXISTS `smscarriers`;
CREATE TABLE `smscarriers` (
  `carrierID` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `description` text,
  `smsSuffix` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`carrierID`),
  UNIQUE KEY `carrierID_UNIQUE` (`carrierID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `usergroupassociations`;
CREATE TABLE `usergroupassociations` (
  `userId` bigint(20) unsigned NOT NULL,
  `userGroupId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`userId`,`userGroupId`),
  KEY `FK_usergroupassociations_UGID` (`userGroupId`) USING BTREE,
  KEY `UGAuseridIdx` (`userId`) USING HASH,
  KEY `UGAuserGroupIdIdx` (`userGroupId`) USING HASH,
  CONSTRAINT `FK_usergroupassociations_UGID` FOREIGN KEY (`userGroupId`) REFERENCES `usergroups` (`userGroupId`),
  CONSTRAINT `FK_usergroupassociations_UID` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `usergroups`;
CREATE TABLE `usergroups` (
  `userGroupId` bigint(20) unsigned NOT NULL,
  `userGroupName` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `globalGroup` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`userGroupId`),
  UNIQUE KEY `userGroupId_UNIQUE` (`userGroupId`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*LOCK TABLES `usergroups` WRITE;
INSERT INTO `usergroups` VALUES ('10000','EVERYONE','All Users');
UNLOCK TABLES;*/

DROP TABLE IF EXISTS `userlogins`;
CREATE TABLE `userlogins` (
  `userLoginId` varchar(32) NOT NULL,
  `userPassword` varchar(64) NOT NULL,
  `userEmail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userLoginId`),
  UNIQUE KEY `userLoginId_UNIQUE` (`userLoginId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
LOCK TABLES `userlogins` WRITE;
INSERT INTO `userlogins` VALUES ('admin','1234',NULL),('tech','1234',NULL);
UNLOCK TABLES;
*/

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userId` bigint(20) unsigned NOT NULL,
  `userName` varchar(64) DEFAULT NULL,
  `badgeNum` varchar(32) DEFAULT NULL,
  `pinNum` varchar(16) DEFAULT NULL,
  `employeeId` varchar(16) DEFAULT NULL,
  `badgePrintedNum` varchar(64) DEFAULT NULL,
  `external` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deletedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userId_UNIQUE` (`userId`) USING HASH,
  KEY `user_username` (`userName`),
  KEY `user_badgeid` (`badgeNum`) USING HASH,
  KEY `user_pin` (`pinNum`) USING HASH,
  KEY `user_external` (`external`) USING HASH,
  KEY `user_del` (`deletedOn`),
  KEY `user_act` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `xbeesensors`;
CREATE TABLE `xbeesensors` (
  `sensor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sensor_type_id` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `mac_address` varchar(128) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`sensor_id`),
  KEY `fk_xs_xst_stid` (`sensor_type_id`) USING HASH,
  KEY `idx_xs_name` (`name`) USING BTREE,
  KEY `idx_xs_address` (`mac_address`) USING HASH,
  CONSTRAINT `fk_xs_xst_stid` FOREIGN KEY (`sensor_type_id`) REFERENCES `xbeesensortypes` (`sensor_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `xbeesensortypes`;
CREATE TABLE `xbeesensortypes` (
  `sensor_type_id` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  PRIMARY KEY (`sensor_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `xbeesensortypes` WRITE;
INSERT INTO `xbeesensortypes` VALUES ('THL_SENSOR','Temperature/Humidity/Light Sensor');
UNLOCK TABLES;
