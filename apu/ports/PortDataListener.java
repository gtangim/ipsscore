package apu.ports;

import java.util.EventListener;

public interface PortDataListener extends EventListener {

	public void onPortData(PortDataEvent evt);

}
