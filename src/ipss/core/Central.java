package ipss.core;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.net.ClientSocketPool;
import apu.util.*;
import com.mysql.jdbc.ConnectionImpl;
import com.thoughtworks.xstream.XStream;
import ipss.core.dsc.AlarmPanelListener;
import ipss.core.dsc.IAlarmPanel;
import ipss.core.dsc.it100.IT100;
import ipss.core.dsc.it100.ZoneDiscovery;
import ipss.core.dsc.states.AlarmPanelEventType;
import ipss.core.dsc.states.AlarmPanelState;
import ipss.core.entities.*;
import ipss.core.isonas.*;
import ipss.core.isonas.commands.IsonasResponseDecompiler;
import ipss.core.isonas.responses.AsyncEventResponse;
import ipss.core.isonas.states.DoorStates;
import org.joda.time.DateTime;
import org.w3c.dom.*;

import java.io.*;

import java.sql.Time;
import java.util.*;


/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/26/11
 * Time: 9:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class Central extends Loggable implements AlarmPanelListener, DoorListener {
    public static final String VERSION = "2.60";
    public static String IMPORT_FILE = "import.xml";
    public static String DBNAME = "ip360";

    String configFileName;
    String dbConnString = null;
    String logFileName = null;
    String shutDownCommand = null;
    boolean lockLinuxComPort = false;
    boolean debugMode = true;
    boolean wdtEnabled = false;
    WatchDog wdt = null;

    //SimpleLogger logger = null;
    GenericDB db = null;
    IpssDbHelper h = null;
    ClientSocketPool pool = null;
    int socketRetryInterval = 60000;
    int port = 10001;


    DateTime startupTime = new DateTime();
    String errorMessage = null;
    boolean success = false;

    DateTime lastReaderPoll = new DateTime();
    int readerPollTimeout = 30000;

    DateTime lastAlarmPanelPoll = new DateTime();
    int alarmPanelPollTimeout = 30000;

    protected Map<Long, Door> doors = new HashMap<Long, Door>();
    protected List<IAlarmPanel> alarmPanels = new ArrayList<IAlarmPanel>();

    private IsonasDiscovery isonasPNP;
    private ZoneDiscovery apZonePNP;
    private XStream xmlConverter;

    public static int minIsonasSeekInterval = 6*1000;
    public static int pingInterval = 15*1000;
    private DateTime lastIsonasSeek = (new DateTime()).minusMinutes(1);
    private DateTime lastPing = (new DateTime()).minusMinutes(1);
    private Object apLock = new Object();
    private boolean programmingStarted = false;


    public Central(String configFileName) {
        super(null);
        try {
            xmlConverter = new XStream();
            xmlConverter.alias("AlarmPanelInfo", AlarmPanelInfo.class);
            xmlConverter.alias("AlertActionInfo", AlertActionInfo.class);
            xmlConverter.alias("AlertConditionInfo", AlertConditionInfo.class);
            xmlConverter.alias("AlertInfo", AlertInfo.class);
            xmlConverter.alias("CameraInfo", CameraInfo.class);
            xmlConverter.alias("DoorGroupInfo", DoorGroupInfo.class);
            xmlConverter.alias("DoorInfo", DoorInfo.class);
            xmlConverter.alias("HolidayInfo", HolidayInfo.class);
            xmlConverter.alias("IP360Data", IP360Data.class);
            xmlConverter.alias("PermissionInfo", PermissionInfo.class);
            xmlConverter.alias("ScheduleInfo", ScheduleInfo.class);
            xmlConverter.alias("SensorInfo", SensorInfo.class);
            xmlConverter.alias("SmsCarrierInfo", SmsCarrierInfo.class);
            xmlConverter.alias("GlobalInfo", GlobalInfo.class);
            xmlConverter.alias("UserInfo", UserInfo.class);
            xmlConverter.alias("UserGroupInfo", UserGroupInfo.class);
            xmlConverter.alias("UserLoginInfo", UserLoginInfo.class);
            xmlConverter.alias("XBeeSensorInfo", XBeeSensorInfo.class);
            xmlConverter.registerConverter(new DateTimeXmlConverter());
            xmlConverter.registerConverter(new BooleanXmlConverter());

            this.configFileName = configFileName;
            loadConfig();

            success = true;
        } catch (Exception e) {
            logDbSys("ERROR: Unable to start the core!", e);
            //e.printStackTrace();
            errorMessage = e.toString();
            success = false;
        }
    }


    // ********** MAIN APPLICATION THREAD **********

    public void logDbSys(String message, Exception ex) {
        log(ex);
        if (h != null) h.logDb(message + " Details:" + ex.toString(), "SYSTEM_ERROR");
    }

    public void logDbSys(String msg) {
        boolean isError = false;
        boolean isWarning = false;
        if (msg == null || msg.trim().equals("")) return;
        if (msg.toLowerCase().startsWith("error:")) {
            String msg2 = msg.substring(6).trim();
            if (h != null && debugMode) h.logDb(msg2, "SYSTEM_ERROR");
            logError(msg2);
        } else if (msg.toLowerCase().startsWith("warning:")) {
            String msg2 = msg.substring(8).trim();
            if (h != null && debugMode) h.logDb(msg2, "SYSTEM_WARNING");
            logWarning(msg2);
        } else if (msg.toLowerCase().startsWith("important:")) {
            String msg2 = msg.substring(10).trim();
            if (h != null && debugMode) h.logDb(msg2, "SYSTEM_LOG");
            logWarning(msg2);
        } else {
            if (h != null && debugMode) h.logDbSys(msg);
            log(msg);
        }
    }


    protected void programDoors() {
        try {
            List<Door> doorsToProgram = new ArrayList<Door>();
            Map<Long, GenericRecord> devices = new HashMap<Long, GenericRecord>();
            for (Door d : doors.values())
                if (d.isDoorConnected()) {
                    GenericRecord device = h.getDevice(d.getMacAddress());
                    if (device != null && !device.isNull("next_config_schedule")
                            && device.getDateTime("next_config_schedule").isBefore(new DateTime())) {
                        doorsToProgram.add(d);
                        devices.put(d.getDoorId(), device);
                        device.set("last_config_error", null);
                        String lErr = device.getString("last_registration_error");
                        if (lErr == null || !lErr.equals(IsonasDiscovery.CompletedState))
                            device.set("last_registration_error", IsonasDiscovery.CompletedState);
                        h.save(device);
                    }
                }

            if (doorsToProgram.size() > 0) {
                List<GenericRecord> hList = h.getHolidays();
                List<DateTime> holidayList = new ArrayList<DateTime>(16);
                for (GenericRecord h : hList) holidayList.add(h.getDateTime("holidayDate"));
                if (holidayList.size() > 16)
                    logDbSys("WARNING: Database contains too many (>16) holidays. Cannot program door with all!");

                List<GenericRecord> schedules = h.getAllSchedules();
                Map<Integer, Integer> sMap = new HashMap<Integer, Integer>();
                for (int i = 0; i < schedules.size(); i++)
                    sMap.put(schedules.get(i).getInteger("scheduleId"), i + 1);

                for (Door door : doorsToProgram) {
                    if (wdt!=null) wdt.ping();
                    door.programStandalone(devices.get(door.getDoorId())
                            , holidayList, schedules, sMap);
                }
            }
        } catch (Exception ex) {
            //logDbSys(ex);
            log(ex);
            logError("Attempt to program one or more door failed!");
        }
        finally {
            if (wdt!=null) wdt.ping();
        }
    }


    public void createGlobalElements(GenericDB db) throws Exception{
        logDbSys("Checking for REX User...");
        //GenericRecord rexUser = db.selectFirst("select * from users where userid=0");
        GenericRecord rexUser = h.getUser(0);
        if (rexUser == null) {
            rexUser = db.newRecord("Users");
            rexUser.set("userid", 0);
            rexUser.set("username", "REQUEST TO EXIT [SYS]");
            rexUser.set("external", 0);
            rexUser.set("badgeNum","99999");
            rexUser.set("active", true);
            db.insert(rexUser);
            if (!db.isSuccessful()) throw new Exception(db.getError());
        } else {
            rexUser.set("username", "REQUEST TO EXIT [SYS]");
            rexUser.set("badgeNum","99999");
            rexUser.set("external", 0);
            rexUser.set("active", true);
            h.save(rexUser);
        }
        GenericRecord auxUser = h.getUser(1);
        if (auxUser == null) {
            auxUser = db.newRecord("Users");
            auxUser.set("userid", 1);
            auxUser.set("username", "BUTTON EXIT [SYS]");
            auxUser.set("external", 0);
            auxUser.set("badgeNum","99998");
            auxUser.set("active", true);
            db.insert(auxUser);
            if (!db.isSuccessful()) throw new Exception(db.getError());
        } else {
            auxUser.set("username", "BUTTON EXIT [SYS]");
            auxUser.set("external", 0);
            auxUser.set("badgeNum","99998");
            auxUser.set("active", true);
            h.save(auxUser);
        }

        GenericRecord sch = h.getSchedule(0);
        Time st = Time.valueOf("00:00:00");
        Time ed = Time.valueOf("23:59:59");
        if (sch == null) {
            sch = db.newRecord("schedules");
            sch.set("scheduleId", 0);
            sch.set("createdDate", new DateTime());

            sch.set("scheduleName", "ALWAYS [SYS]");
            sch.set("saturdayStart", st);
            sch.set("saturdayEnd", ed);
            sch.set("sundayStart", st);
            sch.set("sundayEnd", ed);
            sch.set("mondayStart", st);
            sch.set("mondayEnd", ed);
            sch.set("tuesdayStart", st);
            sch.set("tuesdayEnd", ed);
            sch.set("wednesdayStart", st);
            sch.set("wednesdayEnd", ed);
            sch.set("thursdayStart", st);
            sch.set("thursdayEnd", ed);
            sch.set("fridayStart", st);
            sch.set("fridayEnd", ed);
            sch.set("exceptHolidays", "No");
            sch.set("lastUpdatedDate", new DateTime());

            db.insert(sch);
            if (!db.isSuccessful()) throw new Exception(db.getError());
        } else {
            sch.set("scheduleName", "ALWAYS [SYS]");
            sch.set("saturdayStart", st);
            sch.set("saturdayEnd", ed);
            sch.set("sundayStart", st);
            sch.set("sundayEnd", ed);
            sch.set("mondayStart", st);
            sch.set("mondayEnd", ed);
            sch.set("tuesdayStart", st);
            sch.set("tuesdayEnd", ed);
            sch.set("wednesdayStart", st);
            sch.set("wednesdayEnd", ed);
            sch.set("thursdayStart", st);
            sch.set("thursdayEnd", ed);
            sch.set("fridayStart", st);
            sch.set("fridayEnd", ed);
            sch.set("exceptHoliday", "No");
            sch.set("lastUpdatedDate", new DateTime());
            h.save(sch);
        }

        GenericRecord everyone = h.getUserGroup(99999);
        if (everyone==null)
        {
            everyone = db.newRecord("usergroups");
            everyone.set("userGroupId", 99999);
            everyone.set("userGroupName", "EVERYONE [SYS]");
            everyone.set("description", "All Users");
            db.insert(everyone);
            if (!db.isSuccessful()) throw new Exception(db.getError());
            if (!db.isSuccessful()) throw new Exception(db.getError());
        }
        db.executeUpdate("insert ignore into usergroupassociations(userId,userGroupId) "
                +" select userId,99999 from users" ,null);
    }

    public void start() {
        try {
            // **************** COPY PROTECTION **********************
/*            try {
                String licenseFile = Utility.appendDir(Utility.getCurrentDir(), "ip360.lic");
                if (!Utility.fileExists(licenseFile)) {
                    // See if keygen exist if so execute it and delete it
                    String keyGenFile = Utility.appendDir(Utility.getCurrentDir(), "IPSSKeygen.jar");
                    if (!Utility.fileExists(keyGenFile)) throw new Exception("e0x0101");
                    String res = Utility.Shell("java -jar " + keyGenFile);
                    if (res == null) throw new Exception("e0x0102");
                    log("KEYGEN RESPONSE: " + res);
                    if (!Utility.deleteFile(keyGenFile, 10)) throw new Exception("e0x0103");
                }
                Verifier copyProtection = new Verifier();
                copyProtection.verifyLicense(licenseFile);
            } catch (Exception ex) {
                logDbSys("ERROR: Copy Protection Failed (" + ex.getMessage() + ")");
                System.exit(0);
                return;
            }*/


            // Reset the restart flag in the database...
            h.updateDatabaseTo253();
            // Cleaning up the logs...
            log("Cleaning up event logs more than 90 days old...");
            h.cleanLogs();


            // Updating the global config...
            GenericRecord cfg = h.getConfigRec();

            /*cfg.set("requestreset", 0);
            cfg.set("requestreload", 0);
            cfg.set("version", VERSION);
            cfg.set("nextDoorProgrammingSchedule",new DateTime());
            h.save(cfg);
            h.resetConfig();*/

            h.saveConfig(Utility.toMap("requestReset",0,"requestReload",0,"version",VERSION
                    ,"nextDoorProgrammingSchedule",new DateTime()));

            createGlobalElements(db);

            // Initializing Device Discovery or Plug and Play resources
            isonasPNP = new IsonasDiscovery(logger, h);
            apZonePNP = new ZoneDiscovery(logger, h);

            logDbSys("Initializing Alarm Panels...");
            List<GenericRecord> apList = h.getAllAlarmPanels();

            for (GenericRecord ap : apList)
                if ("IT100".equals(ap.getString("alarmPanelTypeId"))) {
                    IT100 ap_it100 = new IT100(logger, ap, lockLinuxComPort);
                    ap_it100.setDebugMode(debugMode);
                    ap_it100.addEventHandler(this);
                    alarmPanels.add(ap_it100);
                    logDbSys("System registered an alarm panel: " + ap_it100);
                }
            //for (IAlarmPanel ap : alarmPanels)
            //    if (!ap.poll()) logDbSys("WARNING: " + ap + ": Problem with communication (TIMEOUT)!");
            //else apZonePNP.seek(Long.parseLong(ap.getId()));

            //apZonePNP.seek(1l);
            logDbSys("Initializing Alarm Panels...Done");


            logDbSys("Initializing Doors...");
            initDoors();
            logDbSys("Initialize Doors...Done");


            //programDoors();

            while (true) {
                // Ping the Watch Dog Timer
                if (wdt!=null) wdt.ping();

                // Check for APPLICATION restart...
                if (h.getConfig().isRequestReload()) {
                    logDbSys("WARNING: User Requested Reload of IPSS Core Engine!");
                    break;
                }
                // Check for SYSTEM restart...
                else if (h.getConfig().isRequestReset()) {
                    logDbSys("WARNING: User Requested a machine level Hard Reset, Core will be terminated without further notice!");
                    logDbSys("Issuing Command: " + shutDownCommand);
                    String res = null;
                    if (shutDownCommand != null && !shutDownCommand.trim().equals(""))
                        res = Utility.ShellExecuteScriptAsync(shutDownCommand);
                    if (Utility.shellError != null) logDbSys("ERROR: " + Utility.shellError);
                    Thread.sleep(30000); // Suspend central process while the system restarts...
                }
                else if (Utility.getElapsed(startupTime)>86400000L){
                    logDbSys("WARNING: IPSS Core Engine must perform a routine restart!");
                    break;
                }
                cfg = h.getConfigRec();
                DateTime nextConfig = cfg.getDateTime("nextDoorProgrammingSchedule");
                if (nextConfig != null && nextConfig.isBefore(new DateTime())) {
                    isonasPNP.configureIP();
                    //cfg = h.getConfigRec();
                    /*cfg.set("nextDoorProgrammingSchedule", (new DateTime()).plusMinutes(2));
                    cfg.set("pingTime",new DateTime());
                    h.save(cfg);*/
                    h.saveConfig(Utility.toMap("nextDoorProgrammingSchedule", (new DateTime()).plusMinutes(10)
                            ,"pingTime",new DateTime()));
                }
                else if (Utility.getElapsed(lastPing)>pingInterval)
                {
                    /*cfg.set("pingTime",new DateTime());
                    h.save(cfg);*/
                    h.saveConfig(Utility.toMap("pingTime",new DateTime()));
                }
                // Process the Doors and events.....
                if (wdt!=null) wdt.ping();
                initDoors();
                for (Door d : doors.values()) {
                    if (d.isDoorConnected())
                        d.digestEvent(null);

                    if (d.isUnlockRequested()) {
                        d.manuallyUnlockDoor();
                        GenericRecord dRec = h.getDoor(d.getDoorId());
                        dRec.set("unlockRequested", 0);
                        db.update(dRec);
                    }
                }


                // Check if ip range has been reassigned!
                /*if (h.getConfig().needIpReprogram())
                {
                    List<GenericRecord> dList =
                            db.selectAll("select * from devices where device_ip_address is not null");
                    if (!db.isSuccessful()) logError(db.getError());
                    else if (dList.size()>0)
                    {
                        log("*********** Reprogramming All Device IP Addresses **************");
                        for(GenericRecord d:dList)
                            d.set("device_target_ip_address",h.getConfig().getNextIpAddress(h));
                        db.update(dList);
                        if (!db.isSuccessful()) logError(db.getError());

                    }
                }*/

                if (Utility.getElapsed(lastReaderPoll) > readerPollTimeout) {
                    //log("Polling Doors...");
                    pollAllDoors();
                    lastReaderPoll = new DateTime();
                    //log("Polling Doors...Done");
                }

                for (Door d : doors.values()) d.checkConnection();


                boolean ipConfiguring = h.hasIsonasReaderInIpConfig();
                boolean readerNeedsConfig = h.hasReadersToConfigure();
                if (readerNeedsConfig) ipConfiguring = true;
                //cfg = h.getConfigRec();
                if (cfg.getBoolean("scanDevices")
                        || (ipConfiguring && Utility.getElapsed(lastIsonasSeek)> minIsonasSeekInterval)) {
                    if (wdt!=null) wdt.ping();
                    isonasPNP.seek();
                    lastIsonasSeek = new DateTime();
                }
                if (!readerNeedsConfig) programDoors();


                // Ping/Poll the Alarm Panel status and also carry out alarm panel related tasks
                IAlarmPanel ap = null;
                if (alarmPanels.size()>0) ap = alarmPanels.get(0);
                if (ap!=null) {
                    long apId = Long.parseLong(ap.getId());
                    String apState = h.getAlarmPanelState(apId);
                    if (apState == null || "AP_STATE_READY".equals(apState) || "AP_STATE_CHANGED".equals(apState)
                            || "AP_STATE_ERROR".equals(apState) || "AP_STATE_CONFIRM".equals(apState)) {
                        // operate alarm panel normally
                        if (Utility.getElapsed(lastAlarmPanelPoll) > alarmPanelPollTimeout) {
                            //log("Polling Alarm Panels...");
                            lastAlarmPanelPoll = new DateTime();
                            apZonePNP.resetRecords();
                            if (!ap.poll()) logWarning(ap + ": Problem with communication (TIMEOUT)!");
                            //else apZonePNP.seek(Long.parseLong(alarmPanel.getId()));
                            //apZonePNP.seek(1l);
                            //log("Polling Alarm Panels...Done");
                        }
                    } else if ("AP_STATE_PROGRAM".equals(apState)) {
                        // Carry out the programming of the alarm Panel
                        if (!isProgrammingInProgress()) {
                            final IAlarmPanel ap_f = ap;
                            Thread apThread = new Thread(new Runnable() {
                                public void run() {
                                    startProgramming();
                                    ap_f.program();
                                    endProgramming();
                                }
                            });
                            apThread.setDaemon(true);
                            apThread.start();
                        }
                    } else if ("AP_STATE_EMAIL".equals(apState)) {
                        // Carry out the programming of the alarm Panel
                        logWarning("This is not implemented yet!");
                    }
                }
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            logDbSys("ERROR: Core Fatal Crash!", e);
            log(e);
            log("ERROR: Core has been terminated! ");
            for (IAlarmPanel ap : alarmPanels) ap.close();
        } finally {
            log("***** IP360 Core Stopped! *****");
            for (IAlarmPanel ap : alarmPanels) ap.close();
            if (isonasPNP != null) isonasPNP.close();
        }
    }


    public void pollAllDoors() {
        for (Door d : doors.values()) {
            d.pollReader();
            if (wdt!=null) wdt.ping();
        }
    }

    public void initDoors() {
        // Integrity check....
        List<GenericRecord> doorList = h.getActiveDoors();
        List<GenericRecord> deviceList = h.getDevicesByClass("ISONAS_READER");
        HashSet<String> doorLookup = new HashSet<String>();
        HashSet<String> deviceLookup = new HashSet<String>();
        for (GenericRecord rec : doorList) doorLookup.add(rec.getString("macAddress"));
        for (GenericRecord rec : deviceList) {
            String key = rec.getString("device_address");
            if (!doorLookup.contains(key)) h.deleteDevice(key);
            else deviceLookup.add(rec.getString("device_address"));
        }
        for (GenericRecord rec : doorList) {
            String key = rec.getString("macAddress");
            if (!deviceLookup.contains(key)) h.deactivateDoor(rec);
        }


        for (Door d : doors.values()) d.setChecked(false);
        for (GenericRecord d : doorList) {
            if (doors.containsKey(d.getLong("doorId"))) {
                // Update the door...
                Door door = doors.get(d.getLong("doorId"));
                door.setChecked(true);
                GenericRecord device = h.getDevice(door.getMacAddress());

                if (door.updated(d, device))
                    logDbSys(door.toString() + " ==>> has been updated!");
            } else {
                // Create the door...
                Door door = new Door(d, pool, port, logger, h);
                door.addEventHandler(this);
                if (!door.isSuccessful()) {
                    //logError(door.toString() + " ==>> " + door.getError());
                    logDbSys(("WARNING: " + door.toString() + " ==>> " + door.getError()));
                } else {
                    doors.put(door.getDoorId(), door);
                    door.setChecked(true);
                    logDbSys(door.toString() + " ==>> has been created!");
                }
            }

        }

        LinkedList<Long> idsToRemove = new LinkedList<Long>();
        for (Door d : doors.values())
            if (!d.isChecked())
                idsToRemove.add(d.getDoorId());
        for (Long id : idsToRemove) {
            Door d = doors.get(id);
            doors.remove(id);
            d.removeAllEventHandlers();
            d.shutDown();
            logDbSys(d.toString() + " ==>> has been removed!");
        }
    }


    public void loadConfig() throws Exception {
        Document doc = Utility.xmlFromFile(configFileName);
        if (doc == null) throw new Exception("Could not load config file: " + configFileName);
        Node root = doc.getElementsByTagName("Config").item(0);
        if (root == null) throw new Exception("Invalid XML Config file!");
        Node log = doc.getElementsByTagName("LogFile").item(0);
        logFileName = log.getAttributes().getNamedItem("Name").getNodeValue();
        if (logFileName == null) throw new Exception("Invalid log file name: " + logFileName);
        int logFileSize = 0;
        try {
            logFileSize = Integer.parseInt(log.getAttributes().getNamedItem("MaxSize").getNodeValue());
        } catch (Exception exc) {
            logFileSize = 16 * 1024 * 1024;
        }
        Node portLock = doc.getElementsByTagName("LockLinuxHardwareComPortId").item(0);
        if (portLock != null) {
            String portLock_s = portLock.getAttributes().getNamedItem("Value").getNodeValue();
            if (portLock_s != null) portLock_s = portLock_s.toLowerCase().trim();
            else portLock_s = "";
            boolean portLock_b = (portLock_s.contains("1") || portLock_s.contains("y"));
            String osName = System.getProperty("os.name");
            if (osName != null && osName.toLowerCase().contains("linux") && portLock_b) lockLinuxComPort = true;
        }

        Node debugMode_n = doc.getElementsByTagName("DebugMode").item(0);
        if (debugMode_n != null) {
            String debugMode_s = debugMode_n.getAttributes().getNamedItem("Value").getNodeValue();
            if (debugMode_s != null) debugMode_s = debugMode_s.toLowerCase().trim();
            else debugMode_s = "";
            debugMode = (debugMode_s.contains("1") || debugMode_s.contains("y"));
        }

        Node wdt_n = doc.getElementsByTagName("WatchDogTimer").item(0);
        if (wdt_n != null) {
            String wdt_s = wdt_n.getAttributes().getNamedItem("Value").getNodeValue();
            if (wdt_s != null) wdt_s = wdt_s.toLowerCase().trim();
            else wdt_s = "";
            wdtEnabled = (wdt_s.contains("1") || wdt_s.contains("y"));
        }

        Node iFile_n = doc.getElementsByTagName("ImportFilePath").item(0);
        if (iFile_n != null) {
            String fn = iFile_n.getAttributes().getNamedItem("Value").getNodeValue();
            if (!StringUtil.empty(fn))
            {
                File f = new File(fn);
                IMPORT_FILE = f.getCanonicalPath();
            }
        }

        logger = new SimpleLogger();
        logger.setLogFile(logFileName, logFileSize);
        log("************************************************");
        log("             IP360 CORE V" + VERSION);
        log("************************************************");
        log("Configuration File: " + configFileName);
        log("Log File: " + logFileName);
        log("************************************************");
        log("");
        log("");
        log("");

        log("Loading Settings...");
        if (wdtEnabled) wdt = new WatchDog(logger);




        NodeList portNodeList = doc.getElementsByTagName("IsonasPort");
        if (portNodeList.getLength() > 0)
            port = Integer.parseInt(portNodeList.item(0)
                    .getAttributes().getNamedItem("Value").getNodeValue());
        NodeList socketRetryNodeList = doc.getElementsByTagName("SocketRetryIntervalMillis");
        if (portNodeList.getLength() > 0)
            socketRetryInterval = Integer.parseInt(socketRetryNodeList.item(0)
                    .getAttributes().getNamedItem("Value").getNodeValue());
        NodeList shutDownNodeList = doc.getElementsByTagName("RestartCommand");
        if (shutDownNodeList.getLength() > 0)
            shutDownCommand = shutDownNodeList.item(0).getAttributes().getNamedItem("Value").getNodeValue();

        logDbSys("Loading Settings...Done");

        log("Loading Database...");
        NodeList dbList = doc.getElementsByTagName("Database");
        for (int i = 0; i < dbList.getLength(); i++) {
            Node dbNode = dbList.item(i);
            if (dbNode.getAttributes().getNamedItem("Name").getNodeValue().equals(DBNAME)) {
                dbConnString = dbNode.getAttributes().getNamedItem("ConnectionString").getNodeValue();
            }
        }
        DBNAME = StringUtil.between(dbConnString,"localhost/","?");
        if (dbConnString == null) throw new Exception("Invalid Config File! No Database connection string found!");
        // Load Mysql Driver....
        GenericDB.Initialize(dbConnString);
        db = new GenericDB();
        if (db == null) throw new Exception("Failed to connect to the database!");
        if (!db.isConnected() || Utility.fileExists(IMPORT_FILE)) reloadDatabase();
        if (h==null) h = new IpssDbHelper(db, logger);
        if (!h.logDbSys("***** IP360 CORE V" + VERSION + " STARTED! *****"))
            throw new Exception("Application failed to start! Database error!");
        logDbSys("Loading Database...Done");
        log("Initializing Socket Pool.....");
        pool = new ClientSocketPool(logger
                , Utility.toList(new IsonasResponseDecompiler())
                , Utility.toList(new IsonasCommandPacketizer()));
        pool.setDebug(debugMode);
        pool.setRetryConnection(true);
        pool.setConnectionRetryInterval(socketRetryInterval);
        if (!pool.initialize())
            throw new Exception("Cannot initialize socket pool selector!");
        readerPollTimeout = h.getConfig().getReaderPollInterval();
        alarmPanelPollTimeout = h.getConfig().getAlarmPanelPollInterval();
        Utility.emailHost = h.getConfig().getSmtpServer();
        String emailUser = h.getConfig().getEmailUserId();
        String emailPassword = h.getConfig().getEmailUserPassword();
        if (emailUser != null && !emailUser.trim().equals("") && emailPassword != null && !emailPassword.trim().equals(""))
            Utility.emailAuthenticator = new SMTPAuthenticator(emailUser, emailPassword);
        AsyncEventResponse.setRexReversed(h.getConfig().isRexReversed());
        AsyncEventResponse.setTamperReversed(h.getConfig().isTamperReversed());

        logDbSys("Initializing Socket Pool.....Done");

    }


    public void reloadDatabase() throws Exception {

        log("***** Resetting the database *****");
        log("Retrieving database schema...");

        String rootConnectionString = dbConnString.replaceAll("localhost/.*\\?", "localhost?");
        GenericDB root = new GenericDB(rootConnectionString);
        if (root == null || !root.isConnected())
            throw new Exception("Unable to establish a connection to the database server!");
        String username = (String) ((ConnectionImpl) root.getConnection()).getProperties().get("user");
        String password = (String) ((ConnectionImpl) root.getConnection()).getProperties().get("password");


        GenericRecord dInfo = root.selectFirst(
                "SELECT count(*) FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + DBNAME + "'");
        if (!root.isSuccessful()) throw new Exception(root.getError());
        if (dInfo.getInteger(0) > 0) {
            // Database exists... so take a backup...
            log("Backing up current database to file...");
            String res = Utility.execute(Utility.toList("mysqldump", "-u", username, "-p" + password
                    , "--ignore-table=" + DBNAME + ".events", DBNAME)
                    , null, "ip360data.bak");
            if (res == null) throw new Exception("Unable to create the database backup!");
            res = Utility.execute(Utility.toList("mysqldump", "-u", username, "-p" + password
                    , DBNAME, "events", "--where=1 order by id desc limit 10000")
                    , null, "ip360history.bak");
            if (res == null) throw new Exception("Unable to create the database backup!");

        }

        IP360Data importData = null;
        if (Utility.fileExists(IMPORT_FILE)) {
            log("Loading import file " + IMPORT_FILE);
            try {
                importData = (IP360Data) xmlConverter.fromXML(new File(IMPORT_FILE));
                if (importData == null) throw new Exception("Unable to load file " + IMPORT_FILE);
                log("Validating import file...");
                importData.validate();
            } catch (Exception ex) {
                log(ex);
                logError("Unable to load the data file: " + IMPORT_FILE);
                GenericRecord cfg = db.selectFirst("select * from ip360.globalconfig");
                cfg.set("requestError", "Error: Failed to perform database reload! Reason: " + ex.getMessage());
                db.update(cfg);
                return;
            } finally {
                Utility.deleteFile(IMPORT_FILE,10);
            }
        }

        log("Rewriting IP360 database...");
        root.executeUpdate("drop database if exists " + DBNAME, null);
        if (!root.isSuccessful()) throw new Exception(root.getError());
        root.executeUpdate("create database " + DBNAME, null);
        if (!root.isSuccessful()) throw new Exception(root.getError());
        String res = Utility.execute(Utility.toList("mysql", "-u", username, "-p" + password, DBNAME)
                , "ip360.schema.sql", null);
        if (res == null) throw new Exception("Unable to create the database schema!");

        if (importData != null) {
            try {
                // Now save the data...
                if (h==null) h = new IpssDbHelper(db, logger);
                log("Saving import data to database...");
                db.executeUpdate("use " + DBNAME, null);
                createGlobalElements(db);
                importData.save(db);
                db.executeUpdate("insert ignore into usergroupassociations(userId,userGroupId) "
                        +" select userId,99999 from users" ,null);
                logDbSys("Important: Finished database import!");
                GenericRecord cfg = db.selectFirst("select * from globalconfig");
                if (cfg!=null)
                {
                    cfg.set("requestError","success");
                    db.update(cfg);
                }
            } catch (Exception ex) {
                log(ex);
                logError("Unable to import data to the database!");
                GenericRecord cfg = db.selectFirst("select * from ip360.globalconfig");
                cfg.set("requestError", "Error: Failed to perform database data import! Reason: " + ex.getMessage());
                db.update(cfg);
            }
        }
    }


    public boolean isSuccess() {
        return success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    /*public void log(String message)
    {
        if (logger!=null)
            logger.log("CENTRAL","INFO",message);
    }

    public void logError(String message)
    {
        if (logger!=null)
            logger.log("CENTRAL","ERROR",message);
    }
    public void logError(Exception ex)
    {
        if (logger!=null)
            logger.log("CENTRAL","ERROR",ex.toString());
    }*/
    private static AlarmPanelState lastState = AlarmPanelState.OFFLINE;
    public void onAlarmPanelEvent(IAlarmPanel alarmPanel, AlarmPanelEventType eType) {
        try {
            Long ap_id = Long.parseLong(alarmPanel.getId());
            if (eType == AlarmPanelEventType.STATUS_CHANGE) {
                if (alarmPanel.getAlarmPanelState()==AlarmPanelState.BUSY) alarmPanel.setBusy(true);
                else alarmPanel.setBusy(false);
                AlarmPanelState newState = alarmPanel.getAlarmPanelState();
                if (newState==AlarmPanelState.BUSY) newState = AlarmPanelState.READY;
                if (lastState!=newState)
                {
                    //log("************** NEW STATUS: " + newState + " ********************");
                    GenericRecord arec = h.getAlarmPanel(ap_id);
                    if (arec != null) {
                        arec.set("status", newState.toString());
                        db.update(arec);
                    }
                    String eventType = null;
                    switch (newState) {
                        case ALARM:
                            eventType = "APSTAT_ALARM";
                            break;
                        case ARMED:
                            eventType = "APSTAT_ARMED";
                            break;
                        case BUSY:
                            //eventType = "APSTAT_BUSY";
                            eventType = "APSTAT_READY";     // We no longer have
                            break;
                        case ENTRY_DELAY:
                            eventType = "APSTAT_ENDELAY";
                            break;
                        case EXIT_DELAY:
                            eventType = "APSTAT_EXDELAY";
                            break;
                        case READY:
                            eventType = "APSTAT_READY";
                            break;
                        default:
                            eventType = "APSTAT_READY";
                            break;
                    }
                    lastState = newState;
                    log(alarmPanel + ": Primary status is: " + newState);
                    h.logDbAP(alarmPanel + ": Primary status is: " + newState, eventType, ap_id);
                }
            } else if (eType == AlarmPanelEventType.PARTITION) {
                // We no longer track partition status event, we only have one partition that we support!
                //String msg = alarmPanel + ": Partition " + alarmPanel.getLastPartitionChanged()
                //        + " state is " + alarmPanel.getPartitionState(alarmPanel.getLastPartitionChanged());
                //log(msg);
                //h.logDbAP(msg, "AP_PARTITION", ap_id);

            } else if (eType == AlarmPanelEventType.ZONE_RESTORE) {
                apZonePNP.seek(Long.parseLong(alarmPanel.getId()), alarmPanel.getLastZoneChanged());
                String zoneid = Integer.toString(alarmPanel.getLastZoneChanged());
                log("************** ZONE RESTORE: " + alarmPanel + " >> "
                        + zoneid + " ********************");
                h.logDbAPZone("APZONE_OFF", ap_id, alarmPanel.getLastZoneChanged());
            } else if (eType == AlarmPanelEventType.ZONE_TRIGGER) {
                apZonePNP.seek(Long.parseLong(alarmPanel.getId()), alarmPanel.getLastZoneChanged());
                String zoneid = Integer.toString(alarmPanel.getLastZoneChanged());
                log("************** ZONE TRIGGER: " + alarmPanel + " >> "
                        + zoneid + " ********************");
                h.logDbAPZone("APZONE_TRIGGER", ap_id, alarmPanel.getLastZoneChanged());
            } else if (eType == AlarmPanelEventType.ZONE_ALARM) {
                apZonePNP.seek(Long.parseLong(alarmPanel.getId()), alarmPanel.getLastZoneChanged());
                String zoneid = Integer.toString(alarmPanel.getLastZoneChanged());
                log("************** ZONE ALARM: " + alarmPanel
                        + " >> " + zoneid + " ********************");
                h.logDbAPZone("APZONE_ALARM", ap_id, alarmPanel.getLastZoneChanged());
            } else if (eType == AlarmPanelEventType.DISARMED) {
                log("************** DISARMED: " + alarmPanel + " ********************");
                h.logDbAP("Alarm Panel " + alarmPanel + " has been DISARMED!", "AP_DISARMED", ap_id);
            } else if (eType == AlarmPanelEventType.ERROR) {
                logError("ERROR in " + alarmPanel + " >> " + alarmPanel.getError());
                h.logDbAP("Error: " + alarmPanel.getError(), "AP_ERROR", ap_id);
            } else if (eType == AlarmPanelEventType.INVALID_CODE) {
                logWarning("Invalid Code Entered in alarm panel " + alarmPanel);
                h.logDbAP("Invalid Code Entered!", "AP_INVCODE", ap_id);
            } else if (eType == AlarmPanelEventType.KEYPAD_LOCKOUT) {
                logWarning("Alarm panel " + alarmPanel + " has been locked out!");
                h.logDbAP("Alarm Panel has been locked out!", "AP_LOCKOUT", ap_id);
            } else if (eType == AlarmPanelEventType.HARDWARE_PORT_CHANGED) {
                logDbSys("WARNING: Alarm panel " + alarmPanel + " has been reassigned/registered to a new COM port!");
                GenericRecord arec = h.getAlarmPanel(ap_id);
                if (arec != null) {
                    arec.set("linuxHardwarePortId", alarmPanel.getHardwareId());
                    arec.set("comAddress", alarmPanel.getConnectionAddress());
                    db.update(arec);
                }
            } else if (eType == AlarmPanelEventType.DEVICE_OFFLINE) {
                logDbSys("WARNING: Alarm panel " + alarmPanel + " is OFFLINE!");
                GenericRecord arec = h.getAlarmPanel(ap_id);
                if (arec != null) {
                    arec.set("status", "OFFLINE");
                    db.update(arec);
                }
                h.logDbAPOffline(ap_id, alarmPanel.getName());
            } else if (eType == AlarmPanelEventType.DEVICE_CONNECTED) {
                if (alarmPanel.getAlarmPanelState() != AlarmPanelState.OFFLINE) {
                    logDbSys("Alarm panel " + alarmPanel + " is ONLINE!");
                    GenericRecord arec = h.getAlarmPanel(ap_id);
                    if (arec != null) {
                        arec.set("status", alarmPanel.getAlarmPanelState().toString());
                        db.update(arec);
                    }
                }
            }
        } catch (Exception e) {
            log(e);
            logError("Unable to process Alarm Panel Event!");
        }
    }

    public void onDoorStatusChange(Door door, DoorStates newState) {

        // TODO: This needs to change completely! The alarmpanel will be disarmed only if the user that unlocked
        // the door, is in the disarm group. We no longer arm the door by swiping a badge.

        if (newState == DoorStates.UNLOCKED) {
            for (IAlarmPanel ap : alarmPanels)
                if (door.isDisarmAlarmPanelOnUnlock()
                        && ap.getAlarmPanelState() != AlarmPanelState.READY
                        && ap.getAlarmPanelState() != AlarmPanelState.BUSY
                        && ap.getAlarmPanelState() != AlarmPanelState.OFFLINE) {

                    logDbSys("Valid access credential presented, the alarm panel is being disarmed!");
                    for (int i=0;i<10;i++) {
                        if (ap.getAlarmPanelState()==AlarmPanelState.READY
                                || ap.getAlarmPanelState()==AlarmPanelState.BUSY) break;
                        if (!ap.disarm()) {
                            if (ap.getError() != null) logDbSys("ERROR: " + ap.getError());
                            logDbSys("WARNING: Unable to communicate with the alarm Panel. Failed to disarm!");
                        };
                        if (ap.getAlarmPanelState()==AlarmPanelState.READY
                                || ap.getAlarmPanelState()==AlarmPanelState.BUSY) break;
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            logDbSys("ERROR: " + e.toString());
                        }
                    }
                } else if (door.isArmAlarmPanelOnUnlock()
                        && (ap.getAlarmPanelState() == AlarmPanelState.READY
                        || ap.getAlarmPanelState() == AlarmPanelState.BUSY)) {
                    while (ap.getAlarmPanelState() == AlarmPanelState.BUSY)
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            logDbSys("ERROR: " + e.toString());
                        }
                    logDbSys("Valid access credential presented, the alarm panel is being armed!");
                    if (!ap.arm()) {
                        if (ap.getError() != null) logDbSys("ERROR: " + ap.getError());
                        logDbSys("WARNING: Unable to communicate with the alarm Panel. Failed to arm!");
                    }
                }
        }
        /*else if (newState==DoorStates.FORCED_OPEN)
        {
            for(IAlarmPanel ap:alarmPanels)
                if (ap.getAlarmPanelState()== AlarmPanelState.READY) ap.arm();
                else if(ap.getAlarmPanelState() == AlarmPanelState.ARMED) ap.alarm();
        }*/
    }

    public void startProgramming()
    {
        synchronized (apLock)
        {
            programmingStarted = true;
        }
    }

    public void endProgramming()
    {
        synchronized (apLock)
        {
            programmingStarted = false;
        }
    }

    public boolean isProgrammingInProgress()
    {
        synchronized (apLock)
        {
            return programmingStarted;
        }
    }
}
