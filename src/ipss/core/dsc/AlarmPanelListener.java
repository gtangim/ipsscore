package ipss.core.dsc;

import ipss.core.dsc.states.AlarmPanelEventType;

import java.util.EventListener;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/4/12
 * Time: 6:40 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AlarmPanelListener extends EventListener {
    public void onAlarmPanelEvent(IAlarmPanel alarmPanel, AlarmPanelEventType eType);
}
