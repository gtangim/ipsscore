package ipss.core.dsc.it100.commands;

/**
 * Created by RusselA on 5/4/2016.
 */
public class IT100EnableVirtualKeypad extends IT100BaseCommand {
    public IT100EnableVirtualKeypad(boolean enableKeypad)
    {
        super("058",(enableKeypad?"1":"0"));
    }
}
