package ipss.core.dsc.it100.commands;

import apu.util.StringUtil;
import apu.util.Utility;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/4/12
 * Time: 8:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class IT100CommandDisarm extends IT100BaseCommand {
    public IT100CommandDisarm(int partition, String armCode)
    {
        super("040", Integer.toString(Utility.clamp(partition,1,8))+StringUtil.fixedLengthString(armCode,6,'0',false));
    }
}
