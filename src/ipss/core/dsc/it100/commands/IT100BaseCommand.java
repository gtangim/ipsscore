package ipss.core.dsc.it100.commands;

import apu.net.PacketizedCommand;
import apu.util.StringUtil;
import apu.util.Utility;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/1/12
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class IT100BaseCommand implements PacketizedCommand {


    protected static HashSet<Character> digits = new HashSet<Character>(Arrays.asList('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'));
    protected static Map<String,String> errMap = initErrMap();
    protected String cmd = null;
    protected String data = null;
    protected boolean valid = true;
    
    public IT100BaseCommand(String commandHex, String dataHex)
    {
        cmd=commandHex;
        data=dataHex;
        checkValid();
    }
    public IT100BaseCommand()
    {
        valid=false;
        // For Extensions...
    }
    public IT100BaseCommand(byte[] rawResponse, int start, int end)
    {
        String raw = new String(rawResponse,start,end-start);
        if (raw.length()<=3)
            cmd = raw;
        else
        {
            cmd = raw.substring(0,3);
            data = raw.substring(3);
        }
        checkValid();
    }

    protected void checkValid()
    {
        valid=true;
        if (cmd==null || cmd.length()!=3) {valid=false; return;}
        cmd=cmd.toUpperCase();
        if (cmd!=null) for(int i=0;i<cmd.length();i++) if (!digits.contains(cmd.charAt(i))) {valid=false; return;}
        //if (data!=null)
        //{
        //    data=data.toUpperCase();
        //    for(int i=0;i<data.length();i++) if (!digits.contains(data.charAt(i))) {valid=false; return;}
        //}
    }


    public ByteBuffer getBinary() {
        ByteBuffer ret = ByteBuffer.allocate(cmd.length()+(data==null?0:data.length()));
        ret.put(StringUtil.stringToByte(cmd));
        if (data!=null && data.length()>0) ret.put(StringUtil.stringToByte(data));
        return ret;
    }


    public String getCommand() {
        return cmd;
    }

    public String getData() {
        return data;
    }
    
    public String toString()
    {
        if (!valid) return "[ERR]";
        else if (data==null || data.length()==0) return "["+cmd+"]";
        else return "["+cmd+" - "+data+"]";
    }

    public boolean isValid() {
        return valid;
    }

    public boolean isAckPacket()
    {
        return "500".equals(cmd);
    }

    public boolean isCommandErrorPacket()
    {
        return "501".equals(cmd);
    }
    public boolean isSystemErrorPacket()
    {
        return "502".equals(cmd);
    }

    public String getSystemErrorMessage()
    {
        if (data==null || data.length()!=3) return null;
        if (errMap.containsKey(data)) return errMap.get(data);
        else return "Unknown Error (" + data+")!";
    }
    
    public static HashMap<String,String> initErrMap()
    {
        HashMap<String,String> e = new HashMap<String, String>();
        e.put("017","Keybus Busy - Installer Mode!");
        e.put("021","Requested Partition is out of Range!");
        e.put("023","Partition is not armed!");
        e.put("024","Partition is not ready to arm!");
        e.put("026","User code not required!");
        e.put("028","Virtual keypad is Disabled!");
        e.put("029","Invalid parameter!");
        e.put("030","Keypad does not come out of blank mode!");
        e.put("031","IT-100 is already in thermostat menu!");
        e.put("032","IT-100 is not in thermostat menu!");
        e.put("033","No response from thermostat!");
        return e;
    }

}
