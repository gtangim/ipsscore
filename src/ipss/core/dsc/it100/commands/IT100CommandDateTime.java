package ipss.core.dsc.it100.commands;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by RusselA on 5/4/2016.
 */
public class IT100CommandDateTime extends IT100BaseCommand {
    private static DateTimeFormatter fmt = DateTimeFormat.forPattern("HHmmMMddyy");
    public IT100CommandDateTime(DateTime date)
    {
        super("010",fmt.print(date));
    }
}
