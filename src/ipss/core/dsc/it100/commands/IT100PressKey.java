package ipss.core.dsc.it100.commands;

/**
 * Created by RusselA on 5/4/2016.
 */
public class IT100PressKey extends IT100BaseCommand {
    public IT100PressKey(char c)
    {
        super("070", Character.toString(c));
    }
}
