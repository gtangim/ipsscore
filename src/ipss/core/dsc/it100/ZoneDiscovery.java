package ipss.core.dsc.it100;

import apu.db.GenericRecord;
import apu.util.Loggable;
import apu.util.SimpleLogger;
import apu.util.Utility;
import ipss.core.isonas.IpssDbHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 2/25/13
 * Time: 4:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class ZoneDiscovery extends Loggable {
    private IpssDbHelper h;

    public ZoneDiscovery(SimpleLogger logger, IpssDbHelper helper) throws Exception
    {
        super(logger);
        h = helper;
    }

    private Map<String,GenericRecord> zoneMap = null;
    private Map<String,GenericRecord> sensorMap = null;
    private String toZoneString(Long id)
    {
        String ret = id.toString();
        if (ret.length()==1) ret = "0"+ret;
        return ret;
    }
    public void seek(Long alarmPanelId, int zoneId)
    {
        try {
            if (zoneMap==null)
            {
                List<GenericRecord> zoneDeviceList = h.db.selectAll("select * from devices where "
                        +" device_class_id='AP_SENSOR'"
                        +" and device_address like 'ZONE:%'");
                if (!h.db.isSuccessful()) throw new Exception(h.db.getError());
                zoneMap = new HashMap<String, GenericRecord>();
                for(GenericRecord zd:zoneDeviceList) zoneMap.put(zd.getString("device_address").substring(5),zd);
            }

            if (sensorMap==null)
            {
                List<GenericRecord> sensorList = h.db.selectAll("select * from sensors where alarmPanelId=? "
                        ,Utility.toList(alarmPanelId));
                if (!h.db.isSuccessful()) throw new Exception(h.db.getError());
                sensorMap = new HashMap<String, GenericRecord>();
                for(GenericRecord s:sensorList) sensorMap.put(toZoneString(s.getLong("zoneNumber")),s);
            }

            String zid = toZoneString((long)zoneId);
            if (zoneMap.containsKey(zid) && sensorMap.containsKey(zid))
            {
                // This device should be registered!
                GenericRecord dev = zoneMap.get(zid);
                GenericRecord sensor = sensorMap.get(zid);
                dev.set("device_status_id","DEVICE_STATUS_REGISTERED");
                dev.set("last_registration_error",null);
                dev.set("device_name",sensor.getString("sensorName"));
                dev.set("device_class_id","AP_SENSOR");
                dev.set("device_type_id",sensor.getString("sensorType"));
                h.save(dev);
            }
            else if (zoneMap.containsKey(zid) && !sensorMap.containsKey(zid))
            {
                GenericRecord dev = zoneMap.get(zid);
                // Revert to unregistered status....
                dev.set("device_class_id","AP_SENSOR");
                dev.set("device_type_id",null);
                dev.set("device_name",null);
                dev.set("device_status_id","DEVICE_STATUS_UNREGISTERED");
                dev.set("last_registration_error",null);
                h.save(dev);
            }
            else if (!zoneMap.containsKey(zid) && sensorMap.containsKey(zid))
            {
                // Already added to the system...
                GenericRecord sensor = sensorMap.get(zid);
                GenericRecord z = h.db.newRecord("devices");
                z.set("device_address","ZONE:"+zid);
                z.set("device_class_id","AP_SENSOR");
                z.set("device_type_id",sensor.getString("sensorType"));
                z.set("device_status_id","DEVICE_STATUS_REGISTERED");
                if (!h.create(z)) throw new Exception("Could not create device record: "+zid+"!");
                else zoneMap.put(zid,z);
            }
            else if (!zoneMap.containsKey(zoneId) && !sensorMap.containsKey(zoneId))
            {
                // Brand new zone detected!
                GenericRecord z = h.db.newRecord("devices");
                z.set("device_address","ZONE:"+zid);
                z.set("device_class_id","AP_SENSOR");
                z.set("device_status_id","DEVICE_STATUS_UNREGISTERED");
                if (!h.create(z)) throw new Exception("Could not create device record: "+zoneId+"!");
                else zoneMap.put(zid,z);
            }
        } catch (Exception e) {
            logDbSys(e);
        }

    }


    public void seek(Long alarmPanelId)
    {
        for(int i=1;i<=64;i++)
            seek(alarmPanelId,i);
    }

    public void resetRecords()
    {
        zoneMap=null;
        sensorMap=null;
    }

    /*public void seek(Long alarmPanelId)
    {
        try {
            log("*********** DISCOVERING ALARM PANEL ZONES **************");
            List<GenericRecord> zoneDeviceList = h.db.selectAll("select * from devices where "
                    +" device_class_id='AP_SENSOR'"
                    +" and device_address like 'AP:"+alarmPanelId.toString()+"-%'");
            if (!h.db.isSuccessful()) throw new Exception(h.db.getError());
            Map<Long,GenericRecord> zoneMap = new HashMap<Long, GenericRecord>();
            for(GenericRecord zd:zoneDeviceList) zoneMap.put(getZoneId(zd.getString("device_address")),zd);

            List<GenericRecord> sensorList = h.db.selectAll("select * from sensors where alarmPanelId=? "
                    ,Utility.toList(alarmPanelId));
            if (!h.db.isSuccessful()) throw new Exception(h.db.getError());
            Map<Long,GenericRecord> sensorMap = new HashMap<Long, GenericRecord>();
            for(GenericRecord s:sensorList) sensorMap.put(s.getLong("zoneNumber"),s);

            for (long zoneId=1;zoneId<=64;zoneId++)
            {
                if (zoneMap.containsKey(zoneId) && sensorMap.containsKey(zoneId))
                {
                    GenericRecord dev = zoneMap.get(zoneId);
                    GenericRecord sensor = sensorMap.get(zoneId);
                    dev.set("device_status_id","DEVICE_STATUS_REGISTERED");
                    dev.set("last_registration_error",null);
                    dev.set("device_name",sensor.getString("sensorName"));
                    h.save(dev);
                }
                else if (zoneMap.containsKey(zoneId) && !sensorMap.containsKey(zoneId))
                {
                    GenericRecord dev = zoneMap.get(zoneId);
                    if ("DEVICE_STATUS_MAPPED".equals(dev.getString("device_status_id")))
                    {
                        // Perform Registration...
                        GenericRecord sensor = h.db.newRecord("sensors");
                        sensor.set("id",h.getNextId("sensors","id"));
                        sensor.set("sensorName",dev.getString("device_name"));
                        sensor.set("sensorType",dev.getString("device_type_id"));
                        sensor.set("zoneNumber",zoneId);
                        sensor.set("alarmPanelId",alarmPanelId);
                        if (!h.create(sensor))
                        {
                            dev.set("device_status_id","DEVICE_STATUS_ERROR");
                            dev.set("last_registration_error","Unable to create sensor record!");
                            h.save(dev);
                        }
                        else
                        {
                            dev.set("device_status_id","DEVICE_STATUS_REGISTERED");
                            dev.set("last_registration_error",null);
                            h.save(dev);
                        }
                    }
                    else
                    {
                        // Revert to unregistered status....
                        dev.set("device_type_id",null);
                        dev.set("device_name",null);
                        dev.set("device_status_id","DEVICE_STATUS_UNREGISTERED");
                        dev.set("last_registration_error",null);
                        h.save(dev);
                    }

                }
                else if (!zoneMap.containsKey(zoneId) && sensorMap.containsKey(zoneId))
                {
                    // Already added to the system...
                    GenericRecord sensor = sensorMap.get(zoneId);
                    GenericRecord z = h.db.newRecord("devices");
                    z.set("device_address",getAddress(alarmPanelId,zoneId));
                    z.set("device_class_id","AP_SENSOR");
                    z.set("device_type_id",sensor.getString("sensorType"));
                    z.set("device_status_id","DEVICE_STATUS_REGISTERED");
                    if (!h.create(z)) throw new Exception("Could not create device record: "+zoneId+"!");
                }
                if (!zoneMap.containsKey(zoneId) && !sensorMap.containsKey(zoneId))
                {
                    // Brand new zone detected!
                    GenericRecord z = h.db.newRecord("devices");
                    z.set("device_address",getAddress(alarmPanelId,zoneId));
                    z.set("device_class_id","AP_SENSOR");
                    z.set("device_status_id","DEVICE_STATUS_UNREGISTERED");
                    if (!h.create(z)) throw new Exception("Could not create device record: "+zoneId+"!");
                }
            }



            log("*********** FINISHED DISCOVERING ALARM PANEL ZONES **************");
        } catch (Exception e) {
            logDbSys(e);
        }
    } */


    /*private String getAddress(Long apId, Long zoneId)
    {
        if (apId==null || zoneId==null) return null;
        String id = "AP:"+apId.toString()+"-"+zoneId.toString();
        return id;
    }

    private Long getZoneId(String address)
    {
        try {
            String[] adrSplit = address.split("-");
            if (adrSplit.length!=2) return null;
            return Long.parseLong(adrSplit[1]);
        } catch (Exception e) {
            log(e);
            return null;
        }
    } */


    public void logDbSys(Exception ex)
    {
        log(ex);
        if (h!=null) h.logDb("Error: Failed to perform device discovery! " + ex.toString(),"SYSTEM_ERROR");
    }


}
