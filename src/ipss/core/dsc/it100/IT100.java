package ipss.core.dsc.it100;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.net.IDataFilter;
import apu.ports.GenericSerialPort;
import apu.ports.PortDataEvent;
import apu.ports.PortDataListener;
import apu.util.Loggable;
import apu.util.SimpleLogger;
import apu.util.StringUtil;
import apu.util.Utility;
import ipss.core.dsc.it100.commands.*;
import ipss.core.dsc.states.AlarmPanelEventType;
import ipss.core.dsc.AlarmPanelListener;
import ipss.core.dsc.IAlarmPanel;
import ipss.core.dsc.it100.responses.IT100ResponsePartition;
import ipss.core.dsc.it100.responses.IT100ResponseZone;
import ipss.core.dsc.states.AlarmPanelState;
import ipss.core.dsc.states.AlarmPanelZoneState;
import ipss.core.isonas.IpssDbHelper;
import org.joda.time.DateTime;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/1/12
 * Time: 10:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class IT100 extends Loggable implements IAlarmPanel, PortDataListener {

    protected static final int comReadEventDelay = 200; // Milliseconds
    protected static final int commandWaitInterval = 5000; // Milliseconds

    protected String lastErorr = null;
    protected String lastErrorCode = null;
    protected String name;
    protected String id;
    protected String port;
    protected String portHardwarePath;
    protected int baud;
    protected int dataBits;
    protected String parity;
    protected String stopBits;
    protected String flowControl;
    protected String armCode;
    protected String disarmCode;
    protected boolean lockComPort;
    protected int maxCommandRetry = 1;
    protected GenericRecord alarmPanelRec = null;
    protected boolean debugMode = true;
    protected boolean busy = false;


    protected AlarmPanelState state = AlarmPanelState.OFFLINE;
    protected Map<Integer, AlarmPanelState> partitionStates = new HashMap<Integer, AlarmPanelState>();
    protected AlarmPanelZoneState[] zones = null;
    protected int lastZoneChanged = -1;
    protected int lastPartitionChanged = -1;

    protected IDataFilter inputFilter = (IDataFilter) new IT100ResponseDecompiler();
    protected IDataFilter outputFilter = (IDataFilter) new IT100CommandPacketizer();
    protected ByteBuffer partialBuffer = null;

    protected GenericSerialPort com = null;

    protected Object commandWaiter = new Object();
    protected IT100BaseCommand lastAck = null;

    protected List<AlarmPanelListener> eventHandlers = new LinkedList<AlarmPanelListener>();
    private String display = null;
    private String beep = null;

    public IT100(SimpleLogger logger, GenericRecord alarmPanelRec, boolean lockComPort) {
        super(logger);
        try {
            this.alarmPanelRec = alarmPanelRec;
            this.lockComPort = lockComPort;
            if (!"IT100".equals(alarmPanelRec.getString("alarmPanelTypeId")))
                throw new Exception("IT100 alarm panel expected!");
            id = alarmPanelRec.getLong("alarmPanelId").toString();
            name = alarmPanelRec.getString("name");
            port = alarmPanelRec.getString("comAddress");
            portHardwarePath = alarmPanelRec.getString("linuxHardwarePortId");
            baud = alarmPanelRec.getInteger("baudRate");
            dataBits = alarmPanelRec.getInteger("dataBits");
            parity = alarmPanelRec.getString("parity");
            stopBits = alarmPanelRec.getString("stopBits");
            flowControl = alarmPanelRec.getString("flowControl");
            armCode = alarmPanelRec.getString("armCode");
            disarmCode = alarmPanelRec.getString("disarmCode");
            if (id == null || id.trim().isEmpty()) throw new Exception("Id must not be null/empty!");
            if (name == null || name.trim().isEmpty()) throw new Exception("Name must not be null/empty!");
            if (parity == null) parity = "None";
            if (stopBits == null) stopBits = "1";
            if (flowControl == null) stopBits = "None";
            // *** I dont think it's any longer necessary to try to connect in the constructor
            //tryConnect();

            zones = new AlarmPanelZoneState[65];
            zones[0] = AlarmPanelZoneState.OUT_OF_RANGE;
            for (int i = 1; i <= 64; i++) zones[i] = AlarmPanelZoneState.OFF;

            for (int i = 1; i <= 8; i++) partitionStates.put(i, AlarmPanelState.OFFLINE);
        } catch (Exception ex) {
            lastErorr = "Error: Failed to initialize IT100 panel. " + ex.getMessage();
            log(ex);
            com = null;
        }
    }

    protected void adjustPortMapping() {
        String osName = System.getProperty("os.name");
        if (osName != null && osName.toLowerCase().contains("linux") && lockComPort) {
            log("*********************************************");
            log("********   DISCOVERING PORT MAPPING   *******");
            String res = Utility.Shell(new String[]{"bash", "-c", "ls -l /dev/serial/by-path"});
            if (res != null) {
                log(res);
                String[] lines = res.split("\\r?\\n");
                for (String line : lines) {
                    String[] primaryMap = line.split("->");
                    if (primaryMap.length == 2) {
                        String[] idList = primaryMap[0].split("\\s+");
                        String id = idList[idList.length - 1];
                        String portId = primaryMap[1].replaceAll("../../", "/dev/").trim();
                        if (portHardwarePath == null && portId.equals(port)) {
                            portHardwarePath = id;
                            fireEvent(AlarmPanelEventType.HARDWARE_PORT_CHANGED);
                        } else if (portHardwarePath != null && id.equals(portHardwarePath) && !portId.equals(port)) {
                            port = portId;
                            fireEvent(AlarmPanelEventType.HARDWARE_PORT_CHANGED);
                        }
                    }
                }
            }


            log("*********************************************");
        }
    }


    public boolean tryConnect() {
        try {
            if (com != null && com.isOpen()) return true;
            if (com != null) disconnect();
            adjustPortMapping();

            // just try to connect....
            com = new GenericSerialPort(logger, port, baud, dataBits, stopBits, parity, flowControl, 0);
            com.addDataListener(this);
            com.setDataEventMode(true);
            //com.setDebugMode(debugMode);
            com.open();
            fireEvent(AlarmPanelEventType.DEVICE_CONNECTED);
            return true;
        } catch (Exception ex) {
            log(ex);
            disconnect();
            fireEvent(AlarmPanelEventType.DEVICE_OFFLINE);
            return false;
        }
    }

    public void disconnect() {
        try {
            if (com != null && com.isOpen()) com.close();
        } finally {
            com = null;
        }
    }


    public void sendCommand(IT100BaseCommand cmd) throws Exception {
        synchronized (commandWaiter) {
            lastAck = null;
            if (cmd == null || !cmd.isValid() || cmd.isAckPacket() || cmd.isCommandErrorPacket() || cmd.isSystemErrorPacket())
                throw new IOException("Invalid command issued: " + cmd + " !");
            List<Object> cList = new LinkedList<Object>();
            if (debugMode)
            {
                log("IT100.tx >> " + cmd.getCommand()+" : " + cmd.getData());
            }
            cList.add(cmd);
            outputFilter.process(null, cList);
            List processedPacket = outputFilter.getMaturedObjects();
            if (processedPacket != null)
                for (Object dataElement : processedPacket)
                    if (dataElement instanceof byte[]) com.writeAsync((byte[]) dataElement);
                    else if (dataElement instanceof String) com.writeAsync((String) dataElement);
                    else throw new Exception("Output filter configuration is invalid! must return byte[] or String");
            else throw new Exception("Cannot send packet, object did not pass through the filter!");
            commandWaiter.wait(commandWaitInterval);
            if (lastAck == null) throw new Exception("IT100 Command Timeout!");
            else if (lastAck.isCommandErrorPacket()) throw new Exception("IT100 Command Transmission Failed!");
        }
    }


    protected boolean tryCommandSequence(IT100BaseCommand cmd, int level) {
        if (level > maxCommandRetry) return false;
        lastErorr = null;
        lastErrorCode = null;
        try {
            if (!tryConnect()) throw new Exception("Unable to bind to com port: " + port);
            sendCommand(cmd);
            return true;
        } catch (Exception e) {
            disconnect();
            if (level >= maxCommandRetry) {
                lastErorr = "Error: Failed to send Data. " + e.getMessage();
                log(e);
                fireError(null, lastErorr);
                fireEvent(AlarmPanelEventType.DEVICE_OFFLINE);
                return false;
            } else return tryCommandSequence(cmd, level + 1);
        }
    }


    public boolean arm() {
        return tryCommandSequence(new IT100CommandArm(1, armCode), 0);
    }

    public boolean disarm() {
        return tryCommandSequence(new IT100CommandDisarm(1, disarmCode), 0);
    }

    public boolean alarm() {
        return tryCommandSequence(new IT100CommandAlarm(), 0);
    }

    public boolean arm(int partition) {
        if (partition < 1 || partition > 8) {
            lastErorr = "Error: Failed to arm. Out of range partition: " + Integer.toString(partition);
            logError(lastErorr);
            fireError(null, lastErorr);
            return false;
        }
        return tryCommandSequence(new IT100CommandArm(partition, armCode), 0);
    }


    public boolean disarm(int partition) {
        if (partition < 1 || partition > 8) {
            lastErorr = "Error: Failed to disarm. Out of range partition: " + Integer.toString(partition);
            logError(lastErorr);
            fireError(null, lastErorr);
            return false;
        }
        return tryCommandSequence(new IT100CommandDisarm(partition, disarmCode), 0);
    }

    public boolean alarm(int partition) {
        if (partition < 1 || partition > 8) {
            lastErorr = "Error: Failed to set alarm. Out of range partition: " + Integer.toString(partition);
            logError(lastErorr);
            fireError(null, lastErorr);
            return false;
        }
        return tryCommandSequence(new IT100CommandAlarm(), 0);
    }


    public boolean poll() {
        return tryCommandSequence(new IT100CommandPoll(), 0);
    }

    public void sendKeypad(String keys) throws Exception {
        for (char c : keys.toCharArray()) {sendKeypad(c); Thread.sleep(50);}
    }

    public void sendKeypad(char key) throws Exception {
        if (!tryCommandSequence(new IT100PressKey(key), 0))
            throw new Exception("Could not send key press command to the Alarm Panel!");
    }

    public void waitForDisplay() throws Exception {
        String disp = null;
        for (int i = 0; i < 100; i++) {
            disp = display;
            if (disp == null) Thread.sleep(200);
            else break;
        }
        if (disp == null)
            throw new Exception("Display change expected but didn't happen!");
    }

    public void waitForDisplay(String searchPhrase) throws Exception {
        String disp = null;
        for (int i = 0; i < 100; i++) {
            disp = display;
            if (disp == null || !disp.contains(searchPhrase)) Thread.sleep(200);
            else break;
        }
        if (disp == null || !disp.contains(searchPhrase))
            throw new Exception("Display change didn't occur properly! Expected: " + searchPhrase);
    }

    public void waitForDisplay(String searchPhrase1, String searchPhrase2) throws Exception {
        String disp = null;
        for (int i = 0; i < 100; i++) {
            disp = display;
            if (disp == null || (!disp.contains(searchPhrase1) && !disp.contains(searchPhrase2)))
                Thread.sleep(200);
            else break;
        }
        if (disp == null || (!disp.contains(searchPhrase1) && !disp.contains(searchPhrase2)))
            throw new Exception("Display change didn't occur properly! Expected: ("
                    + searchPhrase1 + ", " + searchPhrase2 + ")");
    }


    public void bypassZones() throws Exception {
        sendKeypad(END_KEYS);
        sendKeypad(END_KEYS);
        sendKeypad(END_KEYS);
        display=null;
        sendKeypad("**");
        waitForDisplay("Bypass Zones");
        for (int i=0;i<16;i++) {
            display=null;
            sendKeypad('>');
            waitForDisplay("Zone");
            sendKeypad('*');
        }
        sendKeypad("###");
    }


    public void goToMainScreen() throws Exception
    {
        display=null;
        sendKeypad(END_KEYS);
        Thread.sleep(500);
        sendKeypad(">>");
        Thread.sleep(500);
        waitForDisplay();
        if (display.contains("Enter Section"))
        {
            display=null;
            sendKeypad(END_KEYS);
            Thread.sleep(500);
            sendKeypad(">>");
            Thread.sleep(500);
            waitForDisplay();
        }

        if (!display.contains("Secure System") && !display.contains("Ready to Arm")){
            boolean systemFound = false;
            int tryCount=0;
            while(!systemFound) {
                display = null;
                sendKeypad('<');
                waitForDisplay();
                if (display.startsWith("System")) systemFound = true;
                else tryCount++;
                if (tryCount>10) throw new Exception("IT100 in invalid state, please factory reset manually!");
            }
            display=null;
            sendKeypad('*');
            waitForDisplay();
            tryCount=0;
            while(!display.contains("Secure System") && !display.contains("Ready to Arm")) {
                Thread.sleep(100);
                tryCount++;
                if (tryCount>50) break;
            }
            if (!display.contains("Secure System") && !display.contains("Ready to Arm"))
                throw new Exception("IT100 in invalid state, please factory reset manually!");
        }
    }


    private static final String END_KEYS = "####";
    //private static final String INSTALLER_ENTRY_KEYS ="*8";
    private static final String USER_ENTRY_KEYS ="*5";
    private static final String CHANGE_INSTALLER_CODE_FUNCTION = "006";
    private static final String CHANGE_MASTER_CODE_FUNCTION = "007";
    private static final String ENTRY_EXIT_FUNCTION = "005";
    private static final String PARTITION_SETUP_FUNCTION = "201";
    private static final String SYSTEM_ACCOUNT_FUNCTION = "310";
    private static final String PARTITION_ACCOUNT_FUNCTION = "311";
    private static final String PHONE_FUNCTION1 = "301";
    private static final String PHONE_FUNCTION2 = "302";
    private static final String SETUP_COMM_LINK_FUNCTION = "382";
    private static final String TEST_TX_ENABLE_FUNCTION = "018";
    private static final String TEST_TX_TIME_FUNCTION = "378";
    private static final String PASSWORD6_TOGGLE_FUNCTION = "701";
    private static final String TLINK_CONFIG_FUNCTION = "851";
    private static final int    ZONE_TOGGLE_FUNCTION_BASE = 202;
    private static final int    ZONE_DEFINITION_FUNCTION_BASE = 1;

    private static final String PARTITION_ID = "01";
    private static final String PASSWORD6_OPTION = "5";
    private static final String TEST_TX_HOUR = "2200";
    private static final int    MAX_LABEL_SIZE=26;
    private static final String TLINK_IP = "001";
    private static final String TLINK_SUBNET = "002";
    private static final String TLINK_ACCOUNT = "003";
    private static final String TLINK_MON_IP = "007";
    private static final String TLINK_MON_GATEWAY = "008";
    private static final String TLINK_SUPER = "023";
    private static final String TLINK_PHONE = "*3**1**1*";


    private String installerCodeDefault4 = "5555";
    private String installerCodeDefault6 = "555555";
    //private String masterCodeDefault4 = "1234";
    private String installerCode = null;
    private String masterCode = null;
    private String emergencyDisarmCode=null;

    public void program()
    {
        IpssDbHelper h = null;
        long apId = Long.parseLong(id);
        try {
            log("Starting programming the IT100 alarm Panel Module...");
            GenericDB db = new GenericDB();
            if (db == null) throw new Exception("Failed to connect to the database!");
            h = new IpssDbHelper(db, logger);
            h.setAlarmPanelState(apId,"AP_STATE_PROGRAM","Programming the Alarm Panel...");
            if (!tryCommandSequence(new IT100EnableVirtualKeypad(true),0))
                throw new Exception("Unable to enable the IT100 virtual keypad mode!");
            alarmPanelRec = h.db.selectFirst("select * from alarmpanels where alarmPanelId=?", Utility.toList(apId));

            emergencyDisarmCode = alarmPanelRec.getString("emergencyDisarmCode");
            if (emergencyDisarmCode==null)
            {
                emergencyDisarmCode = h.getRandom(6,3);
                alarmPanelRec.set("emergencyDisarmCode", emergencyDisarmCode);
                db.executeUpdate("update alarmpanels set emergencyDisarmCode=? where alarmPanelId=?"
                        , Utility.toList(emergencyDisarmCode, apId));
            }

            installerCode = alarmPanelRec.getString("installerCode");
            masterCode = alarmPanelRec.getString("masterCode");
            //if (installerCode==null) installerCode = installerCodeDefault;
            //if (masterCode==null) masterCode = masterCodeDefault;



            // Factory Reset
            log("Performing a factory reset...");
            h.setAlarmPanelState(apId, "AP_STATE_PROGRAM", "Performing factory reset on alarm panel...");
            try {
                goToMainScreen();
            } catch (Exception e) {
                goToMainScreen();
            }
            /*bypassZones();*/
            try {
                goToMainScreen();
            } catch (Exception e) {
                goToMainScreen();
            }

            executeFactoryReset();
            if (!tryCommandSequence(new IT100CommandDateTime(new DateTime()), 0))
                throw new Exception("Unable to set the alarm panel Date-Time!");




            log("Programming basic settings...");
            h.setAlarmPanelState(apId,"AP_STATE_PROGRAM","Programming basic settings...");
            boolean saveCodes=false;
            if (installerCode==null)
            {
                installerCode = h.getRandom(6,3);
                saveCodes=true;
            }
            if (masterCode==null)
            {
                masterCode = h.getRandom(6,3);
                saveCodes=true;
            }
            if (saveCodes)
            {
                alarmPanelRec.set("installerCode", installerCode);
                alarmPanelRec.set("masterCode", masterCode);
                h.db.executeUpdate("update alarmpanels set installerCode=?, masterCode=? where alarmPanelId=?"
                        , Utility.toList(installerCode, masterCode, apId));
            }
            programInstallerCodes();
            programBasic();


            // Programming the TLINK - TL250
            log("Programming the TL250...");
            h.setAlarmPanelState(apId,"AP_STATE_PROGRAM","Programming the TL250...");
            programTL250();



            log("Programming the user codes...[Preparing]");
            h.setAlarmPanelState(apId,"AP_STATE_PROGRAM","Programming the user codes...[Preparing]");
            List<GenericRecord> userList = h.db.selectAll("select * from users where userId in "
                    + "(select userId from usergroupassociations where userGroupId='99998')");
            List<String> userCodes = new ArrayList<String>(100);
            userCodes.add(emergencyDisarmCode);
            for(GenericRecord user:userList)
            {
                if (user.isNull("disarmCode"))
                {
                    user.set("disarmCode",h.getRandom(6,3));
                    h.db.update(user);
                }
                userCodes.add(user.getString("disarmCode"));
            }
            int maxUserCount = alarmPanelRec.getInteger("userLimit");
            programUserCodes(apId, h, userCodes, maxUserCount);


            log("Programming the zones...[Preparing]");
            h.setAlarmPanelState(apId,"AP_STATE_PROGRAM","Programming the zones...[Preparing]");

            //Map<String,Long> sensorAlarmLookup = new HashMap<String, Long>();
            Map<String,String> sensorAlarmCodeLookup = new HashMap<String, String>();

            List<GenericRecord> sensorConds = h.db.selectAll("select * from sensoralarmconditions");
            for(GenericRecord cond: sensorConds)
            {
                String key = cond.getString("sensorType")+"_"+cond.getString("alarmCondition");
                //sensorAlarmLookup.put(key,cond.getLong("alarmId"));
                sensorAlarmCodeLookup.put(key,
                        StringUtil.fixedLengthString(cond.getInteger("alarmCode").toString(),2,'0',true));
            }

            List<GenericRecord> zones = h.db.selectAll("select * from sensors where active=1");
            boolean[] zoneFlags = new boolean[64];
            Map<Integer,String> zoneLabel = new HashMap<Integer, String>();
            Map<Integer,String> zoneAlarmCodes = new HashMap<Integer, String>();
            for(GenericRecord zone:zones)
            {
                int zoneId = zone.getInteger("zoneNumber");
                zoneLabel.put(zoneId,zone.getString("sensorName"));
                zoneAlarmCodes.put(zoneId,sensorAlarmCodeLookup.get(zone.getString("sensorType")
                        +"_"+zone.getString("alarmCondition")));
                zoneFlags[zoneId-1]=true;
            }

            log("Programming the zones...[Activating Zones]");
            h.setAlarmPanelState(apId, "AP_STATE_PROGRAM", "Programming the zones...[Activating Zones]");
            activateZones(zoneFlags);
            log("Programming the zones...[Zones Alarm Codes]");
            h.setAlarmPanelState(apId, "AP_STATE_PROGRAM", "Programming the zones...[Zone Alarm Codes]");
            programZoneAlarmCodes(zoneAlarmCodes);
            programZoneLabels(apId, h, zoneLabel);
            discoverModules();


            h.setAlarmPanelState(apId,"AP_STATE_EMAIL","Preparing to send Email...");
        } catch (Exception e) {
            logError("Failed to program the alarm panel successfully!");
            log(e);
            if (h!=null) h.setAlarmPanelState(apId,"AP_STATE_ERROR",e.getMessage());
        }
    }


    public void installerEntry() throws Exception
    {
        display=null;
        sendKeypad('*');
        waitForDisplay();
        display=null;
        sendKeypad('8');
        waitForDisplay("Installers Code");
        display=null;
        sendKeypad(installerCode);
        waitForDisplay("Enter Section");
        Thread.sleep(500);
        int ind = display.lastIndexOf("Enter Section");
        if (ind>=0 && !display.substring(ind).contains("---"))
        {
            display=null;
            sendKeypad('#');
            waitForDisplay("Enter Section");
        }
    }
    public void installerEntryWithFail() throws Exception
    {
        display=null;
        sendKeypad('*');
        waitForDisplay();
        display=null;
        sendKeypad('8');
        waitForDisplay("Installers Code");
        display=null;
        sendKeypad(installerCode);
        waitForDisplay("Enter Section", "Invalid Access");
    }
    public void installerEntryDefault() throws Exception
    {
        display=null;
        sendKeypad('*');
        waitForDisplay();
        display=null;
        sendKeypad('8');
        waitForDisplay("Installers Code");
        display=null;
        sendKeypad(installerCodeDefault6);
        waitForDisplay("Enter Section", "Invalid Access");
    }
    public void installerEntryDefault4() throws Exception
    {
        display=null;
        sendKeypad('*');
        waitForDisplay();
        display=null;
        sendKeypad('8');
        waitForDisplay("Installers Code");
        display=null;
        sendKeypad(installerCodeDefault4);
        waitForDisplay("Enter Section", "Invalid Access");
    }

    public void discoverModules() throws Exception
    {
        try {
            goToMainScreen();
        } catch (Exception e) {
            goToMainScreen();
        }
        installerEntry();
        display=null;
        sendKeypad("902");
        waitForDisplay("Enter Section");
        try {
            goToMainScreen();
        } catch (Exception e) {
            goToMainScreen();
        }
    }
    public void programTL250() throws Exception
    {
        if (alarmPanelRec.getBoolean("tlinkEnabled")) {
            String tlinkIp = alarmPanelRec.getString("ipAddress");
            if (StringUtil.empty(tlinkIp)) throw new Exception("TLink TL250 ipAddress is not assigned!");
            String tlinkSubnet = alarmPanelRec.getString("subnetMask");
            if (StringUtil.empty(tlinkSubnet)) throw new Exception("TLink TL250 Subnet is not assigned!");
            String account = alarmPanelRec.getString("monitoringAccountNum");
            String monitoringIpAddress = alarmPanelRec.getString("monitoringIpAddress");
            if (StringUtil.empty(monitoringIpAddress))
                throw new Exception("Monitoring Company ipAddress is not assigned!");
            String monitoringGateway = alarmPanelRec.getString("monitoringGateway");
            if (StringUtil.empty(monitoringGateway))
                throw new Exception("Monitoring Company Gateway is not assigned!");

            try {
                goToMainScreen();
            } catch (Exception e) {
                goToMainScreen();
            }
            installerEntry();

            display=null;
            sendKeypad(SETUP_COMM_LINK_FUNCTION);
            waitForDisplay("Toggle Option");
            if (!display.contains("5")) sendKeypad('5');
            sendKeypad('#');
            waitForDisplay("Enter Section");

            display=null;
            sendKeypad(TLINK_CONFIG_FUNCTION);
            waitForDisplay("Enter Section");

            display=null;
            sendKeypad(TLINK_IP);
            waitForDisplay("Enter Data");
            display=null;
            sendKeypad(ip(tlinkIp));
            waitForDisplay("Enter Section");

            display=null;
            sendKeypad(TLINK_SUBNET);
            waitForDisplay("Enter Data");
            display=null;
            sendKeypad(ip(tlinkSubnet));
            waitForDisplay("Enter Section");

            display=null;
            sendKeypad(TLINK_ACCOUNT);
            waitForDisplay("Enter Hex Data");
            display=null;
            sendKeypad(StringUtil.fixedLengthString(account,10,'0',true));
            waitForDisplay("Enter Section");

            display=null;
            sendKeypad(TLINK_MON_IP);
            waitForDisplay("Enter Data");
            display=null;
            sendKeypad(ip(monitoringIpAddress));
            waitForDisplay("Enter Section");

            display=null;
            sendKeypad(TLINK_MON_GATEWAY);
            waitForDisplay("Enter Data");
            display=null;
            sendKeypad(ip(monitoringGateway));
            waitForDisplay("Enter Section");

            display=null;
            sendKeypad(TLINK_SUPER);
            waitForDisplay("Enter Hex Data");
            display=null;
            sendKeypad("01");
            waitForDisplay("Enter Section");

            display=null;
            sendKeypad('#');
            //sendKeypad("999");
            //waitForDisplay("Enter Data");
            //sendKeypad("55");

        }
        else log("Skipping configuration of the tlink, since it is not enabled!");
    }

    private String ip(String ip)
    {
        String[] ipSplit=ip.split("[.]");
        for(int i=0;i<4;i++)
            ipSplit[i]=StringUtil.fixedLengthString(ipSplit[i],3,'0',true);
        return ipSplit[0]+ipSplit[1]+ipSplit[2]+ipSplit[3];
    }
    private void sendLabelChar(char c) throws Exception
    {
        if (c=='A') sendKeypad("1");
        else if(c=='B') sendKeypad("11");
        else if(c=='C') sendKeypad("111");
        else if(c=='D') sendKeypad("2");
        else if(c=='E') sendKeypad("22");
        else if(c=='F') sendKeypad("222");
        else if(c=='G') sendKeypad("3");
        else if(c=='H') sendKeypad("33");
        else if(c=='I') sendKeypad("333");
        else if(c=='J') sendKeypad("4");
        else if(c=='K') sendKeypad("44");
        else if(c=='L') sendKeypad("444");
        else if(c=='M') sendKeypad("5");
        else if(c=='N') sendKeypad("55");
        else if(c=='O') sendKeypad("555");
        else if(c=='P') sendKeypad("6");
        else if(c=='Q') sendKeypad("66");
        else if(c=='R') sendKeypad("666");
        else if(c=='S') sendKeypad("7");
        else if(c=='T') sendKeypad("77");
        else if(c=='U') sendKeypad("777");
        else if(c=='V') sendKeypad("8");
        else if(c=='W') sendKeypad("88");
        else if(c=='X') sendKeypad("888");
        else if(c=='Y') sendKeypad("9");
        else if(c=='Z') sendKeypad("99");
        else if(c==' ') sendKeypad("0");
        else if(c=='1') sendKeypad("1111");
        else if(c=='2') sendKeypad("2222");
        else if(c=='3') sendKeypad("3333");
        else if(c=='4') sendKeypad("4444");
        else if(c=='5') sendKeypad("5555");
        else if(c=='6') sendKeypad("6666");
        else if(c=='7') sendKeypad("7777");
        else if(c=='8') sendKeypad("8888");
        else if(c=='9') sendKeypad("999");
        else if(c=='0') sendKeypad("9999");
        else if(c=='*') sendKeypad("*>>*039*");
        else if(c==',') sendKeypad("*>>*044*");
        else if(c=='.') sendKeypad("*>>*046*");
        else if(c=='-') sendKeypad("*>>*045*");
        else if(c=='\'') sendKeypad("*>>*039*");
        else return;
        sendKeypad('>');
    }

    public void programZoneLabels(long apId, IpssDbHelper h, Map<Integer,String> zoneLabels) throws Exception
    {
        display=null;
        sendKeypad('*');
        waitForDisplay("Enter LCD");
        int i=1;
        for(Map.Entry z:zoneLabels.entrySet())
        {
            log("Programming the zone labels...["+Integer.toString(i)+" of " + zoneLabels.size()+"]");
            h.setAlarmPanelState(apId,"AP_STATE_PROGRAM"
                    ,"Programming the zone labels...["+Integer.toString(i)+" of " + zoneLabels.size()+"]");
            String zoneId = StringUtil.fixedLengthString(z.getKey().toString(),3,'0',true);
            sendKeypad(zoneId);
            String label = (String)z.getValue();
            label = label.toUpperCase();
            if (label.length()>MAX_LABEL_SIZE) label = label.substring(0,MAX_LABEL_SIZE);
            for(char c: label.toCharArray()) {
                display=null;
                sendLabelChar(c);
            }
            display=null;
            sendKeypad("**");
            waitForDisplay("Enter LCD");
        }
    }


    public void activateZones(boolean[] zoneFlags) throws Exception
    {
        try {
            goToMainScreen();
        } catch (Exception e) {
            goToMainScreen();
        }
        installerEntry();
        for (int bank=0;bank<8;bank++) {
            String zoneFunction = Integer.toString(ZONE_TOGGLE_FUNCTION_BASE + bank);
            display=null;
            sendKeypad(zoneFunction);
            waitForDisplay("Toggle Option");

            for (int offset = 1; offset <= 8; offset++) {
                String zoneKey = Integer.toString(offset);
                int zoneIndex = bank * 8 + offset -1;
                if (zoneFlags[zoneIndex] && !display.contains(zoneKey))
                {
                    display=null;
                    sendKeypad(zoneKey);
                    waitForDisplay();
                }
                else if (!zoneFlags[zoneIndex] && display.contains(zoneKey))
                {
                    display=null;
                    sendKeypad(zoneKey);
                    waitForDisplay();
                }
            }
            display=null;
            sendKeypad("#");
            waitForDisplay("Enter Section");
        }
    }

    public void programZoneAlarmCodes(Map<Integer,String> zoneAlarmCodes) throws Exception
    {
        for(int bank=0;bank<4;bank++) {
            String zoneSegment = StringUtil.fixedLengthString(
                    Integer.toString(ZONE_DEFINITION_FUNCTION_BASE + bank),3,'0',true);
            display=null;
            sendKeypad(zoneSegment);
            waitForDisplay("Enter Data");
            for (int offset = 1; offset <= 16; offset++) {
                //waitForDisplay(StringUtil.fixedLengthString(Integer.toString(offset),2,'0',true));
                int zoneId = bank * 16 + offset;
                String code = "00";
                if (zoneAlarmCodes.containsKey(zoneId)) code = zoneAlarmCodes.get(zoneId);
                display=null;
                sendKeypad(code);
                Thread.sleep(500);
            }
            waitForDisplay("Enter Section");
        }
    }


    public void programUserCodes(long apId, IpssDbHelper h, List<String> userCodes, int maxUserCount) throws Exception
    {
        try {
            goToMainScreen();
        } catch (Exception e) {
            goToMainScreen();
        }
        display=null;
        sendKeypad('*');
        waitForDisplay();
        display=null;
        sendKeypad('5');
        //sendKeypad(USER_ENTRY_KEYS);
        waitForDisplay("Enter Master");
        display=null;
        sendKeypad(masterCode);
        waitForDisplay("User Code");
        int i = 1;
        for(String userCode:userCodes)
        {
            log("Programming the user codes...["+Integer.toString(i)+" of " + userCodes.size()+"]");
            h.setAlarmPanelState(apId,"AP_STATE_PROGRAM"
                    ,"Programming the user codes...["+Integer.toString(i)+" of " + userCodes.size()+"]");
            if (i==40) i++;
            display=null;
            sendKeypad(StringUtil.fixedLengthString(Integer.toString(i),2,'0',true));
            waitForDisplay("Enter New Code");
            display=null;
            sendKeypad(userCode);
            //sendKeypad("*");
            waitForDisplay("User Code");

            i++;
            if (i>=maxUserCount) break;
        }
        sendKeypad('#');
    }


    public void programInstallerCodes() throws Exception {
        // Disable all zones first...

        // Programming the new installer Code...
        try {
            goToMainScreen();
        } catch (Exception e) {
            goToMainScreen();
        }

        installerEntryDefault4();
        display=null;
        sendKeypad(PASSWORD6_TOGGLE_FUNCTION);
        waitForDisplay("Toggle Option");
        display=null;
        sendKeypad(PASSWORD6_OPTION);
        waitForDisplay(PASSWORD6_OPTION);
        display=null;
        sendKeypad('#');
        waitForDisplay("Enter Section");


        display = null;
        sendKeypad(CHANGE_INSTALLER_CODE_FUNCTION);
        waitForDisplay("Enter Data");
        display=null;
        sendKeypad(installerCode);
        waitForDisplay("Enter Section");

        // Programming the new master Code...
        goToMainScreen();
        installerEntry();
        display = null;
        sendKeypad(CHANGE_MASTER_CODE_FUNCTION);
        waitForDisplay("Enter Data");
        display=null;
        sendKeypad(masterCode);
        waitForDisplay("Enter Section");
    }


    public void programBasic() throws Exception
    {

        // Setup Partitions
        display=null;
        sendKeypad(PARTITION_SETUP_FUNCTION);
        waitForDisplay("Toggle Option");
        if (!display.contains("1")) sendKeypad('1');
        for (int i=2;i<=8;i++)
        {
            char p = (char)(48+i);
            if (display.indexOf(p)>0)
            {
                display=null;
                sendKeypad(p);
                waitForDisplay();
            }
        }
        display=null;
        sendKeypad('#');
        waitForDisplay("Enter Section");


        // Program the system Account number...
        String sysAccount = alarmPanelRec.getString("monitoringAccountNum");
        if (sysAccount==null)
            throw new Exception("IP360 system is not properly registered. Missing Account# for monitoring company!");
        else if (sysAccount.length()>6) throw new Exception("Invalid Account# for monitoring company!");
        display=null;
        sendKeypad(SYSTEM_ACCOUNT_FUNCTION);
        waitForDisplay("Enter Hex Data");
        display=null;
        sendKeypad(StringUtil.fixedLengthString(sysAccount,6,'0',true));
        waitForDisplay("Enter Section");

        display=null;
        sendKeypad(PARTITION_ACCOUNT_FUNCTION);
        display=null;
        waitForDisplay("Enter Hex Data");
        display=null;
        sendKeypad(StringUtil.fixedLengthString(sysAccount,4,'0',true));
        waitForDisplay("Enter Section");



        // Setup the Phone line...
        //String phone = alarmPanelRec.getString("monitoringPhoneNum");
        //for (int i=0;i<phone.length();i++) if (!Character.isDigit(phone.charAt(i)))
        //    throw  new Exception("IP360 has an invalid phone number for the monitoring company!");
        //if (phone!=null && phone.length()>6) {
        display=null;
        sendKeypad(PHONE_FUNCTION1);
        waitForDisplay("FFFFFF");
        display=null;
        sendKeypad(TLINK_PHONE);
        //sendKeypad("*6#");
        display=null;
        sendKeypad("#");
        waitForDisplay("Enter Section");
        display=null;
        sendKeypad(PHONE_FUNCTION2);
        waitForDisplay("FFFFFF");
        sendKeypad(TLINK_PHONE);
        display=null;
        sendKeypad("*6#");
        waitForDisplay("Enter Section");
        //}


        // Setup the COMM Link
        display=null;
        sendKeypad(SETUP_COMM_LINK_FUNCTION);
        waitForDisplay("Toggle Option");
        if (!display.contains("5")) {
            display=null;
            sendKeypad('5');
            waitForDisplay();
        }
        if (display.contains("7")) sendKeypad('7');
        sendKeypad('#');
        waitForDisplay("Enter Section");


        // Test Transmission Time
        display=null;
        sendKeypad(TEST_TX_ENABLE_FUNCTION);
        waitForDisplay("Toggle Option");
        if (!display.contains("1")) sendKeypad('1');
        display=null;
        sendKeypad('#');
        waitForDisplay("Enter Section");
        display=null;
        sendKeypad(TEST_TX_TIME_FUNCTION);
        waitForDisplay("Enter Data");
        display=null;
        sendKeypad(TEST_TX_HOUR);
        waitForDisplay("Enter Section");


        // Program the Entry Exit Delay
        String entryDelay1 = StringUtil.fixedLengthString(alarmPanelRec.getInteger("entryDelay1").toString(),3,'0',true);
        String entryDelay2 = StringUtil.fixedLengthString(alarmPanelRec.getInteger("entryDelay2").toString(),3,'0',true);
        String exitDelay = StringUtil.fixedLengthString(alarmPanelRec.getInteger("exitDelay").toString(),3,'0',true);
        display=null;
        sendKeypad(ENTRY_EXIT_FUNCTION);
        waitForDisplay("Enter Section");
        display=null;
        sendKeypad(PARTITION_ID);
        waitForDisplay("Enter Data");
        display=null;
        sendKeypad(entryDelay1);
        waitForDisplay("Enter Data");
        display=null;
        sendKeypad(entryDelay2);
        waitForDisplay("Enter Data");
        display=null;
        sendKeypad(exitDelay);
        waitForDisplay("Enter Section");
        display=null;
        sendKeypad('#');
        waitForDisplay("Enter Section");




    }


    public void executeFactoryReset()  throws Exception
    {
        installerEntryDefault();
        String currentPassword = null;

        if (display.contains("Invalid Access"))
        {
            if (installerCode==null)
                throw new Exception("IT100 installer password not know, please do a manual factory reset!");
            try {
                goToMainScreen();
            } catch (Exception e) {
                goToMainScreen();
            }
            installerEntryWithFail();
            if (display.contains("Invalid Access"))
                throw new Exception("IT100 installer password not know, please do a manual factory reset!");
            currentPassword=installerCode;
        }
        else
        {
            Thread.sleep(1000);
            if (display.contains("5--") || display.contains("55-"))
            {
                currentPassword = installerCodeDefault4;
                display=null;
                sendKeypad("#");
                waitForDisplay("Enter Section");
            }
            else currentPassword = installerCodeDefault6;
        }
        Thread.sleep(500);
        int ind = display.lastIndexOf("Enter Section");
        if (ind>=0 && !display.substring(ind).contains("---"))
        {
            display=null;
            sendKeypad('#');
            waitForDisplay("Enter Section");
        }
        display=null;
        sendKeypad("999");
        waitForDisplay("Installers Code");
        display=null;
        sendKeypad(currentPassword);
        waitForDisplay("Enter Section");
        Thread.sleep(1000);
        int ind2 = display.lastIndexOf("Enter Section");
        if (ind2>=0 && !display.substring(ind2).contains("---"))
        {
            display=null;
            sendKeypad('#');
            waitForDisplay("Enter Section");
        }
        display=null;
        sendKeypad("999");
        //sendKeypad(END_KEYS);
        waitForDisplay("Ready to Arm", "Secure System");
    }




    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getError() {
        return lastErorr;
    }
    public String getErrorCode()
    {
        return lastErrorCode;
    }

    public AlarmPanelState getAlarmPanelState() {
        return state;
    }

    public AlarmPanelState getPartitionState(int partition)
    {
        if (partition<1 || partition>8) return AlarmPanelState.OFFLINE;
        else if (partitionStates.containsKey(partition)) return partitionStates.get(partition);
        else return AlarmPanelState.OFFLINE;
    }

    public AlarmPanelZoneState getZoneStatus(int zoneInd) {
        if (zoneInd<1 || zoneInd>64) return AlarmPanelZoneState.OUT_OF_RANGE;
        else return zones[zoneInd];
    }


    public AlarmPanelZoneState[] getZones()
    {
        AlarmPanelZoneState[] newZones = zones.clone();
        return newZones;
    }

    public int getLastZoneChanged() {
        return lastZoneChanged;
    }


    public int getLastPartitionChanged() {
        return lastPartitionChanged;
    }

    public String getConnectionAddress() {
        return port;
    }

    public String getHardwareId() {
        return portHardwarePath;
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode=debugMode;
    }

    @Override
    public String toString()
    {
        return "[" + id+": " + name + " - IT100]";
    }

    protected void fireEvent(AlarmPanelEventType eType)
    {
        if (eventHandlers!=null)
            for(AlarmPanelListener e:eventHandlers) e.onAlarmPanelEvent(this, eType);
    }
    protected void fireChangeState(AlarmPanelState newState)
    {
        if (state==newState) return;
        state = newState;
        partitionStates.put(1,state);
        fireEvent(AlarmPanelEventType.STATUS_CHANGE);
    }
    protected void fireChangePartitionState(int partition, AlarmPanelState newPartitionState)
    {
        if (!partitionStates.containsKey(partition)) return;
        AlarmPanelState oldPartitionState = partitionStates.get(partition);
        if (oldPartitionState==newPartitionState) return;
        partitionStates.put(partition,newPartitionState);
        lastPartitionChanged=partition;
        fireEvent(AlarmPanelEventType.PARTITION);
    }
    protected void fireError(String errorCode, String errorMessage)
    {
        lastErrorCode=errorCode;
        lastErorr=errorMessage;
        fireEvent(AlarmPanelEventType.ERROR);
    }
    protected void fireZone(int zoneInd, AlarmPanelZoneState newState)
    {
        if (zoneInd>=1 && zoneInd<=64)
        {
            if (zones[zoneInd]==newState) return; // No need to fire, same state...
            zones[zoneInd] = newState;
            lastZoneChanged = zoneInd;
            if (newState==AlarmPanelZoneState.OFF) fireEvent(AlarmPanelEventType.ZONE_RESTORE);
            else if (newState==AlarmPanelZoneState.ALARM) fireEvent(AlarmPanelEventType.ZONE_ALARM);
            else fireEvent(AlarmPanelEventType.ZONE_TRIGGER);
        }
    }

    public void onPortData(PortDataEvent evt) {
        try
        {
            if (evt.getEventType()==PortDataEvent.PortDataEventType.WRITE_COMPLETE)
            {
                // For Now Do Nothing....
            }
            else if (evt.getEventType()==PortDataEvent.PortDataEventType.DATA_RECEIVED)
            {
                List inputPacked = new LinkedList();
                inputPacked.add(evt.getValue());
                partialBuffer = (ByteBuffer)inputFilter.process(partialBuffer,inputPacked);
                List processedPackets = inputFilter.getMaturedObjects();
                if (processedPackets!=null)
                {
                    for(Object p:processedPackets)
                    {
                        IT100BaseCommand cmd = (IT100BaseCommand)p;
                        if (debugMode)
                        {
                            log("IT100.rx << " + cmd.getCommand()+" : " + cmd.getData());
                        }
                        if (cmd.isAckPacket() || cmd.isCommandErrorPacket())
                            synchronized (commandWaiter)
                            {
                                lastAck=cmd;
                                commandWaiter.notifyAll();
                            }
                        else if (IT100ResponseZone.isCompatible(cmd))
                        {
                            IT100ResponseZone zr = new IT100ResponseZone(cmd);
                            if (zr.isAlarm()) fireZone(zr.getZone(),AlarmPanelZoneState.ALARM);
                            else if (zr.isTriggered()) fireZone(zr.getZone(),AlarmPanelZoneState.ON);
                            else if (zr.isRestored()) fireZone(zr.getZone(),AlarmPanelZoneState.OFF);
                        }
                        else if (IT100ResponsePartition.isCompatible(cmd))
                        {
                            IT100ResponsePartition pr = new IT100ResponsePartition(cmd);
                            if (pr.getPartition()>1)
                            {
                                // For other partition log the event into the event table
                                AlarmPanelState pstate = null;
                                if (pr.isReady() || pr.isReadyToForceArm()) pstate = AlarmPanelState.READY;
                                else if (pr.isNotReady()) pstate = AlarmPanelState.BUSY;
                                else if (pr.isArmed()) pstate = AlarmPanelState.ARMED;
                                else if (pr.isAlarmed()) pstate = AlarmPanelState.ALARM;
                                else if (pr.isDisarmed()) pstate = AlarmPanelState.READY;
                                else if (pr.isBusy()) pstate=AlarmPanelState.BUSY;
                                if (pstate!=null) fireChangePartitionState(pr.getPartition(),pstate);
                                continue;
                                // We only support one partition...
                                // But we still record events from other partitions...
                            }
                            if (pr.isReady() || pr.isReadyToForceArm()) fireChangeState(AlarmPanelState.READY);
                            else if (pr.isNotReady()) fireChangeState(AlarmPanelState.BUSY);
                            else if (pr.isArmed()) fireChangeState(AlarmPanelState.ARMED);
                            else if (pr.isAlarmed()) fireChangeState(AlarmPanelState.ALARM);
                            else if (pr.isDisarmed())
                            {
                                fireChangeState(AlarmPanelState.READY);
                                fireEvent(AlarmPanelEventType.DISARMED);
                            }
                            else if (pr.isExitDelay()) fireChangeState(AlarmPanelState.EXIT_DELAY);
                            else if (pr.isEntryDelay()) fireChangeState(AlarmPanelState.ENTRY_DELAY);
                            else if (pr.isKeypadLockedOut()) fireEvent(AlarmPanelEventType.KEYPAD_LOCKOUT);
                            else if (pr.isInvalidAccessCode()) fireEvent(AlarmPanelEventType.INVALID_CODE);
                            else if (pr.isFailedToArm()) fireEvent(AlarmPanelEventType.FAILED_ARM);
                            else if (pr.isBusy()) fireChangeState(AlarmPanelState.BUSY);
                        }
                        else
                        {
                            //if (cmd.getCommand().equals("904"))
                            //{
                            //    beep = cmd.getData();
                            //}
                            //else
                            if (cmd.getCommand().equals("901"))
                            {
                                String data = cmd.getData();
                                if (data.contains("Toggle Option"))
                                    display=data.substring(5);
                                else {
                                    if (display == null) display = data.substring(5);
                                    else {
                                        if (display.length() > 500) display = display.substring(0, 100);
                                        display = display + data.substring(5);
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
        catch (Exception ex)
        {
            log(ex);
            fireError(null,"Error: Failed to process incoming event. " + ex.getMessage());
        }


    }

    public void close()
    {
        disconnect();
    }




    public void addEventHandler(AlarmPanelListener eventHandler)
    {
        try {
            synchronized (eventHandlers)
            {
                eventHandlers.add(eventHandler);
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to attach event handler to " + this.toString());
        }
    }

    public void removeEventHandler(AlarmPanelListener eventHandler)
    {
        try {
            synchronized (eventHandlers)
            {
                eventHandlers.remove(eventHandler);
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to remove event handler from " + this.toString());
        }
    }

    public void removeAllEventHandlers()
    {
        try {
            synchronized (eventHandlers)
            {
                eventHandlers.clear();
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to remove event handlers from " + this.toString());
        }
    }


    public void setBusy(boolean isBusy)
    {
        busy = isBusy;
    }

    public boolean isBusy() {return busy;}



}
