package ipss.core.dsc.it100.responses;

import ipss.core.dsc.it100.commands.IT100BaseCommand;
import ipss.core.isonas.commands.SetStandaloneParamCommand;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/4/12
 * Time: 5:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class IT100ResponseZone extends IT100BaseCommand {
    private static HashSet<String> acceptableCmd = new HashSet<String>(Arrays.asList("601","602","603","604","605","606","609","610"));
    int partition=-1;
    int zone=-1;

    public IT100ResponseZone(IT100BaseCommand baseCmd)
    {
        cmd = baseCmd.getCommand();
        data = baseCmd.getData();
        checkValid();
        if (valid)
        {
            if (data!=null && data.length()==4)
            {
                partition = (int)data.charAt(0) - 32;
                zone = Integer.parseInt(data.substring(1));
            }
            else if (data!=null && data.length()==3)
            {
                zone = Integer.parseInt(data);
            }
        }
    }

    @Override
    protected void checkValid()
    {
        super.checkValid();
        if (valid)
        {
            if (!isCompatible(cmd,data)) valid=false;
        }
    }

    public static boolean isCompatible(IT100BaseCommand baseCmd)
    {
        return isCompatible(baseCmd.getCommand(),baseCmd.getData());
    }
    
    private static boolean isCompatible(String cmd, String data)
    {
        if (data==null) return false;
        if (data.length()!=4 && data.length()!=3) return false;
        if (!acceptableCmd.contains(cmd)) return false;

        if (data.length()==4)
        {
            char c0 = data.charAt(0);
            if (c0<'0' || c0>'8') return false;
        }
        try {
            int z = Integer.parseInt((data.length()==4?data.substring(1):data));
            if (z>0 && z<=64) return true;
        } catch (NumberFormatException e) {
        }
        return false;
    }

    public boolean isAlarm(){return "601".equals(cmd);}
    public boolean isAlarmRestored(){return "602".equals(cmd);}
    public boolean isTamper(){return "603".equals(cmd);}
    public boolean isTamperRestored(){return "604".equals(cmd);}
    public boolean isFault(){return "605".equals(cmd);}
    public boolean isFaultRestored(){return "606".equals(cmd);}
    public boolean isTriggered(){return "609".equals(cmd);}
    public boolean isRestored(){return "610".equals(cmd);}

    public int getPartition() {
        return partition;
    }

    public int getZone() {
        return zone;
    }
}
