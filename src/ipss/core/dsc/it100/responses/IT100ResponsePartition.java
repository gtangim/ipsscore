package ipss.core.dsc.it100.responses;

import ipss.core.dsc.it100.commands.IT100BaseCommand;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/4/12
 * Time: 6:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class IT100ResponsePartition extends IT100BaseCommand {
    private static HashSet<String> acceptableCmd = new HashSet<String>(Arrays.asList("626", "650", "651", "652", "653", "654", "655", "656", "657", "658", "659", "660", "670","671","672","673","700","701","702","750","751"));
    int partition=0;
    int code=0;

    public IT100ResponsePartition(IT100BaseCommand baseCmd)
    {
        cmd = baseCmd.getCommand();
        data = baseCmd.getData();
        checkValid();
        if (valid)
        {
            partition = (int)data.charAt(0) - 48;
            if (data.length()>1) code = Integer.parseInt(data.substring(1));
        }
    }

    @Override
    protected void checkValid()
    {
        super.checkValid();
        if (valid)
        {
            if (!isCompatible(cmd,data)) valid=false;
        }
    }

    public static boolean isCompatible(IT100BaseCommand baseCmd)
    {
        return isCompatible(baseCmd.getCommand(),baseCmd.getData());
    }

    private static boolean isCompatible(String cmd, String data)
    {
        if (data==null) return false;
        if (data.length()<1 || data.length()>5) return false;
        if (!acceptableCmd.contains(cmd)) return false;
        char c0 = data.charAt(0);
        if (c0<'0' || c0>'8') return false;
        if (data.length()>1)
        {
            try {
                int z = Integer.parseInt(data.substring(1));
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
        else return true;
    }

    public int getPartition() {
        return partition;
    }

    public int getCode() {
        return code;
    }

    public boolean isReady(){return "626".equals(cmd) || "650".equals(cmd);}
    public boolean isNotReady(){return "651".equals(cmd);}
    public boolean isArmed(){return "652".equals(cmd);}
    public boolean isReadyToForceArm(){return "653".equals(cmd);}
    public boolean isAlarmed(){return "654".equals(cmd);}
    public boolean isDisarmed(){return "655".equals(cmd);}
    public boolean isExitDelay(){return "656".equals(cmd);}
    public boolean isEntryDelay(){return "657".equals(cmd);}
    public boolean isKeypadLockedOut(){return "658".equals(cmd);}
    public boolean isKeypadBlanked(){return "659".equals(cmd);}
    public boolean isCommandOutProgress(){return "660".equals(cmd);}
    public boolean isInvalidAccessCode(){return "670".equals(cmd);}
    public boolean isFuncNotAvailable(){return "671".equals(cmd);}
    public boolean isFailedToArm(){return "672".equals(cmd);}
    public boolean isBusy(){return "673".equals(cmd);}
    public boolean isUserClosed(){return "700".equals(cmd);}
    public boolean isSpecialClosed(){return "701".equals(cmd);}
    public boolean isPartiallyClosed(){return "702".equals(cmd);}
    public boolean isUserOpened(){return "750".equals(cmd);}
    public boolean isSpecialOpened(){return "751".equals(cmd);}

}
