package ipss.core.dsc.it100;

import apu.net.IDataFilter;
import apu.net.PacketizedCommand;
import apu.util.StringUtil;
import apu.util.Utility;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/1/12
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class IT100CommandPacketizer implements IDataFilter{
    private ByteBuffer result = ByteBuffer.allocate(8192);
    private int n;

    public Object process(Object partialDataContainer, List inputData) throws Exception {
        result.clear();
        for(Object x:inputData)
        {
            ByteBuffer source = ((PacketizedCommand)x).getBinary();
            int m = source.position();
            source.rewind();
            int checksum=0;
            for(int i=0;i<m;i++)
            {
                byte b = source.get();
                result.put((byte)b);
                checksum+=((int)b) & 0xff;
            }
            result.put(StringUtil.toHex(checksum,2).getBytes());
        }
        result.put((byte)0x0D);
        result.put((byte)0x0A);
        n=result.position();
        return null;
    }

    public List getMaturedObjects() {
        List ret = new LinkedList();
        result.rewind();
        byte[] target = new byte[n];
        result.get(target);
        ret.add(target);
        return ret;
    }
}
