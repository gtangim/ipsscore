package ipss.core.dsc.it100;

import apu.net.IDataFilter;
import apu.util.StringUtil;
import ipss.core.dsc.it100.commands.IT100BaseCommand;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/1/12
 * Time: 3:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class IT100ResponseDecompiler implements IDataFilter {
    public List packets = null;

    public Object process(Object partialDataContainer, List inputData) throws Exception {
        packets = null;
        
        ByteBuffer dataContainer = (ByteBuffer)partialDataContainer;
        if (dataContainer==null) dataContainer = ByteBuffer.allocate(8192);
        for(Object x: inputData)
        {
            dataContainer.put((byte[])x);
        }
        int n = dataContainer.position();
        byte[] b = dataContainer.array();
        int pos = 0;
        while (pos<n)
        {
            int ind = findTail(b,n,pos);
            if (ind==-1) break;
            // At this point we found a packet, check for validation....
            if (ind-pos<7) {pos=ind;continue;}
            byte[] checksum = calculateCheckSum(b,pos,ind-4);
            if (checksum[0]!=b[ind-4] || checksum[1]!=b[ind-3]) {pos=ind;continue;}
            // A valid packet is detected here, extract it and create a packet object...
            if (packets==null) packets=new LinkedList();
            packets.add(new IT100BaseCommand(b,pos,ind-4));
            pos = ind;
        }

        if (pos==n) return null; // All data processed no need to keep partial buffer...
        else
        {
            dataContainer.rewind();
            dataContainer.put(b,pos,n-pos);
            return dataContainer;
        }
    }

    public List getMaturedObjects() {
        return packets;
    }
    
    private int findTail(byte[] buf, int length, int startIndex)
    {
        int i=startIndex;
        while(i<=length-2)
        {
            if (buf[i]==0x0D && buf[i+1]==0x0A) return i+2;
            i++;
        }
        return -1;
    }
    
    private byte[] calculateCheckSum(byte[] buf, int start, int end)
    {
        int checksum = 0;
        for(int i=start;i<end;i++)
        {
            checksum += buf[i];
        }
        byte csb = (byte)(checksum & 0xff);
        return StringUtil.stringToByte(StringUtil.toHex(csb,2));
    }
}
