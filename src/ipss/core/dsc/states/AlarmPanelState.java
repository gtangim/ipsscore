package ipss.core.dsc.states;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/4/12
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */
public enum AlarmPanelState {
    READY, BUSY, ARMED, ALARM, ENTRY_DELAY, EXIT_DELAY, OFFLINE
}
