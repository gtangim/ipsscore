package ipss.core.dsc.states;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/4/12
 * Time: 6:33 PM
 * To change this template use File | Settings | File Templates.
 */
public enum AlarmPanelEventType {
    STATUS_CHANGE, INVALID_CODE, ERROR, ZONE_ALARM, ZONE_TRIGGER, ZONE_RESTORE, FAILED_ARM, DISARMED, KEYPAD_LOCKOUT, PARTITION, HARDWARE_PORT_CHANGED,DEVICE_OFFLINE, DEVICE_CONNECTED
}
