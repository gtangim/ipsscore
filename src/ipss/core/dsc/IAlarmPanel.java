package ipss.core.dsc;

import ipss.core.dsc.states.AlarmPanelState;
import ipss.core.dsc.states.AlarmPanelZoneState;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/1/12
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IAlarmPanel {

    public boolean arm();
    public boolean disarm();
    public boolean alarm();

    public boolean arm(int partition);
    public boolean disarm(int partition);
    public boolean alarm(int partition);

    public boolean poll();
    public void program();


    public String getId();
    public String getName();
    public String getConnectionAddress();
    public String getHardwareId();
    public String getError();
    public String getErrorCode();
    public AlarmPanelState getAlarmPanelState();
    public AlarmPanelState getPartitionState(int partition);
    public AlarmPanelZoneState getZoneStatus(int zone);
    public AlarmPanelZoneState[] getZones();
    public int getLastZoneChanged();
    public int getLastPartitionChanged();
    public boolean isBusy();
    public void close();

    public void addEventHandler(AlarmPanelListener eventHandler);
    public void removeEventHandler(AlarmPanelListener eventHandler);
    public void removeAllEventHandlers();
    public void setBusy(boolean isBusy);
    public void setDebugMode(boolean debugMode);

}
