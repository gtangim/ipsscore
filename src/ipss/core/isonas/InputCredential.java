package ipss.core.isonas;

import apu.util.Utility;
import ipss.core.isonas.responses.states.OriginStatus;
import ipss.core.isonas.states.CredentialType;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 10/4/11
 * Time: 3:45 PM
 * To change this template use File | Settings | File Templates.
 */
public final class InputCredential {
    protected long doorId;
    protected long userId;
    protected String code;
    protected CredentialType codeType;
    protected DateTime createdStamp;
    protected boolean pin = false;
    protected boolean card = false;
    protected boolean bio = false;

    public InputCredential(long doorId, long userId, String code, OriginStatus codeOrigin) throws Exception
    {
        this.doorId=doorId;
        this.userId=userId;
        createdStamp=new DateTime();
        setCredential(code,codeOrigin);
    }

    public void setCredential(String code, OriginStatus codeOrigin) throws Exception
    {
        this.code=code;
        if (codeOrigin==OriginStatus.KEYPAD) codeType=CredentialType.PIN;
        else if (codeOrigin==OriginStatus.PROX_CARD) codeType=CredentialType.CARD;
        else if (codeOrigin==OriginStatus.PROX_CARD_HID) codeType=CredentialType.CARD;
        else throw new Exception("Unsupported Input Credential Type: "+codeOrigin.toString());
        if (codeType==CredentialType.PIN) pin=true;
        else if (codeType==CredentialType.CARD) card=true;
        else if (codeType==CredentialType.BIO) bio=true;
    }


    public long getDoorId() {
        return doorId;
    }

    public long getUserId() {
        return userId;
    }

    public String getCode() {
        return code;
    }

    public CredentialType getCodeType() {
        return codeType;
    }

    public DateTime getCreatedStamp() {
        return createdStamp;
    }

    public long getElapsedMillis()
    {
        return Utility.getElapsed(createdStamp);
    }

    public boolean hasPin() {
        return pin;
    }

    public boolean hasCard() {
        return card;
    }

    public boolean hasBio() {
        return bio;
    }

    @Override
    public String toString() {
        return "User "+Long.toString(userId)+" using "+code+":"+codeType.toString();
    }
}
