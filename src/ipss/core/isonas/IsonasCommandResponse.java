package ipss.core.isonas;

import ipss.core.isonas.states.ResponseStatus;

import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 4:34 PM
 * To change this template use File | Settings | File Templates.
 */
public final class IsonasCommandResponse {
    ResponseStatus status;
    ByteBuffer response;
    byte[] responseBytes;
    int length;

    public IsonasCommandResponse()
    {
        status=ResponseStatus.INVALID;
        length=0;
        response=null;
        responseBytes=null;
    }

    public IsonasCommandResponse(ResponseStatus status)
    {
        this.status=status;
        length=0;
        response=null;
        responseBytes=null;
    }
    public IsonasCommandResponse(ByteBuffer buffer)
    {
        this.status=ResponseStatus.OK;
        response=buffer;
        if (buffer!=null) responseBytes=buffer.array();
        else responseBytes=null;
        length=buffer.position();
    }
    public IsonasCommandResponse(ResponseStatus status,ByteBuffer buffer)
    {
        this.status=status;
        response=buffer;
        if (buffer!=null) responseBytes=buffer.array();
        else responseBytes=null;
        length=buffer.position();
    }
    public int size()
    {
        return length;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public ByteBuffer getResponse() {
        return response;
    }

    public boolean isOk()
    {
        return status==ResponseStatus.OK;
    }
    public boolean isTimeout()
    {
        return status==ResponseStatus.TIMEOUT;
    }
    public boolean isInvalid()
    {
        return status==ResponseStatus.INVALID;
    }
    public boolean isClosed()
    {
        return status==ResponseStatus.CLOSED;
    }
    public boolean isErrror()
    {
        return status== ResponseStatus.IOERROR;
    }

    public boolean isAck()
    {
        if (length==1 && responseBytes!=null && responseBytes.length>0 && responseBytes[0]==0x01) return true;
        else return false;
    }
    public boolean isNak()
    {
        if (length==1 && responseBytes!=null && responseBytes.length>0 && responseBytes[0]==0x02) return true;
        else return false;
    }
    public boolean isReject()
    {
        if (length==1 && responseBytes!=null && responseBytes.length>0 && responseBytes[0]==0x03) return true;
        else return false;
    }


    private static IsonasCommandResponse ack = null;
    public static IsonasCommandResponse getAck(){
        if (ack!=null) return ack;
        else
        {
            ack = new IsonasCommandResponse(ResponseStatus.OK);
            ack.responseBytes = new byte[1];
            ack.responseBytes[0]=0x01;
            ack.length=1;
            return ack;
        }
    }
}
