package ipss.core.isonas;

import apu.db.GenericRecord;
import apu.util.Utility;
import org.joda.time.DateTime;

import javax.xml.datatype.Duration;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 10/4/11
 * Time: 4:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class IpssConfig {
    protected boolean requestReset;
    protected boolean requestReload;

    protected int relayInterval;
    protected int heldOpenInterval;
    protected int lockDownOnCount;
    protected int lockDownOffCount;
    protected int lockDownInterval;
    protected int tamperSustainInterval;
    protected int forcedOpenSustainInterval;
    protected int credentialSustainInterval;
    protected int readerPollInterval;
    protected int alarmPanelPollInterval;
    protected boolean rexEnabled;
    protected boolean lockDownEnabled;
    protected boolean tamperEnabled;
    protected boolean isRexReversed;
    protected boolean isTamperReversed;
    protected DateTime createdStamp;
    protected DateTime nextDoorProgrammingSchedule;
    
    protected String smtpServer;
    protected String emailUserId;
    protected String emailUserPassword;
    protected String emailSender;

    //protected Long deviceStartIp=null;
    //protected Long deviceEndIp=null;
    //protected Long nextIp=null;

    protected boolean scanDevices;


    public IpssConfig(GenericRecord cfg)
    {
        createdStamp=new DateTime();
        requestReset = getBoolean(cfg,"requestReset");
        //completeReset = getBooleanComplete(cfg, "requestReset");
        requestReload = getBoolean(cfg, "requestReload");
        //completeReload = getBooleanComplete(cfg, "requestReload");

        relayInterval = cfg.getInteger("relayInterval")*1000;
        heldOpenInterval = cfg.getInteger("heldOpenInterval")*1000;
        lockDownInterval = cfg.getInteger("lockDownInterval")*1000;
        tamperSustainInterval = cfg.getInteger("tamperSustainInterval")*1000;
        forcedOpenSustainInterval = cfg.getInteger("forcedOpenSustainInterval")*1000;
        credentialSustainInterval = cfg.getInteger("credentialSustainInterval")*1000;
        readerPollInterval = cfg.getInteger("readerPollInterval")*1000;
        alarmPanelPollInterval = cfg.getInteger("alarmPanelPollInterval")*1000;

        lockDownOnCount=cfg.getInteger("lockDownOnCount");
        lockDownOffCount=cfg.getInteger("lockDownOffCount");

        rexEnabled = getBoolean(cfg,"rexEnabled");
        lockDownEnabled = getBoolean(cfg,"lockDownEnabled");
        tamperEnabled = getBoolean(cfg,"tamperEnabled");
        isRexReversed = getBoolean(cfg,"isRexReversed");
        isTamperReversed = getBoolean(cfg,"isTamperReversed");

        smtpServer = cfg.getString("smtpServer");
        emailUserId = cfg.getString("emailUserId");
        emailUserPassword = cfg.getString("emailUserPassword");
        emailSender = cfg.getString("emailSender");

        nextDoorProgrammingSchedule = cfg.getDateTime("nextDoorProgrammingSchedule");

        scanDevices = cfg.getBoolean("scanDevices");

        /*deviceStartIp =  Utility.ipAddressToLong(cfg.getString("startIpRange"));
        deviceEndIp =  Utility.ipAddressToLong(cfg.getString("endIpRange"));
        nextIp = Utility.ipAddressToLong(cfg.getString("nextIpAddress"));*/
    }


    /*public boolean needIpReprogram()
    {
        return deviceStartIp!=null && nextIp==null;
    }

    public boolean isIpValid(String ip)
    {
        try
        {
            if (deviceStartIp==null) return true;
            Long ipInt = Utility.ipAddressToLong(ip);
            if (ipInt==null) return false;
            if (ipInt<deviceStartIp) return false;
            if (deviceEndIp!=null && ipInt>deviceEndIp) return false;
            if (nextIp!=null && nextIp<=ipInt) return false;
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public String getNextIpAddress(IpssDbHelper h)
    {
        try
        {
            Long ipToReturn = null;
            if (deviceStartIp==null) return null;
            Set<Long> ipSet = h.getAllocatedIpSet();
            boolean success=false;
            while(!success)
            {
                ipToReturn = nextIp;
                if (nextIp==null) ipToReturn=nextIp=deviceStartIp;
                if (deviceEndIp!=null && deviceEndIp<ipToReturn) return null;
                nextIp++;
                if (!ipSet.contains(ipToReturn)) success=true;
            }
            h.saveNextIp(Utility.longToIpAddress(nextIp));
            return Utility.longToIpAddress(ipToReturn);
        }
        catch (Exception ex)
        {
            return null;
        }
    }*/


    public String getSmtpServer() {
        return smtpServer;
    }

    public String getEmailUserId() {
        return emailUserId;
    }

    public String getEmailUserPassword() {
        return emailUserPassword;
    }

    public boolean isRequestReset() {
        return requestReset;
    }

    /*public boolean isCompleteReset() {
        return completeReset;
    }*/

    public boolean isRequestReload() {
        return requestReload;
    }

    public boolean isRexReversed() {
        return isRexReversed;
    }

    public boolean isTamperReversed() {
        return isTamperReversed;
    }

    /*public boolean isCompleteReload() {
        return completeReload;
    }*/

    public DateTime getNextDoorProgrammingSchedule() {
        return nextDoorProgrammingSchedule;
    }

    public int getRelayInterval() {
        return relayInterval;
    }

    public int getHeldOpenInterval() {
        return heldOpenInterval;
    }

    public int getLockDownOnCount() {
        return lockDownOnCount;
    }

    public int getLockDownOffCount() {
        return lockDownOffCount;
    }

    public int getLockDownInterval() {
        return lockDownInterval;
    }

    public int getTamperSustainInterval() {
        return tamperSustainInterval;
    }

    public int getForcedOpenSustainInterval() {
        return forcedOpenSustainInterval;
    }

    public boolean isRexEnabled() {
        return rexEnabled;
    }

    public boolean isLockDownEnabled() {
        return lockDownEnabled;
    }

    public boolean isTamperEnabled() {
        return tamperEnabled;
    }

    public DateTime getCreatedStamp() {
        return createdStamp;
    }

    public long getElapsedMillis()
    {
        return Utility.getElapsed(createdStamp);
    }

    public int getCredentialSustainInterval() {
        return credentialSustainInterval;
    }

    private static boolean getBoolean(GenericRecord cfg, String field)
    {
        int v = cfg.getInteger(field);
        return v!=0;
    }

    private static boolean getBooleanComplete(GenericRecord cfg, String field)
    {
        int v = cfg.getInteger(field);
        return v==2;
    }

    public int getReaderPollInterval() {
        return readerPollInterval;
    }

    public int getAlarmPanelPollInterval() {
        return alarmPanelPollInterval;
    }

    public String getEmailSender() {
        return emailSender;
    }

    public boolean isScanDevices() {
        return scanDevices;
    }
}
