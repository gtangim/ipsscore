package ipss.core.isonas;

import ipss.core.isonas.states.DoorStates;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/4/12
 * Time: 10:11 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DoorListener {
    public void onDoorStatusChange(Door door, DoorStates newState);
}
