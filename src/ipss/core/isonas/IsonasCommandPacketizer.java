package ipss.core.isonas;

import apu.net.IDataFilter;
import apu.net.PacketizedCommand;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 12:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class IsonasCommandPacketizer implements IDataFilter {
    private ByteBuffer result = ByteBuffer.allocate(8192);
    private int n;

    public Object process(Object partialDataContainer, List inputData) throws Exception {
        result.clear();
        for(Object x:inputData)
        {
            ByteBuffer source = ((PacketizedCommand)x).getBinary();
            int m = source.position();
            source.rewind();
            short checksum=0;
            result.put((byte)0x10);
            result.put((byte)0x02);
            result.put((byte)0x01);
            checksum+=0x13;
            for(int i=0;i<m;i++)
            {
                byte b = source.get();
                if (b==0x10)
                {
                    result.put((byte)0x10);
                    result.put((byte)0x01);
                    checksum+=0x11;
                }
                else
                {
                    result.put((byte)b);
                    checksum+=((short)b) & 0xff;
                }
            }
            result.putShort(checksum);
        }
        n=result.position();
        return null;
    }

    public List getMaturedObjects() {
        List ret = new ArrayList(1);
        result.rewind();
        byte[] target = new byte[n];
        result.get(target);
        ret.add(target);
        return ret;
    }
}
