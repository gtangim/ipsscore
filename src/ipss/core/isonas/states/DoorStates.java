package ipss.core.isonas.states;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 10/4/11
 * Time: 3:33 PM
 * To change this template use File | Settings | File Templates.
 */
public enum DoorStates {
    ACTIVE, LOCKDOWN, UNLOCKED, OPENED, FORCED_OPEN, TAMPER, HELD_OPEN
}
