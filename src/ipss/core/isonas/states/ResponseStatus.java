package ipss.core.isonas.states;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 4:35 PM
 * To change this template use File | Settings | File Templates.
 */
public enum ResponseStatus {
    OK,TIMEOUT,INVALID,CLOSED,IOERROR
}
