package ipss.core.isonas;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.Loggable;
import apu.util.SimpleLogger;
import apu.util.StringUtil;
import apu.util.Utility;
import ipss.core.dsc.IAlarmPanel;
import ipss.core.isonas.commands.UserSchedule;
import ipss.core.isonas.states.DoorStates;
import org.joda.time.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 10/3/11
 * Time: 10:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class IpssDbHelper extends Loggable {
    public GenericDB db=null;
    protected IpssConfig config = null;

    protected boolean alarmRequested = false;
    protected Map<String,DoorStates> doorStatesMap = null;

    private Random r = new Random();

    public IpssDbHelper(GenericDB db, SimpleLogger logger)
    {
        super(logger);
        this.db=db;
        doorStatesMap = Utility.toMap("DOOR_ACTIVE",DoorStates.ACTIVE
                ,"DOOR_FOPEN",DoorStates.FORCED_OPEN
                ,"DOOR_HOPEN",DoorStates.HELD_OPEN
                ,"DOOR_LOCKDOWN",DoorStates.LOCKDOWN
                ,"DOOR_OPEN", DoorStates.OPENED);
        doorStatesMap.put("DOOR_TAMPER",DoorStates.TAMPER);
        doorStatesMap.put("DOOR_UNLOCK",DoorStates.UNLOCKED);
        loadAssignedIpList();
    }

    public GenericRecord getUser(long userId)
    {
        try {
            GenericRecord res = db.selectFirst("select * from users where userId=?", Utility.toList(userId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public GenericRecord getUserGroup(long userGroupId)
    {
        try {
            GenericRecord res = db.selectFirst("select * from usergroups where userGroupId=?"
                    , Utility.toList(userGroupId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }

    public List<GenericRecord> getUsersByPin(long pin)
    {
        try {
            String pinS=Long.toString(pin);
            List<GenericRecord> uList = db.selectAll("select * from users where pinNum=? and active=1",Utility.toList(pinS));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return uList;
        } catch (Exception e) {
            log(e);
            return null;
        }
    }

    public List<GenericRecord> getUsersByBadge(long badgeNum)
    {
        try {
            String badgeS=Long.toString(badgeNum);
            List<GenericRecord> uList = db.selectAll("select * from users where badgeNum=? and active=1",Utility.toList(badgeS));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return uList;
        } catch (Exception e) {
            log(e);
            return null;
        }
    }

    public GenericRecord getDoor(long doorId)
    {
        try {
            GenericRecord res = db.selectFirst("select * from doors where doorId=?", Utility.toList(doorId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public GenericRecord getDoorByMac(String macAddress)
    {
        try {
            GenericRecord res = db.selectFirst("select * from doors where macAddress=? and active=1", Utility.toList(macAddress));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public List<GenericRecord> getActiveDoors()
    {
        try {
            List<GenericRecord> res = db.selectAll("select * from doors "
                    + " where macAddress is not null and active=1 ");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public GenericRecord getDevice(String address)
    {
        try {
            GenericRecord res = db.selectFirst("select * from devices where device_address=?", Utility.toList(address));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public List<GenericRecord> getDevicesByClass(String deviceClass)
    {
        try {
            List<GenericRecord> res = db.selectAll("select * from devices "
                    + " where device_class_id=? and device_status_id='DEVICE_STATUS_REGISTERED'"
                    , Utility.toList(deviceClass));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public boolean hasReadersToConfigure(){
        try
        {
            List<GenericRecord> res = db.selectAll("select * from devices where device_class_id='ISONAS_READER' "
                    +" and device_status_id='DEVICE_STATUS_REGISTERED' "
                    +" and (device_ip_address is null "
                    +"   || device_target_ip_address is null "
                    +"   || (device_ip_address!=device_target_ip_address))");
            if (res!=null && res.size()>0) return true;
        }
        catch (Exception ex){
            log(ex);
        }
        return false;
    }
    public void clearUnregisteredDevices(String deviceClass)
    {
        try {
            db.executeUpdate("delete from devices "
                    +" where device_class_id=? and device_status_id!='DEVICE_STATUS_REGISTERED'"
                    ,Utility.toList(deviceClass));
            if (!db.isSuccessful()) throw new Exception(db.getError());
        } catch (Exception e) {
            log(e);
        }
    }
    public void deleteDevice(String address)
    {
        try {
            db.executeUpdate("update doors set macAddress=null, ipAddress=null, active=0, deletedOn=now() "
                    +" where macAddress=?"
                    ,Utility.toList(address));
            db.executeUpdate("delete from devices "
                    +" where device_address=?"
                    ,Utility.toList(address));
            if (!db.isSuccessful()) throw new Exception(db.getError());
        } catch (Exception e) {
            log(e);
        }
    }
    public GenericRecord createDevice(String address, String deviceClass
            ,String deviceType, String ipAddress, int firmwareVersion)
    {
        try {
            GenericRecord dev = db.newRecord("devices");
            dev.set("device_address",address);
            dev.set("device_class_id",deviceClass);
            dev.set("device_type_id",deviceType);
            dev.set("device_ip_address",ipAddress);
            dev.set("firmware_version", Integer.toString(firmwareVersion));
            dev.set("device_status_id","DEVICE_STATUS_UNREGISTERED");
            dev.set("next_config_schedule",new DateTime());
            db.insert(dev);
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return dev;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public void deactivateDoor(GenericRecord doorRec)
    {
        try {
            db.executeUpdate("delete from devices "
                    +" where device_address=?"
                    ,Utility.toList(doorRec.getString("macAddress")));
            doorRec.set("ipAddress", null);
            doorRec.set("macAddress", null);
            doorRec.set("status","DELETED");
            doorRec.set("active",false);
            doorRec.set("deletedOn",new DateTime());
            db.update(doorRec);
            if (!db.isSuccessful()) throw new Exception(db.getError());
        } catch (Exception e) {
            log(e);
        }
    }



    public Long getNextId(String table, String primaryKey)
    {
        try {
            GenericRecord res = db.selectFirst("select max("+primaryKey+") as id from "+table);
            if (!db.isSuccessful()) throw new Exception(db.getError());
            Long id = res.getLong("id");
            if (id==null) id=1l;
            return id+1;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }

    public String getAlarmPanelState(long alarmPanelId)
    {
        try {
            GenericRecord res = db.selectFirst("select deviceState from alarmpanels where alarmPanelId=?"
                    , Utility.toList(alarmPanelId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res.getString(0);
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public void setAlarmPanelState(long alarmPanelId, String state, String message)
    {
        try {
            db.executeUpdate("update alarmpanels set deviceState=?, deviceMessage=? "
                    +" where alarmPanelId=?", Utility.toList(state, message, alarmPanelId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
        } catch (Exception e) {
            log(e);
        }
    }
    public GenericRecord getAlarmPanel(long alarmPanelId)
    {
        try {
            GenericRecord res = db.selectFirst("select * from alarmpanels where alarmPanelId=?"
                    , Utility.toList(alarmPanelId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public List<GenericRecord> getAllAlarmPanels()
    {
        try {
            List<GenericRecord> res = db.selectAll("select * from alarmpanels where comAddress is not null");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }



    public List<Long> getUserSet(long userId)
    {
        List<Long> ret = new ArrayList<Long>();
        try {
            ret.add(userId);
            List<GenericRecord> resList = db.selectAll("select * from usergroupassociations where userId=?"
                    ,Utility.toList(userId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            for (GenericRecord res:resList)
                ret.add(res.getLong("userGroupId"));
        } catch (Exception e) {
            log(e);
        }
        return ret;
    }

    public List<Long> getDoorSet(long doorId)
    {
        List<Long> ret = new ArrayList<Long>();
        try {
            ret.add(doorId);
            List<GenericRecord> resList = db.selectAll("select * from doorgroupassociations where doorId=?"
                    ,Utility.toList(doorId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            for (GenericRecord res:resList)
                ret.add(res.getLong("doorGroupId"));
        } catch (Exception e) {
            log(e);
        }
        return ret;
    }

    public List<GenericRecord> getPermissions(long userId, long doorId)
    {
        try {
            List<GenericRecord> res = db.selectAll("select * from permissions inner join schedules "
                    +"on permissions.scheduleId=schedules.scheduleId "
                    +"where userId in [?] and doorId in [?]"
                    , Utility.toList(getUserSet(userId),getDoorSet(doorId)));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }
    public GenericRecord getSchedule(long scheduleId)
    {
        try {
            GenericRecord res = db.selectFirst("select * from schedules where scheduleId=?"
                    ,Utility.toList(scheduleId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
            return null;
        }
    }

    public List<GenericRecord> getSchedules(long scheduleId)
    {
        try {
            List<GenericRecord> res = db.selectAll("select * from schedules where scheduleId=?"
                    ,Utility.toList(scheduleId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return new ArrayList<GenericRecord>();
    }
    public List<GenericRecord> getAllSchedules()
    {
        try {
            List<GenericRecord> res = db.selectAll("select * from schedules");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return new ArrayList<GenericRecord>();
    }

    private void addUserSchedule(Map<Long,UserSchedule> userSchedules, Long userId, Long scheduleId) throws Exception
    {
        if (!userSchedules.containsKey(userId))
        {
            GenericRecord u = db.selectFirst("select * from users where userid=? and active=1",Utility.toList(userId));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            if (u==null) throw new Exception("Unable to find user: " + userId.toString() +" in the database!");
            UserSchedule usch = new UserSchedule(u.getLong("userid"),u.getString("username")
                    ,u.getString("badgenum"),u.getString("pinnum"));
            usch.addSchedule(scheduleId);
            userSchedules.put(userId,usch);
        }
        userSchedules.get(userId).addSchedule(scheduleId);
    }

    public Map<Long,UserSchedule> getDoorUsersAndSchedules(long doorId,Map<Integer,Integer> scheduleMap)
    {
        try
        {
            List<Long> dSet = getDoorSet(doorId);
            if (dSet==null) return null;
            List<GenericRecord> permissions = db.selectAll("select * from permissions where doorId in [?]"
                    ,Utility.toList(dSet));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            if (permissions==null) throw new Exception("Unable to retrieve permission list from database!");
            HashMap<Long,UserSchedule> users = new HashMap<Long,UserSchedule>();

            HashSet userGroups = new HashSet();
            List<GenericRecord> usgList = db.selectAll("select usergroupid from usergroups");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            if (usgList==null) throw new Exception("Unable to retrieve User Group list from database!");
            for(GenericRecord usg:usgList) userGroups.add(usg.getLong("usergroupid"));

            HashSet userLookup = new HashSet();
            List<GenericRecord> usList = db.selectAll("select userId from users");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            if (usList==null) throw new Exception("Unable to retrieve User list from database!");
            for(GenericRecord us:usList) userLookup.add(us.getLong("userId"));


            HashSet<Long> conflictUsers = getStandaloneConflictUsers(permissions);

            for(GenericRecord permission:permissions)
            {
                Long sid = permission.getLong("scheduleid");
                Long uid = permission.getLong("userId");
                Long sid2 = null;
                if (scheduleMap.containsKey(sid.intValue())) sid2=scheduleMap.get(sid.intValue()).longValue();
                else
                {
                    logWarning("Unable to map schedule: "+sid.toString()+" to reader timezone!");
                    continue;
                }

                if (userGroups.contains(uid))
                {
                    List<GenericRecord> userGroupUsers = db.selectAll("select userid from usergroupassociations"
                            +" where usergroupid=?",Utility.toList(uid));
                    if (!db.isSuccessful()) throw new Exception(db.getError());
                    if (userGroupUsers==null)
                        throw new Exception("Unable to retrieve user group user list from the Database!");
                    for(GenericRecord u:userGroupUsers)
                    {
                        if (!conflictUsers.contains(u.getLong("userid")))
                            addUserSchedule(users,u.getLong("userid"),sid2);
                    }
                }
                else if (userLookup.contains(uid))
                {
                    if (!conflictUsers.contains(uid)) addUserSchedule(users,uid,sid2);
                }
            }
            return users;
        }
        catch (Exception e)
        {
            log(e);
            return null;
        }
    }


    public HashSet<Long> getStandaloneConflictUsers(List<GenericRecord> doorPermissions)
    {
        try
        {
            List<GenericRecord> uGroupList = db.selectAll("select UserGroupId from usergroups");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            HashSet<Long> uGroups = new HashSet<Long>();
            for(GenericRecord ug:uGroupList) uGroups.add(ug.getLong("userGroupId"));


            List<Long> doorUsers = new ArrayList<Long>();
            for(GenericRecord p:doorPermissions)
                if (uGroups.contains(p.getLong("userId")))
                {
                    List<Long> usersToAdd = getUserSet(p.getLong("userId"));
                    doorUsers.addAll(usersToAdd);
                }
                else doorUsers.add(p.getLong("userId"));

            HashSet<Long> ret = new HashSet<Long>();
            if (doorUsers.size()==0) return ret;
            List<String> conflictPinList = new ArrayList<String>();
            List<GenericRecord> cpList = db.selectAll("select pinNum,count(userId) as numUsers from users"
                    +" where userId in [?]"
                    +" group by pinNum having coalesce(pinNum,'')!='' and count(userId)>1"
                    ,Utility.toList(doorUsers));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            if (cpList==null)
                throw new Exception("Unable to retrieve conflict pin List from the Database!");
            for(GenericRecord cp:cpList) conflictPinList.add(cp.getString("pinNum"));
            if (conflictPinList.size()>0)
            {
                List<GenericRecord> cuList = db.selectAll("select userId from users where pinNum in [?]"
                                                ,Utility.toList(conflictPinList));
                if (!db.isSuccessful()) throw new Exception(db.getError());
                if (cuList==null)
                    throw new Exception("Unable to retrieve conflicted user List from the Database!");
                for(GenericRecord cu:cuList) ret.add(cu.getLong("userId"));
            }
            return ret;
        }
        catch (Exception e)
        {
            log(e);
            return null;
        }
    }


    public GenericRecord getHoliday()
    {
        try {
            LocalDate today = (new DateTime()).toLocalDate();
            //DateTime today = new DateTime(2014,7,8,0,0,0,0);
            GenericRecord res = db.selectFirst("select * from holidays where holidayDate=?"
                    ,Utility.toList(today));
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }

    public List<GenericRecord> getHolidays()
    {
        try {
            List<GenericRecord> res = db.selectAll("select * from holidays");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return res;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }

    public List<GenericRecord> filterBySchedule(List<GenericRecord> schedules)
    {
        List<GenericRecord> ret = new ArrayList<GenericRecord>(schedules.size());
        GenericRecord holiday = getHoliday();
        DateTime now = new DateTime();
        int day = now.getDayOfWeek();
        DateTime start = null;
        DateTime end = null;
        for (GenericRecord schedule:schedules)
        {
            start=null;
            end=null;
            switch (day)
            {
                case DateTimeConstants.SATURDAY:
                    start = schedule.getDateTime("saturdayStart");
                    end = schedule.getDateTime("saturdayEnd");
                    break;
                case DateTimeConstants.SUNDAY:
                    start = schedule.getDateTime("sundayStart");
                    end = schedule.getDateTime("sundayEnd");
                    break;
                case DateTimeConstants.MONDAY:
                    start = schedule.getDateTime("mondayStart");
                    end = schedule.getDateTime("mondayEnd");
                    break;
                case DateTimeConstants.TUESDAY:
                    start = schedule.getDateTime("tuesdayStart");
                    end = schedule.getDateTime("tuesdayEnd");
                    break;
                case DateTimeConstants.WEDNESDAY:
                    start = schedule.getDateTime("wednesdayStart");
                    end = schedule.getDateTime("wednesdayEnd");
                    break;
                case DateTimeConstants.THURSDAY:
                    start = schedule.getDateTime("thursdayStart");
                    end = schedule.getDateTime("thursdayEnd");
                    break;
                case DateTimeConstants.FRIDAY:
                    start = schedule.getDateTime("fridayStart");
                    end = schedule.getDateTime("fridayEnd");
                    break;
            }
            if (start!=null && end!=null)
            {
                String eh = schedule.getString("exceptHolidays");
                if (eh!=null) eh=eh.toLowerCase().trim();
                if (holiday==null || "no".equals(eh))
                {
                    if (now.getMillisOfDay()>=start.getMillisOfDay()
                            && now.getMillisOfDay()<=end.getMillisOfDay())
                        ret.add(schedule);
                }
            }
        }

        return ret;
    }

    public void resetConfig()
    {
        config=null;
    }

    public IpssConfig getConfig()
    {

        try {
            if (config==null || config.getElapsedMillis()>5000)
            {
                GenericRecord cfg = db.selectFirst("select * from globalconfig");
                if (!db.isSuccessful()) throw new Exception(db.getError());
                config = new IpssConfig(cfg);
            }
            return config;
        } catch (Exception e) {
            log(e);
            return config;
        }
    }
    public GenericRecord getConfigRec()
    {

        try {
            GenericRecord cfg = db.selectFirst("select * from globalconfig");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return cfg;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }

    public void saveNextIp(String ip)
    {
        try
        {
            db.executeUpdate("update globalconfig set nextIpAddress=?",Utility.toList(ip));
            if (!db.isSuccessful()) throw new Exception(db.getError());
        }
        catch (Exception ex)
        {
            log(ex);
        }
    }
    public Set<Long> getAllocatedIpSet()
    {
        try {
            List<GenericRecord> res = db.selectAll("select distinct device_target_ip_address from devices");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            Set<Long> ret = new HashSet<Long>();
            for(GenericRecord r:res)
                if (!r.isNull(0))
                    ret.add(Utility.ipAddressToLong(r.getString(0)));
            return ret;
        } catch (Exception e) {
            log(e);
        }
        return null;
    }

    public boolean hasIsonasReaderInIpConfig()
    {
        try
        {
            GenericRecord cnt = db.selectFirst("select count(*) from devices where last_registration_error='"
                    + IsonasDiscovery.ConfigureState+"'");
            if (cnt.getInteger(0)>0) return true;
            else return false;
        }
        catch (Exception ex)
        {
            log(ex);
            return false;
        }
    }

    //public boolean logAlarm()

    public boolean logDbAP(String message, String eventType, long alarmPanelId)
    {
        try
        {
            if (eventType==null) return false;
            GenericRecord s = null;
            GenericRecord e = db.newRecord("events");
            e.set("eventType",eventType);
            e.set("createdDate",new DateTime());
            if (message!=null) e.set("description",message);
            e.set("alarmPanelId",alarmPanelId);
            processAlerts(e);
            if (!db.insert(e)) throw new Exception("Unable to enter log into DB! " + db.getError());
            return true;
        }
        catch (Exception ex)
        {
            log(ex);
            return false;
        }
    }

    public Map<Integer, String> zoneStates = new HashMap<Integer, String>();

    public boolean logDbAPZone(String state, long alarmPanelId, int zoneId)
    {
        try
        {
            if (state==null) return false;
            GenericRecord s = db.selectFirst("select * from sensors where alarmPanelId=? and zoneNumber=? and active=1"
                    , Utility.toList(alarmPanelId,zoneId));
            if (s==null) return false;
            String lastState = null;
            if (zoneStates.containsKey(zoneId)) lastState=zoneStates.get(zoneId);
            else zoneStates.put(zoneId,state);
            if (state.equals(lastState)) return true;    // do not log identical zone state...

            GenericRecord e = db.newRecord("events");
            e.set("eventType",state);
            e.set("createdDate",new DateTime());
            String message = s.getString("sensorName")+" (Zone:"+zoneId+") state changed to " + state;
            e.set("description",message);
            e.set("alarmPanelId",alarmPanelId);
            e.set("zoneId",zoneId);
            e.set("sensorId",s.getLong("id"));
            boolean res = processAlerts(e);
            if (state.equals("APZONE_TRIGGER") && !res) return true; // do not log if zone is triggered but didnt alert
            zoneStates.put(zoneId,state);
            if (!db.insert(e)) throw new Exception("Unable to enter log into DB! " + db.getError());
            return true;
        }
        catch (Exception ex)
        {
            log(ex);
            return false;
        }

    }


    public boolean logDbSensor(String message, String eventType, Long sensorId, BigDecimal sensorValue)
    {
        try
        {
            if (sensorValue==null) sensorValue=BigDecimal.ZERO;
            GenericRecord e = db.newRecord("events");
            e.set("eventType",eventType);
            e.set("createdDate",new DateTime());
            if (message!=null) e.set("description",message);
            if (sensorId!=null) e.set("sensorId",sensorId);
            e.set("sensorValue",sensorValue);
            processAlerts(e);
            if (!db.insert(e)) throw new Exception("Unable to enter log into DB! " + db.getError());
            return true;
        }
        catch (Exception ex)
        {
            log(ex);
            return false;
        }
    }

    public boolean logDb(String message, String eventType, Long doorId, Long userId, String inp, String inpType)
    {
        try
        {
            GenericRecord e = db.newRecord("events");
            e.set("eventType",eventType);
            e.set("createdDate",new DateTime());
            if (message!=null) e.set("description",message);
            if (doorId!=null) e.set("doorId",doorId);
            if (userId!=null) e.set("userId",userId);
            if (inp!=null) e.set("inputNum",inp);
            if (inpType!=null) e.set("inputType",inpType);
            processAlerts(e);
            if (!db.insert(e)) throw new Exception("Unable to enter log into DB! " + db.getError());
            return true;
        }
        catch (Exception ex)
        {
            log(ex);
            return false;
        }
    }
    public boolean logDb(String message,String eventType)
    {
        try
        {
            GenericRecord e = db.newRecord("events");
            //e.set("id",0);
            e.set("eventType",eventType);
            e.set("createdDate",new DateTime());
            e.set("description",message);
            if (!db.insert(e)) throw new Exception("Unable to enter log into DB! " + db.getError());
            return true;
        }
        catch (Exception ex)
        {
            log(ex);
            return false;
        }
    }
    public boolean logDbSys(String message)
    {
        return logDb(message,"SYSTEM_LOG");
    }


    public void logDbDoorOffline(long doorId, String doorName)
    {
        logDb("ISONAS Reader ["+doorId+"] "+doorName
                +" is OFFLINE!","DEVICE_OFFLINE",doorId,null,null,null);
    }
    public void logDbAPOffline(long apId, String apName)
    {
        logDbAP("Alarm Panel [" + apId + "] " + apName + " is OFFLINE!", "DEVICE_OFFLINE", apId);
    }



    protected void sendAlert(Long alertId) throws Exception
    {
        if (alertId==null) return;
        GenericRecord alert = db.selectFirst("select * from alerts where id=?",Utility.toList(alertId));
        String subject = "IP360 ALERT: "+alert.getString("name");
        String message = alert.getString("textTemplate");


        String logMessage = message;
        if (logMessage.length()>255) logMessage = logMessage.substring(0,255);
        logDb(logMessage,"ALERT");

        String email = alert.getString("email");
        if (email!=null && !email.trim().equals(""))
            Utility.sendEmailAlert(subject,message,email.trim(),getConfig().getEmailSender(), logger);

        String smsNumber = alert.getString("textNumber");
        Integer cellCarrierId = alert.getInteger("cellCarrier");
        GenericRecord cellCarrier=null;
        if (cellCarrierId!=null) cellCarrier = db.selectFirst("select * from smscarriers where carrierID=?"
                ,Utility.toList(cellCarrierId));
        if (smsNumber!=null && !smsNumber.trim().equals("") && cellCarrier!=null)
        {
            smsNumber = smsNumber.trim();
            Utility.sendEmailAlert(subject,message,smsNumber+cellCarrier.getString("smsSuffix")
                    ,getConfig().getEmailSender(), logger);
        }
        if (alert.getInteger("triggerAlarm")==1) setAlarmRequest();
    }


    private boolean alertMatchApStatus(String apStatus, String alertStatus)
    {
        if (alertStatus.equals("ALERT_APSTAT_ANY")) return true;
        else if (alertStatus.equals("ALERT_APSTAT_DISARMED") && ("READY".equals(apStatus) || "BUSY".equals(apStatus)))
            return true;
        else if (alertStatus.equals("ALERT_APSTAT_ARMED") && "ARMED".equals(apStatus)) return true;
        else if (alertStatus.equals("ALERT_APSTAT_ALARM") && "ALARM".equals(apStatus)) return true;
        else if (alertStatus.equals("ALERT_APSTAT_ENDEL") && "ENTRY_DELAY".equals(apStatus)) return true;
        else if (alertStatus.equals("ALERT_APSTAT_EXDEL") && "EXIT_DELAY".equals(apStatus)) return true;
        else if (alertStatus.equals("ALERT_APSTAT_OFF") && "OFFLINE".equals(apStatus)) return true;
        else return false;
    }

    private boolean matchZoneStatus(String eventType, String conditionValue)
    {
        if (eventType.endsWith("_OFF") && conditionValue.equals("ALERT_ZSTAT_OFF")) return true;
        else if (eventType.endsWith("_TRIGGER") && conditionValue.equals("ALERT_ZSTAT_ON")) return true;
        else if (eventType.endsWith("_ALARM") && conditionValue.equals("ALERT_DSTAT_ALARM")) return true;
        return false;
    }

    private String getXBeeConditionEventType(String condType)
    {
        if (condType==null) return "XBEE_UNKNOWN";
        else if ("ALERT_COND_HUM".equals(condType)) return "XBEE_HUM";
        else if ("ALERT_COND_LIGHT".equals(condType)) return "XBEE_LIGHT";
        else if ("ALERT_COND_TEMP".equals(condType)) return "XBEE_TEMP";

        return "XBEE_UNKNOWN";
    }

    private boolean xBeeConditionMatch(String comparator, BigDecimal actualValue
            , BigDecimal compareValue1, BigDecimal compareValue2)
    {
        if (actualValue!=null && comparator!=null && comparator.startsWith("ALERT_XBEE_COMP_"))
        {
            if (comparator.endsWith("_BET") && compareValue1!=null && compareValue2!=null
                    && actualValue.compareTo(compareValue1)>=0 && actualValue.compareTo(compareValue2)<=0)
                return true;
            else if (comparator.endsWith("_NBET") && compareValue1!=null && compareValue2!=null
                    && actualValue.compareTo(compareValue1)<0 && actualValue.compareTo(compareValue2)>0)
                return true;
            else if (comparator.endsWith("_GT") && compareValue1!=null
                    && actualValue.compareTo(compareValue1)>0)
                return true;
            else if (comparator.endsWith("_LT") && compareValue1!=null
                    && actualValue.compareTo(compareValue1)>0)
                return true;
        }
        return false;
    }
    private boolean xBeeIncConditionMatch(String comparator, BigDecimal changeValue
            , BigDecimal presentValue, BigDecimal pastValue)
    {
        if (presentValue!=null && pastValue!=null && changeValue!=null
                && comparator!=null && comparator.startsWith("ALERT_XBEE_COMP_"))
        {
            changeValue=changeValue.abs();
            if (comparator.endsWith("_INC") && presentValue.subtract(pastValue).compareTo(changeValue)>0)
                return true;
            else if (comparator.endsWith("_DEC") && pastValue.subtract(presentValue).compareTo(changeValue)>0)
                return true;
        }
        return false;
    }

    public String getEventDescription(GenericRecord event, GenericRecord cond) throws Exception
    {
        String cType = cond.getString("cond_type_enum_id");
        String comp = cond.getString("value_comp_enum_id");
        BigDecimal v1 = cond.getBigDecimal("value1");
        BigDecimal v2 = cond.getBigDecimal("value2");
        BigDecimal dur = cond.getBigDecimal("duration");
        String ve = cond.getString("value_enum_id");
        Long deviceId = cond.getLong("device_id");
        BigDecimal val = event.getBigDecimal("sensorValue");

        if ("ALERT_COND_DOOR".equals(cType) && ve!=null)
        {
            GenericRecord dev = db.selectFirst("select * from doors where doorId=?",Utility.toList(deviceId));
            if (dev!=null)
            {
                GenericRecord dState = db.selectFirst("select * from enumerations where enum_id=?",Utility.toList(ve));
                if (dState!=null) ve = dState.getString("description");
                String msg = "the door \""+dev.getString("doorName")
                        +" ("+dev.getLong("doorId").toString()+")\" is "+ve+".";
                return msg;
            }
        }
        else if ("ALERT_COND_ZONE".equals(cType))
        {
            GenericRecord dev = db.selectFirst("select * from sensors where id=?",Utility.toList(deviceId));
            if (dev!=null)
            {
                GenericRecord dState = db.selectFirst("select * from enumerations where enum_id=?",Utility.toList(ve));
                if (dState!=null) ve = dState.getString("description");
                String msg = "the alarm panel sensor \""+dev.getString("sensorName")+" ("
                        +dev.getLong("id").toString()+")\" is "+ve;
                if (dur!=null && dur.compareTo(BigDecimal.ZERO)>0) msg = msg+" for a sustained duration of "
                        + dur.toString() + " minutes";
                return msg+".";
            }
        }
        else if ("ALERT_COND_HUM".equals(cType) || "ALERT_COND_TEMP".equals(cType) || "ALERT_COND_LIGHT".equals(cType))
        {
            String sType = "humidity";
            String sUom = "%";
            if ("ALERT_COND_TEMP".equals(cType))
            {
                sType="temperature";
                sUom=" Deg. Cel.";
            }
            else if ("ALERT_COND_TEMP".equals(cType)) sType="light";

            GenericRecord dev = db.selectFirst("select * from xbeesensors where sensor_id=?",Utility.toList(deviceId));
            GenericRecord compEnum = db.selectFirst("select * from enumerations where enum_id=?",Utility.toList(comp));
            String msg = "";
            if (dev!=null && compEnum!=null)
            {
                if (v1==null) v1=BigDecimal.ZERO;
                if (v2==null) v2=BigDecimal.ZERO;
                if (comp.endsWith("_DEC") || comp.endsWith("_INC"))
                {
                    msg = "the "+sType+" sensor \""+dev.getString("name")+" ("
                            + dev.getLong("sensor_id").toString()+")\" reading has "
                            + compEnum.getString("description") + " "+v1.toString() + sUom;
                    if (dur!=null && dur.compareTo(BigDecimal.ZERO)>0) msg = msg+" within a period of "
                            + dur.toString() + " minutes";
                }
                else if (comp.endsWith("_BET") || comp.endsWith("_NBET"))
                {
                    msg = "the "+sType+" sensor \""+dev.getString("name")+" ("
                            + dev.getLong("sensor_id").toString()+")\" reading is "
                            + compEnum.getString("description") + " "+v1.toString() +sUom+" and "+v2.toString()+sUom;
                    if (dur!=null && dur.compareTo(BigDecimal.ZERO)>0) msg = msg+" for a sustained period of "
                            + dur.toString() + " minutes";
                }
                else
                {
                    msg = "the "+sType+" sensor \""+dev.getString("name")+" ("
                            + dev.getLong("sensor_id").toString()+")\" reading is "
                            + compEnum.getString("description") + " "+v1.toString() +sUom;
                    if (dur!=null && dur.compareTo(BigDecimal.ZERO)>0) msg = msg+" for a sustained period of "
                            + dur.toString() + " minutes";
                }
                return msg+".";
            }
        }
        return "";
    }


    public boolean processAlerts(GenericRecord event)
    {
        boolean ret = false;
        try
        {
            // First get a list of schedules that apply to the current time frame
            List<GenericRecord> schedules = db.selectAll("select * from schedules");
            if (!db.isSuccessful()) throw new Exception(db.getError());
            schedules = filterBySchedule(schedules);
            HashSet<Long> scheduleSet = new HashSet<Long>();
            for(GenericRecord s:schedules) scheduleSet.add(s.getLong("scheduleId"));

            // Now find the current alarm panel state
            // ***** NOTE: WE ASSUME HERE THAT THERE IS ONLY ONE ALARM PANEL IN THE SYSTEM *********
            GenericRecord aRec = db.selectFirst("select * from alarmpanels");
            long apId=-1;
            String apStatus="OFFLINE";
            if (aRec!=null)
            {
                apStatus = aRec.getString("status");
                apId = aRec.getLong("alarmPanelId");
            }
            // Now filter through all conditions and see which one applies
            // for each one that applies we must carry out that alert
            List<GenericRecord> conditions = db.selectAll(
                    "select ac.alert_id, ac.seq_id, ac.cond_type_enum_id, ac.device_id, ac.value_comp_enum_id, "
                            + " ac.value1, ac.value2, ac.value_enum_id, ac.duration, a.schedule_id, a.ap_status_enum_id "
                            + " from alertconditions ac "
                            + " join alerts a on ac.alert_id=a.id");
            if (!db.isSuccessful()) throw new Exception(db.getError());

            HashSet<Long> alertsToExecute = new HashSet<Long>();
            HashMap<Long,GenericRecord> alertCondition = new HashMap<Long, GenericRecord>();

            String eType = event.getString("eventType");

            for(GenericRecord cond:conditions)
            {
                if (!cond.isNull("schedule_id") && !scheduleSet.contains(cond.getLong("schedule_id"))) continue;
                if (!cond.isNull("ap_status_enum_id")
                        && !alertMatchApStatus(apStatus,cond.getString("ap_status_enum_id"))) continue;

                // Now check if the condition actually matches the event
                long alertId = cond.getLong("alert_id");
                String cType = cond.getString("cond_type_enum_id");
                String xType = getXBeeConditionEventType(cType);
                if ("ALERT_COND_DOOR".equals(cType) && (eType.startsWith("DOOR_") || eType.startsWith("DEVICE_")) && !event.isNull("doorId")
                        && event.getLong("doorId")==cond.getLong("device_id"))
                {
                    String cv = cond.getString("value_enum_id");
                    if (eType.endsWith("_ACTIVE") && cv.equals("ALERT_DSTAT_LOC")) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                    else if (eType.endsWith("_FOPEN") && cv.equals("ALERT_DSTAT_FOP")) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                    else if (eType.endsWith("_HOPEN") && cv.equals("ALERT_DSTAT_HOP")) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                    else if (eType.endsWith("_OPEN") && cv.equals("ALERT_DSTAT_OPN")) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                    else if (eType.endsWith("_TAMPER") && cv.equals("ALERT_DSTAT_TMP")) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                    else if (eType.endsWith("_UNLOCK") && cv.equals("ALERT_DSTAT_UNL")) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                    else if (eType.endsWith("_OFFLINE") && cv.equals("ALERT_DSTAT_OFF")) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                }
                else if ("ALERT_COND_ZONE".equals(cType) && eType.startsWith("APZONE_") && !event.isNull("alarmPanelId")
                        && event.getLong("alarmPanelId")==apId && !event.isNull("sensorId")
                        && event.getLong("sensorId")==cond.getLong("device_id"))
                {
                    String cv = cond.getString("value_enum_id");
                    if (matchZoneStatus(eType,cv))
                    {
                        alertsToExecute.add(alertId);
                        alertCondition.put(alertId,cond);
                        /*if (cond.isNull("duration")) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                        else
                        {
                            // Read history and compare....
                            int durationMillis = cond.getBigDecimal("duration").multiply(new BigDecimal(60000)).intValue();
                            DateTime from = new DateTime().minusMillis(durationMillis);
                            List<GenericRecord> eList = db.selectAll("select id,eventType from events "
                                    +"where createdDate>=? and sensorId=? and alarmPanelId=? and eventType in [?]"
                                    , Utility.toList(from,cond.getLong("device_id"),apId
                                    , Utility.toList("APZONE_OFF","APZONE_TRIGGER","APZONE_ALARM")));
                            boolean allMatch = true;
                            for(GenericRecord oldEvent:eList)
                                if (!matchZoneStatus(oldEvent.getString("eventType"),cv))
                                {
                                    allMatch = false;
                                    break;
                                }
                            if (allMatch) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                        }*/
                    }
                }
                else if (xType.equals(eType) && !event.isNull("sensorId")
                        && event.getLong("sensorId")==cond.getLong("device_id"))
                {
                    BigDecimal v1 = cond.getBigDecimal("value1");
                    BigDecimal v2 = cond.getBigDecimal("value2");
                    String comp = cond.getString("value_comp_enum_id");
                    BigDecimal dur = cond.getBigDecimal("duration");
                    if ("ALERT_XBEE_COMP_INC".equals(comp) || "ALERT_XBEE_COMP_DEC".equals(comp))
                    {
                        if (v1!=null && dur!=null)
                        {
                            // Get a list of events within dur minutes and check each of them...
                            int durationMillis = cond.getBigDecimal("duration").multiply(new BigDecimal(60000)).intValue();
                            DateTime from = new DateTime().minusMillis(durationMillis);
                            List<GenericRecord> eList = db.selectAll("select id,sensorValue from events "
                                    +" where createdDate>=? and eventType=? and sensorId=? and alarmPanelId is null"
                                    , Utility.toList(from,xType, cond.getLong("device_id")));
                            BigDecimal currentValue = event.getBigDecimal("sensorValue");
                            for(GenericRecord pastEvent:eList)
                                if (xBeeIncConditionMatch(comp,v1,currentValue
                                        ,pastEvent.getBigDecimal("sensorValue")))
                                {
                                    alertsToExecute.add(alertId);
                                    alertCondition.put(alertId,cond);
                                    break;
                                }
                        }
                    }
                    else
                    {
                        BigDecimal av = event.getBigDecimal("sensorValue");
                        if (xBeeConditionMatch(comp,av,v1,v2))
                        {
                            if (dur!=null)
                            {
                                // Need to check condition for sustained value
                                int durationMillis = cond.getBigDecimal("duration").multiply(new BigDecimal(60000)).intValue();
                                DateTime from = new DateTime().minusMillis(durationMillis);
                                List<GenericRecord> eList = db.selectAll("select id,sensorValue from events "
                                        +" where createdDate>=? and eventType=? and sensorId=? and alarmPanelId is null"
                                        , Utility.toList(from,xType, cond.getLong("device_id")));
                                boolean allMatch=true;
                                for(GenericRecord pastEvent:eList)
                                    if (!xBeeConditionMatch(comp, pastEvent.getBigDecimal("sensorValue"), v1, v2))
                                    {
                                        allMatch=false;
                                        break;
                                    }
                                if (allMatch) {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                            }
                            else {alertsToExecute.add(alertId);alertCondition.put(alertId,cond);}
                        }
                    }
                }


            }


            // Now execute the alerts
            for(Long alertId:alertsToExecute)
            {
                ret = true;
                GenericRecord alert = db.selectFirst("select * from alerts where id=?", Utility.toList(alertId));
                if (alert!=null)
                {
                    List<GenericRecord> actions = db.selectAll("select * from alertactions where alert_id=? "
                            +" order by seq_id", Utility.toList(alertId));

                    if (actions!=null) for(GenericRecord action:actions)
                    {
                        String aType = action.getString("action_type_enum_id");
                        if (aType.equals("ALERT_ACTION_EMAIL"))
                        {
                            String v1 = action.getString("value1");
                            String v2 = action.getString("value2");
                            if (v1!=null && !v1.equals("")) v2=v1+"<"+v2+">";
                            String msg = alert.getString("message");
                            String msg2 = "";
                            if (alertCondition.containsKey(alertId))
                                msg2 = getEventDescription(event,alertCondition.get(alertId));
                            if (!msg2.equals(""))
                                msg = msg+"\r\n\r\nThis alert was triggered because "+msg2;
                            logDb(msg,"ALERT");
                            Utility.sendEmailAlert(alert.getString("name"),msg,v2,getConfig().getEmailSender(), logger);
                        }
                        else if (aType.equals("ALERT_ACTION_TEXT"))
                        {
                            String v2 = action.getString("value2");
                            String msg = alert.getString("message");
                            String msg2 = "";
                            if (alertCondition.containsKey(alertId))
                                msg2 = getEventDescription(event,alertCondition.get(alertId));
                            if (!msg2.equals(""))
                                msg = msg+"\r\nThis alert was sent to you because "+msg2;
                            logDb(msg,"ALERT");
                            Utility.sendEmailAlert(alert.getString("name"),msg,v2,getConfig().getEmailSender(), logger);
                        }
                    }
                }

            }
        }
        catch(Exception ex)
        {
            log(ex);
        }
        return ret;
    }

    HashSet<Long> assignedIpList = null;
    public void loadAssignedIpList()
    {
        try {
            assignedIpList = new HashSet<Long>();
            GenericRecord cfg = getConfigRec();
            if (!cfg.isNull("hostIpAddress"))
                assignedIpList.add(Utility.ipAddressToLong(cfg.getString("hostIpAddress")));
            List<GenericRecord> apList = getAllAlarmPanels();
            if (apList!=null)
                for(GenericRecord ap: apList) if (!ap.isNull("ipAddress"))
                    assignedIpList.add(Utility.ipAddressToLong(ap.getString("ipAddress")));
            List<GenericRecord> isonasTargets = db.selectAll("select distinct device_target_ip_address from devices "
                    + " where device_target_ip_address is not null");
            if (isonasTargets!=null)
                for(GenericRecord target:isonasTargets)
                    assignedIpList.add(Utility.ipAddressToLong(target.getString(0)));
        } catch (Exception e) {
            log(e);
        }


    }

    public void updateHostEntries()
    {
        try {
            if (!Utility.isWindows) {
                log("Configuring Linux Network Configuration Files...");
                GenericRecord cfg = getConfigRec();
                String hostIp = cfg.getString("hostIpAddress");
                if (StringUtil.empty(hostIp))
                    throw new Exception("Invalid or empty host IP range. Host is not configured!");
                String subnet = cfg.getString("systemSubnet");
                if (StringUtil.empty(subnet))
                    throw new Exception("Invalid or empty subnet mask. Host is not configured!");
                String gateway = cfg.getString("systemGateway");
                if (StringUtil.empty(gateway))
                    throw new Exception("Invalid or empty gateway. Host is not configured!");
                String setupNetIp = "192.168.0.1";
                String setupNetMask = "255.255.255.0";
                String setupNetGateway = "192.168.0.254";
                String setupNetRange = "192.168.0.2,192.168.0.21";
                String setupNetBroadcast = "192.168.0.255";
                if (hostIp.startsWith("192.")) {
                    setupNetIp = "10.1.1.1";
                    setupNetGateway = "10.1.1.254";
                    setupNetRange = "10.1.1.2,10.1.1.21";
                    setupNetBroadcast = "10.1.1.255";
                }

                saveConfig(Utility.toMap("isonasBroadcast2",setupNetBroadcast));

                String interfaceStr = new StringBuilder()
                        .append("# UNCONFIGURED INTERFACES\n")
                        .append("# remove the above line if you edit this file\n")
                        .append("\n")
                        .append("auto lo\n")
                        .append("iface lo inet loopback \n")
                        .append("\n")
                        .append("auto eth0\n")
                        .append("iface eth0 inet static\n")
                        .append("address " + hostIp + "\n")
                        .append("netmask " + subnet + "\n")
                        .append("gateway " + gateway + "\n")
                        .append("\n")
                        .append("allow-hotplug eth1\n")
                        .append("auto eth1\n")
                        .append("iface eth1 inet static\n")
                        .append("address " + setupNetIp + "\n")
                        .append("netmask " + setupNetMask + "\n")
                        .append("gateway " + setupNetGateway + "\n").toString();
                if (!Utility.stringToFile("/uploads/interfaces", interfaceStr))
                    throw new Exception("Unable to write to the interface file!");

                String hostStr = new StringBuilder()
                        .append(setupNetIp + " \tip360.local \tip360\n")
                        .append("\n")
                        .append("127.0.0.1 \tlocalhost\n")
                        .append("127.0.1.1 \tip360.local \tip360\n")
                        .append("\n")
                        .append("# The following lines are desirable for IPv6 capable hosts\n")
                        .append("::1     localhost ip6-localhost ip6-loopback\n")
                        .append("ff02::1 ip6-allnodes\n")
                        .append("ff02::2 ip6-allrouters\n").toString();
                if (!Utility.stringToFile("/uploads/hosts", hostStr))
                    throw new Exception("Unable to write to the host file!");

                String dnsStr = new StringBuilder()
                        .append("interface=eth1\n")
                        .append("dhcp-range=" + setupNetRange+ ",1m").toString();
                //.append("address=" + setupNetIp + ",1m")
                //.append("dhcp-host=ip360," + setupNetIp + ",infinite").toString();
                if (!Utility.stringToFile("/uploads/dnsmasq.conf", dnsStr))
                    throw new Exception("Unable to write to the DnsMasq file!");

                log("Reinitializing the network adapters...");
                String response = Utility.execute(Utility.toList("/etc/init.d/networking", "restart"), null, null);
                String[] respLines = response.split("\\r?\\n");
                for(String line:respLines)
                    log(":: NET :: " + line);
                response = Utility.execute(Utility.toList("service", "dnsmasq" , "restart"), null, null);
                respLines = response.split("\\r?\\n");
                for(String line:respLines)
                    log(":: NET :: " + line);
            }
        }
        catch (Exception ex)
        {
            log(ex);
            logError("Unable to update host files!");
        }
    }


    public String getRandom(int len, int maxRepeatAllowed)
    {
        long limit = 1000000;
        if (len<2) len=2;
        else if (len>8) len=8;
        if (len==2) limit = 100;
        else if (len==3) limit = 1000l;
        else if (len==4) limit = 10000l;
        else if (len==5) limit = 100000l;
        else if (len==6) limit = 1000000l;
        else if (len==7) limit = 10000000l;
        else if (len==8) limit = 100000000l;

        while(true) {
            // Loop until a viable code is found. Don't care if it's an infinite loop

            long val = r.nextLong();
            if (val<0) val=-val;
            val = val % limit;
            String code = StringUtil.fixedLengthString(Long.toString(val),len,'0',true);
            char last = 'K';
            int lastCount = 0;
            int maxCount = 0;
            for (int i=0;i<code.length();i++)
            {
                char d = code.charAt(i);
                if (d!=last)
                {
                    last = d;
                    lastCount = 1;
                }
                else lastCount++;
                if (maxCount<lastCount) maxCount=lastCount;
            }

            if (maxCount>maxRepeatAllowed) continue;

            try {
                GenericRecord crec = db.newRecord("randomizer");
                crec.set("code",code);
                crec.set("createdDateTime",new DateTime());
                if (db.insert(crec)) return code;
            } catch (Exception e) {
            }
        }

    }

    public String getNextIpAddress()
    {
        try
        {
            GenericRecord cfg = getConfigRec();
            cfg.set("nextDoorProgrammingSchedule",new DateTime().plusSeconds(2));
            String startIpStr = cfg.getString("startIpRange");
            String endIpStr = cfg.getString("endIpRange");
            String nextIpStr = cfg.getString("nextIpAddress");
            if (startIpStr==null || endIpStr==null)
                throw new Exception("Ip Range is not configured properly!");
            startIpStr = startIpStr.trim().toLowerCase();
            endIpStr = endIpStr.trim().toLowerCase();
            long startIp = Utility.ipAddressToLong(startIpStr);
            long endIp = Utility.ipAddressToLong(endIpStr);
            long nextIp = 0;
            if (nextIpStr!=null) nextIp = Utility.ipAddressToLong(nextIpStr);

            if (startIp>=endIp)
                throw new Exception("Ip Range is not configured properly!");

            if (nextIpStr!=null) {
                // First try to find a hole in the assigned IP, recycle that unused IP if possible...

                for (long availableIp = startIp; availableIp <nextIp; availableIp++)
                    if (!assignedIpList.contains(availableIp)) {
                        assignedIpList.add(availableIp);
                        return Utility.longToIpAddress(availableIp);
                    }
            }
            // Lease a new IP Address and return it...
            if (nextIpStr==null)
            {
                saveConfig(Utility.toMap("nextIpAddress",Utility.longToIpAddress(startIp+1)));
                assignedIpList.add(startIp);
                return startIpStr;
            }
            else
            {
                if (nextIp>endIp) throw new Exception("No more Ip Address available for assignment!");
                saveConfig(Utility.toMap("nextIpAddress",Utility.longToIpAddress(nextIp+1)));
                assignedIpList.add(nextIp);
                return nextIpStr;
            }
        }
        catch (Exception ex)
        {
            log(ex);
            return null;
        }
    }






    public void processAlertsOld(GenericRecord event) throws Exception
    {
        GenericRecord s = null;
        GenericRecord a = null;
        GenericRecord u = null;
        GenericRecord d = null;
        if (event.getLong("alarmPanelId")!=null)
            a = db.selectFirst("select * from alarmPanels where alarmPanelId=?",Utility.toList(event.getLong("alarmPanelId")));
        if (event.getLong("sensorId")!=null)
            s = db.selectFirst("select * from sensors where id=?",Utility.toList(event.getLong("sensorId")));
        if (event.getLong("doorId")!=null)
            d = db.selectFirst("select * from doors where doorId=?",Utility.toList(event.getLong("doorId")));
        if (event.getLong("userId")!=null)
            u = db.selectFirst("select * from users where userId=?",Utility.toList(event.getLong("userId")));

        String et = event.getString("eventType");
        if (et!=null && et.startsWith("APZONE_") && s!=null)
        {
            // process alert for sensor events...

        }
        else if (et!=null && et.startsWith("DOOR_") && d!=null)
        {
            // process alert for door events....
            Set<Long> alertList = new HashSet<Long>();
            List<GenericRecord> daList = db.selectAll("select * from doorAlerts where id=?"
                    ,Utility.toList(d.getLong("doorId")));
            if (daList!=null)
                for(GenericRecord da:daList)
                {
                    boolean capture = false;
                    if (et.endsWith("ACTIVE") && da.getInteger("closed")==1) capture=true;
                    else if (et.endsWith("HOPEN") && da.getInteger("heldOpen")==1) capture=true;
                    else if (et.endsWith("FOPEN") && da.getInteger("forcedOpen")==1) capture=true;
                    else if (et.endsWith("OPEN") && da.getInteger("opened")==1) capture=true;
                    if (!capture) continue;

                    LocalTime now = new DateTime().toLocalTime();
                    DateTime stTime = da.getDateTime("startTime");
                    if (stTime!=null && stTime.toLocalTime().isAfter(now)) continue;
                    DateTime etTime = da.getDateTime("endTime");
                    if (etTime!=null && etTime.toLocalTime().isBefore(now)) continue;
                    alertList.add(da.getLong("alertId"));
                }
            for(Long alertId:alertList) sendAlert(alertId);
        }
    }

    public DoorStates getLastDoorState(Long doorId)
    {
        try
        {
            GenericRecord dstate = db.selectFirst("select id,eventtype from events where doorId=? and eventType like 'DOOR_%' and eventType!='DOOR_ERROR' order by createdDate desc", Utility.toList(doorId));
            if (dstate!=null)
            {
                return doorStatesMap.get(dstate.getString("eventtype"));
            }
            return DoorStates.ACTIVE;
        }
        catch (Exception ex)
        {
            log(ex);
            return DoorStates.ACTIVE;
        }
    }

    public void updateDatabaseTo253() {
        try {
            GenericRecord cfg = db.selectFirst("select * from globalconfig");
            if (cfg != null && cfg.getHeader().getIndex("hostIpAddress")==-1) {
                db.executeUpdate("ALTER TABLE globalconfig ADD COLUMN hostIpAddress VARCHAR(32) NULL  AFTER nextIpAddress",null);
                db.executeUpdate("ALTER TABLE devices ADD COLUMN firmware_version VARCHAR(32) NULL  AFTER last_config_error", null);
            }
        } catch (Exception ex) {
            log(ex);
        }
    }

    public void cleanLogs() {
        try {
            DateTime oldLogThreshold = new DateTime();
            oldLogThreshold = oldLogThreshold.minusDays(90);
            db.executeUpdate("delete from events where createdDate<?",Utility.toList(oldLogThreshold));
        } catch (Exception ex) {
            log(ex);
        }
    }



    public boolean save(GenericRecord rec)
    {
        try
        {
            db.update(rec);
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return true;
        } catch (Exception e) {
            log(e);
            return false;
        }
    }

    public boolean saveConfig(Map valueMap)
    {
        try
        {
            db.update("globalconfig",valueMap,null,null);
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return true;
        } catch (Exception e) {
            log(e);
            return false;
        }
    }

    public boolean create(GenericRecord rec)
    {
        try
        {
            db.insert(rec);
            if (!db.isSuccessful()) throw new Exception(db.getError());
            return true;
        } catch (Exception e) {
            log(e);
            return false;
        }
    }

    public synchronized boolean isAlarmRequested() {
        boolean ret = alarmRequested;
        alarmRequested=false;
        return ret;
    }



    private synchronized void setAlarmRequest()
    {
        alarmRequested=true;
    }





}
