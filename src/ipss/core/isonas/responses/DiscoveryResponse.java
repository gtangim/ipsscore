package ipss.core.isonas.responses;

import apu.util.StringUtil;

import java.nio.ByteBuffer;

/**
 * Created with IntelliJ IDEA.
 * User: mdt
 * Date: 10/9/13
 * Time: 12:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class DiscoveryResponse {

    private int firmwareVersion;
    private String deviceType;
    private String macAddress;
    private String sourceMacAddress;
    private String currentIpAddress;
    private String currentGateway;
    private boolean valid=false;
    private String error=null;

    protected String getMac(ByteBuffer source, int startIndex)        {
        String m = "";
        for (int i=startIndex;i<startIndex+6;i++)
        {
            byte b = source.get(i);
            m+= StringUtil.toHex(b, 2).toUpperCase();
            if (i!=startIndex+5) m+="-";
        }
        return m;
    }

    protected String getIp(ByteBuffer source, int startIndex){
        String m = "";
        for (int i=startIndex;i<startIndex+4;i++)
        {
            int b = (int)source.get(i) & 0xff;
            m+= Integer.toString(b);
            if (i!=startIndex+3) m+=".";
        }
        return m;
    }

    public int getFirmwareVersion() {
        return firmwareVersion;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public boolean isValid() {
        return valid;
    }

    public String getError() {
        return error;
    }

    public String toString()
    {
        if (valid) return "[DISCOVERED ISONAS:  MAC="+macAddress+", Type="+deviceType+", VER="
                +Integer.toString(firmwareVersion)+", Ip:"+currentIpAddress+", Gateway:"+currentGateway + "]";
        else return "[INVALID ISONAS DISCOVERY RESPONSE: "+error+"]";
    }
}
