package ipss.core.isonas.responses;

import apu.util.StringUtil;
import apu.util.Utility;

import java.net.DatagramPacket;
import java.nio.ByteBuffer;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 2/1/13
 * Time: 11:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class IsonasDiscoveryResponse_RC03 extends DiscoveryResponse {

    protected int firmwareVersion;
    protected String deviceType;
    protected String macAddress;
    protected String sourceMacAddress;
    protected String currentIpAddress;
    protected String currentGateway;
    protected boolean valid=false;
    protected String error=null;

    @Override
    protected String getMac(ByteBuffer source, int startIndex)
    {
        String m = "";
        for (int i=startIndex;i<startIndex+6;i++)
        {
            byte b = source.get(i);
            m+= StringUtil.toHex(b,2).toUpperCase();
            if (i!=startIndex+5) m+="-";
        }
        return m;
    }

    @Override
    protected String getIp(ByteBuffer source, int startIndex)
    {
        String m = "";
        for (int i=startIndex;i<startIndex+4;i++)
        {
            int b = (int)source.get(i) & 0xff;
            m+= Integer.toString(b);
            if (i!=startIndex+3) m+=".";
        }
        return m;
    }

    public IsonasDiscoveryResponse_RC03(ByteBuffer source)
    {
        try {
            byte[] ver = new byte[2];
            int n = source.position();
            if (source==null || (source.limit()!=40 && source.limit()!=41))
                throw new Exception("Expecting a 40 byte response, got " + Integer.toString(n)+" bytes!");
            source.rewind();
            long id = source.getInt();
            if (id!=0xf7) throw  new Exception("Could not match response header!");
            byte ignore = source.get();
            ignore = source.get();
            ver[1]=source.get();
            ver[0]=source.get();
            String firmwareStr = StringUtil.byteToHex(ver);
            firmwareVersion = Integer.parseInt(firmwareStr);
            deviceType = "" + (char)source.get();
            deviceType += (char)source.get();
            if (deviceType!=null && deviceType.equals("P3")) deviceType="IR_R3";
            else throw new Exception(("This is not a RC03 Reader!"));
            macAddress = getMac(source,24);
            currentIpAddress = getIp(source,16);
            currentGateway = getIp(source,20);
            sourceMacAddress = getMac(source,10);
            this.valid=true;
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    @Override
    public int getFirmwareVersion() {
        return firmwareVersion;
    }

    @Override
    public String getDeviceType() {
        return deviceType;
    }

    @Override
    public String getMacAddress() {
        return macAddress;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public String getError() {
        return error;
    }

    @Override
    public String toString()
    {
        if (valid) return "[DISCOVERED ISONAS:  MAC="+macAddress+", Type="+deviceType+", VER="
                +Integer.toString(firmwareVersion)+", Ip:"+currentIpAddress+", Gateway:"+currentGateway + "]";
        else return "[INVALID ISONAS DISCOVERY RESPONSE: "+error+"]";
    }
}
