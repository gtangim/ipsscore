package ipss.core.isonas.responses;

import apu.util.Utility;
import ipss.core.isonas.IsonasCommandResponse;
import ipss.core.isonas.responses.states.BadgeAcceptState;
import ipss.core.isonas.responses.states.OriginStatus;
import ipss.core.isonas.states.ResponseStatus;
import org.joda.time.DateTime;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/29/11
 * Time: 4:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class AsyncResponse {

    protected static boolean isRexReversed = false;
    protected static boolean isTamperReversed = false;

    protected DateTime timestamp;
    protected BadgeAcceptState state;
    protected OriginStatus source;
    protected boolean relayOn;
    protected boolean auxOn;
    protected boolean detectedTamper;
    protected boolean requestToExit;
    protected boolean doorOpen;
    protected long cardId;


    public AsyncResponse()
    {

    }

    public AsyncResponse(Object data) throws Exception
    {
        // Do nothing
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public BadgeAcceptState getState() {
        return state;
    }

    public OriginStatus getSource() {
        return source;
    }

    public boolean isRelayOn() {
        return relayOn;
    }

    public boolean isAuxOn() {
        return auxOn;
    }

    public boolean isDetectedTamper() {
        return detectedTamper;
    }

    public boolean isRequestToExit() {
        return requestToExit;
    }

    public boolean isDoorOpen() {
        return doorOpen;
    }

    public long getCardId() {
        return cardId;
    }

    public void setCardId(long cardId, OriginStatus source) {
        this.cardId = cardId;
        this.source = source;
    }

    @Override
    public String toString()
    {
        return "EVENT[State: " + state.toString() + ", Origin: "+source.toString()+", Relay:"
                + relayOn + ", Aux:"+auxOn+", Tamper:" + detectedTamper + ", Rex: "+requestToExit+", DoorOpen:"
            + doorOpen + ", Card: "+cardId+  "]";
    }
}
