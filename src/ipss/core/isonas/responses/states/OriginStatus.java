package ipss.core.isonas.responses.states;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/29/11
 * Time: 4:26 PM
 * To change this template use File | Settings | File Templates.
 */
public enum OriginStatus {
    NO_PROX_CARD, PROX_CARD, PROX_CARD_HID, KEYPAD, EXTERNAL
}
