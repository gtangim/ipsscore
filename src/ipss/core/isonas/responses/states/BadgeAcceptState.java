package ipss.core.isonas.responses.states;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/29/11
 * Time: 4:24 PM
 * To change this template use File | Settings | File Templates.
 */
public enum BadgeAcceptState {
    STATUS_CHANGE, ACCEPT, REJECT_NOBADGE, REJECT_NOTIMEZONE, REJECT_LOCKDOWN, REJECT_TAMPER
}
