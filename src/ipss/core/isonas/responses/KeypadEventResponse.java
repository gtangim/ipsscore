package ipss.core.isonas.responses;

import apu.util.Utility;
import ipss.core.isonas.IsonasCommandResponse;
import ipss.core.isonas.responses.states.OriginStatus;
import ipss.core.isonas.states.ResponseStatus;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 1/10/12
 * Time: 12:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeypadEventResponse {
    protected long cardId;
    protected OriginStatus source;

    public KeypadEventResponse(Object data) throws Exception
    {
        if (!(data instanceof IsonasCommandResponse)) throw new Exception("Invalid Isonas response object passed!");
        IsonasCommandResponse res = (IsonasCommandResponse) data;
        if (res.getStatus()!=ResponseStatus.OK) throw new Exception("Invalid response!");
        ByteBuffer buf = res.getResponse().order(ByteOrder.BIG_ENDIAN);
        if (res.size()!=7) throw new Exception("Invalid response!");
        buf.rewind();
        if (buf.get()!=(byte)0xff) throw new Exception("Invalid response!");
        int length = Utility.byteToInt(buf.get());
        if (length!=5) throw new Exception("Invalid Format for Keypad event!");
        int origin = Utility.byteToInt(buf.get());
        int id = buf.getInt();
        cardId = ((long)id) & 0xffffffffl;
        if (origin==0x00) source=OriginStatus.NO_PROX_CARD;
        else if (origin==0x01) source=OriginStatus.PROX_CARD;
        else if (origin==0x02) source=OriginStatus.KEYPAD;
        else if (origin==0x03) source=OriginStatus.PROX_CARD_HID;
        else if (origin==0x04) source=OriginStatus.EXTERNAL;
        else throw new Exception("Invalid origin code in the event response!");
    }

    public long getCardId() {
        return cardId;
    }

    public OriginStatus getSource() {
        return source;
    }

    public static boolean canDigest(Object data)
    {
        if (data==null) return false;
        if (!(data instanceof IsonasCommandResponse)) return false;
        IsonasCommandResponse res = (IsonasCommandResponse) data;
        if (res.getStatus()!= ResponseStatus.OK) return false;
        ByteBuffer buf = res.getResponse();
        if (res.size()!=7) return false;
        buf.rewind();
        byte cmd = buf.get();
        if (cmd!=(byte)0xff) return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "EVENT[Origin: "+source.toString()+", Keypad Input: "+cardId+  "]";
    }

}
