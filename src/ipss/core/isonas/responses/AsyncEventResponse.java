package ipss.core.isonas.responses;

import apu.util.Utility;
import ipss.core.isonas.IsonasCommandResponse;
import ipss.core.isonas.responses.states.BadgeAcceptState;
import ipss.core.isonas.responses.states.OriginStatus;
import ipss.core.isonas.states.ResponseStatus;
import org.joda.time.DateTime;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/29/11
 * Time: 4:22 PM
 * To change this template use File | Settings | File Templates.
 */
public final class AsyncEventResponse extends AsyncResponse {

    /*protected static boolean isRexReversed = false;
    protected static boolean isTamperReversed = false;

    protected DateTime timestamp;
    protected BadgeAcceptState state;
    protected OriginStatus source;
    protected boolean relayOn;
    protected boolean auxOn;
    protected boolean detectedTamper;
    protected boolean requestToExit;
    protected boolean doorOpen;
    protected long cardId;*/


    public AsyncEventResponse(BadgeAcceptState state,OriginStatus source
            , boolean relayOn, boolean auxOn, boolean detectedTamper, boolean requestToExit
            , boolean doorOpen, long cardId)
    {
        this.state=state;
        this.source=source;
        this.relayOn=relayOn;
        this.auxOn=auxOn;
        this.detectedTamper=detectedTamper;
        this.requestToExit=requestToExit;
        this.doorOpen=doorOpen;
        this.cardId=cardId;
    }

    public AsyncEventResponse(Object data) throws Exception
    {
        if (!(data instanceof IsonasCommandResponse)) throw new Exception("Invalid Isonas response object passed!");
        IsonasCommandResponse res = (IsonasCommandResponse) data;
        if (res.getStatus()!=ResponseStatus.OK)
            throw new Exception("Invalid response!");
        ByteBuffer buf = res.getResponse().order(ByteOrder.BIG_ENDIAN);
        if (res.size()!=13) throw new Exception("Invalid response!");
        buf.rewind();
        byte cmd = buf.get();
        if (cmd!=(byte)0xb5 && cmd!=(byte)0xb4) throw new Exception("Invalid response!");
        int year = Utility.byteToInt(buf.get());
        int month = Utility.byteToInt(buf.get());
        int day = Utility.byteToInt(buf.get());
        int hour = Utility.byteToInt(buf.get());
        int min = Utility.byteToInt(buf.get());
        int sec = Utility.byteToInt(buf.get());
        int acceptOrigin = Utility.byteToInt(buf.get());
        int flags = Utility.byteToInt(buf.get());
        int id = buf.getInt();
        if (year<50) year+=2000;
        else year+=1900;
        timestamp = new DateTime(year,month,day,hour,min,sec,0);
        int accept = acceptOrigin>>5;
        int origin = acceptOrigin&0x07;
        if (accept==0) state=BadgeAcceptState.STATUS_CHANGE;
        else if (accept==1) state=BadgeAcceptState.ACCEPT;
        else if (accept==2) state=BadgeAcceptState.REJECT_NOBADGE;
        else if (accept==3) state=BadgeAcceptState.REJECT_NOTIMEZONE;
        else if (accept==4) state=BadgeAcceptState.REJECT_LOCKDOWN;
        else if (accept==5) state=BadgeAcceptState.REJECT_TAMPER;
        else throw new Exception("Invalid acceptance state in the event response!");
        if (origin==0x00) source=OriginStatus.NO_PROX_CARD;
        else if (origin==0x01) source=OriginStatus.PROX_CARD;
        else if (origin==0x02) source=OriginStatus.KEYPAD;
        else if (origin==0x03) source=OriginStatus.PROX_CARD_HID;
        else if (origin==0x04) source=OriginStatus.EXTERNAL;
        else throw new Exception("Invalid origin code in the event response!");
        relayOn = ((flags & 0x80)!=0);
        auxOn = ((flags & 0x08)!=0);
        detectedTamper = ((flags & 0x04)!=0);
        requestToExit = !((flags & 0x02)!=0);
        doorOpen = !((flags & 0x01)!=0);
        cardId = ((long)id) & 0xffffffffl;
        if (isRexReversed) requestToExit=!requestToExit;
        if (isTamperReversed) detectedTamper=!detectedTamper;
    }

    public static boolean canDigest(Object data)
    {
        if (data==null) return false;
        if (!(data instanceof IsonasCommandResponse)) return false;
        IsonasCommandResponse res = (IsonasCommandResponse) data;
        if (res.getStatus()!= ResponseStatus.OK) return false;
        ByteBuffer buf = res.getResponse();
        if (res.size()!=13) return false;
        buf.rewind();
        byte cmd = buf.get();
        if (cmd!=(byte)0xb5 && cmd!=(byte)0xb4) return false;
        return true;
    }

    /*@Override
    public String toString()
    {
        return "EVENT[State: " + state.toString() + ", Origin: "+source.toString()+", Relay:"
                + relayOn + ", Aux:"+auxOn+", Tamper:" + detectedTamper + ", Rex: "+requestToExit+", DoorOpen:"
            + doorOpen + ", Card: "+cardId+  "]";
    }*/

    public static void setTamperReversed(boolean tamperReversed) {
        isTamperReversed = tamperReversed;
    }

    public static void setRexReversed(boolean rexReversed) {
        isRexReversed = rexReversed;
    }
}
