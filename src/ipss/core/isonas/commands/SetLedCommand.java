package ipss.core.isonas.commands;

import ipss.core.isonas.commands.states.LedState;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 12:59 PM
 * To change this template use File | Settings | File Templates.
 */
public final class SetLedCommand extends SingleByteCommand {
    public SetLedCommand(int led, LedState lState)
    {
        if (led<0) led=0;
        else if (led>1) led=1;
        cmd = 0x90;
        cmd = cmd | (led<<2);
        if (lState==LedState.GREEN) cmd=cmd | 1;
        else if (lState==LedState.RED) cmd=cmd | 2;
        else if (lState== LedState.AMBER) cmd=cmd | 3;
    }
}
