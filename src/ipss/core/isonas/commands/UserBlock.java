package ipss.core.isonas.commands;

import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 6/18/12
 * Time: 12:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserBlock implements Comparable<UserBlock>{
    long uid;
    int hashCode;
    long flags;
    byte typeFlag;


    public UserBlock()
    {
        uid=0;
        hashCode = 0;
        flags=0;
        typeFlag=0;
    }


    public UserBlock(Long userId, HashSet<Long> schedules, boolean isBadge)
    {
        this.uid=userId;
        Long id = uid;
        hashCode=id.hashCode();
        flags=0;
        for(Long s:schedules)
            if(s>0 && s<=32)
                if(s==1) flags=flags | 1;
                else flags=flags | (1<<(s-1));
        if (isBadge) typeFlag=0x01;
        else typeFlag=0x02;
    }

    public int compareTo(UserBlock b) {
        if (uid<b.uid) return -1;
        else if (uid==b.uid) return 0;
        else return 1;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserBlock) return uid==((UserBlock)obj).uid;
        else return false;
    }

    public int getUserId()
    {
        return (int)(uid & 0xffffffff);
    }
    public int getTimeZones()
    {
        return (int)(flags & 0xffffffff);
    }
    public byte getAccessType()
    {
        return typeFlag;
    }

    public void aggregate(UserBlock b2)
    {
        if (uid==b2.uid) flags=flags | b2.flags;
    }
}
