package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;

import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class SingleByteCommand implements PacketizedCommand {
    int cmd;
    public SingleByteCommand()
    {
        cmd=0;
    }
    public SingleByteCommand(int cmd)
    {
        this.cmd=cmd;
    }

    public ByteBuffer getBinary() {
        byte b = (byte)(cmd & 0xff);
        ByteBuffer ret = ByteBuffer.allocate(1);
        ret.put(b);
        return ret;
    }

}
