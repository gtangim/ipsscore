package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;
import org.joda.time.DateTime;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 6/15/12
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetTimeZoneIntervalCommand implements PacketizedCommand{
    int zoneId;
    int intervalId;
    boolean exceptHolidays;
    DateTime monStart;
    DateTime monEnd;
    DateTime tueStart;
    DateTime tueEnd;
    DateTime wedStart;
    DateTime wedEnd;
    DateTime thuStart;
    DateTime thuEnd;
    DateTime friStart;
    DateTime friEnd;
    DateTime satStart;
    DateTime satEnd;
    DateTime sunStart;
    DateTime sunEnd;



    public SetTimeZoneIntervalCommand(int zoneId, int intervalId, boolean exceptHolidays
        ,DateTime monStart, DateTime monEnd, DateTime tueStart, DateTime tueEnd, DateTime wedStart, DateTime wedEnd
        ,DateTime thuStart, DateTime thuEnd, DateTime friStart, DateTime friEnd, DateTime satStart, DateTime satEnd
        ,DateTime sunStart, DateTime sunEnd)
    {
        this.zoneId=zoneId;
        this.intervalId=intervalId;
        if (this.zoneId<1) zoneId=1;
        else if (this.zoneId>32) zoneId=32;
        if (this.intervalId<1) intervalId=1;
        else if (this.intervalId>32) intervalId=32;

        this.exceptHolidays=exceptHolidays;

        this.monStart   =monStart;
        this.monEnd     =monEnd;
        this.tueStart   =tueStart;
        this.tueEnd     =tueEnd;
        this.wedStart   =wedStart;
        this.wedEnd     =wedEnd;
        this.thuStart   =thuStart;
        this.thuEnd     =thuEnd;
        this.friStart   =friStart;
        this.friEnd     =friEnd;
        this.satStart   =satStart;
        this.satEnd     =satEnd;
        this.sunStart   =sunStart;
        this.sunEnd     =sunEnd;
    }

    public ByteBuffer getBinary() {
        ByteBuffer ret = ByteBuffer.allocate(46).order(ByteOrder.BIG_ENDIAN);
        ret.put((byte)0x63);
        ret.put((byte)zoneId);
        ret.put((byte)intervalId);
        int flags = 0;

        if (sunStart!=null && sunEnd!=null) flags |= 0x40;
        if (monStart!=null && monEnd!=null) flags |= 0x20;
        if (tueStart!=null && tueEnd!=null) flags |= 0x10;
        if (wedStart!=null && wedEnd!=null) flags |= 0x08;
        if (thuStart!=null && thuEnd!=null) flags |= 0x04;
        if (friStart!=null && friEnd!=null) flags |= 0x02;
        if (satStart!=null && satEnd!=null) flags |= 0x01;

        ret.put((byte)(flags & 0xff));

        putTime(ret,sunStart,true);
        putTime(ret,sunEnd,false);
        putTime(ret,monStart,true);
        putTime(ret,monEnd,false);
        putTime(ret,tueStart,true);
        putTime(ret,tueEnd,false);
        putTime(ret,wedStart,true);
        putTime(ret,wedEnd,false);
        putTime(ret,thuStart,true);
        putTime(ret,thuEnd,false);
        putTime(ret,friStart,true);
        putTime(ret,friEnd,false);
        putTime(ret,satStart,true);
        putTime(ret,satEnd,false);

        return ret;
    }



    private void putTime(ByteBuffer b, DateTime t, boolean insertHeader)
    {
        if (t!=null)
        {
            int tLinear = t.getSecondOfDay();
            int h = (tLinear>>16) & 0xff;
            if (insertHeader)
            {
                if (exceptHolidays) h=h|0x80;
                else h=h|0xc0;
            }
            b.put((byte)h);
            b.put((byte)((tLinear>>8) & 0xff));
            b.put((byte)(tLinear & 0xff));
        }
        else
        {
            b.put((byte)0);
            b.put((byte)0);
            b.put((byte)0);
        }
    }
}
