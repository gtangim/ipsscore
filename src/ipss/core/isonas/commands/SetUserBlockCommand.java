package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;
import sun.rmi.log.LogInputStream;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 6/18/12
 * Time: 11:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class SetUserBlockCommand implements PacketizedCommand {

    UserBlock[] blocks;

    protected SetUserBlockCommand(List<UserBlock> userMasterList, int startIndex)
    {
        blocks = new UserBlock[7];

        int n = userMasterList.size();
        int sz=0;
        if (startIndex>=n) return;
        sz=n-startIndex;
        if (sz>7) sz=7;
        for(int i=0;i<sz;i++) blocks[i]=userMasterList.get(startIndex+i);
    }



    public ByteBuffer getBinary() {
        ByteBuffer ret = ByteBuffer.allocate(67).order(ByteOrder.BIG_ENDIAN);
        ret.put((byte)0x51);
        ret.put((byte)0);  // Not Used Byte
        ret.put((byte)0);  // Not Used Byte
        for (int i=0;i<7;i++)
            if (blocks[i]==null)
            {
                ret.putInt(0);
                ret.putInt(0);
                ret.put((byte)0);
            }
            else
            {
                ret.putInt(blocks[i].getUserId());
                ret.putInt(blocks[i].getTimeZones());
                ret.put(blocks[i].getAccessType());
            }
        ret.put((byte)0); // Not Used Byte
        return ret;
    }


    public static List<SetUserBlockCommand> constructCommandBlocks(List<UserSchedule> userSchedules)
    {
        if (userSchedules==null || userSchedules.size()==0) return new ArrayList<SetUserBlockCommand>();
        Map<Long,UserBlock> blockMap = new HashMap<Long, UserBlock>();
        for(UserSchedule u:userSchedules)
        {
            if (u.getPin()!=null)
            {
                try
                {
                    long pin = Long.parseLong(u.getPin());
                    UserBlock pinBlock = new UserBlock(pin,u.getSchedules(),false);
                    if (blockMap.containsKey(pin)) blockMap.get(pin).aggregate(pinBlock);
                    else blockMap.put(pin,pinBlock);
                }
                catch (Exception ex){}
            }
            if (u.getBadgeNumber()!=null)
            {
                try
                {
                    long badge = Long.parseLong(u.getBadgeNumber());
                    UserBlock badgeBlock = new UserBlock(badge,u.getSchedules(),true);
                    if (blockMap.containsKey(badge)) blockMap.get(badge).aggregate(badgeBlock);
                    else blockMap.put(badge,badgeBlock);
                }
                catch (Exception ex){}
            }
        }
        List<UserBlock> masterBlock = new ArrayList<UserBlock>(blockMap.values());
        if (masterBlock==null || masterBlock.size()==0) return new ArrayList<SetUserBlockCommand>();
        Collections.sort(masterBlock);
        List<SetUserBlockCommand> ret = new ArrayList<SetUserBlockCommand>();
        int n = masterBlock.size();
        for (int i=0;i<n;i+=7)
            ret.add(new SetUserBlockCommand(masterBlock,i));
        return ret;
    }

}
