package ipss.core.isonas.commands;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 1:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetRelayCommand extends SingleByteCommand{
    public SetRelayCommand(boolean relayOn)
    {
        cmd = 0x9C;
        if (relayOn) cmd=0x9D;
    }
}
