package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;
import ipss.core.isonas.commands.states.CardIdType;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/23/11
 * Time: 4:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddCardCommand implements PacketizedCommand {
    int cardId=0;
    CardIdType cardIdType = CardIdType.PIN;
    boolean[] zones = new boolean[32];

    public AddCardCommand(long cardId, CardIdType cardIdType, boolean[] zones32)
    {
        this.cardId= (int)(cardId & 0xffffffff);
        this.cardIdType=cardIdType;
        if (zones32!=null)
        {
            int sz = 32;
            if (zones32.length<32) sz = zones32.length;
            for(int i=0;i<sz;i++) this.zones[i]=zones32[i];
        }
    }

    public AddCardCommand(long cardId, CardIdType cardIdType, Map zoneMap)
    {
        this.cardId= (int)(cardId & 0xffffffff);
        this.cardIdType=cardIdType;
        if (zoneMap!=null)
        {
            for(Object entryObj:zoneMap.entrySet())
            {
                Map.Entry entry = (Map.Entry) entryObj;
                if ((entry.getKey() instanceof Integer) && (entry.getValue() instanceof Boolean))
                {
                    int ind = (Integer)entry.getKey();
                    if (ind>=0 && ind<=31) this.zones[ind] = (Boolean)entry.getValue();
                }
                else return; // This is not a valid zone map...
            }
        }
    }



    public ByteBuffer getBinary() {
        ByteBuffer ret = ByteBuffer.allocate(10).order(ByteOrder.BIG_ENDIAN);
        ret.put((byte)0x20);
        ret.putInt(cardId);
        int mask = 0;
        for (int i=0;i<32;i++)
            if (zones[i])
                mask |= (((int)1)<<i);
        ret.putInt(mask);
        if (cardIdType==CardIdType.BADGE) ret.put((byte)0x01);
        else if (cardIdType==CardIdType.PIN) ret.put((byte)0x02);
        else ret.put((byte)0x02);
        return ret;
    }
}
