package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;
import ipss.core.isonas.commands.states.BeepAcceptState;

import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/23/11
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class SetStandaloneParamCommand implements PacketizedCommand {

    protected boolean alarmUnauthOpen=true;
    protected boolean alarmTamper=true;
    protected boolean alarmRex = false;
    protected boolean beepRex=false;
    protected boolean beepTamper=true;
    protected boolean beepReject=true;
    protected BeepAcceptState beepAccept=BeepAcceptState.SHORT;
    protected int relayTimeout = 5;
    protected int networkTimeout = 14;

    public SetStandaloneParamCommand()
    {
    }

    public SetStandaloneParamCommand(BeepAcceptState beepAccept, boolean beepReject, boolean beepTamper
            ,boolean beepRex, boolean alarmRex, boolean alarmTamper, boolean alarmUnauthOpen
            ,int relayTimeout, int networkTimeout)
    {
        if (relayTimeout<0) relayTimeout=0;
        else if (relayTimeout>255) relayTimeout=255;
        this.relayTimeout=relayTimeout;
        if (networkTimeout<0) networkTimeout=0;
        else if (networkTimeout>255) networkTimeout=255;
        this.networkTimeout=networkTimeout;
        this.beepAccept=beepAccept;
        this.beepReject=beepReject;
        this.beepTamper=beepTamper;
        this.beepRex=beepRex;
        this.alarmRex=alarmRex;
        this.alarmTamper=alarmTamper;
        this.alarmUnauthOpen=alarmUnauthOpen;
    }

    public boolean isAlarmUnauthOpen() {
        return alarmUnauthOpen;
    }

    public void setAlarmUnauthOpen(boolean alarmUnauthOpen) {
        this.alarmUnauthOpen = alarmUnauthOpen;
    }

    public boolean isAlarmTamper() {
        return alarmTamper;
    }

    public void setAlarmTamper(boolean alarmTamper) {
        this.alarmTamper = alarmTamper;
    }

    public boolean isAlarmRex() {
        return alarmRex;
    }

    public void setAlarmRex(boolean alarmRex) {
        this.alarmRex = alarmRex;
    }

    public boolean isBeepRex() {
        return beepRex;
    }

    public void setBeepRex(boolean beepRex) {
        this.beepRex = beepRex;
    }

    public boolean isBeepTamper() {
        return beepTamper;
    }

    public void setBeepTamper(boolean beepTamper) {
        this.beepTamper = beepTamper;
    }

    public boolean isBeepReject() {
        return beepReject;
    }

    public void setBeepReject(boolean beepReject) {
        this.beepReject = beepReject;
    }

    public BeepAcceptState getBeepAccept() {
        return beepAccept;
    }

    public void setBeepAccept(BeepAcceptState beepAccept) {
        this.beepAccept = beepAccept;
    }

    public int getRelayTimeout() {
        return relayTimeout;
    }

    public void setRelayTimeout(int relayTimeout) {
        if (relayTimeout<0) relayTimeout=0;
        else if (relayTimeout>255) relayTimeout=255;
        this.relayTimeout=relayTimeout;
    }

    public int getNetworkTimeout() {
        return networkTimeout;
    }

    public void setNetworkTimeout(int networkTimeout) {
        if (networkTimeout<0) networkTimeout=0;
        else if (networkTimeout>255) networkTimeout=255;
        this.networkTimeout = networkTimeout;
    }

    public ByteBuffer getBinary() {
        int cmd = 0;
        if (beepAccept==BeepAcceptState.SHORT) cmd|=0x01;
        else if (beepAccept==BeepAcceptState.LONG) cmd|=0x02;
        if (beepReject) cmd|=0x04;
        if (beepTamper) cmd|=0x08;
        if (beepRex) cmd|=0x10;
        if (alarmTamper) cmd|=0x20;
        if (alarmUnauthOpen) cmd|=0x40;
        if (alarmRex) cmd|=0x80;

        byte b = (byte)(cmd & 0xff);
        ByteBuffer ret = ByteBuffer.allocate(4);
        ret.put((byte)0x40);
        ret.put(b);
        ret.put((byte)(relayTimeout & 0xff));
        ret.put((byte)(networkTimeout & 0xff));
        return ret;
    }
}
