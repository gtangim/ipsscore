package ipss.core.isonas.commands.states;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/28/14
 * Time: 10:17 AM
 * To change this template use File | Settings | File Templates.
 */
public enum KeypadMode {
    Disabled, Local, PassThrough, KeypadSilent, Format1, Format2, KeypadBeeping
}
