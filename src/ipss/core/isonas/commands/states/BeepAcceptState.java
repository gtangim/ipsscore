package ipss.core.isonas.commands.states;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/23/11
 * Time: 11:49 AM
 * To change this template use File | Settings | File Templates.
 */
public enum BeepAcceptState {
    SILENT, SHORT, LONG
}
