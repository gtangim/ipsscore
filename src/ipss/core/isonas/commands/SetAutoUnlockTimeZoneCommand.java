package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 1/29/13
 * Time: 4:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetAutoUnlockTimeZoneCommand implements PacketizedCommand {
    int tzId = 1;
    public SetAutoUnlockTimeZoneCommand(int timeZoneId)
    {
        if (timeZoneId<1) timeZoneId=1;
        if (timeZoneId>32) timeZoneId=32;
        tzId=timeZoneId;
    }


    public ByteBuffer getBinary() {
        ByteBuffer ret = ByteBuffer.allocate(10).order(ByteOrder.BIG_ENDIAN);
        ret.put((byte)0x6a);
        byte tid = (byte)(tzId & 0xff);
        ret.put(tid);
        return ret;
    }
}
