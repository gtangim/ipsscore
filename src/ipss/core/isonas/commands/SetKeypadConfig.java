package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;
import ipss.core.isonas.commands.states.BeepAcceptState;
import ipss.core.isonas.commands.states.KeypadMode;

import java.nio.ByteBuffer;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/23/11
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class SetKeypadConfig implements PacketizedCommand {

    int mode;
    int len;
    int timeout;
    byte[] codes;

    public SetKeypadConfig(KeypadMode keypadMode, int codeLength, int codeTimeoutMS, String prefix, String suffix) {
        if (keypadMode == KeypadMode.Disabled) mode = 0;
        else if (keypadMode == KeypadMode.Local) mode = 1;
        else if (keypadMode == KeypadMode.PassThrough) mode = 2;
        else if (keypadMode == KeypadMode.KeypadSilent) mode = 3;
        else if (keypadMode == KeypadMode.Format1) mode = 4;
        else if (keypadMode == KeypadMode.Format2) mode = 5;
        else if (keypadMode == KeypadMode.KeypadBeeping) mode = 6;
        else mode = 0;

        len = codeLength;
        if (len < 0 || len > 64) len = 0;

        timeout = (codeTimeoutMS * 16)/1000;
        if (timeout < 0 || timeout >= 255) timeout = 0;

        codes = new byte[4];
        codes[0] = codes[1] = codes[2] = codes[3] = (byte) 0xff;
        if (prefix!=null && prefix.length()>0) codes[0]=(byte)prefix.charAt(0);
        if (prefix!=null && prefix.length()>1)  codes[1]=(byte)prefix.charAt(1);
        if (suffix!=null && suffix.length()>0) codes[2]=(byte)suffix.charAt(0);
        if (suffix!=null && suffix.length()>1)  codes[3]=(byte)suffix.charAt(1);
    }


    public ByteBuffer getBinary() {

        byte m = (byte) (mode & 0xff);
        byte l = (byte) (len & 0xff);
        byte t = (byte) (timeout & 0xff);

        ByteBuffer ret = ByteBuffer.allocate(8);
        ret.put((byte) 0x2b);
        ret.put(m);
        ret.put(l);
        ret.put(t);
        ret.put(codes);
        return ret;
    }
}
