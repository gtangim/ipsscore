package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;
import org.joda.time.DateTime;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 6/15/12
 * Time: 5:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetHolidaysCommand implements PacketizedCommand {

    List<DateTime> holidays;

    public SetHolidaysCommand(List<DateTime> holidays)
    {
        this.holidays = holidays;
    }


    public ByteBuffer getBinary() {
        ByteBuffer ret = ByteBuffer.allocate(33).order(ByteOrder.BIG_ENDIAN);
        ret.put((byte)0x6e);
        for(int i=0;i<16;i++)
        {
            int data = 0;
            if (i<holidays.size())
            {
                DateTime d = holidays.get(i);
                data = data | ((d.getDayOfMonth())<<1);
                data = data | ((d.getMonthOfYear())<<6);
                data = data | ((d.getYear())<<10);
            }
            ret.putShort((short)(data & 0xffff));
        }
        return ret;
    }
}
