package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;
import apu.util.Utility;
import ipss.core.isonas.commands.states.CardIdType;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 2/25/13
 * Time: 12:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChangeIpCommand implements PacketizedCommand {
    byte[] mac = new byte[2];
    int ip = 0;
    public final char[] signature="IP-SETUP".toCharArray();
    public byte[] password;
    int gateway = -1;

    public ChangeIpCommand(String macAddress,String newIp, byte[] readerPassword, String gateway) throws Exception
    {
        Long ipL = Utility.ipAddressToLong(newIp);
        if (ipL==null) throw new Exception("Invalid Ip Address: " +newIp+"!");
        ip = ipL.intValue();
        String[] macSplit = macAddress.split("-");
        if (macSplit.length!=6) throw new Exception("Invalind Mac Address: "+macAddress+"!");
        int b1 = Integer.parseInt(macSplit[4],16);
        int b2 = Integer.parseInt(macSplit[5],16);
        mac[0]=(byte)(b1&0xff);
        mac[1]=(byte)(b2&0xff);

        if (readerPassword!=null && readerPassword.length==4) password=readerPassword;
        else password = new byte[]{(byte)0xf0,(byte)0xf1,(byte)0xf2,(byte)0xf3};
        if (gateway!=null)
        {
            Long ipG = Utility.ipAddressToLong(gateway);
            if (ipG==null) throw new Exception("Invalid Gateway Ip Address: " +gateway+"!");
            this.gateway = ipG.intValue();
        }
    }

    public ByteBuffer getBinary() {
        int sz = 24;
        if (gateway!=-1) sz=32;
        ByteBuffer ret = ByteBuffer.allocate(sz).order(ByteOrder.BIG_ENDIAN);
        ret.putInt(0xfc);
        for(char c: signature)
            ret.put((byte)c);
        ret.put((byte)0);
        ret.put((byte)0);
        ret.put(mac[0]);
        ret.put(mac[1]);
        ret.putInt(ip);
        ret.put(password);
        if (gateway!=-1)
        {
            // Issue the extended configure IP command from version 25.0
            ret.put((byte)0); //DHCP_ENABLED=false
            ret.putInt(gateway);
            ret.put((byte)0); //NETMASK=false
            ret.put((byte)0); //PORT_MSB=false
            ret.put((byte)0); //PORT_LSB=false
        }
        return ret;
    }
}
