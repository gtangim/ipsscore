package ipss.core.isonas.commands;

import apu.util.StringUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 6/18/12
 * Time: 9:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserSchedule {
    protected long userId;
    protected String name;
    protected String badgeNumber;
    protected String pin;
    protected HashSet<Long> scheduleIdList;


    public UserSchedule(long userId, String userName, String bagdeNum, String pinNum)
    {
        this.userId=userId;
        this.name=userName;
        if (name==null || name.trim().equals("")) name="UNKNOWN";
        this.badgeNumber = bagdeNum;
        if(badgeNumber!=null && badgeNumber.trim().equals("")) badgeNumber=null;
        this.pin=pinNum;
        if(pin!=null && pin.trim().equals("")) pin=null;
        scheduleIdList = new HashSet<Long>();
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBadgeNumber() {
        return badgeNumber;
    }

    public void setBadgeNumber(String badgeNumber) {
        this.badgeNumber = badgeNumber;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public void addSchedule(Long ScheduleId)
    {
        scheduleIdList.add(ScheduleId);
    }
    public HashSet<Long> getSchedules()
    {
        return scheduleIdList;
    }
    public int getScheduleCount()
    {
        return scheduleIdList.size();
    }


    private StringBuilder sb=new StringBuilder();
    @Override
    public String toString() {
        sb.setLength(0);
        sb.append(StringUtil.fixedLengthString(name, 32, ' ', false));
        sb.append("     BADGE: ");
        sb.append(StringUtil.fixedLengthString(badgeNumber == null ? "N/A" : badgeNumber, 10, ' ', false));
        sb.append("     PIN: ");
        sb.append(StringUtil.fixedLengthString(pin==null?"N/A":pin,6,' ',false));
        sb.append("     SCHEDULES: ");
        if (scheduleIdList.size()==0) sb.append("N/A");
        else for(Long sid:scheduleIdList) sb.append(StringUtil.fixedLengthString(sid.toString(),5,' ',true));

        return sb.toString();
    }
}
