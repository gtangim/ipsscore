package ipss.core.isonas.commands;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 1:30 PM
 * To change this template use File | Settings | File Templates.
 */
public final class SetBuzzerCommand extends SingleByteCommand {
    public SetBuzzerCommand(boolean buzzerOn)
    {
        cmd = 0x9E;
        if (buzzerOn) cmd=0x9F;
    }
}
