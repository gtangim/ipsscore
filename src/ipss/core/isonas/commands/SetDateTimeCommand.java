package ipss.core.isonas.commands;

import apu.net.PacketizedCommand;
import org.joda.time.DateTime;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 6/15/12
 * Time: 4:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetDateTimeCommand implements PacketizedCommand {

    DateTime date;

    public SetDateTimeCommand()
    {
        date = new DateTime();
    }
    public SetDateTimeCommand(DateTime date)
    {
        this.date=date;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public ByteBuffer getBinary() {
        ByteBuffer ret = ByteBuffer.allocate(6).order(ByteOrder.BIG_ENDIAN);
        ret.put((byte)0x22);
        int dtLinear = date.getSecondOfMinute();
        dtLinear = dtLinear | ((date.getMinuteOfHour())<<6);
        dtLinear = dtLinear | ((date.getHourOfDay())<<12);
        dtLinear = dtLinear | ((date.getDayOfMonth())<<17);
        dtLinear = dtLinear | ((date.getMonthOfYear())<<22);
        dtLinear = dtLinear | ((date.getYear())<<26);
        ret.putInt(dtLinear);
        int d = date.getDayOfWeek();
        d--;
        if (d==0) d=7;
        ret.put((byte)d);
        return ret;
    }
}
