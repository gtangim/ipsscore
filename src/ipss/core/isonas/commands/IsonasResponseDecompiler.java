package ipss.core.isonas.commands;

import apu.net.IDataFilter;
import com.sun.imageio.plugins.common.I18N;
import ipss.core.isonas.IsonasCommandResponse;
//import sun.security.pkcs11.P11TlsKeyMaterialGenerator;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/15/11
 * Time: 3:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class IsonasResponseDecompiler implements IDataFilter{
    public List packets=null;

    private int findHead(byte[] buff, int length, int startIndex)
    {
        int i=startIndex;
        while(i<length-2)
        {
            if (buff[i]==0x10 && buff[i+1]==0x03 && buff[i+2]==0x01) return i;
            i++;
        }
        return -1;
    }

    private int checkSumIndex(byte[] buf, int pos)
    {
        pos--;
        if (pos<1) return -1;
        if (buf[pos]==0x01 && buf[pos-1]==0x10) pos-=2;
        else pos--;
        if (buf[pos]==0x01 && pos>0 && buf[pos-1]==0x10) pos-=2;
        else pos--;
        return pos+1;
    }
    private int getCheckSum(byte[] buf, int pos)
    {
        int b1;
        int b2;
        if (buf[pos]==0x10 && buf[pos+1]==0x01)
        {
            pos+=2;
            b1=0x10;
        }
        else
        {
            b1=((int)buf[pos]) & 0xff;
            pos++;
        }
        if (buf[pos]==0x10 && buf[pos+1]==0x01)
        {
            pos+=2;
            b2=0x10;
        }
        else
        {
            b2=((int)buf[pos]) & 0xff;
            pos++;
        }
        return (b1<<8)+b2;
    }

    public Object process(Object partialDataContainer, List inputData) throws Exception {
        packets=null;
        ByteBuffer dataContainer = (ByteBuffer)partialDataContainer;
        if (dataContainer==null) dataContainer=ByteBuffer.allocate(8192*2);
        for(Object x:inputData)
            dataContainer.put((byte[])x);
        int n = dataContainer.position();
        byte[] b = dataContainer.array();
        int headsFound=0;
        packets=new ArrayList(10);
        int pos=0;
        while(pos<n)
        {
            int i=findHead(b,n,pos);
            if (i==-1) break;
            if (i>0) pos=i;
            int i2=findHead(b,n,i+3);
            if (i2==-1) i2=n;

            int i3=checkSumIndex(b,i2);
            if (i3==-1) break;
            int checksum1=getCheckSum(b,i3);
            int checksum2=0;
            for (int k=i;k<i3;k++)
                checksum2+= ((int)b[k]) & 0xff;
            if (checksum1==checksum2)
            {
                // A Valid packet is found...
                ByteBuffer resp = ByteBuffer.allocate(i3-i-3);
                for(int k=i+3;k<i3;k++)
                    if (b[k]==0x10 && b[k+1]==0x01)
                    {
                        resp.put((byte)0x10);
                        k++;
                    }
                    else resp.put(b[k]);
                IsonasCommandResponse response = new IsonasCommandResponse(resp);
                if (packets==null) packets = new ArrayList(10);
                packets.add(response);
                pos=i2;
            }
            else if (i2!=n) pos=i2;
            else break; // This is a partial packet till the end of the stream, do nothing now, try later
        }

        if (pos==n) return null;
        else
        {
            dataContainer.rewind();
            dataContainer.put(b,pos,n-pos);
            return dataContainer;
        }
    }

    public List getMaturedObjects() {
        return packets;
    }
}
