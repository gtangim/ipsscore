package ipss.core.isonas;

import apu.db.GenericRecord;
import apu.net.*;
import apu.util.Loggable;
import apu.util.SimpleLogger;
import apu.util.Utility;
import ipss.core.isonas.commands.*;
import ipss.core.isonas.commands.states.BeepAcceptState;
import ipss.core.isonas.commands.states.KeypadMode;
import ipss.core.isonas.commands.states.LedState;
import ipss.core.isonas.responses.*;
import ipss.core.isonas.responses.states.BadgeAcceptState;
import ipss.core.isonas.responses.states.OriginStatus;
import ipss.core.isonas.states.DoorStates;
import org.joda.time.DateTime;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 9/26/11
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class Door extends Loggable implements ClientSocketListener{
    public static String RC02_TYPE = "IR_R2";
    public static String RC03_TYPE = "IR_R3";

    protected Long doorId;
    protected String macAddress;
    protected String ipAddress;
    protected String doorName;
    protected boolean successful=false;
    protected String error=null;
    protected String deviceType=null;
    protected boolean isChecked = false;
    protected boolean isTamperEnabled = false;
    protected boolean isRexEnabled = false;
    protected boolean armAlarmPanelOnUnlock = false;
    protected boolean disarmAlarmPanelOnUnlock = false;
    protected boolean unlockRequested = false;
    protected boolean rexUnlockLocalMode = false;
    protected Long rexUnlockUserId = null;
    protected Long auxUnlockUserId = null;
    protected Long unlockScheduleId = null;
    private boolean forceDoorUnlock = false;
    private ClientSocketPool pool = null;
    private int socketHandle = -9999;
    private int port=0;
    private IpssDbHelper h;
    private volatile DateTime lastRex = null;
    private volatile DateTime pollStarted = null;

    protected volatile boolean doorConnected = false;

    protected volatile DoorStates currentState=DoorStates.ACTIVE;
    protected volatile DateTime lastStateChange=new DateTime();
    protected volatile boolean stateChanged=true;
    protected Map<Long,InputCredential> credentials = new HashMap<Long, InputCredential>();
    protected volatile int invalidCredentialCount=0;
    protected volatile int validCredentialCount=0;
    protected GenericRecord lastAccessGrantedRule = null;
    protected InputCredential lastSuccessfulCredential = null;
    protected volatile boolean waitForNextCredential = false;
    
    protected List<DoorListener> eventHandlers = new LinkedList<DoorListener>();
    private BlockingQueue<IsonasCommandResponse> commandResponses = new LinkedBlockingQueue<IsonasCommandResponse>();
    private BlockingQueue<IsonasCommandResponse> pollResponses = new LinkedBlockingQueue<IsonasCommandResponse>();
    final int UNLOCK_REPEAT_TIME = 10 * 60 * 1000;

    public Door(GenericRecord dRec, ClientSocketPool pool, int port, SimpleLogger logger, IpssDbHelper helper)
    {
        super(logger);
        try {
            doorId=dRec.getLong("doorId");
            macAddress = dRec.getString("macAddress");
            ipAddress = dRec.getString("ipAddress");
            doorName = dRec.getString("doorName");
            isRexEnabled = dRec.getBoolean("rexEnabled");
            isTamperEnabled = dRec.getBoolean("tamperEnabled");
            rexUnlockUserId = dRec.getLong("rexUnlockUserId");
            rexUnlockLocalMode = dRec.getBoolean("rexUnlockLocalMode");
            auxUnlockUserId = dRec.getLong("auxUnlockUserId");
            armAlarmPanelOnUnlock = dRec.getBoolean("armAlarmPanel");
            disarmAlarmPanelOnUnlock = dRec.getBoolean("disarmAlarmPanel");
            unlockRequested = dRec.getBoolean("unlockRequested");
            unlockScheduleId = dRec.getLong("unlockScheduleId");
            this.pool = pool;
            this.port = port;
            this.h = helper;
            init();
            resetSocket();

            currentState = h.getLastDoorState(doorId);
            // On Load make sure not to unlock any door that is a security risk...
            // The door can remain in unlock state (without unlocking the door)...
            if (currentState==DoorStates.UNLOCKED || currentState==DoorStates.ACTIVE) stateChanged=false;

            successful=true;
        } catch (Exception e) {
            error="Error: Failed to initialize door! " + e.toString();
            successful=false;
        }
    }

    public boolean updated(GenericRecord dRec, GenericRecord deviceRec)
    {
        try {
            error=null;
            String oldMacAddress = macAddress;
            String oldIpAddress = ipAddress;
            macAddress = dRec.getString("macAddress");
            ipAddress = dRec.getString("ipAddress");
            doorName = dRec.getString("doorName");
            isRexEnabled = dRec.getBoolean("rexEnabled");
            isTamperEnabled = dRec.getBoolean("tamperEnabled");
            rexUnlockUserId = dRec.getLong("rexUnlockUserId");
            rexUnlockLocalMode = dRec.getBoolean("rexUnlockLocalMode");
            auxUnlockUserId = dRec.getLong("auxUnlockUserId");
            armAlarmPanelOnUnlock = dRec.getBoolean("armAlarmPanel");
            disarmAlarmPanelOnUnlock = dRec.getBoolean("disarmAlarmPanel");
            unlockRequested = dRec.getBoolean("unlockRequested");
            unlockScheduleId = dRec.getLong("unlockScheduleId");
            deviceType = deviceRec.getString("device_type_id");
            init();
            successful=true;
            if (oldMacAddress.equals(macAddress) && oldIpAddress.equals(ipAddress)) return false;
            else
            {
                resetSocket();
                return true;
            }
        } catch (Exception e) {
            error="Error: Failed to initialize door! " + e.toString();
            successful=false;
            return false;
        }
    }

    public void manuallyUnlockDoor()
    {
        lastSuccessfulCredential = null;
        changeState(DoorStates.UNLOCKED);
        manifestState();
    }

    public void checkDoorUnlockSchedule()
    {
        if (unlockScheduleId!=null)
        {
            List<GenericRecord> sList = h.getSchedules(unlockScheduleId);
            sList = h.filterBySchedule(sList);
            if (sList!=null && sList.size()>0)
            {
                if (forceDoorUnlock==false)
                    log("The Door " + this.toString() + " is now entering an unlock Schedule!");
                forceDoorUnlock=true;
            }
            else
            {
                if (forceDoorUnlock==true)
                    log("The Door " + this.toString() + " is now leaving an unlock Schedule, it will be locked!");
                forceDoorUnlock=false;
            }
        }
        else forceDoorUnlock=false;
    }

    private void init() throws Exception
    {
        if (macAddress==null) throw new Exception("Mac Address is empty!");
        macAddress=macAddress.trim().replace(":","-");
        String macAddressValidate=macAddress.toLowerCase().replace("-","");
        if (macAddressValidate.isEmpty()) throw new Exception("Mac Address is empty!");
        if (!macAddressValidate.matches("[0-9a-f]{12,12}"))  throw new Exception("Invalid Mac Address!");
        if (ipAddress==null) throw new Exception("IP Address is empty!");
        ipAddress=ipAddress.trim();
        if (ipAddress.isEmpty()) throw new Exception("IP Address is empty!");
        if (!ipAddress.matches("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}"))
            throw new Exception("Invalid IP Address!");
        if (pool==null) throw new Exception("Socket pool is null!");
    }


    public synchronized void digestEvent(long cardId, OriginStatus source)
    {
        boolean relayOn=false;
        boolean doorOpen=false;

        if (currentState==DoorStates.FORCED_OPEN || currentState==DoorStates.HELD_OPEN
                || currentState==DoorStates.OPENED) doorOpen=true;

        if (currentState==DoorStates.UNLOCKED) relayOn=true;

        AsyncEventResponse customResponse = new AsyncEventResponse(
         BadgeAcceptState.ACCEPT,source,relayOn,false,false,false,doorOpen,cardId
        );
        digestEvent(customResponse);
    }

    public synchronized void digestEvent(AsyncResponse event)
    {
        IpssConfig c = h.getConfig();
        int credentialsAdded=0;
        boolean credDuringUnlock = false;

        checkDoorUnlockSchedule();

        // Clear stale credential in the buffer...
        if (event==null)
        {
            // We only want to do this when the monitor thread calls digest for routine maintenance...
            int credentialsRemoved = 0;
            List<Long> credToRemove = new ArrayList<Long>();
            for(InputCredential credential:credentials.values())
                if (credential.getElapsedMillis()>c.getCredentialSustainInterval())
                    credToRemove.add(credential.userId);

            for(Long cKey: credToRemove)
            {
                InputCredential credential = credentials.get(cKey);
                credentials.remove(credential.getUserId());
                log("User Credential " + credential.toString() + " has been removed from the buffer (Timeout)!");
                GenericRecord user = h.getUser(credential.getUserId());
                h.logDb("User " + user.getString("username") + " timeout on multi-credential for door "
                        + this.toString(),"ACCESS_TIMEOUT",doorId
                        ,credential.getUserId(),credential.getCode(),credential.getCodeType().toString());
                if (credential.getUserId()!=0) credentialsRemoved++;
            }
            if (credentialsRemoved>0 && credentials.size()==0)
            {
                resetCredentials();
                sendAccessDeniedCommands();
            }
        }


        // Process Request to Exit
        if (event!=null && event.isRequestToExit() && isRexEnabled) lastRex=new DateTime();

        // Process code input...
        if (event!=null && event.getCardId()>0 && event.getCardId()<0xfffffff0l)
        {
            // A Card ID is presented...
            if (currentState==DoorStates.UNLOCKED)  credDuringUnlock=true;
            String cardIdS = Long.toString(event.getCardId());
            if (event.getSource()==OriginStatus.KEYPAD
                    || event.getSource()==OriginStatus.PROX_CARD
                    || event.getSource()==OriginStatus.PROX_CARD_HID)
            {
                List<GenericRecord> uList = null;

                if (event.getSource()==OriginStatus.KEYPAD)
                    uList=h.getUsersByPin(event.getCardId());
                else uList=h.getUsersByBadge(event.getCardId());
                if (uList!=null)
                {
                    if (uList.size()==0)
                    {
                        h.logDb("No user found with id:" + Long.toString(event.getCardId()), "ACCESS_DENIED"
                                , doorId, null, cardIdS, event.getSource().toString());
                        log("Invalid code entered at " + this.toString() + ". No credential match found: "+ cardIdS);
                        invalidCredentialCount++;
                        sendAccessDeniedCommands();
                    }
                    else
                        for(GenericRecord user:uList)
                        {
                            try {
                                Long userId=user.getLong("userId");
                                InputCredential ic = null;
                                if (credentials.containsKey(userId))
                                {
                                    ic = credentials.get(userId);
                                    ic.setCredential(cardIdS,event.getSource());
                                }
                                else
                                {
                                    ic = new InputCredential(doorId,user.getLong("userId")
                                            ,Long.toString(event.getCardId()),event.getSource());
                                    credentials.put(userId, ic);
                                }
                                String cMessage = this.toString() + " received credential: " + ic.toString();
                                // TODO: Log this event only in the case it is buffered until a second credential is received
                                // in a dual credential situation. I still dont know how to do this!
                                // Store added credentials to a temporary list and only log event if it didnt trigger
                                // an access grant (see below)!
                                h.logDb(cMessage,"CREDENTIAL_OK",null,ic.getUserId(),ic.getCode(),ic.getCodeType().toString());
                                log(cMessage);
                                credentialsAdded++;
                                invalidCredentialCount=0;
                            } catch (Exception e) {
                                log(e);
                                logError("Unable to add this credential because an error was encountered!");
                            }
                        }
                    if (credentialsAdded>0) validCredentialCount++;
                }
                else logError("Unable to process user credential due to an internal error!");
                // The following logic shouldnt happen and it shouldnt trigger an event!
                /*
                else h.logDb("Unknown credential was presented at door " 
                        + this.toString() + ": " + cardIdS + " using " 
                        + event.getSource().toString(),"ACCESS_DENIED",doorId,null
                        ,cardIdS,event.getSource().toString());
                        */

            }
            else logError("Unknown input type from keypad/reader: " + event.getSource().toString());
        }

        // Manage state transitions...
        boolean rex = false;
        if (isRexEnabled)
        {
            if (event!=null && event.isRequestToExit()) rex=true;
            else if (lastRex!=null && Utility.getElapsed(lastRex)<c.getRelayInterval()) rex=true;
        }

        if (currentState==DoorStates.ACTIVE)
        {
            if (forceDoorUnlock) changeState(DoorStates.UNLOCKED);
            else if (!rex && event!=null && !(event.getCardId()>0 && event.getCardId()<0xfffffff0l)
                    && event.isRelayOn()==false && event.isDoorOpen()) changeState(DoorStates.FORCED_OPEN);
            else if (rex && event!=null && event.isDoorOpen()) changeState(DoorStates.OPENED);
            else if (isTamperEnabled && event!=null && event.isDetectedTamper()) changeState(DoorStates.TAMPER);
            else if (c.isLockDownEnabled() && invalidCredentialCount>=c.getLockDownOnCount())
                changeState(DoorStates.LOCKDOWN);
            else
            {
                if (checkAccessGrant()) {
                    changeState(DoorStates.UNLOCKED);
                    resetCredentials();
                }
                else if (waitForNextCredential==true
                        && event!=null && credentialsAdded>0)
                {
                    sendDoorActiveCommands();
                    try {
                        sendBuzzerPattern(new int[]{25, 50, 25, 1});
                    } catch (Exception e) {
                        log(e);
                    }
                }
            }
        }
        else if (currentState==DoorStates.FORCED_OPEN)
        {
            if (forceDoorUnlock) changeState(DoorStates.UNLOCKED);
            else if (event!=null && event.isDoorOpen()) changeState(DoorStates.FORCED_OPEN);
            else if (Utility.getElapsed(lastStateChange)>c.getForcedOpenSustainInterval())
                changeState(DoorStates.ACTIVE);
        }
        else if (currentState==DoorStates.HELD_OPEN)
        {
            if (forceDoorUnlock) changeState(DoorStates.UNLOCKED);
            else if (event!=null && !event.isDoorOpen()) changeState(DoorStates.ACTIVE);
            else if (c.isTamperEnabled() && event!=null && event.isDetectedTamper()) changeState(DoorStates.TAMPER);
        }
        else if (currentState==DoorStates.LOCKDOWN)
        {
            if (c.isLockDownEnabled() && invalidCredentialCount>c.getLockDownOnCount())
                changeState(DoorStates.LOCKDOWN);
            else if (Utility.getElapsed(lastStateChange)>c.getLockDownInterval()) changeState(DoorStates.ACTIVE);
            else if (validCredentialCount>=c.getLockDownOffCount()) changeState(DoorStates.ACTIVE);
        }
        else if (currentState==DoorStates.OPENED)
        {
            if (forceDoorUnlock && event!=null && !event.isDoorOpen()) changeState(DoorStates.UNLOCKED);
            else if (!forceDoorUnlock && Utility.getElapsed(lastStateChange)>c.getHeldOpenInterval())
                changeState(DoorStates.HELD_OPEN);
            else if (event!=null && !event.isDoorOpen()) changeState(DoorStates.ACTIVE);
            else if (c.isTamperEnabled() && event!=null && event.isDetectedTamper()) changeState(DoorStates.TAMPER);
        }
        else if (currentState==DoorStates.TAMPER)
        {
            if (isTamperEnabled && event!=null && event.isDetectedTamper()) changeState(DoorStates.TAMPER);
            else if (Utility.getElapsed(lastStateChange)>c.getTamperSustainInterval())
                changeState(DoorStates.ACTIVE);
        }
        else if (currentState==DoorStates.UNLOCKED)
        {
            if (event!=null && event.isDoorOpen()) changeState(DoorStates.OPENED);
            else if (!forceDoorUnlock && Utility.getElapsed(lastStateChange)>c.getRelayInterval()) changeState(DoorStates.ACTIVE);
            else if (forceDoorUnlock && event!=null && !event.isRelayOn()) changeState(DoorStates.ACTIVE);
            else if (c.isTamperEnabled() && event!=null && event.isDetectedTamper()) changeState(DoorStates.TAMPER);
            else if (forceDoorUnlock && Utility.getElapsed(lastStateChange)>UNLOCK_REPEAT_TIME)
            {
                // This code segment is a fix for the issue where the isonas reader locks the door when the door
                // should be open due to a scheduled unlock timezone.
                // The side effect of that fix is that the door will keep repeating the unlock state change!

                changeState(DoorStates.ACTIVE);
                changeState(DoorStates.UNLOCKED);
            }
            else if (credDuringUnlock) {
                credDuringUnlock=false;
                sendDoorActiveCommands();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
                lastStateChange=new DateTime();
                sendUnlockCommands();
                resetCredentials();
            }
        }
        else
        {
            logError("The Door " + this.toString() + " entered an invalid state: " + currentState.toString());
            if (forceDoorUnlock) changeState(DoorStates.UNLOCKED);
            else changeState(DoorStates.ACTIVE);
        }

        // Now take action based on the state Change...
        manifestState();
    }

    protected void reflectStatus()
    {
        GenericRecord drec = h.getDoor(doorId);
        if (drec!=null)
        {
            if (isDoorConnected()) drec.set("status",currentState.toString());
            else if (!"OFFLINE".equals(drec.getString("status")))
            {
                drec.set("status","OFFLINE");
                h.logDbDoorOffline(doorId,doorName);
            }
            h.save(drec);
        }

    }



    protected void initializeDoorState()
    {
        reflectStatus();
        switch (currentState)
        {
            case ACTIVE:
                sendDoorActiveCommands();
                break;
            case FORCED_OPEN:
                sendForcedOpenCommands();
                break;
            case HELD_OPEN:
                sendHeldOpenCommands();
                break;
            case LOCKDOWN:
                sendLockdownCommands();
                break;
            case OPENED:
                sendOpenCommands();
                break;
            case TAMPER:
                sendTamperCommands();
                break;
            case UNLOCKED:
                sendUnlockCommands();
                break;
        }
    }


    public synchronized void manifestState()
    {
        reflectStatus();

        /*try
        {
            sendCommand(new SetBuzzerCommand(false));
        }
        catch(Exception ex){}
          */
        if (stateChanged)
        {
            Long userId = null;
            String msg = this.toString() + " >> State changed to: " + currentState.toString();
            String eventType = "DOOR_ACTIVE";
            switch (currentState)
            {
                case ACTIVE:
                    sendDoorActiveCommands();
                    resetCredentials();
                    eventType="DOOR_ACTIVE";
                    break;
                case FORCED_OPEN:
                    sendForcedOpenCommands();
                    resetCredentials();
                    eventType="DOOR_FOPEN";
                    break;
                case HELD_OPEN:
                    sendHeldOpenCommands();
                    eventType="DOOR_HOPEN";
                    break;
                case LOCKDOWN:
                    sendLockdownCommands();
                    resetCredentials();
                    eventType="DOOR_LOCKDOWN";
                    break;
                case OPENED:
                    sendOpenCommands();
                    eventType="DOOR_OPEN";
                    break;
                case TAMPER:
                    sendTamperCommands();
                    eventType="DOOR_TAMPER";
                    break;
                case UNLOCKED:
                    sendUnlockCommands();
                    if (lastSuccessfulCredential!=null) userId = lastSuccessfulCredential.userId;
                    resetCredentials();
                    eventType="DOOR_UNLOCK";
                    break;
                default:
                    msg=this.toString() + " >> Entered an unsupported state: " + currentState.toString();
                    //eventType="DOOR_ERROR";
                    eventType=null;
                    break;
            }

            if (eventType!=null)
                h.logDb(msg,eventType,doorId,userId,null,null);
            log(msg);
            for(DoorListener e:eventHandlers) e.onDoorStatusChange(this,currentState);
            stateChanged=false;
        }
    }

    public synchronized void sendBuzzerPattern(int[] delays) throws Exception
    {
        boolean on = false;
        for (int d:delays)
        {
            on=(!on);
            sendCommand(new SetBuzzerCommand(on));
            Thread.sleep(d);
        }
    }

    public synchronized void sendDoorActiveCommands()
    {
        try {
            //log(this.doorName +  " >> Sending Active/Lock command.");
            Thread.sleep(20);
            sendCommand(new SetBuzzerCommand(false));
            sendCommand(new ResetTamperCommand());
            sendCommand(new SetLedCommand(0, LedState.RED));
            LedState second = LedState.OFF;
            if (waitForNextCredential) second=LedState.GREEN;
            sendCommand(new SetLedCommand(1, second));
            sendCommandRetry(new SetRelayCommand(false),3);
        } catch (Exception e) {
            log(e);
        }
    }


    public synchronized void sendLockdownCommands()
    {
        try {
            Thread.sleep(20);
            sendCommand(new SetLedCommand(0, LedState.RED));
            sendCommand(new SetLedCommand(1, LedState.RED));
            sendBuzzerPattern(new int[] {50,100,50,100,50,100,50,100,50,1});
        } catch (Exception e) {
            log(e);
        }
    }

    public synchronized void sendUnlockCommands()
    {
        try {
            //log(this.doorName +  " >> Sending Unlock command. ");
            Thread.sleep(20);
            sendCommand(new SetLedCommand(0, LedState.GREEN));
            sendCommand(new SetLedCommand(1, LedState.OFF));
            sendCommandRetry(new SetRelayCommand(true),3);
            if (!forceDoorUnlock)
            {
                Thread.sleep(50);
                sendBuzzerPattern(new int[] {500,1});
            }
        } catch (Exception e) {
            log(e);
        }
    }

    public void pollReader()
    {
        try {
            if (!isDoorConnected()) return;
            if (pool.isDebug()) log(this.toString() + " >> Polling the reader....");
            pollResponses.clear();
            pollStarted = new DateTime();
            sendCommand(new SetBuzzerCommand(false));
            //sendCommand(new SetBuzzerCommand(false));
            if (forceDoorUnlock)
            {
                //log(this.doorName +  " >> Setting Relay=ON ");
                sendCommand(new SetRelayCommand(true));
            }
            else
            {
                if (this.currentState==DoorStates.UNLOCKED) {
                    //log(this.doorName +  " >> Setting Relay=ON ");
                    sendCommand(new SetRelayCommand(true));
                }
                else {
                    //log(this.doorName +  " >> Setting Relay=OFF ");
                    sendCommand(new SetRelayCommand(false));
                }
            }
            //sendCommandIgnoreResponse(new PollCommand());
        } catch (Exception e) {
            log(e);
        }
    }
    public synchronized void checkConnection()
    {
        try {
            DateTime pollStart = pollStarted;
            if (pollStart!=null && Utility.getElapsed(pollStart)>pool.getCommandTimeout()*2)
            {
                if (!pollResponses.isEmpty()){
                    pollStarted=null;
                    pollResponses.clear();
                    return;
                }
                pollStarted=null;
                pollResponses.clear();
                logWarning(this.toString() + " >> Did not receive a poll response! Aborting connection!");
                SocketInfo si = pool.get(socketHandle);
                pool.abortConnection(si.getId());
            }
        } catch (Exception e) {
            log(e);
        }
    }

    public synchronized void sendOpenCommands()
    {
        try {
            //log(this.doorName +  " >> Sending Open command. ");
            Thread.sleep(20);
            sendCommand(new SetLedCommand(0, LedState.GREEN));
            sendCommand(new SetLedCommand(1, LedState.GREEN));
            if (!forceDoorUnlock) sendCommandRetry(new SetRelayCommand(false),3);
        } catch (Exception e) {
            log(e);
        }
    }

    public synchronized void sendHeldOpenCommands()
    {
        try {
            //log(this.doorName +  " >> Sending HeldOpen command. ");
            Thread.sleep(20);
            sendCommand(new SetLedCommand(0, LedState.GREEN));
            sendCommand(new SetLedCommand(1, LedState.AMBER));
            if (!forceDoorUnlock) sendCommandRetry(new SetRelayCommand(false),3);
        } catch (Exception e) {
            log(e);
        }
    }

    public synchronized void sendForcedOpenCommands()
    {
        try {
            //log(this.doorName +  " >> Sending ForcedOpen command. ");
            Thread.sleep(20);
            sendCommand(new SetLedCommand(0, LedState.RED));
            sendCommand(new SetLedCommand(1, LedState.RED));
            sendCommandRetry(new SetRelayCommand(false),3);
            sendCommandRetry(new SetBuzzerCommand(true),3);
        } catch (Exception e) {
            log(e);
        }
    }

    public synchronized void sendTamperCommands()
    {
        try {
            //log(this.doorName +  " >> Sending Tamper command. ");
            Thread.sleep(20);
            sendCommand(new ResetTamperCommand());
            sendCommand(new SetLedCommand(0, LedState.RED));
            sendCommand(new SetLedCommand(1, LedState.AMBER));
            sendCommandRetry(new SetRelayCommand(false),3);
            sendCommandRetry(new SetBuzzerCommand(true),3);
        } catch (Exception e) {
            log(e);
        }
    }

    public synchronized void sendAccessDeniedCommands()
    {
        try {
            Thread.sleep(20);
            sendCommand(new SetLedCommand(0, LedState.AMBER));
            sendCommand(new SetLedCommand(1, LedState.OFF));
            sendBuzzerPattern(new int[] {50,200,50,1});
            sendCommand(new SetLedCommand(0, LedState.RED));
            if (currentState==DoorStates.UNLOCKED)
            {
                Thread.sleep(50);
                sendCommand(new SetLedCommand(0, LedState.GREEN));
            }
            else if (currentState==DoorStates.OPENED)
            {
                Thread.sleep(50);
                sendCommand(new SetLedCommand(0, LedState.GREEN));
                sendCommand(new SetLedCommand(1, LedState.GREEN));
            }
            resetCredentials();
        } catch (Exception e) {
            log(e);
        }
    }

    /*public synchronized void sendCommandIgnoreResponse(Object cmd) throws Exception
    {
        if (isDoorConnected())
        {
            if (pool==null)
                throw new Exception(this.toString()+" >> Socket Pool is null!");
            SocketInfo si = pool.get(socketHandle);
            commandResponses.clear();
            if (!pool.send(si.getId(),cmd))
                throw new Exception(this.toString()+" >> Unable to send command to the door reader!");
        }
        else reflectStatus();
    }*/

    public synchronized void sendCommandRetry(Object cmd, int retryCount)
    {
        boolean success=false;
        for(int i=0;i<retryCount;i++)
        {
            try
            {
                sendCommand(cmd);
                success=true;
                break;
            }
            catch (Exception ex)
            {
            }
        }
        if (!success) logWarning(toString() + " >> Unable to send "+cmd.getClass().toString()+" command!");
    }

    public synchronized void sendCommand(Object cmd) throws Exception
    {
        if (isDoorConnected())
        {
            if (pool==null)
                throw new Exception(this.toString()+" >> Socket Pool is null!");
            SocketInfo si = pool.get(socketHandle);

            commandResponses.clear();
            Thread.sleep(10);
                if (!pool.send(socketHandle, cmd))
                    throw new Exception(this.toString() + " >> Unable to send command to the door reader!");

            IsonasCommandResponse res = commandResponses.poll(pool.getCommandTimeout(), TimeUnit.MILLISECONDS);
            if (res==null) throw new Exception(this.toString()+" >> Door command timeout!");
            if (!res.isAck())
            {
                if (!(res.isOk() && res.isErrror()==false && res.isInvalid()==false
                        && res.isNak()==false && res.isReject()==false))
                    throw new Exception(this.toString() + " >> Door command did not return ACK!");
            }
        }
        //else logError(this.toString() + " >> Cannot send command, door is not connected!");
        else reflectStatus();
    }

    public synchronized void resetCredentials()
    {
        credentials.clear();
        invalidCredentialCount=0;
        validCredentialCount=0;
        waitForNextCredential=false;
    }

    public synchronized void changeState(DoorStates state)
    {
        stateChanged = (state!=currentState);
        currentState=state;
        if (stateChanged) lastStateChange=new DateTime();
    }

    public synchronized boolean checkAccessGrant()
    {
        lastAccessGrantedRule=null;
        lastSuccessfulCredential=null;
        waitForNextCredential = false;


        for(Long userId: credentials.keySet())
        {
            List<GenericRecord> permissions = h.getPermissions(userId,doorId);
            //String s = permissions.get(0).toString();
            permissions = h.filterBySchedule(permissions);
            if (permissions!=null && permissions.size()>0)
                for(GenericRecord permission:permissions)
                {
                    String ct = permission.getString("credentialType");
                    if (!"single".equals(ct)) waitForNextCredential=true;
                    int numMatching=0;
                    InputCredential userCredential= credentials.get(userId);
                    if (permission.getInteger("pin")>0 && userCredential.hasPin()) numMatching++;
                    if (permission.getInteger("card")>0 && userCredential.hasCard()) numMatching++;
                    if (permission.getInteger("bio")>0 && userCredential.hasBio()) numMatching++;
                    int requirement=1;
                    if ("dual".equals(ct)) requirement=2;
                    else if ("tripple".equals(ct)) requirement=3;
                    if (numMatching>=requirement)
                    {
                        lastAccessGrantedRule = permission;
                        lastSuccessfulCredential = userCredential;
                        GenericRecord user = h.getUser(userId);
                        // TODO: Instead of this, add the user information with the DOOR_UNLOCK event
                        /*h.logDb("Access Granted for user " + user.getString("username")
                                + "for door " + this.toString()
                                ,"ACCESS_GRANTED",doorId,userId,userCredential.getCode()
                                , userCredential.getCodeType().toString()); */
                        return true;
                    }
                }
        }

        if (credentials.size()>0)
        {
            if (!waitForNextCredential)
            {
                for(InputCredential user: credentials.values())
                {
                    GenericRecord u = h.getUser(user.getUserId());
                    String cmsg = "Rejected valid credential (permission denied) at door " + this.toString() + ": " + user.toString();
                    String msg = "Access Denied to: " + u.getString("username");
                    //h.logDb(cmsg,"CREDENTIAL_ERROR",doorId,null,user.getCode(),user.getCodeType().toString());
                    h.logDb(msg,"ACCESS_DENIED",doorId,user.getUserId(),user.getCode(),user.getCodeType().toString());
                    logWarning(msg);
                }
                sendAccessDeniedCommands();
            }
        }
        return false;
    }

    public void shutDown() {
        if (socketHandle >= 0) pool.close(socketHandle);
        socketHandle = -9999;
    }

    public synchronized void resetSocket() throws Exception
    {
        if (socketHandle >=0) pool.close(socketHandle);
        socketHandle = pool.add(ipAddress,port,this);
        if (socketHandle <0) throw new Exception("Unable to initialize socketHandle in the socketHandle pool!");
    }
    public void programStandalone(GenericRecord device, List<DateTime> holidays,
                                               List<GenericRecord> schedules, Map<Integer,Integer> scheduleMap)
    {
        if (deviceType.equals(Door.RC02_TYPE)) programStandalone_RC02(device, holidays, schedules, scheduleMap);
        else if (deviceType.equals(Door.RC03_TYPE)) programStandalone_RC03(device, holidays, schedules, scheduleMap);
        else
        {
            setDeviceState(device,"ERROR: Unknown device type!",false);
            h.save(device);
        }
    }
    private void programStandalone_RC02(GenericRecord device, List<DateTime> holidays,
                                                     List<GenericRecord> schedules, Map<Integer,Integer> scheduleMap)
    {
        log("=============================================================");
        log("Programming Door for standalone mode:"+toString());
        //h.logDbSys("Programming Door for standalone mode:"+toString());
        try
        {
            log(toString()+": Programming Standalone Parameters....");
            setupStandalone();
            log(toString() + ": Setting DateTime....");
            sendCommandRetry(new SetDateTimeCommand(), 3);
            log(toString() + ": Setting Global Parameters....");
            if (this.unlockScheduleId==null) sendCommandRetry(new DisableAutomaticUnlockTimeZoneCommand(), 3);
            sendCommandRetry(new DisableBadgeUnlockTimeZoneCommand(), 3);
            sendCommandRetry(new DisableAuxInputCommand(), 3);
            sendCommandRetry(new ClearStoredEventsCommand(), 3);
            sendCommandRetry(new ClearStoredCardsCommand(), 3);
            sendCommandRetry(new ClearTimeZonesCommand(), 3);
            log(toString() + ": Setting Holidays....");
            for(int i=0;i<holidays.size();i++)
                log("Holiday " + Integer.toString(i + 1) + ": " + holidays.get(i).toString());
            sendCommandRetry(new SetHolidaysCommand(holidays), 3);
            log(toString() + ": Setting TimeZones....");
            int ind=0;
            Integer unlockTZ = null;
            for(GenericRecord s:schedules)
            {
                SetTimeZoneIntervalCommand tcmd = new SetTimeZoneIntervalCommand(
                    ++ind,1
                    ,s.getString("exceptHolidays").toLowerCase().contains("y")
                        ,s.getDateTime("mondayStart"),s.getDateTime("mondayEnd")
                        ,s.getDateTime("tuesdayStart"),s.getDateTime("tuesdayEnd")
                        ,s.getDateTime("wednesdayStart"),s.getDateTime("wednesdayEnd")
                        ,s.getDateTime("thursdayStart"),s.getDateTime("thursdayEnd")
                        ,s.getDateTime("fridayStart"),s.getDateTime("fridayEnd")
                        ,s.getDateTime("saturdayStart"),s.getDateTime("saturdayEnd")
                        ,s.getDateTime("sundayStart"),s.getDateTime("sundayEnd")
                );
                log("Time Zone "+Integer.toString(ind)+": " + s);
                if (unlockScheduleId!=null && s.getLong("scheduleId")==unlockScheduleId)
                {
                    log("*** AUTO UNLOCK ON THIS TIME ZONE ***");
                    unlockTZ = ind;
                }
                sendCommandRetry(tcmd, 3);
            }
            if (unlockTZ!=null) sendCommandRetry(new SetAutoUnlockTimeZoneCommand(unlockTZ),3);

            log(toString() + ": Setting Users....");
            Map<Long,UserSchedule> userSchedules = h.getDoorUsersAndSchedules(doorId,scheduleMap);
            if (userSchedules==null)
            {
                logError("Unable to retrieve userList from database while programming the reader for standalone mode!");
               // h.logDb("Unable to retrieve userList from database while programming the reader for standalone mode!",
               //         "SYSTEM_ERROR");
            }
            else
            {
                // Program the users here...
                List<UserSchedule> userScheduleList = new ArrayList<UserSchedule>(userSchedules.values());
                for(UserSchedule u:userScheduleList)
                    log("Programming User: " + u);
                List<SetUserBlockCommand> cmdList = SetUserBlockCommand.constructCommandBlocks(userScheduleList);
                if (cmdList!=null)
                    for(SetUserBlockCommand cmd:cmdList) sendCommandRetry(cmd,3);
            }

            log(toString() + ": Setting Rex/Aux Input Config....");
            if (auxUnlockUserId==null) sendCommandRetry(new DisableAuxInputCommand(),3);
            else sendCommandRetry(new SetAuxInputAuthorizedAccessCommand(),3);


            initializeDoorState();
            log("FINISHED Programming Door for standalone mode:"+toString());
            DateTime now = new DateTime();
            DateTime next = new DateTime(now.getYear(), now.getMonthOfYear(),
                    now.getDayOfMonth(), 3,0,0,0).plusDays(1);
            setDeviceState(device,IsonasDiscovery.CompletedState,true);
            device.set("last_config_error",null);
            device.set("next_config_schedule",next);
            h.save(device);
            //h.logDbSys("FINISHED Programming Door for standalone mode:"+toString());
        }
        catch (Exception ex)
        {
            log(ex);
            //h.logDb("Unable to program door " + toString() + ". Reason: " + ex.toString(), "SYSTEM_ERROR");
            logError("Unable to program door " + toString());
        }
        log("=============================================================");

    }

    /**
     * Stand-alone program for RC03
     * @param holidays
     * @param schedules
     * @param scheduleMap
     */
    private void programStandalone_RC03(GenericRecord device, List<DateTime> holidays,
                                                     List<GenericRecord> schedules, Map<Integer,Integer> scheduleMap)
    {
        log("=============================================================");
        log("Programming Door for standalone mode:"+toString());
        //h.logDbSys("Programming Door for standalone mode:"+toString());
        try
        {
            log(toString()+": Programming Standalone Parameters....");
            setupStandalone();
            log(toString() + ": Setting DateTime....");
            sendCommandRetry(new SetDateTimeCommand(), 3);
            log(toString() + ": Setting Global Parameters....");
            if (this.unlockScheduleId==null) sendCommandRetry(new DisableAutomaticUnlockTimeZoneCommand(), 3);
            sendCommandRetry(new DisableBadgeUnlockTimeZoneCommand(), 3);
            sendCommandRetry(new DisableAuxInputCommand(), 3);
            sendCommandRetry(new ClearStoredEventsCommand(), 3);
            sendCommandRetry(new ClearStoredCardsCommand(), 3);
            sendCommandRetry(new ClearTimeZonesCommand(), 3);
            log(toString() + ": Setting Holidays....");
            for(int i=0;i<holidays.size();i++)
                log("Holiday " + Integer.toString(i + 1) + ": " + holidays.get(i).toString());
            sendCommandRetry(new SetHolidaysCommand(holidays), 3);
            log(toString() + ": Setting TimeZones....");
            int ind=0;
            Integer unlockTZ = null;
            for(GenericRecord s:schedules)
            {
                SetTimeZoneIntervalCommand tcmd = new SetTimeZoneIntervalCommand(
                        ++ind,1
                        ,s.getString("exceptHolidays").toLowerCase().contains("y")
                        ,s.getDateTime("mondayStart"),s.getDateTime("mondayEnd")
                        ,s.getDateTime("tuesdayStart"),s.getDateTime("tuesdayEnd")
                        ,s.getDateTime("wednesdayStart"),s.getDateTime("wednesdayEnd")
                        ,s.getDateTime("thursdayStart"),s.getDateTime("thursdayEnd")
                        ,s.getDateTime("fridayStart"),s.getDateTime("fridayEnd")
                        ,s.getDateTime("saturdayStart"),s.getDateTime("saturdayEnd")
                        ,s.getDateTime("sundayStart"),s.getDateTime("sundayEnd")
                );
                log("Time Zone "+Integer.toString(ind)+": " + s);
                if (unlockScheduleId!=null && s.getLong("scheduleId")==unlockScheduleId)
                {
                    log("*** AUTO UNLOCK ON THIS TIME ZONE ***");
                    unlockTZ = ind;
                }
                sendCommandRetry(tcmd, 3);
            }
            if (unlockTZ!=null) sendCommandRetry(new SetAutoUnlockTimeZoneCommand(unlockTZ),3);

            log(toString() + ": Setting Users....");
            Map<Long,UserSchedule> userSchedules = h.getDoorUsersAndSchedules(doorId,scheduleMap);

            if (userSchedules==null)
            {
                logError("Unable to retrieve userList from database while programming the reader for standalone mode!");
                //h.logDb("Unable to retrieve userList from database while programming the reader for standalone mode!",
                //        "SYSTEM_ERROR");
            }
            else
            {
                // Program the users here...
                List<UserSchedule> userScheduleList = new ArrayList<UserSchedule>(userSchedules.values());
                for(UserSchedule u:userScheduleList)
                    log("Programming User: " + u);
                if(this.getDeviceType().equals(RC02_TYPE)){
                    List<SetUserBlockCommand> cmdList = SetUserBlockCommand.constructCommandBlocks(userScheduleList);
                    if (cmdList!=null)
                        for(SetUserBlockCommand cmd:cmdList) sendCommandRetry(cmd,3);
                }
                else if(this.getDeviceType().equals(RC03_TYPE)){
                    List<SetUserBlockCommand_RC03> cmdList = SetUserBlockCommand_RC03.constructCommandBlocks(userScheduleList);
                    if (cmdList!=null)
                        for(SetUserBlockCommand_RC03 cmd:cmdList) sendCommandRetry(cmd,3);
                }

            }

            log(toString() + ": Setting Rex/Aux Input Config....");
            if (auxUnlockUserId==null) sendCommandRetry(new DisableAuxInputCommand(),3);
            else sendCommandRetry(new SetAuxInputAuthorizedAccessCommand(),3);


            initializeDoorState();
            log("FINISHED Programming Door for standalone mode:"+toString());
            DateTime now = new DateTime();
            DateTime next = new DateTime(now.getYear(), now.getMonthOfYear(),
                    now.getDayOfMonth(), 3,0,0,0).plusDays(1);
            setDeviceState(device,IsonasDiscovery.CompletedState,true);
            device.set("last_config_error",null);
            device.set("next_config_schedule",next);
            h.save(device);
            //h.logDbSys("FINISHED Programming Door for standalone mode:"+toString());
        }
        catch (Exception ex)
        {
            log(ex);
            //h.logDb("Unable to program door " + toString() + ". Reason: " + ex.toString(), "SYSTEM_ERROR");
            logError("Unable to program door " + toString());
        }
        log("=============================================================");

    }






    private void alterBadgeNumber(AsyncResponse event, Long userId)
    {
        GenericRecord u = null;
        try {
            if (userId==null) return;
            u = h.getUser(userId);
            if (u!=null)
            {
                String badgeStr = u.getString("badgeNum");
                if (badgeStr!=null) event.setCardId(Long.parseLong(badgeStr),OriginStatus.PROX_CARD);
            }
        } catch (Exception e) {
            log(e);
            h.logDb("ERROR: User(" + (u!=null?u.getString("userName"):"UNKNOWN")
                    + ") Badge Number is not setup properly. " + e.toString(),"SYSTEM_ERROR");
        }

    }

    public void onData(ClientSocketPool pool, SocketInfo si, Object data) {

        try {
            if (data instanceof IsonasCommandResponse){
                IsonasCommandResponse presp = (IsonasCommandResponse) data;
                pollResponses.add(presp);
                pollStarted = null;
            }

            if (AsyncEventResponse.canDigest(data))
            {
                // Need to do the following because reader forgets to send ack during an event
                commandResponses.add(IsonasCommandResponse.getAck());
                pollResponses.add(IsonasCommandResponse.getAck());
                pollStarted=null;


                AsyncEventResponse event = new AsyncEventResponse(data);
                if (event.isRequestToExit() && rexUnlockUserId!=null) alterBadgeNumber(event, rexUnlockUserId);
                else if (event.isAuxOn() && auxUnlockUserId!=null) alterBadgeNumber(event, auxUnlockUserId);
                String msg = toString() + ": " + event.toString();
                log(msg);
                //h.logDb(msg,"SYSTEM_LOG");

                digestEvent(event);

            }
            else if (AsyncEventResponse_RC03.canDigest(data))
            {
                // Need to do the following because reader forgets to send ack during an event
                commandResponses.add(IsonasCommandResponse.getAck());
                pollResponses.add(IsonasCommandResponse.getAck());
                pollStarted=null;

                AsyncEventResponse_RC03 event = new AsyncEventResponse_RC03(data);
                if (event.isRequestToExit() && rexUnlockUserId!=null) alterBadgeNumber(event, rexUnlockUserId);
                else if (event.isAuxOn() && auxUnlockUserId!=null) alterBadgeNumber(event, auxUnlockUserId);
                String msg = toString() + ": " + event.toString();
                log(msg);
                //h.logDb(msg,"SYSTEM_LOG");
                digestEvent(event);
            }
            else if (KeypadEventResponse.canDigest(data))
            {
                // Need to do the following because reader forgets to send ack during an event
                commandResponses.add(IsonasCommandResponse.getAck());
                pollResponses.add(IsonasCommandResponse.getAck());
                pollStarted=null;

                KeypadEventResponse event = new KeypadEventResponse(data);
                String msg = toString() + ": " + event.toString();
                log(msg);
                h.logDb(msg, "SYSTEM_LOG");
                digestEvent(event.getCardId(),event.getSource());
            }
            else if (data instanceof IsonasCommandResponse) {
                IsonasCommandResponse res = (IsonasCommandResponse)data;
                if (res.isAck() || res.isNak() || res.isErrror() || res.isReject()){
                    commandResponses.add((IsonasCommandResponse) data);
                }
            }
        } catch (Exception e) {
            log(e);
        }

    }

    public void onError(ClientSocketPool pool, SocketInfo si, SocketErrorType errorType) {
        String msg = this.toString() + " >> A socketHandle error has occurred: " + errorType.toString()+"!";
        //h.logDb(msg,"DOOR_ERROR",doorId,null,null,null);
        logError(msg);
    }

    private void setDeviceState(GenericRecord dev, String stat, boolean registrationOnly)
    {
        String lErr = dev.getString("last_registration_error");
        if (lErr==null || !lErr.equals(IsonasDiscovery.CompletedState)) dev.set("last_registration_error",stat);
        else if (!registrationOnly) dev.set("last_config_error",stat);
    }
    public void OnConnect(ClientSocketPool pool, SocketInfo si, boolean firstTimeConnect) {
        try {
            String msg = this.toString() + " >> Connection established with isonas reader!";
            log(msg);
            doorConnected=true;
            setupStandalone();
            initializeDoorState();
            checkDoorUnlockSchedule();
            pollReader();
            GenericRecord dev = h.getDevice(macAddress);
            if (dev==null) throw  new Exception("Invalid Door Record (no matching device record found)!");
            dev.set("device_ip_address",si.getIpAddressString());
            //setDeviceState(dev,"Connected...",true);
            h.save(dev);
        } catch (Exception e) {
            log(e);
        }
    }

    private void setupStandalone() throws Exception
    {
        IpssConfig cfg = h.getConfig();
        // The following has been readjusted due to timeout (relay locks) artifact
        //int timeout = (cfg.getReaderPollInterval() * 3) / 2;
        int timeout = cfg.getReaderPollInterval() * 2;
        boolean isForcedOpenEnabled = (cfg.getForcedOpenSustainInterval()>0);
        if (!rexUnlockLocalMode) isForcedOpenEnabled=false;
        int relaySustainInterval = cfg.getRelayInterval();
        sendCommandRetry(new SetDateTimeCommand(), 3);
        sendCommand(new ResetTamperCommand());
        sendCommand(new SetStandaloneParamCommand(BeepAcceptState.SHORT,true,isTamperEnabled,false
                ,!rexUnlockLocalMode,false,isForcedOpenEnabled,relaySustainInterval/1000,timeout/1000));
        sendCommand(new SetKeypadConfig(KeypadMode.KeypadBeeping,0,5000,null,"#"));
    }

    public void onDisconnect(ClientSocketPool pool, SocketInfo si) {
        doorConnected=false;
        String msg = this.toString() + " >> Door Has been disconnected!";
        logWarning(msg);
        reflectStatus();
        //log("Attempting to reconnect the isonal reader!");

    }

    public void onClose(ClientSocketPool pool, SocketInfo si) {
        doorConnected=false;
        String msg = this.toString() + " >> Door has been removed!";
        log(msg);
        reflectStatus();
    }


    @Override
    public String toString()
    {
        return "["+doorId.toString() + ": "+doorName+" <"+ipAddress+">]";
    }

    public Long getDoorId() {
        return doorId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getDoorName() {
        return doorName;
    }

    public String getDeviceType(){
        return deviceType;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getError() {
        return error;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isDoorConnected() {
        return doorConnected;
    }

    public boolean isArmAlarmPanelOnUnlock() {
        return armAlarmPanelOnUnlock;
    }

    public boolean isDisarmAlarmPanelOnUnlock() {
        return disarmAlarmPanelOnUnlock;
    }

    public boolean isUnlockRequested() {
        return unlockRequested;
    }

    public Long getUnlockScheduleId() {
        return unlockScheduleId;
    }

    public void addEventHandler(DoorListener eventHandler)
    {
        try {
            synchronized (eventHandlers)
            {
                eventHandlers.add(eventHandler);
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to attach event handler to " + this.toString());
        }
    }

    public void removeEventHandler(DoorListener eventHandler)
    {
        try {
            synchronized (eventHandlers)
            {
                eventHandlers.remove(eventHandler);
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to remove event handler from " + this.toString());
        }
    }

    public void removeAllEventHandlers()
    {
        try {
            synchronized (eventHandlers)
            {
                eventHandlers.clear();
            }
        } catch (Exception e) {
            log(e);
            logWarning("Unable to remove event handlers from " + this.toString());
        }
    }
}
