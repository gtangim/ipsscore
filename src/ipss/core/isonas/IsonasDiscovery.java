package ipss.core.isonas;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.Loggable;
import apu.util.SimpleLogger;
import apu.util.StringUtil;
import apu.util.Utility;
import ipss.core.isonas.commands.ChangeIpCommand;
import ipss.core.isonas.responses.DiscoveryResponse;
import ipss.core.isonas.responses.IsonasDiscoveryResponse;
import ipss.core.isonas.responses.IsonasDiscoveryResponse_RC03;
import org.joda.time.DateTime;
import sun.font.CreatedFontTracker;
/*import sun.org.mozilla.javascript.internal.NativeObject;*/

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 2/1/13
 * Time: 12:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class IsonasDiscovery extends Loggable {

    private IpssDbHelper h;
    ExecutorService worker = null;
    DatagramSocket server = null;
    String broadcast1 = null;
    String broadcast2 = null;
    public static String ConfigureState = "Configuring Reader";
    public static String ProgrammingState = "Ready To Install";
    public static String CompletedState = "Ready To Use";

    public static final int EXTEND_CONFIG_VERSION1_START = 2500;
    public static final int EXTEND_CONFIG_VERSION2_START = 5300;
    public static final int EXTEND_CONFIG_FREESCALE_LIMIT = 5000;

    public IsonasDiscovery(SimpleLogger logger, IpssDbHelper helper) throws Exception {
        super(logger);
        h = helper;
        server = new DatagramSocket(0x77fe);
        server.setSoTimeout(5000);
        server.setBroadcast(true);
        worker = Executors.newFixedThreadPool(1);
        GenericRecord cfg = h.getConfigRec();
        if (cfg!=null)
        {
            broadcast1 = cfg.getString("isonasBroadcast1");
            broadcast2 = cfg.getString("isonasBroadcast2");
        }
    }

    private void setDeviceStat(GenericRecord dev, String newStat) {
        String lErr = dev.getString("last_registration_error");
        if (lErr == null || !lErr.equals(IsonasDiscovery.CompletedState))
            dev.set("last_registration_error", newStat);
        else dev.set("last_config_error", newStat);
    }


    private void sendConfigureCommand(String deviceAddress, String deviceIp, String gateway) throws Exception
    {
        boolean success=false;
        ChangeIpCommand cipCommand = new ChangeIpCommand(deviceAddress,
                deviceIp, null, gateway);
        if (broadcast1!=null)
        {
            try {
                InetAddress broadcastIP = InetAddress.getByName(broadcast1);
                byte[] sendData = cipCommand.getBinary().array();
                DatagramPacket txPacket = new DatagramPacket(sendData, sendData.length, broadcastIP, 0x77fe);
                server.send(txPacket);
            } catch (IOException ex) {
                logWarning("Unable to broadcast reader IPConfig through address " + broadcast1 + "! Reason: " + ex.getMessage());
            }
            success=true;
        }
        if (broadcast2!=null)
        {
            try {
                InetAddress broadcastIP = InetAddress.getByName(broadcast2);
                byte[] sendData = cipCommand.getBinary().array();
                DatagramPacket txPacket = new DatagramPacket(sendData, sendData.length, broadcastIP, 0x77fe);
                server.send(txPacket);
            } catch (IOException ex) {
                logWarning("Unable to broadcast reader IPConfig through address " + broadcast2 + "! Reason: " + ex.getMessage());
            }
            success=true;
        }
        if (!success) throw  new Exception("Could not configure IP since no broadcast address is configured!");
    }

    public void configureIP() {
        GenericRecord device = null;
        try {
            h.loadAssignedIpList();
            boolean reassignIp = false;
            GenericRecord cfg = h.getConfigRec();
            if (cfg.isNull("nextIpAddress")) reassignIp = true;
            else if (cfg.isNull("hostIpAddress")) reassignIp = true;

            if (reassignIp) {
                h.saveConfig(Utility.toMap("nextIpAddress",null));
                h.db.executeUpdate("update alarmpanels set ipAddress=null", null);
                h.db.executeUpdate("update devices set device_target_ip_address=null", null);
                h.loadAssignedIpList();
                String hostIp = h.getNextIpAddress();
                h.saveConfig(Utility.toMap("hostIpAddress", hostIp));
                h.updateHostEntries();

                List<GenericRecord> apList = h.getAllAlarmPanels();
                if (apList != null)
                    for(GenericRecord ap:apList) {
                        ap.set("ipAddress", h.getNextIpAddress());
                        h.db.update(ap);
                    }
            }

            List<GenericRecord> devices = h.getDevicesByClass("ISONAS_READER");
            for (GenericRecord dev : devices) {
                device = dev;
                if (reassignIp || dev.isNull("device_target_ip_address")) {
                    GenericRecord door = h.getDoorByMac(dev.getString("device_address"));
                    String nextIp = h.getNextIpAddress();
                    dev.set("device_target_ip_address", nextIp);
                    door.set("ipAddress", nextIp);
                    setDeviceStat(dev, IsonasDiscovery.ConfigureState);
                    h.save(dev);
                    h.save(door);
                    log("******** IP ASSIGNED: " + dev.getString("device_name")+" >> "
                            + dev.getString("device_target_ip_address"));
                }
            }
            for (GenericRecord dev : devices) {
                device = dev;
                if (!dev.getString("device_target_ip_address").equals(dev.getString("device_ip_address"))) {
                    //setDeviceStat(dev, "programming IP...");
                    //h.save(dev);
                    String gateway = null;
                    String firmwareVersion = dev.getString("firmware_version");
                    boolean sendExtended = false;
                    boolean sendBasic = false;
                    gateway = cfg.getString("systemGateway");
                    int v = 0;
                    if (firmwareVersion!=null) v = Integer.parseInt(firmwareVersion);
                    if (v==0)
                    {
                        sendBasic=true;
                        sendExtended=true;
                    }
                    else if ((v>=EXTEND_CONFIG_FREESCALE_LIMIT && v>=EXTEND_CONFIG_VERSION2_START)
                            || (v<EXTEND_CONFIG_FREESCALE_LIMIT && v>=EXTEND_CONFIG_VERSION1_START))
                        sendExtended=true;
                    else
                        sendBasic=true;

                    if (StringUtil.empty(gateway)) sendExtended=false;
                    else if (sendExtended) sendConfigureCommand(dev.getString("device_address")
                            ,dev.getString("device_target_ip_address"), gateway);
                    if (sendBasic)
                    {
                        if (sendExtended) Thread.sleep(5000);
                        sendConfigureCommand(dev.getString("device_address")
                                ,dev.getString("device_target_ip_address"), null);
                    }
                    log("Configuring ip of " + dev.getString("device_name")+" to "
                            + dev.getString("device_target_ip_address"));
                }
            }
        } catch (Exception ex) {
            log(ex);
            log("IP Reconfiguration for ISONAS Readers failed!");
            if (device!=null) setDeviceStat(device,"ERROR: Ip Assignment failed! " + ex.getMessage());
        }
    }


    /*public void configureIPOld() {
        try {
            // Configure the ip addresses of isonas doors....
            List<GenericRecord> doorsToReconfigure = h.db.selectAll("select * from devices where "
                    + " device_status_id!='DEVICE_STATUS_UNREGISTERED' "
                    + " and device_status_id!='DEVICE_STATUS_ERROR' "
                    + " and device_class_id='ISONAS_READER'"
                    + " and device_target_ip_address is not null"
                    + " and device_target_ip_address!=device_ip_address");
            if (h.db.isSuccessful() && doorsToReconfigure.size() > 0) {
                log("*********** CONFIGURING ISONAS DEVICE IP ADDRESSES **************");
                for (GenericRecord dev : doorsToReconfigure) {
                    ChangeIpCommand cipCommand = new ChangeIpCommand(dev.getString("device_address"),
                            dev.getString("device_target_ip_address"), null);
                    InetAddress broadcastIP = InetAddress.getByName("255.255.255.255");
                    byte[] sendData = cipCommand.getBinary().array();
                    DatagramPacket txPacket = new DatagramPacket(sendData, sendData.length, broadcastIP, 0x77fe);
                    server.send(txPacket);
                }
            } else if (!h.db.isSuccessful()) throw new Exception(h.db.getError());
        } catch (Exception ex) {
            log(ex);
            log("IP Reconfiguration for ISONAS Readers failed!");
            //logDbSys(ex);
        }
    }*/


    public void seek() {
        GenericRecord cfg;
        try {
            synchronized (this) {
                /*Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
                while (interfaces.hasMoreElements()) {
                    NetworkInterface networkInterface = interfaces.nextElement();
                    if (networkInterface.isLoopback())
                    continue;    // Don't want to broadcast to the loopback interface
                    for (InterfaceAddress interfaceAddress :
                    networkInterface.getInterfaceAddresses()) {
                        InetAddress broadcast = interfaceAddress.getBroadcast();
                        if (broadcast != null)
                        {
                            log ("Found Broadcast address: " + broadcast.toString());
                            broadcastIP = broadcast;
                        }
                    }
                } */

                log("*********** DISCOVERING ISONAS DEVICES **************");
                h.clearUnregisteredDevices("ISONAS_READER");
                /*h.resetConfig();
                cfg = h.getConfigRec();
                cfg.set("requestError", null);
                h.save(cfg);*/
                h.saveConfig(Utility.toMap("requestError", null));
                final IsonasDiscovery thisObject = this;
                final DatagramSocket serverSocket = server;
                byte[] sendData = {0, 0, 0, (byte) 0xf6};
                //InetAddress broadcastIP = InetAddress.getByName("255.255.255.255");
                cfg = h.getConfigRec();
                if (cfg!=null)
                {
                    broadcast1 = cfg.getString("isonasBroadcast1");
                    broadcast2 = cfg.getString("isonasBroadcast2");
                }

                boolean success=false;

                if (broadcast1!=null)
                {
                    try {
                        log("Broadcasting to "+broadcast1);
                        InetAddress broadcastIP = InetAddress.getByName(broadcast1);
                        DatagramPacket txPacket = new DatagramPacket(sendData, sendData.length, broadcastIP, 0x77fe);
                        serverSocket.send(txPacket);
                    } catch (IOException ex) {
                        logWarning("Unable to broadcast reader discovery through address " + broadcast1 + "! Reason: " + ex.getMessage());
                    }
                    success=true;
                }

                if (broadcast2!=null)
                {
                    try {
                        log("Broadcasting to "+broadcast2);
                        InetAddress broadcastIP = InetAddress.getByName(broadcast2);
                        DatagramPacket txPacket = new DatagramPacket(sendData, sendData.length, broadcastIP, 0x77fe);
                        serverSocket.send(txPacket);
                    } catch (IOException ex) {
                        logWarning("Unable to broadcast reader discovery through address " + broadcast2 + "! Reason: " + ex.getMessage());
                    }
                    success=true;
                }

                if (!success)
                {
                    logWarning("Unable to discover Isonas devices because no broadcast address is configured!");
                    return;
                }

                worker.execute(new Runnable() {
                    public void run() {
                        // Process discovery responses here
                        try {
                            synchronized (thisObject) {
                                log("Listening for Isonas devices...");
                                while (true) {
                                    try {
                                        byte[] rxData = new byte[100];
                                        DatagramPacket rxPacket = new DatagramPacket(rxData, rxData.length);
                                        serverSocket.receive(rxPacket);
                                        log("UDP.RX: " + StringUtil.byteToHexSep(rxData, 0
                                                , rxPacket.getLength(), "[",",","]"));
                                        ByteBuffer dataBuffer = ByteBuffer.wrap(rxData, 0, rxPacket.getLength());
                                        IsonasDiscoveryResponse response = new IsonasDiscoveryResponse(dataBuffer);
                                        IsonasDiscoveryResponse_RC03 response_rc03 = new IsonasDiscoveryResponse_RC03(
                                                dataBuffer);
                                        if (response.isValid())
                                            processReceivedDiscoveryResponse(response, rxPacket.getAddress());
                                        else if (response_rc03.isValid())
                                            processReceivedDiscoveryResponse(response_rc03
                                                    , rxPacket.getAddress());
                                    }
                                    catch (IOException iox)
                                    {
                                        log("Finished listening for Isonas devices");
                                        /*GenericRecord cfg = h.getConfigRec();
                                        cfg.set("requestError", null);
                                        cfg.set("scanDevices", false);
                                        h.save(cfg);*/
                                        h.saveConfig(Utility.toMap("requestError", null,"scanDevices", false));
                                        if (h.hasReadersToConfigure())
                                            worker.execute((new Runnable() {
                                                public void run() {
                                                    try {
                                                        configureIP();
                                                    } catch (Exception e) {
                                                        log(e);
                                                    }
                                                }
                                            }) );
                                        return;
                                    }
                                    catch (Exception ex) {
                                        log(ex);
                                        log("Device Discovery failed!");
                                        /*GenericRecord cfg = h.getConfigRec();
                                        cfg.set("requestError", "ERROR: Device Discovery Failed! " + ex.getMessage());
                                        cfg.set("scanDevices", false);
                                        h.save(cfg);*/
                                        h.saveConfig(Utility.toMap("requestError"
                                                , "ERROR: Device Discovery Failed! " + ex.getMessage()
                                                ,"scanDevices", false));
                                        return;
                                    }
                                }
                            }
                        } catch (Exception e) {
                            log(e);
                            logError("Device discovery Failed!");
                            /*GenericRecord cfg = h.getConfigRec();
                            cfg.set("requestError","ERROR: Device Discovery Failed! " + e.getMessage());
                            cfg.set("scanDevices",false);
                            h.save(cfg);*/
                            h.saveConfig(Utility.toMap("requestError"
                                    , "ERROR: Device Discovery Failed! " + e.getMessage()
                                    ,"scanDevices", false));
                        }

                    }
                });
            }

        } catch (Exception ex) {
            //logDbSys(ex);
            log(ex);
            logError("Device discovery Failed!");
            /*cfg = h.getConfigRec();
            cfg.set("requestError","ERROR: Device Discovery Failed! " + ex.getMessage());
            cfg.set("scanDevices",false);
            h.save(cfg);*/
            h.saveConfig(Utility.toMap("requestError"
                    , "ERROR: Device Discovery Failed! " + ex.getMessage()
                    ,"scanDevices", false));
        }

        log("*********** FINISHED ISONAS DEVICE DISCOVERY **************");

    }

    protected void processReceivedDiscoveryResponse(DiscoveryResponse response, InetAddress ipAddress)
    {
        String actualDeviceIp = Utility.formatIPAddress(ipAddress);
        log("Detected Device:   "  + actualDeviceIp + "  >>>  " + response);
        GenericRecord deviceRec = h.getDevice(response.getMacAddress());
        if (deviceRec!=null)
        {
            GenericRecord doorRec = h.getDoorByMac(response.getMacAddress());
            deviceRec.set("device_ip_address",actualDeviceIp);
            deviceRec.set("device_type_id",response.getDeviceType());
            if (response.getFirmwareVersion()!=0)
                deviceRec.set("firmware_version",Integer.toString(response.getFirmwareVersion()));
            String lerr = deviceRec.getString("last_registration_error");
            String dtIp = deviceRec.getString("device_target_ip_address");
            if (lerr!=null && dtIp!=null && lerr.equals(IsonasDiscovery.ConfigureState)
                    && dtIp.equals(actualDeviceIp))
                deviceRec.set("last_registration_error",IsonasDiscovery.ProgrammingState);
            h.save(deviceRec);
        }
        else h.createDevice(response.getMacAddress(), "ISONAS_READER",response.getDeviceType()
                , actualDeviceIp, response.getFirmwareVersion());
    }


    /*protected void processReceivedDiscoveryResponseOld(DiscoveryResponse response, InetAddress ipAddress
            , Map<String, Boolean> conflictIps) {
        String actualDeviceIp = Utility.formatIPAddress(ipAddress);
        if (!conflictIps.containsKey(actualDeviceIp))
            conflictIps.put(actualDeviceIp, false);
        else conflictIps.put(actualDeviceIp, true);
        log(ipAddress + "  >>>  " + response);
        GenericRecord doorRec = h.getDoorByMac(response.getMacAddress());
        GenericRecord deviceRec = h.getDevice(response.getMacAddress());
        if (doorRec != null && deviceRec != null) {
            // Existing Active Device with a door record...
            deviceRec.set("device_ip_address", actualDeviceIp);
            doorRec.set("ipAddress", actualDeviceIp);

            deviceRec.set("device_name", doorRec.getString("doorName"));
            if ("DEVICE_STATUS_UNREGISTERED".equals(
                    deviceRec.getString("device_status_id"))) {
                deviceRec.set("device_status_id", "DEVICE_STATUS_REGISTERED");
                deviceRec.set("last_registration_error", null);
            }

            String oldTargetIp = deviceRec.getString("device_target_ip_address");

            IpssConfig cfg = h.getConfig();
            if (!cfg.isIpValid(actualDeviceIp)
                    && (oldTargetIp == null || !cfg.isIpValid(oldTargetIp)))
                deviceRec.set("device_target_ip_address", cfg.getNextIpAddress(h));

            h.save(deviceRec);
            h.save(doorRec);
        } else if (doorRec != null && deviceRec == null
                && doorRec.getString("macAddress") != null
                && !doorRec.getString("macAddress").trim().equals("")) {
            // Existing door but newly discovered device
            deviceRec = h.db.newRecord("devices");
            deviceRec.set("device_address", doorRec.getString("macAddress"));
            deviceRec.set("device_class_id", "ISONAS_READER");
            deviceRec.set("device_type_id", response.getDeviceType());
            deviceRec.set("device_ip_address", actualDeviceIp);
            doorRec.set("ipAddress", actualDeviceIp);
            deviceRec.set("device_name", doorRec.getString("doorName"));
            deviceRec.set("device_status_id", "DEVICE_STATUS_REGISTERED");
            deviceRec.set("last_registration_error", null);

            if (!h.getConfig().isIpValid(actualDeviceIp))
                deviceRec.set("device_target_ip_address"
                        , h.getConfig().getNextIpAddress(h));
            h.create(deviceRec);
            h.save(doorRec);
        } else if (doorRec == null && deviceRec == null) {
            // Just Detected a New Device....
            deviceRec = h.db.newRecord("devices");
            deviceRec.set("device_address", response.getMacAddress());
            deviceRec.set("device_class_id", "ISONAS_READER");
            deviceRec.set("device_ip_address", actualDeviceIp);
            deviceRec.set("device_status_id", "DEVICE_STATUS_UNREGISTERED");
            deviceRec.set("last_registration_error", null);
            deviceRec.set("device_type_id", response.getDeviceType());
            //if (!h.getConfig().isIpValid(actualDeviceIp)
            //        || (conflictIps.containsKey(actualDeviceIp)
            //        && conflictIps.get(actualDeviceIp)==true))
            deviceRec.set("device_target_ip_address"
                    , h.getConfig().getNextIpAddress(h));
            //else
            //    deviceRec.set("device_target_ip_address",actualDeviceIp);
            h.create(deviceRec);
        } else if (doorRec == null && deviceRec != null) {
            // Device is no longer registered...
            if ("DEVICE_STATUS_MAPPED".equals(deviceRec.getString("device_status_id"))) {
                // New Map request found...
                // create the record so that the door can be connected and programmed!
                String dName = deviceRec.getString("device_name");
                if (dName == null || dName.trim().equals("")) {
                    deviceRec.set("device_status_id", "DEVICE_STATUS_ERROR");
                    deviceRec.set("last_registration_error"
                            , "Error: Cannot register device because device name is empty!");
                    h.save(deviceRec);
                } else {
                    String ipDev = deviceRec.getString("device_target_ip_address");
                    if (ipDev == null || ipDev.trim().equals(""))
                        ipDev = deviceRec.getString("device_ip_address");
                    Long ipL = Utility.ipAddressToLong(ipDev);
                    if (ipL == null) {
                        deviceRec.set("device_status_id", "DEVICE_STATUS_ERROR");
                        deviceRec.set("last_registration_error"
                                , "Error: Invalid ip address: '" + ipDev + "'!");
                        h.save(deviceRec);
                    } else {
                        Long doorId = h.getNextId("doors", "doorId");
                        if (doorId == null) {
                            deviceRec.set("device_status_id", "DEVICE_STATUS_ERROR");
                            deviceRec.set("last_registration_error"
                                    , "Error: Unable to create door record!");
                            h.save(deviceRec);
                        } else {
                            doorRec = h.db.newRecord("doors");
                            doorRec.set("doorId", doorId);
                            doorRec.set("macAddress"
                                    , deviceRec.getString("device_address"));
                            doorRec.set("ipAddress", ipDev);
                            doorRec.set("doorName", dName);
                            doorRec.set("status", "OFFLINE");
                            doorRec.set("tamperEnabled", 0);
                            doorRec.set("rexEnabled", 0);
                            doorRec.set("armAlarmPanel", 0);
                            doorRec.set("disarmAlarmPanel", 0);
                            doorRec.set("unlockRequested", 0);
                            if (!h.create(doorRec)) {
                                deviceRec.set("device_status_id", "DEVICE_STATUS_ERROR");
                                deviceRec.set("last_registration_error"
                                        , "Error: Unable to create door record!");
                                h.save(deviceRec);
                            } else log("New Door created: " + dName);
                        }


                    }

                }

            } else {
                deviceRec.set("device_type_id", response.getDeviceType());
                deviceRec.set("device_name", null);
                deviceRec.set("device_status_id", "DEVICE_STATUS_UNREGISTERED");
                deviceRec.set("last_registration_error", null);
                h.save(deviceRec);
            }
        }

    }   */


    public void close() {
        server.close();
    }


    // ********** MAIN APPLICATION THREAD **********

    /*public void logDbSys(Exception ex)
    {
        log(ex);
        if (h!=null) h.logDb("Error: Failed to perform device discovery! " + ex.toString(),"SYSTEM_ERROR");
    }*/


}
