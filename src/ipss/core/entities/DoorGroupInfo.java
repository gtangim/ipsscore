package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 5/16/14
 * Time: 4:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class DoorGroupInfo {
    public int DoorGroupId;
    public String DoorGroupName;
    public String GroupDescription;
    public List<Integer> Members;



    public void validate() throws Exception
    {
        if (DoorGroupId<=0) fail();
        //if (DoorGroupId<10000 || DoorGroupId>20000) fail();
        if (StringUtil.empty(DoorGroupName)) fail();
        if (Members==null) Members=new ArrayList<Integer>();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Door-Group Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = db.newRecord("doorgroups");
        r.set("doorGroupId",DoorGroupId);
        r.set("doorGroupName",DoorGroupName);
        r.set("description",GroupDescription);
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
        for(int x: Members)
        {
            GenericRecord m = db.newRecord("doorgroupassociations");
            m.set("doorId",x);
            m.set("doorGroupId",DoorGroupId);
            db.insert(m);
            if (!db.isSuccessful()) throw new Exception(db.getError());
        }
    }


}
