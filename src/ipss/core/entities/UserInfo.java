package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;
import apu.util.Utility;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 5/16/14
 * Time: 4:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserInfo {
    public int UserId;
    public String UserName;
    public String BadgeNumber;
    public String PinNumber;
    public String EmployeeId;
    public String BadgePrintedNumber;
    public Boolean External;



    public void validate() throws Exception
    {
        if (UserId<0 || UserId>=10000) fail();
        if (StringUtil.empty(UserName)) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid User Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        if (UserId==0) return;
        if (UserId==1) return;
        GenericRecord r = db.newRecord("users");
        r.set("userId",UserId);
        r.set("userName",UserName);
        r.set("badgeNum",BadgeNumber);
        r.set("pinNum",PinNumber);
        r.set("EmployeeId",EmployeeId);
        r.set("badgePrintedNum",BadgePrintedNumber);
        r.set("external",External);
        r.set("active",true);
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }


}
