package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;
import org.joda.time.DateTime;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 12:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class SensorInfo {

    public int Id;
    public String SensorName;
    public String SensorType;
    public int ZoneNumber;
    public int AlarmPanelId;

    public void validate() throws Exception
    {
        if (Id<=0) fail();
        if (StringUtil.empty(SensorName)) fail();
        if (StringUtil.empty(SensorType)) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Alarm-Panel-Sensor Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = db.newRecord("sensors");
        r.set("id",Id);
        r.set("sensorName",SensorName);
        r.set("sensorType",SensorType);
        r.set("zoneNumber",ZoneNumber);
        r.set("alarmPanelId",AlarmPanelId);
        r.set("status","OFFLINE");
        r.set("active",true);

        /*GenericRecord d = db.newRecord("devices");
        d.set("device_address",getAddress((long)AlarmPanelId,(long)ZoneNumber));
        d.set("device_class_id","AP_SENSOR");
        d.set("device_type_id",SensorType);
        d.set("device_name",SensorName);
        d.set("device_status_id","DEVICE_STATUS_REGISTERED");*/

        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
        /*db.insert(d);
        if (!db.isSuccessful()) throw new Exception(db.getError());*/
    }

    private String getAddress(Long apId, Long zoneId)
    {
        if (apId==null || zoneId==null) return null;
        String id = "AP:"+apId.toString()+"-"+zoneId.toString();
        return id;
    }


}
