package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 12:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class PermissionInfo {
    public int ScheduleId;
    public int UserId;
    public int DoorId;
    public Boolean Pin;
    public Boolean Card;
    public Boolean Bio;
    public String CredentialType;

    public void validate() throws Exception
    {
        if (ScheduleId<0 || UserId<0 || DoorId<0) fail();
        if (StringUtil.empty(CredentialType)) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Access Permission Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = db.newRecord("permissions");
        r.set("scheduleId",ScheduleId);
        r.set("userId",UserId);
        r.set("doorId",DoorId);
        r.set("pin",Pin);
        r.set("card",Card);
        r.set("Bio",Bio);
        r.set("credentialType",CredentialType);
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }

}
