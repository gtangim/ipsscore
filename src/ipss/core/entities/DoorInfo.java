package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;
import org.joda.time.DateTime;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 12:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class DoorInfo {
    public int DoorId;
    public String MacAddress;
    public String DoorName;
    public String DeviceTypeId;
    public Boolean TamperEnabled;
    public Boolean RexEnabled;
    public Integer RexUnlockUserId;
    public Boolean RexUnlockLocalMode;
    public Integer AuxUnlockUserId;
    public Boolean ArmAlarmPanel;
    public Boolean DisarmAlarmPanel;
    public Integer UnlockScheduleId;


    public void validate() throws Exception
    {
        if (DoorId<=0) fail();
        if (StringUtil.empty(MacAddress)) fail();
        if (StringUtil.empty(DoorName)) fail();
        if (StringUtil.empty(DeviceTypeId)) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Isonas-Reader Data!");
    }


    public void save(GenericDB db) throws Exception
    {
        if (TamperEnabled==null) TamperEnabled=false;
        if (RexEnabled==null) RexEnabled=false;
        if (RexUnlockLocalMode==null) RexUnlockLocalMode=false;
        if (ArmAlarmPanel==null) ArmAlarmPanel=false;
        if (DisarmAlarmPanel==null) DisarmAlarmPanel=false;

        GenericRecord r = db.newRecord("doors");
        r.set("doorId",DoorId);
        r.set("macAddress",MacAddress);
        r.set("ipAddress",null);
        r.set("doorName",DoorName);
        r.set("status","OFFLINE");
        r.set("tamperEnabled",TamperEnabled);
        r.set("rexEnabled",RexEnabled);
        r.set("rexUnlockUserId",RexUnlockUserId);
        r.set("RexUnlockLocalMode",RexUnlockLocalMode);
        r.set("auxUnlockUserId",AuxUnlockUserId);
        r.set("armAlarmPanel",ArmAlarmPanel);
        r.set("disarmAlarmPanel",DisarmAlarmPanel);
        r.set("unlockScheduleId",UnlockScheduleId);
        r.set("unlockRequested",false);
        r.set("active",true);

        GenericRecord d = db.newRecord("devices");
        d.set("device_address",MacAddress);
        d.set("device_class_id","ISONAS_READER");
        d.set("device_type_id",DeviceTypeId);
        d.set("device_name",DoorName);
        d.set("device_status_id","DEVICE_STATUS_REGISTERED");
        d.set("next_config_schedule",new DateTime());

        db.insert(r);
        if (!db.isSuccessful())
            throw new Exception(db.getError());
        db.insert(d);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }

}
