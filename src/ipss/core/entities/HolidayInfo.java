package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;
import org.joda.time.DateTime;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class HolidayInfo {
    public int Id;
    public DateTime HolidayDate;
    public String NameOfHoliday;
    public String Description;

    public void validate() throws Exception
    {
        if (Id<=0) fail();
        if (StringUtil.empty(NameOfHoliday)) fail();
        if (HolidayDate==null) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Holiday Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = db.newRecord("holidays");
        r.set("id",Id);
        r.set("holidayDate",HolidayDate);
        r.set("nameOfHoliday",NameOfHoliday);
        r.set("description",Description);
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }


}
