package ipss.core.entities;

import apu.db.GenericDB;
import apu.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 1:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class IP360Data {
    public GlobalInfo Settings;
    public List<AlarmPanelInfo> AlarmPanels;
    public List<CameraInfo> Cameras;
    public List<UserInfo> Users;
    public List<UserGroupInfo> UserGroups;
    public List<PermissionInfo> Permissions;
    public List<UserLoginInfo> UserLogins;
    public List<DoorInfo> Doors;
    public List<DoorGroupInfo> DoorGroups;
    public List<ScheduleInfo> Schedules;
    public List<HolidayInfo> Holidays;
    public List<SensorInfo> Sensors;
    public List<XBeeSensorInfo> XBeeSensors;
    public List<AlertInfo> Alerts;
    public List<SmsCarrierInfo> SmsCarriers;

    public void validate() throws Exception
    {
        Settings.validate();
        for(int i=0;i<AlarmPanels.size();i++) AlarmPanels.get(i).validate();
        for(int i=0;i<Cameras.size();i++) Cameras.get(i).validate();
        for(int i=0;i<Users.size();i++) Users.get(i).validate();
        for(int i=0;i<UserGroups.size();i++) UserGroups.get(i).validate();
        for(int i=0;i<Permissions.size();i++) Permissions.get(i).validate();
        for(int i=0;i<UserLogins.size();i++) UserLogins.get(i).validate();
        for(int i=0;i<Doors.size();i++) Doors.get(i).validate();
        for(int i=0;i<DoorGroups.size();i++) DoorGroups.get(i).validate();
        for(int i=0;i<Schedules.size();i++) Schedules.get(i).validate();
        for(int i=0;i<Holidays.size();i++) Holidays.get(i).validate();
        for(int i=0;i<Sensors.size();i++) Sensors.get(i).validate();
        for(int i=0;i<XBeeSensors.size();i++) XBeeSensors.get(i).validate();
        for(int i=0;i<Alerts.size();i++) Alerts.get(i).validate();
        for(int i=0;i<SmsCarriers.size();i++) SmsCarriers.get(i).validate();
    }

    public void save(GenericDB db) throws Exception
    {
        Settings.save(db);
        for(UserLoginInfo x : UserLogins) x.save(db);
        for(SmsCarrierInfo x : SmsCarriers) x.save(db);
        for(HolidayInfo x : Holidays) x.save(db);
        for(AlarmPanelInfo x : AlarmPanels) x.save(db);
        for(CameraInfo x : Cameras) x.save(db);
        for(SensorInfo x : Sensors) x.save(db);
        for(XBeeSensorInfo x : XBeeSensors) x.save(db);



        for(ScheduleInfo x : Schedules) x.save(db);
        for(UserInfo x : Users) x.save(db);
        for(UserGroupInfo x : UserGroups) x.save(db);
        for(DoorInfo x : Doors) x.save(db);
        for(DoorGroupInfo x : DoorGroups) x.save(db);
        for (PermissionInfo x : Permissions) x.save(db);

        for(AlertInfo x: Alerts) x.save(db);

    }


}
