package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;
import org.joda.time.DateTime;

import java.sql.Time;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 12:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleInfo {
    public int ScheduleId;
    public String ScheduleName;
    public Time SaturdayStart;
    public Time SaturdayEnd;
    public Time SundayStart;
    public Time SundayEnd;
    public Time MondayStart;
    public Time MondayEnd;
    public Time TuesdayStart;
    public Time TuesdayEnd;
    public Time WednesdayStart;
    public Time WednesdayEnd;
    public Time ThursdayStart;
    public Time ThursdayEnd;
    public Time FridayStart;
    public Time FridayEnd;
    public String ExceptHolidays;
    public DateTime CreatedDate;
    public DateTime LastUpdatedDate;

    public void validate() throws Exception
    {
        if (ScheduleId<=0) fail();
        if (StringUtil.empty(ScheduleName)) fail();
        if (StringUtil.empty(ExceptHolidays)) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Door-Group Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        if (ScheduleId==0) return;
        GenericRecord r = db.newRecord("schedules");
        r.set("scheduleId",ScheduleId);
        r.set("ScheduleName",ScheduleName);
        r.set("saturdayStart",SaturdayStart);
        r.set("saturdayEnd",SaturdayEnd);
        r.set("sundayStart",SundayStart);
        r.set("sundayEnd",SundayEnd);
        r.set("mondayStart",MondayStart);
        r.set("mondayEnd",MondayEnd);
        r.set("tuesdayStart",TuesdayStart);
        r.set("tuesdayEnd",TuesdayEnd);
        r.set("wednesdayStart",WednesdayStart);
        r.set("wednesdayEnd",WednesdayEnd);
        r.set("thursdayStart",ThursdayStart);
        r.set("thursdayEnd",ThursdayEnd);
        r.set("fridayStart",FridayStart);
        r.set("fridayEnd",FridayEnd);
        r.set("exceptHolidays",ExceptHolidays);
        r.set("lastUpdateDate",LastUpdatedDate);
        r.set("createdDate",CreatedDate);
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }

}
