package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 12:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class XBeeSensorInfo {

    public int SensorId;
    public String Name;
    public String SensorTypeId;
    public String MacAddress;


    public void validate() throws Exception
    {
        if (SensorId<=0) fail();
        if (StringUtil.empty(Name)) fail();
        if (StringUtil.empty(SensorTypeId)) fail();
        if (StringUtil.empty(MacAddress)) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid XBee-Sensor Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = db.newRecord("xbeesensors");
        r.set("sensor_id",SensorId);
        r.set("sensor_type_id",SensorTypeId);
        r.set("name",Name);
        r.set("mac_address",MacAddress);
        r.set("status","OFFLINE");
        /*GenericRecord d = db.newRecord("devices");
        d.set("device_address",MacAddress);
        d.set("device_class_id","XBEE_SENSOR");
        d.set("device_type_id",SensorTypeId);
        d.set("device_name",Name);
        d.set("device_status_id","DEVICE_STATUS_REGISTERED");*/

        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
        /*db.insert(d);
        if (!db.isSuccessful()) throw new Exception(db.getError());*/
    }

}
