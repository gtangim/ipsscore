package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 1:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlertActionInfo {
    public int SequenceId;
    public String ActionTypeEnumId;
    public Integer DeviceId;
    public String Value1;
    public String Value2;

    public void validate() throws Exception
    {
        if (SequenceId<0) fail();
        if (StringUtil.empty(ActionTypeEnumId)) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Alert-Action Data!");
    }

    public void save(GenericDB db, int alertId) throws Exception
    {
        GenericRecord r = db.newRecord("alertactions");
        r.set("alert_id",alertId);
        r.set("seq_id",SequenceId);
        r.set("action_type_enum_id",ActionTypeEnumId);
        r.set("device_id",DeviceId);
        r.set("value1",Value1);
        r.set("value2",Value2);
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }

}
