package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 12:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class CameraInfo {
    public int CameraId;
    public String CameraTypeId;
    public String Name;
    public String IpAddress;
    public String AccessCode;
    public String MacAddress;

    public void validate() throws Exception
    {
        if (CameraId<=0) fail();
        if (StringUtil.empty(Name)) fail();
        if (StringUtil.empty(CameraTypeId)) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Ip Camera Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = db.newRecord("cameras");
        r.set("camera_id",CameraId);
        r.set("camera_type_id",CameraTypeId);
        r.set("name",Name);
        r.set("ip_address",IpAddress);
        r.set("access_code",AccessCode);
        r.set("mac_address",MacAddress);
        r.set("status","OFFLINE");
        /*GenericRecord d = db.newRecord("devices");
        d.set("device_address",MacAddress);
        d.set("device_class_id","CAMERA");
        d.set("device_type_id",CameraTypeId);
        d.set("device_name",Name);
        d.set("device_status_id","DEVICE_STATUS_REGISTERED");*/

        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
        /*db.insert(d);
        if (!db.isSuccessful()) throw new Exception(db.getError());*/
    }


}
