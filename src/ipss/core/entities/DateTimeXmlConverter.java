package ipss.core.entities;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/4/14
 * Time: 1:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class DateTimeXmlConverter implements Converter {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    @Override
    public void marshal(Object o, HierarchicalStreamWriter hierarchicalStreamWriter, MarshallingContext marshallingContext) {
        DateTime dt = (DateTime)o;
        String dts = formatter.print(dt);
        hierarchicalStreamWriter.setValue(dts);
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader hierarchicalStreamReader, UnmarshallingContext unmarshallingContext) {
        try
        {
            String value = hierarchicalStreamReader.getValue();
            DateTime dt = formatter.parseDateTime(value);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    @Override
    public boolean canConvert(Class aClass) {
        return DateTime.class == aClass;
    }
}
