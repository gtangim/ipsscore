package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;
import apu.util.Utility;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 12:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserLoginInfo {
    public String UserLoginId;
    public String UserPassword;
    public String UserEmail;

    public void validate() throws Exception
    {
        if (StringUtil.empty(UserLoginId)) fail();
        if (StringUtil.empty(UserPassword)) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid User-Login Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r;
        r = db.selectFirst("select * from userlogins where userLoginId=?", Utility.toList(UserLoginId));
        if (r!=null)
        {
            r.set("userPassword",UserPassword);
            r.set("userEmail",UserEmail);
            db.update(r);
            if (!db.isSuccessful()) throw new Exception(db.getError());
        }
        else
        {
            r = db.newRecord("userlogins");
            r.set("userLoginId",UserLoginId);
            r.set("userPassword",UserPassword);
            r.set("userEmail",UserEmail);
            db.insert(r);
            if (!db.isSuccessful()) throw new Exception(db.getError());
        }

    }

}
