package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 1:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlertInfo {
    public int AlertId;
    public String Name;
    public String Description;
    public String Message;
    public Integer ScheduleId;
    public String ApStatusEnumId;
    public List<AlertConditionInfo> AlertConditions;
    public List<AlertActionInfo> AlertActions;


    public void validate() throws Exception
    {
        if (AlertId<=0) fail();
        if (StringUtil.empty(Name)) fail();
        if (StringUtil.empty(Message)) fail();
        if (AlertActions==null) AlertActions = new ArrayList<AlertActionInfo>();
        if (AlertConditions==null) AlertConditions = new ArrayList<AlertConditionInfo>();
        if (AlertConditions.size()==0 || AlertActions.size()==0) fail();
        for (int i=0;i<AlertConditions.size();i++) if (AlertConditions.get(i).SequenceId!=i) fail();
        for (int i=0;i<AlertActions.size();i++) if (AlertActions.get(i).SequenceId!=i) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Alert Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = db.newRecord("alerts");
        r.set("id",AlertId);
        r.set("name",Name);
        r.set("description",Description);
        r.set("message",Message);
        r.set("schedule_id",ScheduleId);
        r.set("ap_status_enum_id",ApStatusEnumId);
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
        for(AlertConditionInfo x : AlertConditions) x.save(db,AlertId);
        for(AlertActionInfo x : AlertActions) x.save(db,AlertId);

    }

}
