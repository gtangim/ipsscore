package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 1:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlertConditionInfo {
    public int SequenceId;
    public String ConditionTypeEnumId;
    public Integer DeviceId;
    public String ValueCompEnumId;
    public String Value1;
    public String Value2;
    public String ValueEnumId;
    public BigDecimal Duration;
    public String UserId;


    public void validate() throws Exception
    {
        if (SequenceId<0) fail();
        if (StringUtil.empty(ConditionTypeEnumId)) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Alert-Condition Data!");
    }

    public void save(GenericDB db, int alertId) throws Exception
    {
        GenericRecord r = db.newRecord("alertconditions");
        if (!db.isSuccessful()) throw new Exception(db.getError());
        r.set("alert_id",alertId);
        r.set("seq_id",SequenceId);
        r.set("cond_type_enum_id",ConditionTypeEnumId);
        r.set("device_id",DeviceId);
        r.set("value_comp_enum_id",ValueCompEnumId);
        r.set("value1",Value1);
        r.set("value2",Value2);
        r.set("value_enum_id",ValueEnumId);
        r.set("duration",Duration);
        r.set("user_id",UserId);
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }

}
