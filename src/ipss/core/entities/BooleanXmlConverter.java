package ipss.core.entities;

import apu.util.StringUtil;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 4/4/14
 * Time: 1:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class BooleanXmlConverter implements Converter {

    @Override
    public void marshal(Object o, HierarchicalStreamWriter hierarchicalStreamWriter, MarshallingContext marshallingContext) {
        Boolean val = (Boolean)o;
        hierarchicalStreamWriter.setValue(val.toString());
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader hierarchicalStreamReader, UnmarshallingContext unmarshallingContext) {
        try
        {
            String value = hierarchicalStreamReader.getValue();
            if (StringUtil.empty(value)) return null;
            value = value.trim().toLowerCase();
            if (value.equals("0") || value.startsWith("f") || value.startsWith("n")) return Boolean.FALSE;
            else return Boolean.TRUE;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    @Override
    public boolean canConvert(Class aClass) {
        return Boolean.class == aClass;
    }
}
