package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 1:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmsCarrierInfo {
    public int CarrierId;
    public String Description;
    public String SmsSuffix;

    public void validate() throws Exception
    {
        if (CarrierId<=0) fail();
        if (StringUtil.empty(Description)) fail();
        if (StringUtil.empty(SmsSuffix)) fail();

    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Sms-Carrier Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = db.newRecord("smscarriers");
        r.set("carrierID",CarrierId);
        r.set("description",Description);
        r.set("smsSuffix",SmsSuffix);
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }

}
