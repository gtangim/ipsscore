package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;
import apu.util.Utility;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 12:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlarmPanelInfo {
    public int AlarmPanelId;
    public String Name;
    public String AlarmPanelTypeId;
    public String IpAddress;
    public int TcpPort;
    public String LinuxHardwarePortId;
    public String ComAddress;
    public int BaudRate;
    public int DataBits;
    public String Parity;
    public String StopBits;
    public String FlowControl;
    public String ArmCode;
    public String DisarmCode;

    public void validate() throws Exception
    {
        if (AlarmPanelId<=0) fail();
        if (StringUtil.empty(Name)) fail();
        if (StringUtil.empty(AlarmPanelTypeId)) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Alarm Panel Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        db.executeUpdate("delete from alarmpanels",null);
        GenericRecord r = db.newRecord("alarmpanels");
        r.set("alarmPanelId",AlarmPanelId);
        r.set("name",Name);
        r.set("alarmPanelTypeId",AlarmPanelTypeId);
        r.set("ipAddress",IpAddress);
        r.set("tcpPort",TcpPort);
        r.set("linuxHardwarePortId",LinuxHardwarePortId);
        r.set("comAddress",ComAddress);
        r.set("baudRate",BaudRate);
        r.set("dataBits",DataBits);
        r.set("parity",Parity);
        r.set("stopBits",StopBits);
        r.set("FlowControl",FlowControl);
        r.set("armCode",ArmCode);
        r.set("disarmCode",DisarmCode);
        r.set("status", "OFFLINE");
        db.insert(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }

}
