package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import org.joda.time.DateTime;

/**
 * Created with IntelliJ IDEA.
 * User: russela
 * Date: 5/21/14
 * Time: 10:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class GlobalInfo {
    public int Id;
    public String Version;
    public int RelayInterval;
    public int HeldOpenInterval;
    public int LockDownOnCount;
    public int LockDownOffCount;
    public int LockDownInterval;
    public int TamperSustainInterval;
    public boolean RexEnabled;
    public boolean LockDownEnabled;
    public boolean TamperEnabled;
    public int ForcedOpenSustainInterval;
    public int CredentialSustainInterval;
    public int ReaderPollInterval;
    public int AlarmPanelPollInterval;
    public String SmtpServer;
    public String EmailUserId;
    public String EmailUserPassword;
    public String EmailSender;
    public boolean IsRexReversed;
    public boolean IsTamperReversed;
    public String StartIpRange;
    public String EndIpRange;
    public String SystemSubnet;
    public String SystemGateway;
    public String IsonasBroadcast1;
    public String IsonasBroadcast2;


    public void validate() throws Exception
    {
        if (Id<0) fail();
        if (RelayInterval<=0) fail();
        if (HeldOpenInterval<=0) fail();
        if (TamperSustainInterval<=0) fail();
        if (ForcedOpenSustainInterval<=0) fail();
        if (CredentialSustainInterval<=0) fail();
        if (ReaderPollInterval<=0) fail();
        if (AlarmPanelPollInterval<=0) fail();
        if (StartIpRange==null) fail();
        if (EndIpRange==null) fail();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid Global Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = db.selectFirst("select * from globalconfig");
        r.set("version",Version);
        r.set("relayInterval",RelayInterval);
        r.set("heldOpenInterval",HeldOpenInterval);
        r.set("lockDownOnCount",LockDownOnCount);
        r.set("lockDownOffCount",LockDownOffCount);
        r.set("lockDownInterval",LockDownInterval);
        r.set("tamperSustainInterval",TamperSustainInterval);
        r.set("rexEnabled",RexEnabled);
        r.set("lockDownEnabled",LockDownEnabled);
        r.set("tamperEnabled",TamperEnabled);
        r.set("forcedOpenSustainInterval",ForcedOpenSustainInterval);
        r.set("credentialSustainInterval",CredentialSustainInterval);
        r.set("readerPollInterval",ReaderPollInterval);
        r.set("alarmPanelPollInterval",AlarmPanelPollInterval);
        r.set("smtpServer",SmtpServer);
        r.set("emailUserId",EmailUserId);
        r.set("emailUserPassword",EmailUserPassword);
        r.set("emailSender",EmailSender);
        r.set("isRexReversed",IsRexReversed);
        r.set("isTamperReversed",IsTamperReversed);
        r.set("startIpRange",StartIpRange);
        r.set("endIpRange",EndIpRange);
        r.set("scanDevices",true);
        r.set("requestError",null);
        r.set("nextDoorProgrammingSchedule",new DateTime());
        r.set("systemSubnet",SystemSubnet);
        r.set("systemGateway",SystemGateway);
        r.set("isonasBroadcast1",IsonasBroadcast1);
        r.set("isonasBroadcast2",IsonasBroadcast2);
        db.update(r);
        if (!db.isSuccessful()) throw new Exception(db.getError());
    }

}
