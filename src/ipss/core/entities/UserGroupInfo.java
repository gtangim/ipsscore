package ipss.core.entities;

import apu.db.GenericDB;
import apu.db.GenericRecord;
import apu.util.StringUtil;
import apu.util.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RusselA
 * Date: 5/16/14
 * Time: 4:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserGroupInfo {
    public int UserGroupId;
    public String UserGroupName;
    public String GroupDescription;
    public List<Integer> Members;



    public void validate() throws Exception
    {
        if (UserGroupId<=0) fail();
        //if (UserGroupId<10000 || UserGroupId>20000) fail();
        if (StringUtil.empty(UserGroupName)) fail();
        if (Members==null) Members=new ArrayList<Integer>();
    }

    private void fail() throws Exception
    {
        throw new Exception("Invalid User-Group Data!");
    }

    public void save(GenericDB db) throws Exception
    {
        GenericRecord r = null;
        r = db.selectFirst("select * from usergroups where userGroupId=?", Utility.toList(UserGroupId));
        if (r==null) {
            r = db.newRecord("usergroups");
            r.set("userGroupId", UserGroupId);
            r.set("userGroupName", UserGroupName);
            r.set("description", GroupDescription);
            db.insert(r);
            if (!db.isSuccessful()) throw new Exception(db.getError());
        }
        for(int x: Members)
        {
            GenericRecord m = db.newRecord("usergroupassociations");
            m.set("userId",x);
            m.set("userGroupId",UserGroupId);
            db.insert(m);
            if (!db.isSuccessful())
                throw new Exception(db.getError());
        }
    }


}
