package ipss.core;

import apu.util.Loggable;
import apu.util.SimpleLogger;
import apu.util.Utility;
import org.joda.time.DateTime;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by russela on 3/10/2015.
 */
public class WatchDog extends Loggable implements Runnable {

    protected int pingTimeout = 5 * 60 * 1000; // 5 Minutes
    private FileWriter wdtFile = null;
    private DateTime lastPing = new DateTime();
    private Thread wdtThread = null;

    public WatchDog(SimpleLogger logger) {
        super(logger);
        wdtThread = new Thread(this);
        wdtThread.setDaemon(true);
        wdtThread.start();
    }


    public void run() {
        Thread.currentThread().setName("WATCHDOG");
        try {
            wdtFile = new FileWriter("/dev/watchdog");
        } catch (IOException e) {
            logError("Failed to enable Watch Dog Timer!");
            wdtFile = null;
        }
        if (wdtFile == null) return;
        try {
            log("Watch Dog Timer is Enabled!");
            while (true) {
                synchronized (this)
                {
                    if (Utility.getElapsed(lastPing)>pingTimeout)
                        throw new Exception("Main thread stopped responding to watchdog!");
                }
                wdtFile.write("x");
                wdtFile.flush();
                Thread.sleep(5000);
            }
        } catch (Exception ex) {
            log(ex);
            logError("Watchdog is Halted!");
        }
    }

    public void ping(){
        lastPing = new DateTime();
    }
}

