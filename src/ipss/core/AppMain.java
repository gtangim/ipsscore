package ipss.core;



/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 7/25/11
 * Time: 5:08 PM
 * To change this template use File | Settings | File Templates.
 */

import apu.util.SimpleLogger;
import apu.util.Utility;

import java.io.IOException;

import apu.net.*;
import ipss.core.isonas.IsonasCommandPacketizer;
import ipss.core.isonas.IsonasCommandResponse;
import ipss.core.isonas.commands.IsonasResponseDecompiler;
import ipss.core.isonas.commands.states.LedState;
import ipss.core.isonas.commands.SetLedCommand;
import org.joda.time.DateTime;


public class AppMain /*implements ClientSocketListener*/ {
    public static void main(String[] args) throws Exception {
        Class.forName("org.postgresql.Driver");
        /*
        int j = 0;
        if (j==0)
        {
            Utility.emailHost = "HERMES";
            Utility.sendEmailAlert("hello","hello","russela@mycnf.com","noreply@mycnf.com");
            return;
        }
        */

        Utility.initOS();
        String path = Utility.getCurrentDir();
        if (!Utility.fileExists(Utility.appendDir(path,"ip360config.xml")))
        {
            path=Utility.getUserDir();
            if (!Utility.fileExists(Utility.appendDir(path,"ip360config.xml")))
                throw new Exception("Cannot load configuration \"ip360config.xml\" from either \""
                        +Utility.getCurrentDir()+"\" \nor \""+Utility.getUserDir()+"\"...");

        }
        Central central = new Central(Utility.appendDir(path,"ip360config.xml"));

        if (central.isSuccess())
            central.start();
        else throw new Exception(central.getErrorMessage());

        System.exit(0);




        /*

        AppMain me = new AppMain();

        Thread.currentThread().setName("MAIN_APP");
        Utility.initOS();

        SimpleLogger logger = new SimpleLogger();
        logger.setLogFile(Utility.mapPath("log.txt"));

        ClientSocketPool pool = new ClientSocketPool(logger
                ,Utility.toList(new IsonasResponseDecompiler())
                ,Utility.toList(new IsonasCommandPacketizer()));
        pool.setDebug(true);
        if (!pool.initialize())
        {
            logger.log("MAIN","ERROR","Cannot initialize socket pool selector!");
            return;
        }











        int handle = pool.add("192.168.250.19",10001);
        if (handle!=-1)
        {
            pool.addEventHandler(handle, me);
            pool.send(handle, new SetLedCommand(1, LedState.GREEN));
            IsonasCommandResponse resp = (IsonasCommandResponse)pool.getFirstResponse(5000);
            if (resp!=null)
            {
                if (resp.isAck())
                    System.out.println("************************ YEAY I GOT AN ACK ***********************");
            }
        }

        */
        //int handle = pool.add("www.cineplex.com",80);
        //if (handle!=-1)
        //{
        //    pool.addEventHandler(handle,me);
        //    pool.send(handle, "GET /default.aspx HTTP/1.0\r\n\r\n");
        //    List data = pool.getResponses(5000);
        //}

        /*
        try {
            int x = System.in.read();
        } catch (IOException e) {
            logger.log("MAIN","ERROR","IO Error!");
        }
        */

        /*String encryptionKey = Utility.getHostName()+"IPSecu2edServ1ce8";
        System.out.println("Hello World 1!"); // Display the string.
        String enc = StringUtil.encryptAES("I love moon moon ahmed!",encryptionKey);
        System.out.println("Encrypted:" + enc);
        String dec = StringUtil.decryptAES(enc,encryptionKey);
        System.out.println("Decrypted:" + dec);
        System.out.println("Host Name:" + Utility.getHostName());
        System.out.println("MAC Address:" + Utility.getMacAddress(Utility.getHostName()));
        */

            /*Web myweb = new Web("http://tl-150/","admin","admin",5000,5000);
            if (myweb.isSuccessful())
                System.out.println(myweb.getResponseData());
            else
                System.out.println(myweb.getErrorMessage());

            String[][] table1 = new String[32][32];
            if (myweb.findTable(4) && myweb.getNextTable(table1,false))
            {
                String s = table1[0][0];
            }*/

    }

    /*public void onData(ClientSocketPool pool, SocketInfo si, Object data) {
        System.out.println("****************** YEAY I GOT DATA *************");

    }

    public void onError(ClientSocketPool pool, SocketInfo si, SocketErrorType errorType) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void OnConnect(ClientSocketPool pool, SocketInfo si, boolean firstTimeConnect) {
        System.out.println("****************** YEAY I GOT CONNECTED *************");
    }

    public void onDisconnect(ClientSocketPool pool, SocketInfo si) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onClose(ClientSocketPool pool, SocketInfo si) {
        //To change body of implemented methods use File | Settings | File Templates.
    }*/
}
