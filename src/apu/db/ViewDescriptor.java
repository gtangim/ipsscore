package apu.db;

import org.joda.time.DateTime;

import java.io.Console;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 7/28/11
 * Time: 1:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewDescriptor {
    protected static HashMap<Integer,String> sqlFieldTypeMap=null;

    protected String entityName;
    protected HashMap<String,Integer> fieldMap;
    protected String[] fieldNames;
    protected String[] fieldTypeNames;
    protected int[] fieldTypes;
    protected int[] fieldMaxLengths;
    protected int autoIncrementColumn;
    protected int fieldCount;
    protected boolean complexView;
    protected HashSet<String> pkNames = new HashSet<String>(4);
    protected HashSet<Integer> pkIndexes = new HashSet<Integer>(4);
    protected boolean pkAssigned=false;

    public ViewDescriptor(String entityName, int colCount)
    {
        this.entityName=entityName;
        fieldCount = colCount;
        fieldNames = new String[fieldCount];
        fieldTypes = new int[fieldCount];
        fieldTypeNames = new String[fieldCount];
        fieldMaxLengths = new int[fieldCount];
        fieldMap = new HashMap<String, Integer>();
        autoIncrementColumn=-1;
        complexView = false;
    }

    public ViewDescriptor(String entityName, String[] fields)
    {
        this.entityName = entityName;
        initFields(fields);
    }

    public ViewDescriptor(ResultSet tableDefinition) throws Exception
    {
        //try
        //{
            autoIncrementColumn = -1;
            tableDefinition.last();
            fieldCount = tableDefinition.getRow();
            if (fieldCount==0) throw new Exception("Entity meta data contains no fields!");
            fieldNames = new String[fieldCount];
            fieldTypes = new int[fieldCount];
            fieldTypeNames = new String[fieldCount];
            fieldMaxLengths = new int[fieldCount];
            fieldMap = new HashMap<String, Integer>();
            complexView = false;
            tableDefinition.first();
            entityName = tableDefinition.getString("table_name");
            for (int i=0;i<fieldCount;i++)
            {
                if (entityName==null) entityName = tableDefinition.getString("table_name");
                fieldNames[i] = tableDefinition.getString("column_name");
                fieldTypes[i] = tableDefinition.getInt("data_type");
                fieldTypeNames[i] = tableDefinition.getString("type_name");
                fieldMaxLengths[i] = tableDefinition.getInt("column_size");
                if (tableDefinition.getBoolean("is_autoincrement") || tableDefinition.getString("is_autoincrement").equals("YES")) autoIncrementColumn=i;
                fieldMap.put(fieldNames[i].toLowerCase(),i);
                tableDefinition.next();
            }
            /*return;
        }
        catch(Exception ex)
        {
            fieldCount=0;
            complexView=false;
            fieldMap = new HashMap<String, Integer>();
            fieldNames = new String[0];
            fieldTypes = new int[0];
            fieldTypeNames = new String[0];
            fieldMaxLengths = new int[0];
            throw ex;
        } */
    }

    public boolean setPrimaryKeys(ResultSet pkList)
    {
        try
        {
            pkAssigned=false;
            boolean success=true;
            while(pkList.next())
            {
                String pkCol = pkList.getString("column_name");
                if (pkCol!=null)
                {
                    pkCol=pkCol.toLowerCase();
                    pkNames.add(pkCol);
                    int pkInd = getIndex(pkCol);
                    if (pkInd==-1) success=false;
                    else pkIndexes.add(pkInd);
                }
            }
            pkAssigned = success;
            return success;
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    public boolean setPrimaryKeys(Set<String> pkList)
    {
        try
        {
            pkAssigned=false;
            boolean success=true;
            for(String pkCol:pkList)
            {
                if (pkCol!=null)
                {
                    pkCol=pkCol.toLowerCase();
                    pkNames.add(pkCol);
                    int pkInd = getIndex(pkCol);
                    if (pkInd==-1) success=false;
                    else pkIndexes.add(pkInd);
                }
            }
            pkAssigned = success;
            return success;
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    public ViewDescriptor(ResultSetMetaData sqlHeader)
    {
        autoIncrementColumn = -1;
        fieldCount=0;
        complexView=false;
        fieldMap = new HashMap<String, Integer>();
        if (sqlHeader!=null)
        {
            try
            {
                entityName=null;
                fieldCount = sqlHeader.getColumnCount();
                fieldNames = new String[fieldCount];
                fieldTypes = new int[fieldCount];
                fieldTypeNames = new String[fieldCount];
                fieldMaxLengths = new int[fieldCount];
                for (int i=0;i<fieldCount;i++)
                {
                    if (!complexView)
                    {
                        if (entityName==null)
                        {
                            entityName = sqlHeader.getTableName(i+1);
                            if (entityName==null || entityName.trim().equals(""))
                                {entityName="#VIEW#";complexView=true;}
                        }
                        else if (!entityName.equalsIgnoreCase(sqlHeader.getTableName(i+1)))
                            {entityName="#VIEW#";complexView=true;}
                    }
                    String fName = sqlHeader.getColumnName(i+1).trim().toLowerCase();
                    fieldMap.put(fName,i);
                    fieldNames[i] = fName;
                    fieldTypes[i] = sqlHeader.getColumnType(i+1);
                    fieldTypeNames[i] = sqlHeader.getColumnTypeName(i+1);
                    if (fieldTypeNames[i].equalsIgnoreCase("UNKNOWN")) fieldTypeNames[i]=getSqlTypeName(fieldTypes[i]);
                    fieldMaxLengths[i] = sqlHeader.getColumnDisplaySize(i+1);
                    if (sqlHeader.isAutoIncrement(i+1)) autoIncrementColumn=i;

                }
                return;
            }
            catch(Exception ex)
            {
                fieldCount=0;
                complexView=false;
                fieldMap = new HashMap<String, Integer>();
            }
        }
        fieldNames = new String[0];
        fieldTypes = new int[0];
        fieldTypeNames = new String[0];
        fieldMaxLengths = new int[0];
    }

    public void initFields(String[] fields)
    {
        fieldNames = fields;
        autoIncrementColumn = -1;
        for(int i=0; i<fieldNames.length;i++)
        {
            fieldMap.put(fieldNames[i],i);
            fieldTypes[i]=Types.VARCHAR;
            fieldTypeNames[i]=getSqlTypeName(Types.VARCHAR);
            fieldMaxLengths[i]=255;
        }
    }

    public void setFieldType(int colIndex, int sqlType, int maxLength, boolean autoIncrement)
    {
        if (colIndex<0 || colIndex>=fieldCount) return;
        String name = getSqlTypeName(sqlType);
        if (name==null) return;
        fieldTypes[colIndex]=sqlType;
        fieldTypeNames[colIndex]=name;
        fieldMaxLengths[colIndex]=maxLength;
        if (autoIncrement) autoIncrementColumn = colIndex;
    }

    public int addField(String fieldName, int sqlType, int maxLength, boolean autoIncrement)
    {
        try
        {
            fieldCount++;
            fieldNames = Arrays.copyOf(fieldNames,fieldCount);
            fieldTypes = Arrays.copyOf(fieldTypes,fieldCount);
            fieldTypeNames = Arrays.copyOf(fieldTypeNames,fieldCount);
            fieldMaxLengths = Arrays.copyOf(fieldMaxLengths,fieldCount);
            fieldNames[fieldCount-1] = fieldName;
            setFieldType(fieldCount-1,sqlType,maxLength,autoIncrement);
            fieldMap.put(fieldName,fieldCount-1);
            return fieldCount-1;
        }
        catch (Exception ex){ return -1;}

    }

    public ViewDescriptor getSubSchema(Collection<String> selectedFields)
    {
        try
        {
            ViewDescriptor sub = new ViewDescriptor(entityName,selectedFields.size());

            int ind=0;
            sub.complexView = complexView;
            for(String subField:selectedFields)
            {
                subField=subField.toLowerCase();
                int i=getIndex(subField);
                if (i<0) return null;
                sub.fieldNames[ind]=fieldNames[i];
                sub.fieldTypes[ind]=fieldTypes[i];
                sub.fieldTypeNames[ind]=fieldTypeNames[i];
                sub.fieldMaxLengths[ind]=fieldMaxLengths[i];
                sub.fieldMap.put(subField,ind);
                if (isAutoIncrement(i)) sub.autoIncrementColumn=ind;
                ind++;
            }
            sub.setPrimaryKeys(getPkNames());
            return sub;
        }
        catch(Exception ex)
        {
            return null;
        }
    }


    public void setFieldTypes(String colName, int sqlType, int maxLength, boolean autoIncrement)
    {
        int index = getIndex(colName);
        setFieldType(index,sqlType,maxLength, autoIncrement);
    }

    public int getIndex(String fieldName)
    {
        try
        {
            fieldName = fieldName.trim().toLowerCase();
            return fieldMap.get(fieldName);
        }
        catch (Exception ex){return -1;}
    }

    public String getName(int i)
    {
        if (i<0 || i>=fieldCount) return null;
        else return fieldNames[i];
    }


    public int getLength(int i)
    {
        if (i<0 || i>=fieldCount) return -1;
        return fieldMaxLengths[i];
    }
    public int getLength(String fieldName)
    {
        int ind = getIndex(fieldName);
        if (ind<0) return -1;
        return fieldMaxLengths[ind];
    }
    public int getType(int i)
    {
        if (i<0 || i>=fieldCount) return -1;
        return fieldTypes[i];
    }
    public int getType(String fieldName)
    {
        int ind = getIndex(fieldName);
        if (ind<0) return -1;
        return fieldTypes[ind];
    }
    public String getTypeName(int i)
    {
        if (i<0 || i>=fieldCount) return null;
        return fieldTypeNames[i];
    }
    public String getTypeName(String fieldName)
    {
        int ind = getIndex(fieldName);
        if (ind<0) return null;
        return fieldTypeNames[ind];
    }

    public int getAutoIncrementColumn(){return autoIncrementColumn;}
    public boolean isAutoIncrement(int i) {if (i<0 || i>=fieldCount) return false; return i==autoIncrementColumn;}
    public boolean isAutoIncrement(String fieldName)
    {
        int ind = getIndex(fieldName);
        if (ind==-1) return false;
        return isAutoIncrement(ind);
    }

    public int getCount() {
        return fieldCount;
    }

    public String getEntityName() {
        return entityName;
    }

    public boolean isComplexView() {
        return complexView;
    }

    public Set<String> getPkNames() {
        return pkNames;
    }

    public Set<Integer> getPkIndexes() {
        return pkIndexes;
    }

    public boolean isPkAssigned() {
        return pkAssigned;
    }

    public boolean isPrimaryKey(int ind)
    {
        if (pkIndexes.contains(ind)) return true;
        else return false;
    }

    public String getFieldName(int ind)
    {
        if (ind>=0 && ind<fieldCount)
            return fieldNames[ind];
        else return null;
    }

    public String[] getFieldNames()
    {
        return fieldNames;
    }
    
    

    public Object convertToNativeType(int ind, Object value)
    {
        try
        {
            int typ =  fieldTypes[ind];
            if (value==null)
                return null;
            else if (typ== Types.TINYINT || typ==Types.SMALLINT || typ==Types.INTEGER || typ==Types.BIGINT)
            {
                if (value instanceof Long) return value;
                else if (value instanceof Integer) return new Long((Integer)value);
                else if (value instanceof Character) return new Long((Character)value);
                else if (value instanceof Byte) return new Long((Byte)value);
                else if (value instanceof Short) return new Long((Short)value);
                else if (value instanceof Float)
                {
                    float vf = (Float)value;
                    long vl = (long)vf;
                    return vl;
                }
                else if (value instanceof Double)
                {
                    double vd = (Double)value;
                    long vl = (long)vd;
                    return vl;
                }
                else if (value instanceof BigDecimal)
                {
                    double vd = ((BigDecimal)value).doubleValue();
                    long vl = (long)vd;
                    return vl;
                }
                else if (value instanceof Boolean)
                {
                    Boolean b = (Boolean)value;
                    if (b) return new Long(1);
                    else return new Long(0);
                }
                else if (value instanceof String)
                {
                    Long v = Long.parseLong((String)value);
                    return v;
                }
                else return null;
            }
            else if (typ==Types.FLOAT || typ==Types.DOUBLE || typ==Types.REAL)
            {
                if (value instanceof Double) return value;
                else if (value instanceof Float)
                {
                    float vf = (Float)value;
                    double vd = (double)vf;
                    return vd;
                }
                else if (value instanceof Character) return new Double((Character)value);
                else if (value instanceof Byte) return new Double((Byte)value);
                else if (value instanceof Short) return new Double((Short)value);
                else if (value instanceof Integer) return new Double((Integer)value);
                else if (value instanceof Long) return new Double((Long)value);
                else if (value instanceof BigDecimal)
                {
                    double vd = ((BigDecimal)value).doubleValue();
                    return vd;
                }
                else if (value instanceof Boolean)
                {
                    Boolean b = (Boolean)value;
                    if (b) return new Double(1);
                    else return new Double(0);
                }
                else if (value instanceof String)
                {
                    Double v = Double.parseDouble((String) value);
                    return v;
                }
                else return null;
            }
            else if (typ==Types.DECIMAL || typ==Types.NUMERIC)
            {
                if (value instanceof BigDecimal) return value;
                else if (value instanceof Character) return new BigDecimal((Character)value);
                else if (value instanceof Byte) return new BigDecimal((Byte)value);
                else if (value instanceof Short) return new BigDecimal((Short)value);
                else if (value instanceof Integer) return new BigDecimal((Integer)value);
                else if (value instanceof Long) return new BigDecimal((Long)value);
                else if (value instanceof Float) return new BigDecimal((Float)value);
                else if (value instanceof Double) return new BigDecimal((Double)value);
                else if (value instanceof Boolean)
                {
                    Boolean b = (Boolean)value;
                    if (b) return new BigDecimal("1");
                    else return new BigDecimal("0");
                }
                else if (value instanceof String)
                {
                    BigDecimal v = new BigDecimal((String)value);
                    return v;
                }
                else return null;

            }
            else if (typ==Types.BIT)
            {
                if (value instanceof Boolean) return value;
                else if (value instanceof Character)
                {
                    char c = Character.toLowerCase((Character)value);
                    if (c=='1' || c=='t' || c=='y') return new Boolean(true);
                    else return new Boolean(false);
                }
                else if (value instanceof Byte) return new Boolean(((Byte)value)!=0);
                else if (value instanceof Short)  return new Boolean(((Short)value)!=0);
                else if (value instanceof Integer)  return new Boolean(((Integer)value)!=0);
                else if (value instanceof Long)  return new Boolean(((Long)value)!=0);
                else if (value instanceof Float)  return new Boolean(((Float)value)!=0);
                else if (value instanceof Double)  return new Boolean(((Double)value)!=0);
                else if (value instanceof BigDecimal)  return new Boolean(((BigDecimal)value).signum()!=0);
                else if (value instanceof String)
                {
                    String s = (String)value;
                    if (s.length()==0) return null;
                    else
                    {
                        char c = Character.toLowerCase(s.charAt(0));
                        if (c=='1' || c=='t' || c=='y')  return new Boolean(true);
                        else  return new Boolean(false);
                    }
                }
                else return null;
            }
            else if (typ==Types.CHAR || typ==Types.VARCHAR || typ==Types.NCHAR || typ==Types.LONGVARCHAR || typ==Types.LONGNVARCHAR)
            {
                String v;
                if (value instanceof String) v=(String)value;
                else v = value.toString();
                int len = getLength(ind);
                if (len!=0 && v.length()>len)
                    v = v.substring(0,len);
                return v;
            }
            else if (typ==Types.TIMESTAMP || typ==Types.DATE || typ==Types.TIME)
            {
                if (value instanceof DateTime) return new Timestamp(((DateTime)value).getMillis());
                else if (value instanceof Timestamp) return value;
                else if (value instanceof Date)
                {
                    DateTime dt = new DateTime((Date)value);
                    return new Timestamp(dt.getMillis());
                }
                else if (value instanceof Time)
                {
                    DateTime dt = new DateTime((Time)value);
                    return new Timestamp(dt.getMillis());
                }
                else return null;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }


    @Override
    public String toString()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.append(entityName);
            sb.append('[');
            boolean sep=false;
            for(int i=0;i<fieldCount;i++)
            {
                if (sep) sb.append(", ");
                if (isPrimaryKey(i)) sb.append("*");
                sb.append(fieldNames[i]);
                if (i==autoIncrementColumn) sb.append("++");
                sep=true;
            }
            sb.append(']');
            return sb.toString();
        }
        catch(Exception ex){return "schema[ERROR]";}
    }

    public static String getSqlTypeName(int sqlType)
    {
        if (sqlFieldTypeMap==null)
        {
            sqlFieldTypeMap = new HashMap<Integer, String>();
            Field[] fields = Types.class.getFields();
                for (int i = 0; i < fields.length; i++) {
                  try {
                    String name = fields[i].getName();
                    Integer value = (Integer) fields[i].get(null);
                    sqlFieldTypeMap.put(value, name);
                  } catch (IllegalAccessException e) {
                  }
                }
        }
        if (sqlFieldTypeMap.containsKey(sqlType)) return sqlFieldTypeMap.get(sqlType);
        else return null;
    }
}
