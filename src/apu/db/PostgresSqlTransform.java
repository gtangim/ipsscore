package apu.db;

import apu.util.StringUtil;

import java.util.HashMap;

/**
 * Created by RusselA on 8/25/2014.
 */
public class PostgresSqlTransform {
    private static HashMap<String,String> transforms = new HashMap<String, String>();

    private HashMap<String,String> fieldTransforms = new HashMap<String, String>();
    private StringBuilder buffer = new StringBuilder(1000);
    private StringBuilder identBuffer = new StringBuilder(1000);

    private static final int IDENTIFIER = 0;
    private static final int LITERAL = 1;
    private static final int OTHER = 2;


    public PostgresSqlTransform()
    {
    }

    public void add(String identifierName) throws Exception
    {
        if (StringUtil.empty(identifierName)) return;
        String identifierNameL = identifierName.toLowerCase();
        if (identifierNameL.equals(identifierName)) return;
        if (!fieldTransforms.containsKey(identifierNameL)) fieldTransforms.put(identifierNameL,identifierName);
        else
        {
            if (fieldTransforms.get(identifierNameL).equals(identifierName)) return;
            else throw new Exception("Ambiguous identifier found in database (" + identifierName +", "
                    + fieldTransforms.get(identifierNameL) + ")!");
        }
    }


    public String transform(String sql)
    {
        if (transforms.containsKey(sql)) return transforms.get(sql);

        // Write transform code here...
        buffer.setLength(0);
        identBuffer.setLength(0);
        int mode = OTHER;
        int n = sql.length();
        int pos = 0;
        char literalQuote = '.';
        while (pos<n)
        {
            char c = sql.charAt(pos);
            if (mode==IDENTIFIER)
            {
                if (identBuffer.length()==0 && c=='_' || c=='$' || Character.isLetter(c))
                {
                    identBuffer.append(c);
                    pos++;
                }
                else if (identBuffer.length()>0 && c=='_' || c=='$' || Character.isLetterOrDigit(c))
                {
                    identBuffer.append(c);
                    pos++;
                }
                else if (c=='`') pos++;
                else if (c=='\'' || c=='"')
                {
                    if (identBuffer.length()>0) buffer.append(getTransform(identBuffer.toString()));
                    identBuffer.setLength(0);
                    literalQuote='.';
                    mode=LITERAL;
                }
                else
                {
                    if (identBuffer.length()>0) buffer.append(getTransform(identBuffer.toString()));
                    identBuffer.setLength(0);
                    literalQuote='.';
                    mode=OTHER;
                }
            }
            else if (mode==LITERAL)
            {
                if (literalQuote=='.')
                {
                    literalQuote=c;
                    buffer.append(c);
                    pos++;
                }
                else if (c!=literalQuote)
                {
                    buffer.append(c);
                    pos++;
                }
                else
                {
                    if (pos<n-1 && sql.charAt(pos+1)==literalQuote)
                    {
                        buffer.append(literalQuote);
                        buffer.append(literalQuote);
                        literalQuote='.';
                        pos+=2;
                        mode=OTHER;
                    }
                    else
                    {
                        buffer.append(literalQuote);
                        pos++;
                        mode=OTHER;
                    }
                }
            }
            else if (mode==OTHER)
            {
                if (c=='`' || c=='_' || c=='$' || Character.isLetter(c)) mode=IDENTIFIER;
                else if (c=='\'' || c=='"') mode=LITERAL;
                else
                {
                    buffer.append(c);
                    pos++;
                }
            }
        }
        if (identBuffer.length()>0) buffer.append(getTransform(identBuffer.toString()));
        String newSql = buffer.toString();
        buffer.setLength(0);
        transforms.put(sql,newSql);
        return newSql;
    }

    private String getTransform(String identName)
    {
        String identNameL = identName.toLowerCase();
        if (fieldTransforms.containsKey(identNameL)) return "\"" + fieldTransforms.get(identNameL) + "\"";
        else return identName;
    }


}
