package apu.db;

import org.joda.time.DateTime;
import sun.awt.image.OffScreenImage;

import javax.crypto.interfaces.PBEKey;
import javax.tools.JavaCompiler;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 7/29/11
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class GenericRecord {
    ViewDescriptor header;
    HashMap<Integer,Object> values;
    boolean modified;


    GenericRecord(ViewDescriptor header)
    {
        this.header=header;
        modified = false;
        values = new HashMap<Integer, Object>(header.getCount()*2);
    }

    GenericRecord(ViewDescriptor header, ResultSet rec)
    {
        this.header = header;
        modified = false;

        values = new HashMap<Integer, Object>(header.getCount()*2);
        for (int i=0;i<header.getCount();i++)
        {
            int typ = header.getType(i);
            int j = i+1;
            try
            {
                if (typ== Types.TINYINT || typ==Types.SMALLINT || typ==Types.INTEGER || typ==Types.BIGINT)
                {
                    long lVal = rec.getLong(j);
                    if (rec.wasNull()) values.put(i,null);
                    else values.put(i,rec.getLong(j));

                }
                else if (typ==Types.FLOAT || typ==Types.DOUBLE || typ==Types.REAL)
                {
                    double dVal = rec.getDouble(j);
                    if (rec.wasNull()) values.put(i,null);
                    else values.put(i,dVal);
                }
                else if (typ==Types.BIT)
                {
                    boolean bVal = rec.getBoolean(j);
                    if (rec.wasNull()) values.put(i,null);
                    else values.put(i,bVal);
                }
                else if (typ==Types.DECIMAL || typ==Types.NUMERIC)
                    values.put(i,rec.getBigDecimal(j));
                else if (typ==Types.TIMESTAMP)
                {
                    Timestamp ts = rec.getTimestamp(j);
                    //DateTime dt = new DateTime(ts.getTime());
                    values.put(i,ts);
                }
                else if (typ==Types.DATE)
                {
                    Date dtVal = rec.getDate(j);
                    if (dtVal==null) values.put(i,null);
                    else
                    {
                        Timestamp ts = new Timestamp(dtVal.getTime());
                        values.put(i,ts);
                    }
                }
                else if (typ==Types.TIME)
                {
                    Time tVal = rec.getTime(j);
                    if (tVal==null) values.put(i,null);
                    else
                    {
                        Timestamp ts = new Timestamp(tVal.getTime());
                        values.put(i,ts);
                    }
                }
                else values.put(i,rec.getString(j));
            }
            catch(Exception ex)
            {
                try
                {
                    values.put(i,rec.getString(j));
                }
                catch(Exception ex2){}
            }

        }
    }


    public String getString(int ind)
    {
        try
        {
            if (values.containsKey(ind)) return (String)values.get(ind);
            else return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public Integer getInteger(int ind)
    {
        try
        {
            if (values.containsKey(ind))
            {
                Object v = values.get(ind);
                if (v instanceof Long) return ((Long)v).intValue();
                else if (v instanceof Double) return ((Double)v).intValue();
                else if (v instanceof BigDecimal) return ((BigDecimal)v).intValue();
                else if (v instanceof Boolean) return (((Boolean)v)==true?new Integer(1):new Integer(0));
                else return Integer.parseInt(v.toString());

            }
            else return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public Long getLong(int ind)
    {
        try
        {
            if (values.containsKey(ind))
            {
                Object v = values.get(ind);
                if (v instanceof Long) return ((Long)v).longValue();
                else if (v instanceof Double) return ((Double)v).longValue();
                else if (v instanceof BigDecimal) return ((BigDecimal)v).longValue();
                else if (v instanceof Boolean) return (((Boolean)v)==true?new Long(1):new Long(0));
                else return Long.parseLong(v.toString());

            }
            else return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public Double getDouble(int ind)
    {
        try
        {
            if (values.containsKey(ind))
            {
                Object v = values.get(ind);
                if (v instanceof Double) return ((Double)v);
                else if (v instanceof Long) return new Double((Long)v);
                else if (v instanceof BigDecimal) return ((BigDecimal)v).doubleValue();
                else if (v instanceof Boolean) return (((Boolean)v)==true?new Double(1):new Double(0));
                else return Double.parseDouble(v.toString());

            }
            else return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public Float getFloat(int ind)
    {
        try
        {
            if (values.containsKey(ind))
            {
                Object v = values.get(ind);
                if (v instanceof Double) return ((Double)v).floatValue();
                else if (v instanceof Long) return new Float((Long)v);
                else if (v instanceof BigDecimal) return ((BigDecimal)v).floatValue();
                else if (v instanceof Boolean) return (((Boolean)v)==true?new Float(1):new Float(0));
                else return Float.parseFloat(v.toString());

            }
            else return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public BigDecimal getBigDecimal(int ind)
    {
        try
        {
            if (values.containsKey(ind))
            {
                Object v = values.get(ind);
                if (v instanceof BigDecimal) return ((BigDecimal)v);
                else if (v instanceof Double) return new BigDecimal((Double)v);
                else if (v instanceof Long) return new BigDecimal((Long)v);
                else if (v instanceof Boolean) return (((Boolean)v)==true?new BigDecimal("1"):new BigDecimal("0"));
                else return new BigDecimal(v.toString());
            }
            else return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public Boolean getBoolean(int ind)
    {
        try
        {
            if (values.containsKey(ind))
            {
                Object v = values.get(ind);
                if (v instanceof Boolean) return ((Boolean)v);
                else if (v instanceof Long) return ((Long)v)!=0;
                else if (v instanceof BigDecimal) return ((BigDecimal)v).compareTo(BigDecimal.ZERO)==0;
                else if (v instanceof Double) return ((Double)v)==0;
                else
                {
                    String vs = v.toString();
                    if (vs==null || vs.trim().isEmpty()) return null;
                    else vs = vs.trim().toLowerCase();
                    if (vs.startsWith("1") || vs.startsWith("y") || vs.startsWith("t")) return true;
                    else return false;
                }
            }
            else return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public DateTime getDateTime(int ind)
    {
        try
        {
            if (values.containsKey(ind))
            {
                return new DateTime(((Timestamp)values.get(ind)).getTime());
            }
            else return null;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public boolean isNull(String fieldName)
    {
        int ind = header.getIndex(fieldName);
        return isNull(ind);
    }

    public boolean isNull(int ind)
    {
        if (values.containsKey(ind))
        {
            if (values.get(ind)==null) return true;
            else return false;
        }
        else return true;
    }

    public String getString(String fieldName)
    {
        int ind= header.getIndex(fieldName);
        return getString(ind);
    }
    public Integer getInteger(String fieldName)
    {
        int ind= header.getIndex(fieldName);
        return getInteger(ind);
    }
    public Long getLong(String fieldName)
    {
        int ind= header.getIndex(fieldName);
        return getLong(ind);
    }
    public Double getDouble(String fieldName)
    {
        int ind= header.getIndex(fieldName);
        return getDouble(ind);
    }
    public BigDecimal getBigDecimal(String fieldName)
    {
        int ind= header.getIndex(fieldName);
        return getBigDecimal(ind);
    }
    public Boolean getBoolean(String fieldName)
    {
        int ind= header.getIndex(fieldName);
        return getBoolean(ind);
    }
    public DateTime getDateTime(String fieldName)
    {
        int ind= header.getIndex(fieldName);
        return getDateTime(ind);
    }
    public Object get(int ind)
    {
        if (values.containsKey(ind)) return values.get(ind);
        else return null;
    }



    public boolean isModified() {
        return modified;
    }
    public void setModified(boolean isModified) {
        modified = isModified;
    }


    public boolean set(String fieldName, Object value)
    {
        int ind = header.getIndex(fieldName);
        if (ind<0) return false;
        return set(ind,value);
    }


    public boolean set(int ind,Object value)
    {
        if (ind<0 && ind>=header.getCount()) return false;
        int typ = header.getType(ind);
        try
        {
            if (value==null)
            {
                if (values.containsKey(ind) && values.get(ind)!=null) setModified(true);
                values.put(ind,null);
                return true;
            }
            else
            {
                Object cValue = header.convertToNativeType(ind,value);
                if (cValue==null) return false;
                if (!values.containsKey(ind) || values.get(ind)==null || !values.get(ind).equals(cValue))
                    setModified(true);
                values.put(ind,cValue);
            }
            /*else if (typ== Types.TINYINT || typ==Types.SMALLINT || typ==Types.INTEGER || typ==Types.BIGINT)
            {
                if (value instanceof Integer) values.put(ind, new Long((Integer)value));
                else if (value instanceof Character) values.put(ind,new Long((Character)value));
                else if (value instanceof Byte) values.put(ind, new Long((Byte)value));
                else if (value instanceof Short) values.put(ind, new Long((Short)value));
                else if (value instanceof Long) values.put(ind, new Long((Long)value));
                else if (value instanceof Float)
                {
                    float vf = (Float)value;
                    long vl = (long)vf;
                    values.put(ind,vl);
                }
                else if (value instanceof Double)
                {
                    double vd = (Double)value;
                    long vl = (long)vd;
                    values.put(ind,vl);
                }
                else if (value instanceof BigDecimal)
                {
                    double vd = ((BigDecimal)value).doubleValue();
                    long vl = (long)vd;
                    values.put(ind,vl);
                }
                else if (value instanceof Boolean)
                {
                    Boolean b = (Boolean)value;
                    if (b) values.put(ind,1);
                    else values.put(ind,0);
                }
                else if (value instanceof String)
                {
                    Long v = Long.parseLong((String)value);
                    values.put(ind,v);
                }
                else return false;
            }
            else if (typ==Types.FLOAT || typ==Types.DOUBLE || typ==Types.REAL)
            {
                if (value instanceof Double) values.put(ind,(Double)value);
                else if (value instanceof Float)
                {
                    float vf = (Float)value;
                    double vd = (double)vf;
                    values.put(ind,vd);
                }
                else if (value instanceof Character) values.put(ind,new Double((Character)value));
                else if (value instanceof Byte) values.put(ind, new Double((Byte)value));
                else if (value instanceof Short) values.put(ind, new Double((Short)value));
                else if (value instanceof Integer) values.put(ind, new Double((Integer)value));
                else if (value instanceof Long) values.put(ind, new Double((Long)value));
                else if (value instanceof Double) values.put(ind,(Double)value);
                else if (value instanceof BigDecimal)
                {
                    double vd = ((BigDecimal)value).doubleValue();
                    values.put(ind,vd);
                }
                else if (value instanceof Boolean)
                {
                    Boolean b = (Boolean)value;
                    if (b) values.put(ind,(double)1.0);
                    else values.put(ind,(double)0);
                }
                else if (value instanceof String)
                {
                    Double v = Double.parseDouble((String) value);
                    values.put(ind,v);
                }
                else return false;
            }
            else if (typ==Types.DECIMAL)
            {
                if (value instanceof BigDecimal) values.put(ind,(BigDecimal)value);
                else if (value instanceof Character) values.put(ind,new BigDecimal((Character)value));
                else if (value instanceof Byte) values.put(ind, new BigDecimal((Byte)value));
                else if (value instanceof Short) values.put(ind, new BigDecimal((Short)value));
                else if (value instanceof Integer) values.put(ind, new BigDecimal((Integer)value));
                else if (value instanceof Long) values.put(ind, new BigDecimal((Long)value));
                else if (value instanceof Float) values.put(ind, new BigDecimal((Float)value));
                else if (value instanceof Double) values.put(ind,new BigDecimal((Double)value));
                else if (value instanceof Boolean)
                {
                    Boolean b = (Boolean)value;
                    if (b) values.put(ind,new BigDecimal(1));
                    else values.put(ind,new BigDecimal(0));
                }
                else if (value instanceof String)
                {
                    BigDecimal v = new BigDecimal((String)value);
                    values.put(ind,v);
                }
                else return false;

            }
            else if (typ==Types.BIT)
            {
                if (value instanceof Boolean) values.put(ind,(Boolean)value);
                else if (value instanceof Character)
                {
                    char c = Character.toLowerCase((Character)value);
                    if (c=='1' || c=='t' || c=='y') values.put(ind,true);
                    else values.put(ind,false);
                }
                else if (value instanceof Byte) values.put(ind, ((Byte)value)!=0);
                else if (value instanceof Short) values.put(ind, ((Short)value)!=0);
                else if (value instanceof Integer) values.put(ind, ((Integer)value)!=0);
                else if (value instanceof Long) values.put(ind, ((Long)value)!=0);
                else if (value instanceof Float) values.put(ind, ((Float)value)!=0);
                else if (value instanceof Double) values.put(ind,((Double)value)!=0);
                else if (value instanceof BigDecimal) values.put(ind,((BigDecimal)value).signum()!=0);
                else if (value instanceof String)
                {
                    String s = (String)value;
                    if (s.length()==0) values.put(ind,null);
                    else
                    {
                        char c = Character.toLowerCase(s.charAt(0));
                        if (c=='1' || c=='t' || c=='y') values.put(ind,true);
                        else values.put(ind,false);
                    }
                }
                else return false;
            }
            else if (typ==Types.CHAR || typ==Types.VARCHAR || typ==Types.NCHAR)
            {
                String v;
                if (value instanceof String) v=(String)value;
                else v = value.toString();
                int len = header.getLength(ind);
                if (len!=0 && v.length()>len)
                    v = v.substring(0,len);
                values.put(ind,v);
            }
            else if (typ==Types.TIMESTAMP || typ==Types.DATE || typ==Types.TIME)
            {
                if (value instanceof DateTime) values.put(ind,new Timestamp(((DateTime)value).getMillis()));
                else if (value instanceof Timestamp) values.put(ind,(Timestamp)value);
                else if (value instanceof Date)
                {
                    DateTime dt = new DateTime((Date)value);
                    values.put(ind,new Timestamp(dt.getMillis()));
                }
                else if (value instanceof Time)
                {
                    DateTime dt = new DateTime((Time)value);
                    values.put(ind,new Timestamp(dt.getMillis()));
                }
                else return false;
            }
            else
            {
                return false;
            }*/
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    @Override
    public String toString()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.append(header.entityName);
            sb.append('[');
            boolean sep=false;
            if (header.isPkAssigned())
            {
                for(int pk:header.getPkIndexes())
                {
                    if (sep) sb.append(", ");
                    sb.append('*');
                    sb.append(header.fieldNames[pk]);
                    if (header.isAutoIncrement(pk)) sb.append("++");
                    sb.append(": ");
                    Object pkv = get(pk);
                    sb.append(pkv==null?"NULL":pkv.toString());
                    sep=true;
                }
                for(int i=0;i<header.fieldCount;i++)
                    if (!header.isPrimaryKey(i))
                    {
                        if (sep) sb.append(", ");
                        sb.append(header.fieldNames[i]);
                        if (header.isAutoIncrement(i)) sb.append("++");
                        sb.append(": ");
                        Object pkv = get(i);
                        sb.append(pkv==null?"NULL":pkv.toString());
                        sep=true;
                    }
            }
            else
            {
                for(int i=0;i<header.fieldCount;i++)
                {
                    if (sep) sb.append(", ");
                    sb.append(header.fieldNames[i]);
                    if (header.isAutoIncrement(i)) sb.append("++");
                    sb.append(": ");
                    Object val = get(i);
                    if (val==null) sb.append("#NULL#");
                    else sb.append(val);
                    sep=true;
                }
            }
            sb.append(']');
            return sb.toString();
        }
        catch(Exception ex){
            return "entity[ERROR]";
        }
    }

    public static boolean setAll(Collection<GenericRecord> records, String colName, Object value)
    {
        if (records==null || records.isEmpty()) return false;
        boolean res=true;
        int ind=-1;
        for(GenericRecord rec:records)
        {
            if (ind==-1) ind=rec.header.getIndex(colName);
            if (ind<0) return false;
            if (!rec.set(ind,value)) res=false;
        }
        return res;
    }
    public static boolean setAll(Collection<GenericRecord> records, int ind, Object value)
    {
        if (records==null || records.isEmpty()) return false;
        boolean res=true;
        for(GenericRecord rec:records)
            if (!rec.set(ind,value)) res=false;
        return res;
    }

    public ViewDescriptor getHeader() {
        return header;
    }
}
