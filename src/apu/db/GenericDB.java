package apu.db;

//import org.joda.time.DateTime;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.sql.*;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/3/11
 * Time: 5:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class GenericDB {


    protected String connectionString;
    protected Connection connection;
    protected String error;
    protected PreparedStatement currentStatement=null;
    protected ResultSet currentResultSet=null;
    protected ViewDescriptor currentEntityHeader=null;
    protected int currentRecordCount=-1;
    protected DatabaseMetaData dbMeta=null;
    protected HashMap<String,ViewDescriptor> schemaMap = new HashMap<String, ViewDescriptor>();
    protected int rowsAffected = 0;
    protected boolean autoReconnect = true;
    protected boolean manifestError = true;
    protected PostgresSqlTransform postgresAdapter = null;

    protected ReentrantLock lock = new ReentrantLock(true);

    public GenericDB() throws Exception
    {
        connect(null);
    }
    public GenericDB(String connectionStringValue) throws Exception
    {
        connect(connectionStringValue);
    }

    protected void connect(String connectionStringValue) throws Exception
    {

        try
        {
            error = null;
            connection = null;
            connectionString = connectionStringValue;
            if (connectionString==null) connectionString = defaultConnectionString;
            if (connectionString==null || connectionString.trim().isEmpty())
                throw new Exception("Connection String is Empty!");
            connection = DriverManager
                .getConnection(connectionString);
            if (connection==null) throw new Exception("Failed to connect to " + connectionString);
            dbMeta = connection.getMetaData();

            String provider = dbMeta.getDatabaseProductName().toLowerCase();
            if (provider.contains("postgresql"))
            {
                postgresAdapter = new PostgresSqlTransform();
                List<String> tables = new ArrayList<String>();

                String[] types = {"TABLE"};
                ResultSet rs = dbMeta.getTables(null, null, "%", types);
                while (rs.next()) {
                    tables.add(rs.getString(3));
                }
                for(String table : tables)
                {
                    ViewDescriptor entity = getSchema(table);
                    for(String fieldName : entity.getFieldNames())
                        postgresAdapter.add(fieldName);
                }
            }

        }
        catch(Exception ex)
        {
            doError(ex);
        }
    }

    protected void reconnect() throws Exception
    {
        try
        {
            error = null;
            connection = null;
            connection = DriverManager
                    .getConnection(connectionString);
            if (connection==null) throw new Exception("Failed to connect to " + connectionString);
            dbMeta = connection.getMetaData();
        }
        catch(Exception ex)
        {
            doError(ex);
        }
    }

    public boolean executeQuery(String sql) throws Exception
    {
        return executeQuery(sql,null);
    }


    public boolean executeQuery(String sql, List params) throws Exception
    {
        lock.lock();
        try
        {
            currentEntityHeader=null;
            currentRecordCount=-1;
            if (prepare(sql,params))
                currentResultSet = currentStatement.executeQuery();
            else return false;
            if (currentResultSet!=null)
                return true;
            else return false;
        }
        catch (Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }

    public boolean executeUpdate(String sql, List params) throws Exception
    {
        lock.lock();
        try
        {
            rowsAffected=0;
            currentEntityHeader=null;
            currentRecordCount=-1;
            if (prepare(sql,params))
            {
                rowsAffected = currentStatement.executeUpdate();
                if (rowsAffected>=0) return true;
                else return false;
            }
            else return false;
        }
        catch (Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }

    public boolean executeInsert(String sql, List params) throws Exception
    {
        lock.lock();
        try
        {
            rowsAffected=0;
            currentEntityHeader=null;
            currentRecordCount=-1;
            if (prepareInsert(sql, params))
            {
                rowsAffected = currentStatement.executeUpdate();
                if (rowsAffected>=0) return true;
                else return false;
            }
            else return false;
        }
        catch (Exception ex)
        {
            doError(ex);
            return false;
        }
        finally {
            lock.unlock();
        }
    }






    public boolean update(GenericRecord rec) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (rec==null)
                throw new Exception("Cannot update record; Record is null!");
            if (!rec.isModified()) return true;
            if (rec.header.entityName.startsWith("#"))
                throw new Exception("Cannot update record; This record is a complex view!");
            ViewDescriptor schema = getSchema(rec.header.entityName);
            if (schema==null)
                throw new Exception("Cannot update record; Unable find entity "+rec.header.entityName + " in the database!");
            if (!rec.header.setPrimaryKeys(schema.getPkNames()))
                throw new Exception("Cannot update record; Record doesn't contain all primary keys!");
            ArrayList<Object> pList = new ArrayList<Object>(rec.header.fieldCount);
            String sql = constructUpdateSql(rec,pList);
            if (sql==null) throw new Exception("Cannot Update; Invalid Record!");
            return executeUpdate(sql, pList);
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }
    public boolean update(List<GenericRecord> recs) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (recs==null || recs.size()==0) throw new Exception("Cannot update, record list is empty");
            ViewDescriptor header = recs.get(0).header;
            if (header.entityName.startsWith("#"))
                throw new Exception("Cannot update record(s); Given entity is a complex view!");
            for(GenericRecord rec:recs)
            {
                if (rec.header!=header)
                    throw new Exception("Cannot update record(s); All records in the list must be the same!");
            }
            ViewDescriptor schema = getSchema(header.entityName);
            if (schema==null)
                throw new Exception("Cannot update record(s); Unable find entity "
                        +header.entityName + " in the database!");
            if (!header.setPrimaryKeys(schema.getPkNames()))
                throw new Exception("Cannot update record(s); View doesn't contain all primary keys!");
            ArrayList<Integer> pList = new ArrayList<Integer>(header.fieldCount);
            String sql = constructMultiUpdateSql(header,pList);
            if (sql==null) throw new Exception("Cannot Update; Invalid Record!");

            rowsAffected=0;
            currentEntityHeader=null;
            currentRecordCount=-1;
            closeQuery();

            if(!isConnected()) throw new Exception("Database connection is not active!");
            //else if (connection.isClosed()) throw new Exception("Database connection is not active!");
            currentStatement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            if (currentStatement==null) throw new Exception("Cannot update record(s); invalid sql: " + sql);

            int n = pList.size();
            for(GenericRecord rec:recs)
            {
                for(int i=0;i<n;i++)
                {
                    Object v = rec.get(pList.get(i)); // for debug convenience...
                    currentStatement.setObject(i+1,v);
                }
                int res = currentStatement.executeUpdate();
                if (res<0) throw new Exception("Cannot update record(s); row update failed!");
                else rowsAffected+=res;
            }
            return true;
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }
    public boolean update(String entityName, Map valueMap, String whereClause, List whereParams) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (entityName==null || entityName.isEmpty())
                throw new Exception("Cannot update; Must provide an entity name!");
            if (valueMap==null || valueMap.size()==0)
                throw new Exception("Cannot update; Must provide values to update!");
            ViewDescriptor schema = getSchema(entityName);
            if (schema==null)
                throw new Exception("Cannot update record(s); Unable find entity "
                        +entityName + " in the database!");
            Set keys = valueMap.keySet();
            ViewDescriptor subView = schema.getSubSchema(keys);
            if (subView==null)
                throw new Exception(" Cannot map view " + collectionToString(keys)
                        +" to entity " + schema);
            int iParam = 0;
            StringBuilder sb = new StringBuilder();
            sb.append("update ");
            sb.append(entityName);
            sb.append(" set ");
            boolean sep=false;
            for(int i=0;i<subView.fieldCount;i++)
            {
                if (sep) sb.append(",");
                sb.append(subView.fieldNames[i]);
                sb.append("=?");
                sep=true;
            }
            if (whereClause!=null)
            {
                sb.append(" where ");
                sb.append(whereClause);
            }
            String sql = sb.toString();
            int wpCount = 0;
            if (whereParams!=null) wpCount=whereParams.size();
            List<Object> pList = new ArrayList<Object>(subView.fieldCount+wpCount);
            for(int i=0;i<subView.fieldCount;i++)
                pList.add(subView.convertToNativeType(i,valueMap.get(subView.fieldNames[i])));
            if (whereParams!=null)
                for(Object wp:whereParams)
                    pList.add(wp);
            return executeUpdate(sql,pList);
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }



    public boolean insert(GenericRecord rec) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (rec==null)
                throw new Exception("Cannot insert null record list!");
            if (rec.header.entityName.startsWith("#"))
                throw new Exception("Cannot insert record; This record is a complex view!");
            if (!rec.header.isPkAssigned())
                throw new Exception("Cannot insert record; Record doesn't contain all primary keys!");
            ArrayList<Object> pList = new ArrayList<Object>(rec.header.fieldCount);
            String sql = constructInsertSql(rec, pList);
            if (sql==null) throw new Exception("Cannot Insert; Invalid Record!");
            if(executeInsert(sql, pList))
            {
                // get the keys...
                if (rec.header.autoIncrementColumn>=0)
                {
                    Long[] keys = getAutoKeys();
                    if (keys==null || keys.length==0) return false;
                    rec.set(rec.header.autoIncrementColumn,keys[0]);
                }
                return true;
            }
            else return false;
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }
    public boolean insert(List<GenericRecord> recs) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (recs==null)
                throw new Exception("Cannot insert null record list!");
            if (recs.isEmpty())
                throw new Exception("Cannot insert empty record list!");
            ViewDescriptor header = recs.get(0).header;
            if (header.entityName.startsWith("#"))
                throw new Exception("Cannot insert record(s); This record is a complex view!");
            if (!header.isPkAssigned())
                throw new Exception("Cannot insert record(s); Record doesn't contain all primary keys!");
            ArrayList<Object> pList = new ArrayList<Object>(header.fieldCount * recs.size());
            String sql = constructMultiInsertSql(recs, pList);
            if (sql==null) throw new Exception("Cannot Insert; Invalid Record(s)!");
            if(executeInsert(sql, pList))
            {
                // get the keys...
                if (header.autoIncrementColumn>=0)
                {
                    Long[] keys = getAutoKeys();
                    if (keys==null || keys.length==0) return false;
                    for(int i=0;i<keys.length;i++)
                        recs.get(i).set(header.autoIncrementColumn,keys[i]);
                }
                return true;
            }
            else return false;
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }



    public boolean delete(GenericRecord rec) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (rec==null)
                throw new Exception("Cannot delete record; Record is null!");
            if (rec.header.entityName.startsWith("#"))
                throw new Exception("Cannot delete record; This record is a complex view!");
            if (!rec.header.isPkAssigned())
                throw new Exception("Cannot delete record; Primary keys are not found!");

            ArrayList<Object> pList = new ArrayList<Object>(rec.header.fieldCount);
            String sql = constructDeleteSql(rec,pList);
            if (sql==null) throw new Exception("Cannot delete; Invalid Record!");
            if (!executeUpdate(sql, pList)) return false;
            if (rowsAffected<1) throw new Exception("Cannot delete; Record not found!");
            return true;
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }
    public boolean delete(List<GenericRecord> recs) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (recs==null || recs.size()==0) throw new Exception("Cannot delete, record list is empty");
            ViewDescriptor header = recs.get(0).header;
            if (header.entityName.startsWith("#"))
                throw new Exception("Cannot delete record(s); Given entity is a complex view!");
            for(GenericRecord rec:recs)
            {
                if (rec.header!=header)
                    throw new Exception("Cannot delete record(s); All records in the list must be the same!");
            }
            if (!header.isPkAssigned())
                throw new Exception("Cannot delete record; Primary keys are not found!");

            ArrayList<Integer> pList = new ArrayList<Integer>(header.fieldCount);
            String sql = constructMultiDeleteSql(recs.get(0),pList);
            if (sql==null) throw new Exception("Cannot delete; Invalid Record!");

            rowsAffected=0;
            currentEntityHeader=null;
            currentRecordCount=-1;
            closeQuery();

            if(!isConnected()) throw new Exception("Database connection is not active!");
            //else if (connection.isClosed()) throw new Exception("Database connection is not active!");
            currentStatement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            if (currentStatement==null) throw new Exception("Cannot update record(s); invalid sql: " + sql);

            int n = pList.size();
            for(GenericRecord rec:recs)
            {
                for(int i=0;i<n;i++)
                {
                    Object v = rec.get(pList.get(i)); // for debug convenience...
                    currentStatement.setObject(i+1,v);
                }
                int res = currentStatement.executeUpdate();
                if (res<0) throw new Exception("Cannot update record(s); row update failed!");
                else rowsAffected+=res;
            }
            return true;
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }
    public boolean delete(String entityName, String whereClause, List whereParams) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (entityName==null || entityName.isEmpty())
                throw new Exception("Cannot delete; Must provide an entity name!");

            int iParam = 0;
            StringBuilder sb = new StringBuilder();
            sb.append("delete from ");
            sb.append(entityName);
            if (whereClause!=null)
            {
                sb.append(" where ");
                sb.append(whereClause);
            }
            String sql = sb.toString();
            return executeUpdate(sql,whereParams);
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }




    public List<String> getTableList() throws Exception
    {
        lock.lock();
        error=null;
        List<String> ret = new ArrayList<String>();
        try
        {
            if (executeQuery("show tables"))
            {
                while(currentResultSet.next())
                {
                    ret.add(currentResultSet.getString(1));
                }
            }
            return ret;
        }
        catch(Exception ex)
        {
            doError(ex);
            return ret;
        }
        finally {
            closeQuery();
            lock.unlock();
        }
    }

    public boolean executeSimpleUpdate(String sql) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            Statement st = connection.createStatement();
            int res = st.executeUpdate(sql);
            if (res<0)
                throw new Exception("Update error! Failed statement: " + sql);
            rowsAffected=res;
            return true;
        }
        catch (Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }
    public boolean executeSimpleQuery(String sql)  throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            currentEntityHeader=null;
            currentRecordCount=-1;
            Statement st = connection.createStatement();
            currentResultSet = st.executeQuery(sql);
            if (currentResultSet==null) return false;
            else return true;
        }
        catch (Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }


    public boolean exists(String entityName) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (executeSimpleQuery("show tables like '"+entityName+"'"))
            {
                boolean success = currentResultSet.next();
                closeQuery();
                return success;
            }
            else return false;
        }catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }

    public boolean drop(String entityName) throws Exception
    {
        return executeSimpleUpdate("drop table if exists "+entityName);
    }

    public boolean truncate(String entityName) throws Exception
    {
        return executeSimpleUpdate("truncate table "+entityName);
    }

    public int count(String entityName, String whereClause, List params) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            StringBuilder sb = new StringBuilder();
            sb.append("select count(*) from ");
            sb.append(entityName);
            if (whereClause!=null)
            {
                sb.append(" where ");
                sb.append(whereClause);
            }
            String sql=sb.toString();
            if (executeQuery(sql,params))
            {
                if (currentResultSet.next())
                {
                    int ret = currentResultSet.getInt(1);
                    closeQuery();
                    return ret;
                }
                else return -1;
            }
            else return -1;
        }catch(Exception ex)
        {
            doError(ex);
            return -1;
        }
        finally
        {
            closeQuery();
            lock.unlock();
        }
    }

    public GenericRecord selectFirst(String sql) throws Exception
    {
        return selectFirst(sql,null);
    }

    public GenericRecord selectFirst(String sql, List params) throws Exception
    {
        lock.lock();
        try
        {
            if (!select(sql,params)) return null;
            GenericRecord ret = getNext();
            return ret;
        }
        finally
        {
            closeQuery();
            lock.unlock();
        }
    }

    public boolean select(String sql) throws Exception
    {
        return select(sql,null);
    }
    public boolean select(String sql, List params) throws Exception
    {
        error=null;
        List<String> ret = new ArrayList<String>();
        lock.lock();
        try
        {
            currentEntityHeader=null;
            if (executeQuery(sql,params))
            {
                currentEntityHeader = new ViewDescriptor(currentResultSet.getMetaData());
                if (!currentEntityHeader.getEntityName().startsWith("#"))
                {
                    ViewDescriptor entitySchema = getSchema(currentEntityHeader.getEntityName());
                    if (entitySchema!=null) currentEntityHeader.setPrimaryKeys(entitySchema.getPkNames());
                }
                currentResultSet.last();
                currentRecordCount = currentResultSet.getRow();
                currentResultSet.beforeFirst();
                return true;
            }
            else return false;
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }

    public GenericRecord getNext() throws Exception
    {
        try
        {
            error=null;
            if (currentResultSet.next())
            {
                GenericRecord ret = new GenericRecord(currentEntityHeader,currentResultSet);
                return ret;
            }
            return null;
        }
        catch(Exception ex)
        {
            doError(ex);
            return null;
        }
    }

    public List<GenericRecord> getAll() throws Exception
    {
        try
        {
            error=null;
            List<GenericRecord> ret = new ArrayList<GenericRecord>(currentRecordCount * 3 / 2);
            while(currentResultSet.next())
            {
                GenericRecord r = new GenericRecord(currentEntityHeader,currentResultSet);
                ret.add(r);
            }
            closeQuery();
            return ret;
        }
        catch(Exception ex)
        {
            closeQuery();
            doError(ex);
            return null;
        }
    }


    public List<GenericRecord> selectAll(String sql) throws Exception
    {
        return selectAll(sql,null);
    }

    public List<GenericRecord> selectAll(String sql,List params) throws Exception
    {
        lock.lock();
        try
        {
            if (select(sql,params))
                return getAll();
            else return new ArrayList<GenericRecord>(2);
        }
        finally
        {
            lock.unlock();
        }
    }




    public ViewDescriptor getSchema(String entityName) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            String ent = entityName.toLowerCase();
            if (schemaMap.containsKey(ent)) return schemaMap.get(ent);

            ResultSet rs  = dbMeta.getColumns(null,null,entityName,"%");
            if (rs==null) throw new Exception("Unable to retrieve meta data for " + entityName + " from the database!");
            ViewDescriptor ret = new ViewDescriptor(rs);
            ResultSet pk = dbMeta.getPrimaryKeys(null,null,entityName);
            ret.setPrimaryKeys(pk);
            schemaMap.put(ent,ret);
            return ret;
        }
        catch (Exception ex)
        {
            doError(ex);
            return null;
        }
        finally
        {
            lock.unlock();
        }
    }

    public GenericRecord newRecord(String entityName) throws Exception
    {
        return newRecord(entityName,null);
    }

    public GenericRecord newRecord(String entityName,Collection<String> fields) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            ViewDescriptor header = getSchema(entityName);
            if (header==null) throw new Exception("Cannot create new record; Entity " + entityName + " doesn't exist!");

            if (fields!=null)
            {
                if (fields.isEmpty())
                    throw new Exception("Cannot create new record; Empty field list!");
                ViewDescriptor header2 = header.getSubSchema(fields);
                if (header2==null)
                    throw new Exception("Couldn't map "+collectionToString(fields)+
                    " to "+header);
                if (!header2.isPkAssigned())
                    throw new Exception("View "+header2+" doesn't contain all primary keys of table "+header);
                return new GenericRecord(header2);
            }
            else return new GenericRecord(header);
        }
        catch(Exception ex){doError(ex);return null;}
        finally{
            lock.unlock();
        }
    }
    public List<GenericRecord> newRecord(String entityName,int recordCount) throws Exception
    {
        return newRecord(entityName,null,recordCount);
    }
    public List<GenericRecord> newRecord(String entityName,Collection<String> fields, int recordCount) throws Exception
    {
        lock.lock();
        try
        {
            error=null;
            if (recordCount<1) throw new Exception("Cannot create new record; invalid record count!");
            ViewDescriptor header = getSchema(entityName);
            if (header==null) throw new Exception("Cannot create new record; Entity " + entityName + " doesn't exist!");

            if (fields!=null)
            {
                if (fields.isEmpty())
                    throw new Exception("Cannot create new record; Empty field list!");
                ViewDescriptor header2 = header.getSubSchema(fields);
                if (header2==null)
                    throw new Exception("Couldn't map "+collectionToString(fields)+
                    " to "+header);
                if (!header2.isPkAssigned())
                    throw new Exception("View "+header2+" doesn't contain all primary keys of table "+header);
                {
                    ArrayList ret = new ArrayList<GenericRecord>(recordCount);
                    for(int i=0;i<recordCount;i++) ret.add(new GenericRecord(header2));
                    return ret;
                }
            }
            else
            {
                ArrayList ret = new ArrayList<GenericRecord>(recordCount);
                for(int i=0;i<recordCount;i++) ret.add(new GenericRecord(header));
                return ret;
            }
        }
        catch(Exception ex){doError(ex);return null;}
        finally
        {
            lock.unlock();
        }
    }

    public void setAutoReconnect(boolean autoReconnect) {
        this.autoReconnect = autoReconnect;
    }

    protected void doError(Exception ex) throws Exception
    {
        if (error==null) error = "Error: " + ex.getMessage();
        else error = error + " | " + ex.getMessage();
        if (manifestError) throw ex;
    }

    public String getError()
    {
        return error;
    }

    public Connection getConnection() {
        return connection;
    }

    public boolean closeQuery() throws Exception
    {
        lock.lock();
        try
        {
            error = null;
            if (currentResultSet!=null) currentResultSet.close();
            if (currentStatement!=null) currentStatement.close();
            currentRecordCount=-1;
            currentEntityHeader=null;
            currentResultSet = null;
            currentStatement = null;
            return true;
        }
        catch(Exception ex)
        {
            doError(ex);
            return false;
        }
        finally
        {
            lock.unlock();
        }
    }
    public void close() throws Exception
    {
        try
        {
            closeQuery();
            if (connection!=null) connection.close();
        }
        catch(Exception ex)
        {
            doError(ex);
        }
    }

    public boolean isSuccessful() {return error==null;}
    public boolean isConnected()
    {
        try
        {
            examineConnection();
            if (connection==null) return false;
            else if (connection.isClosed()) return false;
            else return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }












    protected Long[] getAutoKeys() throws Exception
    {
        try
        {
            if (rowsAffected<=0) return null;
            Long[] ret = new Long[rowsAffected];
            ResultSet keys = currentStatement.getGeneratedKeys();
            for(int i=0;i<rowsAffected;i++)
            {
                if(!keys.next()) throw new Exception("Could not fill all rows with new keys!");
                ret[i]=keys.getLong(1);
            }
            return ret;
        }
        catch(Exception ex)
        {
            doError(ex);
            return null;
        }
    }


    protected String prepareSqlString(String sql,List params) throws Exception
    {
        try
        {
            closeQuery();
            if(!isConnected()) throw new Exception("Database connection is not active!");

            if (postgresAdapter!=null) sql = postgresAdapter.transform(sql);
            if (params!=null)
            {
                for(Object param:params)
                {
                    if (param instanceof List)
                    {
                        if (param==null)
                            throw new Exception("Array parameter is empty!");
                        int sz = ((List)param).size();
                        if (sz==0)
                            throw new Exception("Array parameter is empty!");
                        int ind = sql.indexOf("[?]");
                        if (ind>=0)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.append(sql,0,ind);
                            sb.append("(?");
                            for (int i=1;i<sz;i++) sb.append(",?");
                            sb.append(')');
                            sb.append(sql,ind+3,sql.length());
                            sql=sb.toString();
                        }
                        else throw new Exception("Could not find matching [?] in sql for given array!");
                    }
                    else if (param instanceof Object[])
                    {
                        if (param==null)
                            throw new Exception("Array parameter is empty!");
                        int sz = ((Object[])param).length;
                        if (sz==0)
                            throw new Exception("Array parameter is empty!");
                        int ind = sql.indexOf("[?]");
                        if (ind>=0)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.append(sql,0,ind);
                            sb.append("(?");
                            for (int i=1;i<sz;i++) sb.append(",?");
                            sb.append(')');
                            sb.append(sql,ind+3,sql.length());
                            sql=sb.toString();
                        }
                        else throw new Exception("Could not find matching [?] in sql for given array!");
                    }
                }
            }
            return sql;
        }
        catch (Exception ex)
        {
            doError(ex);
            return null;
        }
    }

    protected boolean prepare(String sql, List params) throws Exception
    {
        try
        {
            sql = prepareSqlString(sql,params);
            if (sql==null) return false;
            currentStatement = connection.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            int pi = 0;
            if (params!=null)
            {
                for(Object param: params)
                {
                    if (param instanceof List)
                    {
                        List vList = (List)param;
                        for(int i=0;i<vList.size();i++)
                        {
                            pi++;
                            currentStatement.setObject(pi,vList.get(i));
                        }
                    }
                    else if (param instanceof Object[])
                    {
                        Object[] vList = (Object[])param;
                        for(int i=0;i<vList.length;i++)
                        {
                            pi++;
                            currentStatement.setObject(pi,vList[i]);
                        }
                    }
                    else
                    {
                        pi++;
                        if (param instanceof DateTime)
                        {
                            Timestamp t = new Timestamp(((DateTime)param).getMillis());
                            currentStatement.setObject(pi,t);
                        }
                        else if (param instanceof LocalDateTime)
                        {
                            Timestamp t = new Timestamp(((LocalDateTime)param).toDateTime().getMillis());
                            currentStatement.setObject(pi,t);
                        }
                        else if (param instanceof LocalDate)
                        {
                            Timestamp t = new Timestamp(((LocalDate)param).toDateTimeAtStartOfDay().getMillis());
                            currentStatement.setObject(pi,t);
                        }
                        else currentStatement.setObject(pi,param);
                    }
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            doError(ex);
            return false;
        }
    }
    protected boolean prepareInsert(String sql, List params) throws Exception
    {
        try
        {
            sql = prepareSqlString(sql,params);
            if (sql==null) return false;
            currentStatement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
            int pi = 0;
            if (params!=null)
            {
                for(Object param: params)
                {
                    if (param instanceof List)
                    {
                        List vList = (List)param;
                        for(int i=0;i<vList.size();i++)
                        {
                            pi++;
                            currentStatement.setObject(pi,vList.get(i));
                        }
                    }
                    else if (param instanceof Object[])
                    {
                        Object[] vList = (Object[])param;
                        for(int i=0;i<vList.length;i++)
                        {
                            pi++;
                            currentStatement.setObject(pi,vList[i]);
                        }
                    }
                    else
                    {
                        pi++;
                        currentStatement.setObject(pi,param);
                    }
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            doError(ex);
            return false;
        }
    }


    protected String constructMultiUpdateSql(ViewDescriptor header, ArrayList<Integer> clauseList) throws Exception
    {
        try
        {
            ArrayList<Integer> keyList = new ArrayList<Integer>(4);
            StringBuilder sb_clause = new StringBuilder();
            StringBuilder sb_where = new StringBuilder();
            sb_clause.append("update ");
            sb_clause.append(header.entityName);
            sb_clause.append(" set ");
            boolean cSep = false;
            boolean wSep = false;
            for (int i=0;i<header.fieldCount;i++)
            {
                if (header.isPrimaryKey(i))
                {
                    if (wSep) sb_where.append(" and ");
                    sb_where.append(header.getFieldName(i));
                    sb_where.append("=?");
                    keyList.add(i);
                    wSep=true;
                }
                else if (!header.isAutoIncrement(i))
                {
                    if (cSep) sb_clause.append(",");
                    sb_clause.append(header.getFieldName(i));
                    sb_clause.append("=?");
                    clauseList.add(i);
                    cSep=true;
                }
            }

            sb_clause.append(" where ");
            sb_clause.append(sb_where);
            clauseList.addAll(keyList);
            return sb_clause.toString();
        }
        catch(Exception ex)
        {
            doError(ex);
            return null;
        }
    }

    protected String constructDeleteSql(GenericRecord rec, List<Object> pList) throws Exception
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.append("delete from ");
            sb.append(rec.header.entityName);
            sb.append(" where ");
            boolean wSep = false;
            for (int i=0;i<rec.header.fieldCount;i++)
            {
                if (rec.header.isPrimaryKey(i))
                {
                    if (wSep) sb.append(" and ");
                    sb.append(rec.header.getFieldName(i));
                    sb.append("=?");
                    pList.add(rec.get(i));
                    wSep=true;
                }
            }
            return sb.toString();
        }
        catch(Exception ex)
        {
            doError(ex);
            return null;
        }
    }
    protected String constructMultiDeleteSql(GenericRecord rec, List<Integer> pList) throws Exception
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.append("delete from ");
            sb.append(rec.header.entityName);
            sb.append(" where ");
            boolean wSep = false;
            for (int i=0;i<rec.header.fieldCount;i++)
            {
                if (rec.header.isPrimaryKey(i))
                {
                    if (wSep) sb.append(" and ");
                    sb.append(rec.header.getFieldName(i));
                    sb.append("=?");
                    pList.add(i);
                    wSep=true;
                }
            }
            return sb.toString();
        }
        catch(Exception ex)
        {
            doError(ex);
            return null;
        }
    }

    protected String constructUpdateSql(GenericRecord rec, ArrayList<Object> clauseList) throws Exception
    {
        try
        {
            ArrayList<Object> keyList = new ArrayList<Object>(4);
            StringBuilder sb_clause = new StringBuilder();
            StringBuilder sb_where = new StringBuilder();
            sb_clause.append("update ");
            sb_clause.append(rec.header.entityName);
            sb_clause.append(" set ");
            boolean cSep = false;
            boolean wSep = false;
            for (int i=0;i<rec.header.fieldCount;i++)
            {
                if (rec.header.isPrimaryKey(i))
                {
                    if (wSep) sb_where.append(" and ");
                    sb_where.append(rec.header.getFieldName(i));
                    sb_where.append("=?");
                    keyList.add(rec.get(i));
                    wSep=true;
                }
                else if (!rec.header.isAutoIncrement(i))
                {
                    if (cSep) sb_clause.append(",");
                    sb_clause.append(rec.header.getFieldName(i));
                    sb_clause.append("=?");
                    clauseList.add(rec.get(i));
                    cSep=true;
                }
            }

            sb_clause.append(" where ");
            sb_clause.append(sb_where);
            clauseList.addAll(keyList);
            return sb_clause.toString();
        }
        catch(Exception ex)
        {
            doError(ex);
            return null;
        }
    }

    protected String constructInsertSql(GenericRecord rec, ArrayList<Object> pList) throws Exception
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbv = new StringBuilder();
            sb.append("insert into ");
            sb.append(rec.header.entityName);
            sb.append('(');
            boolean sep=false;
            for (int i=0;i<rec.header.fieldCount;i++)
                if (!rec.header.isAutoIncrement(i))
                {
                    if (sep) {sb.append(',');sbv.append(',');}
                    sb.append(rec.header.fieldNames[i]);
                    sbv.append('?');
                    pList.add(rec.get(i));
                    sep=true;
                }
            sb.append(") values (");
            sb.append(sbv);
            sb.append(')');
            return sb.toString();
        }
        catch(Exception ex)
        {
            doError(ex);
            return null;
        }
    }
    protected String constructMultiInsertSql(List<GenericRecord> recs, ArrayList<Object> pList) throws Exception
    {
        try
        {
            ViewDescriptor header = recs.get(0).header;
            StringBuilder sb = new StringBuilder();
            StringBuilder sbv = new StringBuilder();
            sb.append("insert into ");
            sb.append(header.entityName);
            sb.append('(');
            sbv.append('(');
            boolean sep=false;
            for (int i=0;i<header.fieldCount;i++)
                if (!header.isAutoIncrement(i))
                {
                    if (sep) {sb.append(',');sbv.append(',');}
                    sb.append(header.fieldNames[i]);
                    sbv.append('?');
                    sep=true;
                }
            sbv.append(')');
            sb.append(") values ");
            sep=false;
            for(GenericRecord rec:recs)
            {
                if(sep) sb.append(',');
                sb.append(sbv);
                for(int i=0;i<header.fieldCount;i++)
                    if (!header.isAutoIncrement(i))
                        pList.add(rec.get(i));
                sep=true;
            }
            return sb.toString();
        }
        catch(Exception ex)
        {
            doError(ex);
            return null;
        }
    }

    protected void examineConnection() throws Exception
    {
        try
        {
            if (autoReconnect && connection==null || connection.isClosed())
                reconnect();
        }
        catch (Exception ex)
        {
            doError(ex);
        }
    }

    public int getRowsAffected() {
        return rowsAffected;
    }

    public boolean isManifestError() {
        return manifestError;
    }

    public void setManifestError(boolean manifestError) {
        this.manifestError = manifestError;
    }

    private String collectionToString(Collection list)
    {
        try
        {
            StringBuilder sb=new StringBuilder();
            boolean sep=false;
            sb.append('[');
            for(Object v:list)
            {
                if (sep) sb.append(", ");
                sb.append(v.toString());
                sep=true;
            }
            sb.append(']');
            return sb.toString();
        }
        catch (Exception ex) {return "[ERROR]";}
    }


// *********************************************************
    //   S T A T I C      M E T H O D S
    // *********************************************************


    protected static String defaultConnectionString = "";
    protected static String initError = null;
    public static boolean Initialize(String defaultConnection)
    {
        try
        {
            initError = null;
            defaultConnectionString = defaultConnection;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            return true;
        }
        catch (Exception ex)
        {
            initError = ex.getMessage();
            return false;
        }
    }

    public static String getInitError(){return initError;}

}
