package apu.ports;
import apu.util.Loggable;
import apu.util.SimpleLogger;

public abstract class BasePortImplementation extends Loggable {

    // Create the listener list
	protected String portname;
    protected PortDataEventManager eventManager = new PortDataEventManager();
    protected int writeTimeout = 100000;
	

    public BasePortImplementation(SimpleLogger loggerInstance)
    {
    	super(loggerInstance);
    	portname = nextGenericID();
    }
    
    // This methods allows classes to register for MyEvents
    //@Override
	public void addDataListener(PortDataListener listener) {
    	eventManager.addDataListener(listener);
        log("[" + portname + "] " + "Added new data event listener.");
    }

    // This methods allows classes to unregister for MyEvents
    //@Override
    public void removeDataListener(PortDataListener listener) {
    	eventManager.removeDataListener(listener);
        log("[" + portname + "] " + "Removed data event listener.");
    }
    
    //@Override
    public int getDataListenerCount()
    {
    	return eventManager.getDataListenerCount();
    }

    //@Override 
    public void setWriteTimeout(int t)
    {
    	writeTimeout = t;
        log("[" + portname + "] Assigned Write timeout=" + Integer.toString(t));
    }
    
    //@Override
	public char[] ReadChars()
	{
		char[] retBuffer = null;
		synchronized(this)
		{
			StringBuilder localBuffer = eventManager.getLocalBuffer();
			int sz = localBuffer.length();
			retBuffer = new char[sz];
			localBuffer.getChars(0, sz, retBuffer, 0);
			localBuffer.setLength(0);
		}

        log("[" + portname + "] read " + Integer.toString(retBuffer.length)+" bytes.");
		return retBuffer;
	}

    //@Override
	public String Read()
	{
		String ret = null;
		synchronized(this)
		{
			StringBuilder localBuffer = eventManager.getLocalBuffer();
			ret = localBuffer.toString();
			localBuffer.setLength(0);
		}
        log("[" + portname + "] read " + Integer.toString(ret.length())+" bytes.");
		return ret;
	}

    //@Override
	public int readBufferLength()
	{
		int sz;
		synchronized(this)
		{
			sz = eventManager.getLocalBuffer().length();
		}
		return sz;
	}

    //@Override
	public void setDataEventMode(boolean enabled)
	{
		eventManager.setDataEventMode(enabled);
        log("[" + portname + "] Port event mode is turned " + (enabled?"on":"off") + ".");
	}
	
    
    

	protected void assignPortName(String newPortName)
	{
        log("[" + portname + "] renaming port: " + portname + "->" + (newPortName==null?"#NULL#":newPortName));
		portname = newPortName;
	}
	
	private static int nextPortID = 0;
	public static String nextGenericID()
	{
		return "Port_"+ Integer.toString(nextPortID);
	}

	//@Override
	public String getPortName() {
		return portname;
	}
	
	
}
