package apu.ports;
import gnu.io.*;
import apu.util.SimpleLogger;
import apu.util.StringUtil;

import java.util.*;
import java.io.*;

import apu.ports.PortDataEvent.PortDataEventType;

public class GenericSerialPort extends BasePortImplementation implements SerialPortEventListener {
	private final String _classname = this.getClassName();
	//String portname;
    int baud;
	int databits;
	int flowcontrol;
	int stopbits;
	int parity;
    int readEventDelay;
	boolean openflag;
	private SerialPort comport;
	private CommPortIdentifier portid;
	private InputStream comportinputstream;
	private OutputStream comportoutputstream;
	private boolean portopen;
	private boolean busy;
    private boolean debugMode=false;
	private void reset()
	{
		openflag = false;
		comport = null;
		portid = null;
		comportinputstream = null;
		comportoutputstream = null;
		portopen = false;
		busy = false;
	}
	
	public GenericSerialPort(SimpleLogger logger, String port, int baud, int dataBits, String stopBits, String parity, String flowControl, int readEventDelay) throws PortIOException
	{
		super(logger);
		reset();
        assignPortName(port);
        validatePortName();
        this.baud = baud;
        if (dataBits==5) this.databits = SerialPort.DATABITS_5;
        else if (dataBits==6) this.databits = SerialPort.DATABITS_6;
        else if (dataBits==7) this.databits = SerialPort.DATABITS_7;
        else if (dataBits==8) this.databits = SerialPort.DATABITS_8;
        else logAndThrow(new PortIOException("Unable to open serial port. You must assign a valid DataBits value in the configuration!"));

        if (parity==null) parity="";
        parity=parity.toLowerCase().trim();
        if (parity.startsWith("n")) this.parity = SerialPort.PARITY_NONE;
        else if (parity.startsWith("o")) this.parity = SerialPort.PARITY_ODD;
        else if (parity.startsWith("e")) this.parity = SerialPort.PARITY_EVEN;
        else logAndThrow(new PortIOException("Unable to open serial port. You must assign a valid Parity value in the configuration!"));

        if (stopBits==null) stopBits="";
        stopBits = stopBits.toLowerCase().trim();
        if (stopBits.equals("1")) this.stopbits = SerialPort.STOPBITS_1;
        else if (stopBits.equals("1.5")) this.stopbits = SerialPort.STOPBITS_1_5;
        else if (stopBits.equals("2")) this.stopbits = SerialPort.STOPBITS_2;
        else logAndThrow(new PortIOException("Unable to open serial port. You must assign a valid StopBits value in the configuration!"));

        if (flowControl==null) flowControl="";
        flowControl=flowControl.toLowerCase().trim();
        if (flowControl.startsWith("n")) flowcontrol = SerialPort.FLOWCONTROL_NONE;
        else if (flowControl.startsWith("h")) flowcontrol = SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT;
        else if (flowControl.startsWith("s")) flowcontrol = SerialPort.FLOWCONTROL_XONXOFF_IN | SerialPort.FLOWCONTROL_XONXOFF_OUT;
        else logAndThrow(new PortIOException("Unable to open serial port. You must assign a valid StopBits value in the configuration!"));
        this.readEventDelay = readEventDelay;
    }

	protected void validatePortName() throws PortIOException
	{
        log("********* AVAILABLE SERIAL PORTS ******************");
        Enumeration portList = CommPortIdentifier.getPortIdentifiers();
        int nPort = 0;
        while (portList.hasMoreElements())
        {
                
            log("SERIAL PORT " + Integer.toString(++nPort)+ ": " + ((CommPortIdentifier)portList.nextElement()).getName());
        }

		portList = CommPortIdentifier.getPortIdentifiers();
		if (!portList.hasMoreElements()) 
            logWarning("[" + portname + "] System did not return any portname. Comm API may not be installed correctly.");
		boolean portFound = false;
		while (portList.hasMoreElements()) {			
		    portid = (CommPortIdentifier) portList.nextElement();
		    if (portid.getPortType() == CommPortIdentifier.PORT_SERIAL) {
		    	
		    	if (portid.getName().toLowerCase().equals(portname.toLowerCase())) {
		    		portFound = true;
		    		break;		    	
		    	}
			} 
		}
		if (!portFound)
        {
            logAndThrow(new PortIOException("Cannot initialize port "+portname+". Port may be in use by another process or it may not exist."));
        }
        
        
	}	

	//@Override
	public void open() throws PortIOException
	{
		if (portopen) logAndThrow(new PortIOException("Port is already connected!"));
		if (portid==null) logAndThrow(new PortIOException("You must initialize the port first."));
		try {
		    comport = (SerialPort) portid.open("IPSSCore", 5000);
		} catch (PortInUseException e) 
		{
            logAndThrow(new PortIOException("Unable to open serial port. Port is in use by another program! Details: " + e.getMessage()));
		}
		
		try {
		    comportinputstream = comport.getInputStream();
		} catch (IOException e) 
		{
            logAndThrow(new PortIOException("Unable to get port input stream. reason: " + e.toString()));
		}
		try {
		    comportoutputstream = comport.getOutputStream();
		} catch (IOException e) 
		{
            logAndThrow(new PortIOException("Unable to get port output stream. reason: " + e.toString()));
		}

		try {
		    comport.addEventListener(this);
		} catch (TooManyListenersException e) 
		{
            logAndThrow(new PortIOException("Unable to attach event listener. Too many already attached!"));
		}

		comport.notifyOnDataAvailable(true);
		comport.notifyOnOutputEmpty(false);  // This is really important to make IP360 work for linux!

		try {
		    comport.setSerialPortParams(baud, databits, stopbits, parity);
		    comport.setFlowControlMode(flowcontrol);
		}
		catch (UnsupportedCommOperationException e) 
		{
            logAndThrow(new PortIOException("Unable to setup serial port with given settings. Reason: " + e.getMessage()));
		}	
		portopen = true;
		log("[" + portname + "] Port connection established!");
		
	}


	//@Override
	public void close(){		
		if (portopen) 
		{
			comport.notifyOnDataAvailable(false);
			comport.notifyOnOutputEmpty(false);
			comport.removeEventListener();
			try
			{
				comportinputstream.close();
				comportoutputstream.close();
			}
			catch(IOException ex)
			{				
			}
			comport.close();
		}
        log("[" + portname + "] Port connection closed!");
		portopen = false;
	}


	//@Override
	public boolean isOpen() {
		return portopen;
	}

	//@Override

    protected void writeWait() throws IOException
    {
        synchronized(this)
        {
            try
            {
                if (isBusy())
                    this.wait(writeTimeout);
            }catch(InterruptedException ex){}
            if (isBusy()) throw new IOException("Write Timeout!");
        }
    }

	public void write(char data) throws PortIOException {
        setBusy();
		try {
            if (debugMode) log("[" + portname + ".tx] " + Character.toString(data));
			comportoutputstream.write((int)data);
		    writeWait();
        }
		catch (IOException e) {
            logAndThrow(new PortIOException("Unable to write data to serial port. Reason:" + e.getMessage()));
		}
        finally {
            clearBusy();
        }
	}
	//@Override
	public void write(char[] data) throws PortIOException {
		write(StringUtil.charArrayToBytes(data));
	}
	//@Override
	public void write(byte[] data) throws PortIOException {
        setBusy();
		try {
            if (debugMode) log("[" + portname + ".tx] " + new String(data));
			comportoutputstream.write(data);
            writeWait();
		}
		catch (IOException e) {
            logAndThrow(new PortIOException("Unable to write data to serial port. Reason:" + e.getMessage()));
		}
        finally {
            clearBusy();
        }
	}
	//@Override
	public void write(String data) throws PortIOException {
        setBusy();
		try {
            if (debugMode) log("[" + portname + ".tx] " + data);
			comportoutputstream.write(data.getBytes());
            writeWait();
		}
		catch (IOException e) {
            logAndThrow(new PortIOException("Unable to write data to serial port. Reason:" + e.getMessage()));
		}
        finally {
            clearBusy();
        }
	}

	
	//@Override
	public void writeAsync(char data) throws PortIOException {
        setBusy();
		try {
            if (debugMode) log("[" + portname + ".tx] " + Character.toString(data));
			comportoutputstream.write((int) data);
		}
		catch (IOException e) {
            logAndThrow(new PortIOException("Unable to write data to serial port. Reason:" + e.getMessage()));
		}
        finally {
            clearBusy();
        }
	}
	//@Override
	public void writeAsync(byte[] data) throws PortIOException {
        setBusy();
		try {
            if (debugMode) log("[" + portname + ".tx] " + new String(data));
			comportoutputstream.write(data);
		}
		catch (IOException e) {
            logAndThrow(new PortIOException("Unable to write data to serial port. Reason:" + e.getMessage()));
		}
        finally {
            clearBusy();
        }
	}
	//@Override
	public void writeAsync(char[] data) throws PortIOException {
		writeAsync(StringUtil.charArrayToBytes(data));
	}
	//@Override
	public void writeAsync(String data) throws PortIOException {
        setBusy();
		try {
            if (debugMode) log("[" + portname + ".tx] " + data);
			comportoutputstream.write(data.getBytes());
		}
		catch (IOException e) {
            logAndThrow(new PortIOException("Unable to write data to serial port. Reason:" + e.getMessage()));
		}
        finally {
            clearBusy();
        }
	}
	
	private void setBusy() throws PortIOException
	{
		synchronized(this)
		{
			if (busy) throw new PortIOException("Another write operation is in progress. Must use blocked write or wait until WRITE_COMPLETE event.");
			busy = true;
		}
	}
	
	private void clearBusy()
	{
		synchronized(this)
		{
			busy=false;
		}
	}

	//@Override
	public boolean isBusy()
	{
		return busy;
	}

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    //@Override
	public void cancelWrite()
	{
		synchronized(this)
		{
			busy = false;
			try{
				this.notifyAll();
			} catch(Exception ex) {}
		}
	}
	
	//@Override
	public String getClassName() {
		return _classname;
	}
	

    public void serialEvent(SerialPortEvent serialPortEvent) {
        switch (serialPortEvent.getEventType()) {

            case SerialPortEvent.BI:

            case SerialPortEvent.OE:

            case SerialPortEvent.FE:

            case SerialPortEvent.PE:

            case SerialPortEvent.CD:

            case SerialPortEvent.CTS:

            case SerialPortEvent.DSR:

            case SerialPortEvent.RI:
                break;

            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                clearBusy();
                synchronized(this)
                {
                    try
                    {
                        this.notifyAll();
                    }
                    catch(IllegalMonitorStateException ex){}
                }
                eventManager.fireEvent(new PortDataEvent(this, PortDataEventType.WRITE_COMPLETE, null));
                break;

            case SerialPortEvent.DATA_AVAILABLE:
                try {
                    int sz = comportinputstream.available();
                    byte[] readBuffer = new byte[sz];
                    int numBytes = comportinputstream.read(readBuffer);
                    String data = new String(readBuffer);
                    if (readEventDelay>0)
                        try {
                            Thread.sleep(readEventDelay);
                        } catch (InterruptedException e) {
                        }

                    if (debugMode) log("[" + portname + ".rx] " + data);

                    eventManager.fireEvent(new PortDataEvent(this, PortDataEventType.DATA_RECEIVED,readBuffer));

                } catch (IOException e)
                {
                    logError("[" + portname + "] Exception while receiving serial data. Reason: " + e.getMessage());
                }

                break;
        }
    }
}
