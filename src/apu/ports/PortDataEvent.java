package apu.ports;


import java.util.EventObject;


public class PortDataEvent extends EventObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8590013705114939017L;
	public enum PortDataEventType {DATA_RECEIVED, WRITE_COMPLETE, IO_ERROR, UNKNOWN}
	private byte[] _value;
	private PortDataEventType _type;
	
    public PortDataEvent(Object source, PortDataEventType eventType, byte[] value) {
        super(source);
        _type = eventType;
        _value = value;        
    }
    
	
	public byte[] getValue() {return _value;}
	public PortDataEventType getEventType() {return _type;}
	
	
}
