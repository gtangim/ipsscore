package apu.ports;

public class PortIOException extends Exception {

	public PortIOException()
	{
		super();
	}
	public PortIOException(String errorMessage)
	{
		super(errorMessage);
	}
}
