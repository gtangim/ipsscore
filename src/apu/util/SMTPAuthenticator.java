package apu.util;

import javax.mail.PasswordAuthentication;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 3/8/12
 * Time: 4:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class SMTPAuthenticator extends javax.mail.Authenticator {

    String u;
    String p;

    public SMTPAuthenticator(String username, String password)
    {
        super();
        u=username;
        p=password;
    }
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(u,p);
    }
    
    String getUser()
    {
        return u;
    }
    
    String getPassword()
    {
        return p;
    }
}

