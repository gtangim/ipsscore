package apu.net;

import apu.util.Loggable;
import apu.util.SimpleLogger;
import apu.util.StringUtil;
import apu.util.Utility;
import com.sun.org.apache.bcel.internal.generic.NEW;
import sun.awt.geom.AreaOp;

import javax.swing.event.EventListenerList;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/10/11
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClientSocketPool extends Loggable implements Runnable{

    Selector selector=null;
    protected boolean debug = true;

    protected boolean retryConnection=true;
    protected int connectionRetryInterval=30000;
    protected int commandTimeout=5000;
    protected int numWorkerThreads=10;


    protected ConcurrentHashMap<Integer,SocketInfo> socketMap=new ConcurrentHashMap<Integer, SocketInfo>();
    private BlockingQueue<ChangeRequest> socketModeChanges = new LinkedBlockingQueue<ChangeRequest>();


    protected List<IDataFilter> inputFilters;
    protected List<IDataFilter> outputFilters;
    protected ExecutorService workers = null;


    protected ClientSocketPoolMonitor monitor=null;
    protected Thread ioThread=null;
    protected Thread monitorThread=null;

    public ClientSocketPool(SimpleLogger logger, List<IDataFilter> inputFilters, List<IDataFilter> outputFilters)
    {
        super(logger);
        if (inputFilters==null || inputFilters.size()==0)
        {
            this.inputFilters=new ArrayList<IDataFilter>(10);
            this.inputFilters.add(new DefaultDataFilter());
        }
        else this.inputFilters=inputFilters;
        if (outputFilters==null || outputFilters.size()==0)
        {
            this.outputFilters=new ArrayList<IDataFilter>(10);
            this.outputFilters.add(new DefaultDataFilter());
        }
        else this.outputFilters=outputFilters;
    }

    public boolean initialize()
    {
        try {
            selector = SelectorProvider.provider().openSelector();
            if (selector==null)
                throw new IOException("Could not initialize the selector!");
            workers = Executors.newFixedThreadPool(numWorkerThreads);
            ioThread = new Thread(this);
            ioThread.setDaemon(true);
            ioThread.start();
            return true;
        } catch (IOException e) {
            log(e);
            return false;
        }
    }


    public int add(String address, int port, ClientSocketListener eventHandler)
    {
        SocketInfo si = new SocketInfo(address,port, eventHandler, inputFilters.size(), outputFilters.size());
        socketMap.put(si.getId(), si);
        tryConnect(si.getId());
        return si.getId();
    }

    public void tryConnect(int handle)
    {
        try {
            SocketInfo si = get(handle);
            si = get(handle);
            if (si==null)
                throw new Exception("Cannot Reconnect! Socket with handle "
                        + Integer.toString(handle) + " doesnt exist in the socket pool!");
            if (si.isTryingToConnect()) return;
            log("Trying to connect: " + si);
            si.setTryConnect();
            SocketChannel oldSocket = si.socket;
            if (oldSocket!=null) oldSocket.close();
            si.socket = SocketChannel.open();
            si.socket.configureBlocking(false);
            InetSocketAddress addr = si.getIpAddress();
            if (addr==null)
                throw new Exception("Cannot resolve host name while trying to connect!"
                        +" Either invalid hostname, or network is down!");
            // Cannot call register directly because it would block if selector.select() is blocking.
            if (!si.socket.connect(addr)) {
                socketModeChanges.add(new ChangeRequest(si.getId(),SelectionKey.OP_CONNECT));
                selector.wakeup();
            }
            else {
                final boolean firstTimeConnect = !si.isConnectedOnce();
                si.setConnected();
                log("Connection Established: " + si);
                fireOnConnect(si, firstTimeConnect);
                socketModeChanges.add(new ChangeRequest(si.getId(),SelectionKey.OP_READ | SelectionKey.OP_WRITE));
            }
            socketModeChanges.add(new ChangeRequest(si.getId(),SelectionKey.OP_CONNECT));
        } catch (Exception e) {
            log(e);
            abortConnection(handle);
        }
    }

    public void abortConnection(int handle)
    {
        SocketInfo si = null;
        try {
            si = get(handle);
            si.setDisconnected();
            if (si.isConnectedOnce())
                logWarning(si+": Connection has been disrupted!");
            else logWarning("Connection Failed: " + si);
            fireOnDisconnect(si);
        } catch (Exception e) {
            log(e);
        }
        finally
        {
            if (si!=null) si.resetTryConnect();
        }
    }

    public void close(int handle)
    {
        try {
            SocketInfo si = get(handle);
            if (si.socket!=null) si.socket.close();
            socketMap.remove(si);
            if (si.socket!=null) si.socket.close();
            si.socket=null;
            if (si.isConnectedOnce())
                logWarning(si+": Connection has been closed!");
            else logWarning("Connection Cancelled: " + si);
            fireOnClose(si);
        } catch (Exception e) {
            log(e);
        }
    }

    public SocketInfo get(int handle) throws Exception
    {
        try
        {
            SocketInfo ret = socketMap.get(handle);
            return ret;
        }
        catch (Exception ex){
            throw new Exception("Socket with handle " + handle + " not found in the socket pool!");
        }
    }

    private void processIncomingData(SocketInfo si, int numRead)
    {
        try {
            byte[] buf = new byte[numRead];
            si.readBuffer.rewind();
            si.readBuffer.get(buf);
            List data = new ArrayList(2);
            data.add(buf);
            data = processData(inputFilters, si.inputFilterPartials, data);
            if (debug)
            {
                if (data==null) log(si.toString() + " >> 0 data packets detected!");
                else log(si.toString() +" >> " + Integer.toString(data.size()) + " data packets detected");
            }
            if (data!=null) {
                // Now launch the event input data event...
                final SocketInfo siFinal = si;
                final ClientSocketPool thisFinal = this;
                for (Object dataElement : data) {
                    final Object dataElementFinal = dataElement;
                        for (ClientSocketListener listener : si.eventHandlers) {
                            final ClientSocketListener l = listener;
                            workers.execute(new Runnable() {
                                public void run() {
                                    l.onData(thisFinal, siFinal, dataElementFinal);
                                }
                            });
                        }
                }
            }
        } catch (Exception e) {
            log(e);
            logWarning("Failed to process incoming data from the socket!");
        }

    }
    private List processData(List<IDataFilter> filters, Object[] partials, List inputData) throws Exception
    {
        List data = new ArrayList(inputData);
        for(int i=0;i<filters.size();i++)
        {
            partials[i] = filters.get(i).process(partials[i],data);
            data=filters.get(i).getMaturedObjects();
            if (data==null) break;
        }
        return data;
    }



    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        final SocketInfo si=(SocketInfo)key.attachment();
        try {
            // Clear out our read buffer so it's ready for new data
            if (si==null) throw new Exception("This socket is not found in the socket pool! ["
                    +socketChannel.getRemoteAddress().toString()+"]");
            int n;
            si.readBuffer.clear();
            // Attempt to read off the channel
            if (debug) log("Reading socket: " + si);
            n = socketChannel.read(si.readBuffer);
            final int numRead = n;
            if (numRead == -1) {
                // Remote entity shut the socket down cleanly. Do the
                // same from our end and cancel the channel.
                if (debug) logWarning("DETECTED SOCKET CLOSE: " + si);
                key.channel().close();
                key.cancel();
                return;
            }
            else
            {
                // Process the data...
                if (debug)
                {
                    byte[] buf = new byte[numRead];
                    si.readBuffer.rewind();
                    si.readBuffer.get(buf);
                    String data = StringUtil.byteToHexSep(buf,"[","][","]");
                    log(si + " | Received >> " + data);
                    //logRaw(StringUtil.byteToString(buf));
                }
                processIncomingData(si,numRead);
            }

        } catch (Exception e) {
            log(e);
            key.cancel();
            if (si!=null) abortConnection(si.getId());
            else socketChannel.close();
        }

    }
    private void write(SelectionKey key) throws IOException {
        try {
            SocketChannel socketChannel = (SocketChannel) key.channel();
            SocketInfo si = (SocketInfo) key.attachment();

            Queue<ByteBuffer> queue = new LinkedList<ByteBuffer>();
            si.outgoingQueue.drainTo(queue);

            if (queue.isEmpty()) {
                key.interestOps(SelectionKey.OP_READ);
                return;
            }
            // Write until there's not more data ...
            while (!queue.isEmpty()) {
                ByteBuffer buf = (ByteBuffer) queue.remove();
                if (debug) {
                    if (!socketChannel.isConnected()) logWarning("SOCKET DISCONNECTED: " + si);
                    else if (socketChannel.socket() == null || !socketChannel.socket().isConnected()
                            || socketChannel.socket().isOutputShutdown())
                        logWarning("SOCKET DISCONNECTED: " + si);
                }
                if (debug) log("writing to socket: " + si + " >> " + buf.remaining() + " bytes");
                socketChannel.write(buf);
                if (buf.remaining() > 0) {
                    // ... or the socket's buffer fills up
                    if (debug) logWarning("socket write failed to complete: " + si);
                    break;
                } else {
                    if (debug) log("finished writing to socket: " + si);
                }
            }

            key.interestOps(SelectionKey.OP_READ);
        } catch (Exception e)
        {
            log(e);
            key.cancel();
        }
    }

    private void finishConnection(SelectionKey key){
        SocketChannel socketChannel = (SocketChannel) key.channel();
        final SocketInfo si = (SocketInfo)key.attachment();
        // Finish the connection. If the connection operation failed
        // this will raise an IOException.
        try {
            socketChannel.finishConnect();
            // Register an interest in writing on this channel
            key.interestOps(SelectionKey.OP_WRITE | SelectionKey.OP_READ);
            final boolean firstTimeConnect = !si.isConnectedOnce();
            si.setConnected();
            log("Connection Established: " + si);
            fireOnConnect(si, firstTimeConnect);

        } catch (Exception e) {
            // Cancel the channel's registration with our selector
            log(e);
            if (si!=null)
            {
                fireOnError(si,SocketErrorType.ConnetionError);
                fireOnDisconnect(si);
                logWarning("Connection Failed: " + si);
                si.setDisconnected();
            }
            key.cancel();
        }
        finally {
            si.resetTryConnect();
        }

    }



    public boolean send(int handle, Object data)
    {
        List inp = new ArrayList(1);
        inp.add(data);
        return send(handle,inp);
    }
    public boolean send(int handle, List data)
    {
        try{
            SocketInfo si = get(handle);
                data = processData(outputFilters,si.outputFilterPartials,data);
                if (data!=null)
                    for(Object dataElement:data)
                        if(dataElement instanceof byte[]) send(handle,(byte[])dataElement);
                        else if (dataElement instanceof String) send(handle,(String)dataElement);
                        else throw new Exception("Output filter configuration is invalid! Last filter must return byte[] or String");
                else throw new Exception("Cannot send packet, object did not pass through the entire filter chain!");
                return true;
        }
        catch (Exception ex)
        {
            log(ex);
            logWarning("Could not send packet because output filter chain did not manage to process data!");
            return false;
        }
    }
    public boolean send(int handle, String data)
    {
        try {
            byte[] dataBytes = data.getBytes("UTF8");
            return send(handle,dataBytes);
        } catch (UnsupportedEncodingException e) {
            log(e);
            return false;
        }
    }

    public boolean send(int handle, byte[] data) {
        // Queue the data we want written
        SocketInfo si=null;
        try {
            if (data[0]!=16){
                String error = "sdfdsf";
            }
            si = get(handle);
            SocketChannel socket = si.socket;
            if (socket == null || !socket.isConnected())
                throw new Exception("Cannot Write! Socket " + si + " is closed!");
            if (debug) {
                log(si + ", Sending:" + StringUtil.byteToHexSep(data, "[", "][", "]"));
            }
            si.outgoingQueue.add(ByteBuffer.wrap(data));
            // Finally, wake up our selecting thread so it can make the required changes
            socketModeChanges.add(new ChangeRequest(si.getId(), SelectionKey.OP_WRITE | SelectionKey.OP_READ));
            selector.wakeup();
            return true;
        } catch (Exception e) {
            log(e);
            return false;
        }
    }


    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public Selector getSelector() {
        return selector;
    }

    public void run() {
        Thread.currentThread().setName("SOCKET_EVENT_LOOP");
        log("Started Socket Pool IO Thread!");
        try {
            if (monitor == null) {
                monitor = new ClientSocketPoolMonitor(this, logger);
                monitor.setReconnectInterval(connectionRetryInterval);
                monitorThread = new Thread(monitor);
                monitorThread.setDaemon(true);
                monitorThread.start();
            }
            while (true) {
                // Wait for an event one of the registered channels
                try {
                    selector.select();
                } catch (IOException e) {
                    log(e);
                }
                // Iterate over the set of keys for which events are available
                Iterator it = selector.selectedKeys().iterator();
                while (it.hasNext()) {
                    SelectionKey key = (SelectionKey) it.next();
                    it.remove();

                    try {
                        if (!key.isValid()) {
                            continue;
                        }
                        // Check what event is available and deal with it
                        if (key.isConnectable()) {
                            this.finishConnection(key);
                        }
                        else {
                            if (key.isReadable()) {
                                this.read(key);
                            }
                            if (key.isWritable()) {
                                this.write(key);
                            }
                        }
                    } catch (Exception ex) {
                        log(ex);
                    }
                }

                List<ChangeRequest> changes = new LinkedList<ChangeRequest>();
                socketModeChanges.drainTo(changes);
                for(ChangeRequest change:changes){
                    try {
                        SocketInfo si = get(change.socketHandle);
                        if (si.socket!=null) si.socket.register(selector,change.ops, si);
                    }
                    catch(Exception ex){
                        log(ex);
                    }
                }


            }
        } catch (Exception e) {
            log(e);
        }
        logWarning("Socket Pool IO Thread has been stopped!");
    }


    public void addEventHandler(int handle, ClientSocketListener eventHandler)
    {
        try {
                SocketInfo si = get(handle);
                if (!si.eventHandlers.contains(eventHandler)) si.eventHandlers.add(eventHandler);
        } catch (Exception e) {
            log(e);
            logWarning("Unable to attach event handler to socket with handle: "
                    + Integer.toString(handle));
        }
    }

    public void removeEventHandler(int handle, ClientSocketListener eventHandler)
    {
        try {
            SocketInfo si = get(handle);
            si.eventHandlers.remove(eventHandler);
        } catch (Exception e) {
            log(e);
            logWarning("Unable to detach event handler from socket with handle: "
                    + Integer.toString(handle));
        }
    }

    public void removeAllEventHandlers(int handle)
    {
        try {
            SocketInfo si = get(handle);
            si.eventHandlers.clear();
        } catch (Exception e) {
            log(e);
            logWarning("Unable to detach event handlers from socket with handle: "
                    + Integer.toString(handle));
        }
    }

    private void fireOnConnect(SocketInfo si, boolean firstTimeConnect) {
        final SocketInfo siFinal = si;
        final ClientSocketPool thisFinal = this;
        final boolean firstTimeConnectFinal = firstTimeConnect;
        for (ClientSocketListener listener : si.eventHandlers) {
            final ClientSocketListener l = listener;
            workers.execute(new Runnable() {
                public void run() {
                    l.OnConnect(thisFinal, siFinal, firstTimeConnectFinal);
                }
            });
        }
    }
    private void fireOnDisconnect(SocketInfo si) {
        final SocketInfo siFinal = si;
        final ClientSocketPool thisFinal = this;
        for (ClientSocketListener listener : si.eventHandlers) {
            final ClientSocketListener l = listener;
            workers.execute(new Runnable() {
                public void run() {
                    l.onDisconnect(thisFinal, siFinal);
                }
            });
        }
    }
    private void fireOnClose(SocketInfo si)
    {
        final SocketInfo siFinal = si;
        final ClientSocketPool thisFinal = this;
        for (ClientSocketListener listener : si.eventHandlers) {
            final ClientSocketListener l = listener;
            workers.execute(new Runnable() {
                public void run() {
                    l.onClose(thisFinal, siFinal);
                }
            });
        }
    }
    private void fireOnError(SocketInfo si,SocketErrorType errorType)
    {
        final SocketInfo siFinal = si;
        final ClientSocketPool thisFinal = this;
        final SocketErrorType errorTypeFinal = errorType;
        for (ClientSocketListener listener : si.eventHandlers) {
            final ClientSocketListener l = listener;
            workers.execute(new Runnable() {
                public void run() {
                    l.onError(thisFinal, siFinal, errorTypeFinal);
                }
            });
        }
    }


    public boolean isRetryConnection() {
        return retryConnection;
    }

    public void setRetryConnection(boolean retryConnection) {
        this.retryConnection = retryConnection;
    }

    public int getConnectionRetryInterval() {
        return connectionRetryInterval;
    }

    public void setConnectionRetryInterval(int connectionRetryInterval) {
        this.connectionRetryInterval = connectionRetryInterval;
    }

    public int getCommandTimeout() {
        return commandTimeout;
    }

    public void setCommandTimeout(int commandTimeout) {
        this.commandTimeout = commandTimeout;
    }

}
