package apu.net;

import org.joda.time.DateTime;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/12/11
 * Time: 11:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class SocketInfo {


    protected String name=null;
    protected String displayName;
    protected Integer id;
    protected String ipAddressString;
    protected int port;
    protected InetSocketAddress ipAddress;
    protected boolean connectedOnce;
    protected ByteBuffer readBuffer = ByteBuffer.allocate(8192*2); // use a little bit extra buffer...
    //protected boolean eventEnabled;
    protected Object[] inputFilterPartials = null;
    protected Object[] outputFilterPartials = null;
    protected List<ClientSocketListener> eventHandlers = new ArrayList<ClientSocketListener>(10);
    protected BlockingQueue<ByteBuffer> outgoingQueue = new LinkedBlockingQueue<ByteBuffer>();

    protected volatile SocketChannel socket;
    protected volatile DateTime lastDisconnected=null;
    protected volatile DateTime lastTryConnect=null;
    //protected volatile SocketStatus status;
    //protected volatile SocketState state;

    public SocketInfo(String address, int port, ClientSocketListener eventHandler, int inputFilterSize, int outputFilterSize)
    {
        id=getNextId();
        ipAddressString = address;
        if (ipAddressString==null) ipAddressString="127.0.0.1";
        this.port=port;
        translateAddress();
        initDisplayName();
        socket=null;
        connectedOnce=false;

        inputFilterPartials = new Object[inputFilterSize];
        outputFilterPartials = new Object[outputFilterSize];
        if (eventHandler!=null) eventHandlers.add(eventHandler);


        //state=SocketState.IDLE;
        //status=SocketStatus.DISCONNECTED;
        //eventEnabled=true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return displayName;
    }

    private void initDisplayName()
    {
        if (name!=null)
            displayName = "|"+name+"|>>"+ipAddressString+":"+Integer.toString(port);
        else displayName = ipAddressString+":"+Integer.toString(port);
    }

    protected void translateAddress()
    {
        try
        {
            ipAddress = new InetSocketAddress(InetAddress.getByName(ipAddressString),port);
        }
        catch (Exception ex)
        {
        }
    }




    public void setName(String name) {
        this.name = name;
        initDisplayName();
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public SocketChannel getSocket() {
        return socket;
    }

    public String getIpAddressString() {
        return ipAddressString;
    }

    public int getPort() {
        return port;
    }

    public InetSocketAddress getIpAddress() {
        if (ipAddress==null)
            translateAddress();
        return ipAddress;
    }


    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    protected void setDisconnected(){
        try {
            SocketChannel socketToClose = socket;
            this.socket=null;
            lastDisconnected = new DateTime();
            readBuffer.clear();
            if (socketToClose!=null) socketToClose.close();
        } catch (IOException e) {
        }
    }
    protected void setConnected(){
        lastTryConnect=null;
        connectedOnce=true;
        readBuffer.clear();
    }
    protected void setTriedToConnect(){
        lastTryConnect = new DateTime();
    }
    public int getElapsedSinceLastDisconnect()
    {
        if (lastDisconnected==null) return 0;
        else
        {
            DateTime dt = new DateTime();
            return (int)(dt.getMillis()-lastDisconnected.getMillis());
        }
    }

    public boolean isConnectedOnce() {
        return connectedOnce;
    }
    public boolean isTryingToConnect()
    {
        return lastTryConnect!=null;
    }
    public void setTryConnect()
    {
        lastTryConnect=new DateTime();
        lastDisconnected=null;
    }
    public int getElapsedSinceTryConnect()
    {
        if (lastTryConnect==null) return 0;
        DateTime dt = new DateTime();
        return (int)(dt.getMillis()-lastTryConnect.getMillis());
    }
    public void resetTryConnect()
    {
        lastTryConnect=null;
    }

    public static int nextId=0;
    public static int getNextId()
    {
        return nextId++;
    }



}
