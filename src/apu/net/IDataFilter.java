package apu.net;

import apu.util.Loggable;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 8/14/11
 * Time: 12:12 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IDataFilter {
    public Object process(Object partialDataContainer, List inputData) throws Exception;
    public List getMaturedObjects();
}
