package apu.net;

class ChangeRequest {
	public static final int REGISTER = 1;
	public static final int CHANGEOPS = 2;
	public int socketHandle;
	public int ops;
	public ChangeRequest(int handle, int ops) {
        this.socketHandle = handle;
		this.ops = ops;
	}
}
