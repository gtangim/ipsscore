package apu.net;

import apu.util.StringUtil;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: russela
 * Date: 7/26/11
 * Time: 6:26 PM
 * To change this template use File | Settings | File Templates.
 */

public class Web {
    protected String errorMessage;
    protected boolean successful;
    protected String responseData;
    protected String responseDataLower;
    protected int rowCount;
    protected int colCount;
    protected StringBuilder sb = new StringBuilder();
    private int ind = 0;

    public Web(String url, String username, String password, int connectionTimeout, int responseTimeout)
    {
        try
        {
            HttpGet httpGet = new HttpGet(url);
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, connectionTimeout);
            HttpConnectionParams.setSoTimeout(httpParameters, responseTimeout);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY,
                    new UsernamePasswordCredentials(username,password));
            HttpResponse response = httpClient.execute(httpGet);
            successful=true;
            errorMessage="OK";
            if (response.getStatusLine().getStatusCode()!=200)
                throw new Exception("Server response code is "
                        + Integer.toString(response.getStatusLine().getStatusCode()));
            byte[] data = EntityUtils.toByteArray(response.getEntity());
            responseData = StringUtil.byteToString(data);
            responseDataLower=responseData.toLowerCase();
            ind=0;
            rowCount=0;
            colCount=0;

        }
        catch(Exception ex)
        {
            successful=false;
            errorMessage="Error: " + ex.getMessage();

        }

    }


    public Web(String url, int connectionTimeout, int responseTimeout)
    {
        try
        {
            HttpGet httpGet = new HttpGet(url);
            HttpParams httpParameters = new BasicHttpParams();

            HttpConnectionParams.setConnectionTimeout(httpParameters, connectionTimeout);
            HttpConnectionParams.setSoTimeout(httpParameters, responseTimeout);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(httpGet);
            successful=true;
            errorMessage="OK";
            if (response.getStatusLine().getStatusCode()!=200)
                throw new Exception("Server response code is "
                        + Integer.toString(response.getStatusLine().getStatusCode()));
            byte[] data = EntityUtils.toByteArray(response.getEntity());
            responseData = StringUtil.byteToString(data);
            responseDataLower=responseData.toLowerCase();
            ind=0;
            rowCount=0;
            colCount=0;
        }
        catch(Exception ex)
        {
            successful=false;
            errorMessage="Error: " + ex.getMessage();

        }
    }

    public String getInnerText(String html)
    {
        try
        {
            sb.setLength(0);
            boolean isTag = false;
            boolean isAmp = false;
            int n = html.length();
            for (int i=0;i<n;i++)
            {
                char c = html.charAt(i);
                if (c=='<' && isTag==false) isTag=true;
                else if (c=='>' && isTag==true) isTag=false;
                else if (c=='&' && isAmp==false) isAmp=true;
                else if (c==';' && isAmp==true) isAmp=false;
                else if (isAmp==false && isTag==false) sb.append(c);
            }
            return sb.toString().trim();
        }
        catch (Exception ex){return html;}
    }

    public boolean findTable(int tableInd)
    {
        try
        {
            rowCount=0;
            colCount=0;
            int ind0 = ind;
            for (int i=0;i<tableInd;i++)
            {
                int ind1 = responseDataLower.indexOf("<table",ind0);
                if (ind1==-1) return false;
                ind0 = ind1+6;
            }
            ind=ind0-6;
            return true;
        }
        catch(Exception ex){return false;}
    }

    public boolean skipNextTable()
    {
        try
        {
            rowCount=0;
            colCount=0;
            int ind0 = ind;
            int ind1 = responseDataLower.indexOf("<table",ind0);
            if (ind1==-1) return false;
            ind1+=7;
            int ind2 = responseDataLower.indexOf("</table>",ind1);
            if (ind2==-1) return false;
            ind=ind2+8;
            return true;
        }
        catch(Exception ex)
        {return false;}
    }

    public boolean getNextTable(String[][] tableContainer, boolean innerTextOnly)
    {
        try
        {
            rowCount=0;
            colCount=0;
            int maxColNum = -1;
            int n = tableContainer.length;
            int ind0 = ind;
            int ind1 = responseDataLower.indexOf("<table",ind0);
            if (ind1==-1) return false;
            ind1+=7;
            int ind2 = responseDataLower.indexOf("</table>",ind1);
            if (ind2==-1) return false;
            int rowNum = -1;
            int curRowStart = responseDataLower.indexOf("<tr",ind0);
            while (curRowStart!=-1 && curRowStart<ind2)
            {
                curRowStart+=3;
                rowNum++;
                int curRowEnd = responseDataLower.indexOf("</tr>",curRowStart);
                if (curRowEnd==-1) return false; // Format Error...

                int colNum=-1;
                int curColStart = responseDataLower.indexOf("<t",curRowStart);
                while(curColStart!=-1 && curColStart<curRowEnd)
                {
                    curColStart+=3;
                    while (responseDataLower.charAt(curColStart)!='>') curColStart++;
                    colNum++;
                    int curColEnd=responseDataLower.indexOf("</t",curColStart);
                    if (curColEnd==-1 || curColEnd>=curRowEnd) return false;
                    if (rowNum<n && colNum<tableContainer[rowNum].length)
                    {
                        String val = responseData.substring(curColStart+1,curColEnd);
                        if (innerTextOnly) val = getInnerText(val);
                        tableContainer[rowNum][colNum] = val;
                    }
                    curColStart = responseDataLower.indexOf("<t",curColEnd);
                }

                if (colNum>maxColNum) maxColNum=colNum;
                curRowStart = responseDataLower.indexOf("<tr",curRowEnd);
            }

            rowCount=rowNum+1;
            colCount=maxColNum+1;
            ind = ind2+8;
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }
    }


    public void resetParser()
    {
        ind=0;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getResponseData() {
        return responseData;
    }

    public int getRowCount() {
        return rowCount;
    }

    public int getColCount() {
        return colCount;
    }
}
